# Project Name :

Rabindra University Admission System

# developer Instruction

- [ ] clone project

```
git clone https://gitlab.com/iqbal-bigm/ru_admission.git
```

- [ ] pull update from dev branch

```
git fetch && git pull origin dev
```

- [ ] [Create merge requests](https://gitlab.com/iqbal-bigm/ru_admission/-/merge_requests/new)
