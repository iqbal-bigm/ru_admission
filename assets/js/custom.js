     var _validFileExtensions = [".jpg", ".jpeg89"]; //  ".bmp",  ".gif", , ".png", ".jpeg" , ".gif", , ".png"
            function ValidatePhotoInput(oInput, maxFileSize) {
                $("#photo_holder").html("");
                if (oInput.type === "file") {
                    var sFileName = oInput.value;
                    if (sFileName.length > 0) {
                        var blnValid = false;
                        for (var j = 0; j < _validFileExtensions.length; j++) {
                            var sCurExtension = _validFileExtensions[j];
                            if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() === sCurExtension.toLowerCase()) {
                                blnValid = true;
								readURL(oInput);
                                break;
                            }
                        }

                        if (!blnValid) {
                            alert("Sorry, " + sFileName + " is invalid, allowed extensions are: " + _validFileExtensions.join(", "));
                            oInput.value = "";
                            return false;
                        }

                        // var maxFileSize = 30;
                        var sFileSize = oInput.files[0].size / 1024;
                        if (sFileSize > maxFileSize) {
                            alert("The File Size Exceed The Maximum File Size " + maxFileSize + " KB");
                            oInput.value = "";
                            return false;
                        }
                    }
                }
                return true;
            }


     function readURL(input) {

        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#image_upload_preview').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $(document).ready(function (e) {
                $("#photoUpload").on('submit', (function (e) {
                    e.preventDefault();
                    $.ajax({
                        url: siteHost()+"home/ajax_uploadphoto",
                        type: "POST",
                        data: new FormData(this),
                        mimeType: "multipart/form-data",
                        contentType: false,
                        cache: false,
                        dataType: "json",
                        processData: false,
                        success: function (data)
                        {
                            console.log(data.status)
                            if( data.status === "ok" ){
                                    $("#photo_upload_error").html("");
                                    $("#photo_holder").html(data.image_url);
                                    $("#photo_upload_option").hide();
                                    //$("#upload_button_again").show();
                                    $("#is_upload_uploaded").val("yes");
                            }else{
                                 $("#photo_upload_error").html(data.msg);
                                 $("#photo_upload_error").addClass('my_error');
                            }

                        },
                        error: function ()
                        {
                        }
                    });
                }));

            });
function showUploadAgain(){
    $("#upload_button_again").hide();
    $("#photo_upload_option").show();
    return false;
}


$(document).ready(function () {
        $( "input:radio[name=application_unit]" ).click(function () {
					if($(this).attr("value") === "A"){
						$("#arch_showhide").show();
					}else{
						$("#arch_showhide").hide();
					}
                   validate_gpa_and_unit($(this).attr("value"));
        });
});

function validate_gpa_and_unit(unit) {
            var s_group = $('#s_group').val();
            var s_gpa = $('#s_gpa').val();
            var h_group = $('#h_group').val();
            var h_gpa = $('#h_gpa').val();
            var t_gpa = $('#t_gpa').val();
            var REGISID = $('#REGISID').val();

            $.ajax({
                url: siteHost() + "home/ajax_apply_validation",
                beforeSend: function(data) {
                    $("#ajax_loader").show();
                },
                global: false,
                type: "POST",
                dataType: "json",
                data: "unit="+unit+"&s_group="+s_group+"&s_gpa="+s_gpa+"&h_group="+h_group+"&h_gpa="+h_gpa+"&t_gpa="+t_gpa+"&reg_id="+REGISID,
                success: function(response)
                {
                    if(response.can_apply === "yes"){
                        $("#submit_btn_holder").show();
                        $("#personal_info_holder").show();
                        $("#msg_holder_can_apply").html(response.msg).show();
                        $("#ajax_loader").hide();
                        $("#msg_holder_cannot_apply").hide();
                    }else{
                        $("#ajax_loader").hide();
                        $("#msg_holder_cannot_apply").html(response.msg).show();
                         $("#msg_holder_can_apply").html("").hide();
                        $("#submit_btn_holder").hide();
                        $("#personal_info_holder").hide();
                    }

                }
            });
            return false;
}

/*
function  validateFromData(){
    var mobile_no = $("#mobile_no").val();
    var is_upload_uploaded = $("#is_upload_uploaded").val();
    if( mobile_no === ""){
        $("#mobile_no_msg").html("Mobile Number Requried");
        $("#mobile_no_msg").addClass('my_error');
        $("#mobile_no_msg_").html("Mobile Number Requried");
        $("#mobile_no_msg_").addClass('my_error');
        return false;
    }else if( is_upload_uploaded !== "yes"){
        $("#photo_upload_error").html("Please Upload Photo");
        $("#photo_upload_error").addClass('my_error');
        $("#photo_upload_error_").html("Please Upload Photo");
        $("#photo_upload_error_").addClass('my_error');
        $("#mobile_no_msg").html("");
        return false;
    }else{
        $("#mobile_no_msg").html("");
        $("#photo_upload_error").html("");
        $("#mobile_no_msg_").html("");
        $("#photo_upload_error_").html("");
         return true;
    }
}
*/

function  validateFromData(){
   if( validateMobile() && validatePhoto() && validateHomeDistrict() && validateQuestionPaper() ){

	   return true;

   }else{
	   return false;
   }
}

function  validateFromDataEdit(){
   if( validateMobile() && validateHomeDistrict() && validateQuestionPaper() ){

	   return true;

   }else{
	   return false;
   }
}


function validateMobile (){
	var mobile_no = $("#mobile_no").val();
	if( mobile_no === ""){
        $("#mobile_no_msg").html("Mobile Number Requried");
        $("#mobile_no_msg").addClass('my_error');
        $("#mobile_no_msg_").html("Mobile Number Requried");
        $("#mobile_no_msg_").addClass('my_error');
        return false;
    }else{
		$("#mobile_no_msg").html("");
		$("#mobile_no_msg_").html("");
		return true;
	}
}

function validatePhoto (){
	var is_upload_uploaded = $("#is_upload_uploaded").val();
	 if( is_upload_uploaded !== "yes"){
        $("#photo_upload_error").html("Please Upload Photo");
        $("#photo_upload_error").addClass('my_error');
        $("#photo_upload_error_").html("Please Upload Photo");
        $("#photo_upload_error_").addClass('my_error');

        return false;
    }else{

        $("#photo_upload_error").html("");
        $("#mobile_no_msg_").html("");
        $("#photo_upload_error_").html("");
         return true;
    }

}

function validateHomeDistrict (){
	var Home_District = $("#district").val();
	if( Home_District === ""){
        $("#district_msg").html("Home District Requried");
        $("#district_msg").addClass('my_error');
        $("#district_msg_").html("Home District Requried");
        $("#district_msg_").addClass('my_error');
        return false;
    }else{
		$("#district_msg").html("");
		$("#district_msg_").html("");
		return true;
	}
}

function validateQuestionPaper (){
	var exam_question_tpe = $("#exam_question_tpe").val();
	if( exam_question_tpe === ""){
        $("#exam_question_tpe_msg").html("Version of question paper Requried");
        $("#exam_question_tpe_msg").addClass('my_error');
        $("#exam_question_tpe_msg_").html("Version of question paper Requried");
        $("#exam_question_tpe_msg_").addClass('my_error');
        return false;
    }else{
		$("#exam_question_tpe_msg").html("");
		$("#exam_question_tpe_msg_").html("");
		return true;
	}
}


function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

/* $('#hsc_board').on('change', function() {

    if($("#hsc_board").val() === "BISD"){
       $("#hsc_passing_year").prop("selectedIndex", 0);
       $("#hsc_passing_year").find("option").eq(1).hide();
    }else{
        $("#hsc_passing_year").prop("selectedIndex", 0);
        $("#hsc_passing_year").find("option").eq(1).show();
    }

}); */
