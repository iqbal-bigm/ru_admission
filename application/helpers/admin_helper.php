<?php
function get_total_sub_choice()
{
    $ci = &get_instance();
    $sql = "SELECT distinct(unique_id) FROM  tbl_subject_choise";
    $query_all = $ci->db->query($sql);
    return $query_all->num_rows();
}


function get_total_subwise_choice($sub_code)
{
    $ci = &get_instance();
    $sql = "SELECT distinct(unique_id) FROM  tbl_subject_choise WHERE sub_code_ref='" . $sub_code . "'";
    //   $sql = "SELECT distinct(unique_id) FROM  tbl_merit_list WHERE unit='".$sub_code."' and sub_choice_done='YES'";
    $query_all = $ci->db->query($sql);
    return $query_all->num_rows();
}

function get_total_registation_done()
{
    $ci = &get_instance();
    $query = $ci->db->select('id');
    $query = $ci->db->get('tbl_login');
    return $query->num_rows();
}

function get_total_sub_choice_unit($unit)
{
    $ci = &get_instance();
    $sql = "SELECT count(`unique_id`) as total FROM  tbl_subject_choise WHERE unit_name='" . $unit . "'";
    $query_all = $ci->db->query($sql);
    $r =  $query_all->row_array();
    return $r['total'];
}

function get_total_sub_choice_unit_b($unit)
{
    $ci = &get_instance();
    $sql = "SELECT COUNT(DISTINCT(unique_id)) as total  FROM tbl_subject_choise WHERE unit_name='" . $unit . "'";
    $query_all = $ci->db->query($sql);
    $r =  $query_all->row_array();
    return $r['total'];
}

function get_total_sub_choice_unit_c_group($unit)
{
    $arr_sci = 0;
    $arr_hum = 0;
    $arr_bus = 0;
    $ci = &get_instance();
    $sql = "SELECT DISTINCT(unique_id) as unique_id  FROM tbl_subject_choise WHERE unit_name='" . $unit . "'";
    $query_all = $ci->db->query($sql);
    $r =  $query_all->result_array();
    foreach ($r as $key => $value) {
        $unique_id = $value['unique_id'];

        $sql_group = "SELECT c_section_hsc_group  FROM tbl_merit_list WHERE unique_id='" . $unique_id . "'";
        $query_all_group = $ci->db->query($sql_group);
        $r_group =  $query_all_group->row_array();
        $c_section_hsc_group = $r_group['c_section_hsc_group'];
        if ($c_section_hsc_group == "business") {
            $arr_bus += 1;
        } else if ($c_section_hsc_group == "humanities") {
            $arr_hum += 1;
        } else {
            $arr_sci += 1;
        }
    }

    $arr['sci'] = $arr_sci;
    $arr['hum'] = $arr_hum;
    $arr['bus'] = $arr_bus;

    return $arr;
}

function get_studen_personal_data($regid)
{
    $ci = &get_instance();
    $sql = "SELECT `SNAME`, `SFNAME`, `SMNAME` FROM   tbl_pabna_data_final WHERE REGISID='" . $regid . "' limit 1";
    $query_all = $ci->db->query($sql);
    return  $query_all->row_array();
}


function get_cancel_payment_details($tracking_id)
{
    $ci = &get_instance();
    $sql = "SELECT `paid_amount`, `payment_trans_id`, `payment_date`,paid_status FROM  tbl_cancel WHERE 	tracking_id='" . $tracking_id . "' limit 1";
    $query_all = $ci->db->query($sql);
    return  $query_all->row_array();
}

function get_total_paid()
{
    $ci = &get_instance();
    $sql = "SELECT id FROM  tbl_result WHERE paid_status='P'";
    $query_all = $ci->db->query($sql);
    return $query_all->num_rows();
}

function get_total_paid_2nd_merit()
{
    $ci = &get_instance();
    $sql = "SELECT id FROM  tbl_result WHERE paid_status='P' and is_secnd_meirt='YES'";
    $query_all = $ci->db->query($sql);
    return $query_all->num_rows();
}

function get_total_paid_3rd_merit()
{
    $ci = &get_instance();
    $sql = "SELECT id FROM  tbl_result WHERE paid_status='P' and is_third_meirt='YES'";
    $query_all = $ci->db->query($sql);
    return $query_all->num_rows();
}

function get_total_paid_4th_merit()
{
    $ci = &get_instance();
    $sql = "SELECT id FROM  tbl_result WHERE paid_status='P' and is_fourth_meirt='YES'";
    $query_all = $ci->db->query($sql);
    return $query_all->num_rows();
}
function get_total_paid_5th_merit()
{
    $ci = &get_instance();
    $sql = "SELECT id FROM  tbl_result WHERE paid_status='P' and is_fifth_meirt='YES'";
    $query_all = $ci->db->query($sql);
    return $query_all->num_rows();
}
function get_total_paid_6th_merit()
{
    $ci = &get_instance();
    $sql = "SELECT id FROM  tbl_result WHERE paid_status='P' and is_sixth_merit='YES'";
    $query_all = $ci->db->query($sql);
    return $query_all->num_rows();
}

function paid_count_unit($unit)
{
    $ci = &get_instance();
    $sql = "SELECT id FROM  tbl_result WHERE paid_status='P' and  unit_ref='" . $unit . "'  and `cancel_paid_status`='U' and ( cancel_payment_trans_id ='' or  cancel_payment_trans_id is null)";
    $query_all = $ci->db->query($sql);
    return $query_all->num_rows();
}

function admission_cancel()
{
    $ci = &get_instance();
    $sql = "SELECT id FROM  tbl_result WHERE  cancel_register_approv_status='approved' and cancel_payment_trans_id !=''";
    $query_all = $ci->db->query($sql);
    return $query_all->num_rows();
}

function get_total_subwise_paid($code)
{
    $ci = &get_instance();
    $sql = "SELECT id FROM  tbl_result WHERE paid_status='P' and sub_code_4='" . $code . "'  and `cancel_paid_status`='U' and ( cancel_payment_trans_id ='' or  cancel_payment_trans_id is null)";
    $query_all = $ci->db->query($sql);
    return $query_all->num_rows();
}

function get_total_subwise_paid_b_group($code, $group)
{
    $ci = &get_instance();
    $sql = "SELECT id FROM  tbl_result WHERE paid_status='P' and sub_code_4='" . $code . "' and  c_section_hsc_group='" . $group . "'   and `cancel_paid_status`='U' and ( cancel_payment_trans_id ='' or  cancel_payment_trans_id is null)";
    $query_all = $ci->db->query($sql);
    return $query_all->num_rows();
}

function get_total_subwise_paid_no_quota($code)
{
    $ci = &get_instance();
    $sql = "SELECT id FROM  tbl_result WHERE paid_status='P' and quota_ref='' and sub_code_2='" . $code . "'  and `cancel_paid_status`='U' and ( cancel_payment_trans_id ='' or  cancel_payment_trans_id is null)";
    $query_all = $ci->db->query($sql);
    return $query_all->num_rows();
}

function get_total_subwise_paid_quota($code)
{
    $ci = &get_instance();
    $sql = "SELECT id FROM  tbl_result WHERE paid_status='P' and sub_code_2='" . $code . "'  and `cancel_paid_status`='U' and `quota_ref` !='' and ( cancel_payment_trans_id ='' or  cancel_payment_trans_id is null)";
    $query_all = $ci->db->query($sql);
    return $query_all->num_rows();
}


function get_total_logged_in()
{
    $ci = &get_instance();
    $sql = "SELECT id FROM  tbl_login WHERE `last_login_dt` is not null";
    $query_all = $ci->db->query($sql);
    return $query_all->num_rows();
}

function get_total_subject_and_address_fill_done()
{
    $ci = &get_instance();
    $sql = "SELECT id FROM  tbl_merit_list WHERE `sub_choice_done`='YES'";
    $query_all = $ci->db->query($sql);
    return $query_all->num_rows();
}

function paid_count_unit_b_unit($group)
{
    $ci = &get_instance();
    $sql = "SELECT id FROM  tbl_result WHERE paid_status='P' and  unit_ref='B'  and c_section_hsc_group='" . $group . "' and `cancel_paid_status`='U' and ( cancel_payment_trans_id ='' or  cancel_payment_trans_id is null)";
    $query_all = $ci->db->query($sql);
    return $query_all->num_rows();
}

function get_total_watingchoice()
{
    $ci = &get_instance();


    $sql = "SELECT id  FROM tbl_merit_list WHERE  sub_choice_done= 'YES'  and is_prent_in_viva =  'YES' and   wants_to_admit_frm_waiting='YES'";
    $query = $ci->db->query($sql);


    return $query->num_rows();
}

function count_applicaton_by_unit_with_music($unit){
    $ci = &get_instance();
    $sql = "SELECT id FROM  tbl_application_pust WHERE UNIT='".$unit."' and WITH_ARCHITECTURE='YES'";
    $query_all = $ci->db->query($sql);
    return $query_all->num_rows();   
}

function count_paid_applicaton_by_unit_music($unit){
    $ci = &get_instance();
    $sql = "SELECT id FROM  tbl_application_pust WHERE UNIT='".$unit."' and PMTSTATUS='P'  and WITH_ARCHITECTURE='YES'";
    $query_all = $ci->db->query($sql);
    return $query_all->num_rows();    
}