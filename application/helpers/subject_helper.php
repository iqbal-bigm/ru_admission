<?php
/**
 * Created by PhpStorm.
 * User: BigM Resources Ltd
 * Date: 4/25/2017
 * Time: 4:14 PM
 */

/*
 * Get SSC Subject CODE, NAME AND GRADE
 */
if (!function_exists('get_ssc_subject')) {
    function get_ssc_subject($subject_code = null) {

        $subject = array(
            '101' => 'Bangla (compulsory) 1st paper',
            '103' => 'Easy Bangla 1st Paper',
            '105' => 'Bangla language and Culture 1st paper',
            '102' => 'Bangla (compulsory) 2nd paper',
            '104' => 'Easy Bangla 2nd Paper',
            '106' => 'Bangla language and Culture 2nd paper',
            '107' => 'English(compulsory) 1st',
            '108' => 'English(compulsory) 2nd paper',
            '111' => 'Islam and Religious Study',
            '112' => 'Hinduism and Religious Study',
            '113' => 'Buddhism and Religious Study',
            '114' => 'Christians and Religious Study',
            '109' => 'Mathematics(compulsory)',
            '136' => 'Physics',
            '153' => 'Bangladesh History and World Civilization',
            '139' => 'History',
            '152' => 'Finance & Banking',
            '142' => 'Business Studies',
            '137' => 'Chemistry',
            '140' => 'Civics & Citizenship/Civics',
            '143' => 'Business Approach',
            '110' => 'Geography and Environment/Geography',
            '144' => 'Business Geography',
            '127' => 'General Science/Science',
            '126' => 'Higher Mathematics',
            '150' => 'Bangladesh and Global Studies',
            '145' => 'Social Science',
            '138' => 'Biology',
            '141' => 'Economics',
            '119' => 'Bangle Language and Literature',
            '120' => 'English Language and Literature',
            '151' => 'Home Science',
            '129' => 'Home Economics',
            '134' => 'Agriculture',
            '149' => 'Music Studies',
            '146' => 'Accounting',
            '121' => 'Arabic',
            '123' => 'Sanskrit',
            '124' => 'Pali',
            '130' => 'Vocational Studies',
            '131' => 'Computer Studies',
            '135' => 'Basic Trade',
            '133' => 'Physical Studies and Sports');

        if($subject_code){
            $subject_code = explode(':', $subject_code);

            $result['code'] = $subject_code[0];
            $result['name'] = $subject[$subject_code[0]];
            $result['grade'] = $subject_code[1];

            return $result;
        }
    }
}







/*
 * Get SSC Subject CODE, NAME AND GRADE
 */
if (!function_exists('get_hsc_subject')) {
    function get_hsc_subject($subject_code = null) {


        $subject = array(
            '101' => 'Bangla (compulsory) 1st paper',
            '103' => 'Easy Bangla 1st Paper',
            '105' => 'Bangla language and Culture 1st paper',
            '102' => 'Bangla (compulsory) 2nd paper',
            '104' => 'Easy Bangla 2nd Paper',
            '106' => 'Bangla language and Culture 2nd paper',
            '107' => 'English(compulsory) 1st',
            '108' => 'English(compulsory) 2nd paper',
            '109' => 'Economics',
            '275' => 'Information and communication Technology(ICT)(compulsory)',
            '237' => 'Computer Studies 1st paper',
            '238' => 'Computer Studies 2nd paper',
            '292' => 'Finance, Banking & Insurance 1st paper',
            '293' => 'Finance, Banking & Insurance 2nd paper',
            '111' => 'Civics 1st paper',
            '112' => 'Civics 2nd paper',
            '178' => 'Biology 1st paper',
            '179' => 'Biology 2nd paper',
            '174' => 'Physics 1st paper',
            '175' => 'Physics 2nd paper',
            '129' => 'Statistics 1st paper',
            '130' => 'Statistics 2nd paper',
            '253' => 'Accounting 1st paper',
            '254' => 'Accounting 2nd paper',
            '239' => 'Agriculture 1st paper',
            '240' => 'Agriculture 2nd paper',
            '265' => 'Higher Mathematics 1st paper',
            '266' => 'Higher Mathematics 2nd paper',
            '127' => 'Mathematics 1st paper',
            '128' => 'Mathematics 2nd paper',
            '176' => 'Chemistry 1st paper',
            '177' => 'Chemistry 2nd paper',
            '304' => 'History 1st paper',
            '305' => 'History 2nd paper',
            '133' => 'Arabic 1st paper',
            '134' => 'Arabic 2nd paper',
            '137' => 'Sanskrit 1st paper',
            '138' => 'Sanskrit 2nd paper',
            '139' => 'Pali 1st paper',
            '140' => 'Pali 2nd paper',
            '125' => 'Geography 1st paper',
            '126' => 'Geography 2nd paper',
            '249' => 'Islam and Religious Study 1st paper',
            '250' => 'Islam and Religious Study 2nd paper',
            '117' => 'Social Science 1st paper',
            '118' => 'Social Science 2nd paper',
            '131' => 'Home Economics 1st paper',
            '132' => 'Home Economics 2nd paper',
            '273' => 'Home Science 1st paper',
            '274' => 'Home Science 2nd paper',
            '147' => 'Bangle Language and Literature 1st paper',
            '148' => 'Bangle Language and Literature 2nd paper',
            '149' => 'English Language and Literature 1st paper',
            '150' => 'English Language and Literature 2nd paper',
            '123' => 'Psychology 1st',
            '124' => 'Psychology 2nd',
            '277' => 'business Organization & Management' );

//            '' => 'Bangla (compulsory) 1st paper(DIBS)',
//            '' => 'Bangla (compulsory) 2nd paper(DIBS)',
//            '' => 'English(compulsory) 1st(DIBS)',
//            '' => 'English(compulsory) 2nd paper(DIBS)',

        if($subject_code){
            $subject_code = explode(':', $subject_code);

            $result['code'] = $subject_code[0];
            $result['name'] = $subject[$subject_code[0]];
            $result['grade'] = $subject_code[1];

            return $result;
        }
    }
}

