<?php

/**
 * Created by PhpStorm.
 * User: BigM Resources Ltd
 * Date: 4/23/2017
 * Time: 5:43 PM
 */
if (!function_exists('get_year_array')) {
    function get_year_array()
    {
        $data_arry = '';
        $current_year = date('Y');
        $data_arry[''] = 'Year';
        for ($i = 1970; $i <= $current_year; $i++) {
            $data_arry[$i] = $i;
        }
        return $data_arry;
    }
}

if (!function_exists('get_day_array')) {
    function get_day_array()
    {
        $duration_array = array(
            '' => 'Day',
            '01' => '01',
            '02' => '02',
            '03' => '03',
            '04' => '04',
            '05' => '05',
            '06' => '06',
            '07' => '07',
            '08' => '08',
            '09' => '09',
            '10' => '10',
            '11' => '11',
            '12' => '12',
            '13' => '13',
            '14' => '14',
            '15' => '15',
            '16' => '16',
            '17' => '17',
            '18' => '18',
            '19' => '19',
            '20' => '20',
            '21' => '21',
            '22' => '22',
            '23' => '23',
            '24' => '24',
            '25' => '25',
            '26' => '26',
            '27' => '27',
            '28' => '28',
            '29' => '29',
            '30' => '30',
            '31' => '31',
        );
        return $duration_array;
    }
}

if (!function_exists('get_month_array')) {
    function get_month_array()
    {
        $month_array = array(
            '' => 'Month',
            '01' => 'January',
            '02' => 'February',
            '03' => 'March',
            '04' => 'April',
            '05' => 'May',
            '06' => 'June',
            '07' => 'July',
            '08' => 'August',
            '09' => 'September',
            '10' => 'October',
            '11' => 'November',
            '12' => 'December',
        );
        return $month_array;
    }
}

/*
 * gender array
 */
if (!function_exists('gender_array')) {
    function gender_array()
    {
        $data = array(
            '' => 'Select Gender',
            'MALE' => 'Male',
            'FEMALE' => 'Female',
        );

        return $data;
    }
}

/*
 * marital array
 */
if (!function_exists('marital_status_array')) {
    function marital_status_array()
    {
        $data = array(
            '' => 'Select',
            'Single' => 'Single',
            'Married' => 'Married',
        );

        return $data;
    }
}

/*
 * SSC and HSC Board
 */
if (!function_exists('ssc_hsc_board')) {
    function ssc_hsc_board()
    {
        $data = array(
            '' => 'Select Board',
            'dhaka' => 'DHAKA',
            'comilla' => 'COMILLA',
            'rajshahi' => 'RAJSHAHI',
            'jessore' => 'JESSORE',
            'chittagong' => 'CHITTAGONG',
            'barisal' => 'BARISAL',
            'sylhet' => 'SYLHET',
            'dinajpur' => 'DINAJPUR',
            'madrasah' => 'MADRASAH',
            'tec' => 'TECHNICAL'
        );

        return $data;
    }
}

if (!function_exists('hsc_board')) {
    function hsc_board()
    {
        $data = array(
            '' => 'Select Board',
            'dhaka' => 'DHAKA',
            'comilla' => 'COMILLA',
            'rajshahi' => 'RAJSHAHI',
            'jessore' => 'JESSORE',
            'chittagong' => 'CHITTAGONG',
            'barisal' => 'BARISAL',
            'sylhet' => 'SYLHET',
            'dinajpur' => 'DINAJPUR',
            'madrasah' => 'MADRASAH',
            'tec' => 'TECHNICAL/BTEB',
            'dibs' => 'DIBS'
        );

        return $data;
    }
}

/*
 * SSC Board
 */
if (!function_exists('table_name')) {
    function table_name($board = null, $passing_year = null, $type = '')
    {
        $data = array(
            'DHAKA' => 'dhk',
            'COMILLA' => 'com',
            'RAJSHAHI' => 'raj',
            'JESSORE' => 'jes',
            'CHITTAGONG' => 'ctg',
            'BARISAL' => 'bar',
            'SYLHET' => 'syl',
            'DINAJPUR' => 'din',
            'MADRASAH' => 'mrd',
            'TECHNICAL' => 'tnc',
            'BISD' => 'dib',
            'BMTEC' => 'bmt' //technical BM
        );

        if ($type === "s") {
            $tableName = $data[$board] . "s" . substr($passing_year, 2, 2);
        } else {
            $tableName = $data[$board] . "h" . substr($passing_year, 2, 2);
        }

        return $tableName;
    }
}

/*
 * SSC Passing Year
 */
if (!function_exists('ssc_passing_year')) {
    function ssc_passing_year()
    {
        $data = array(
            '' => 'Select Year',
            '2016' => '2016',
            '2017' => '2017'
        );

        return $data;
    }
}

/*
 * HSC Passing Year
 */
if (!function_exists('hsc_passing_year')) {
    function hsc_passing_year()
    {
        $data = array(
            '' => 'Select Year',
            '2018' => '2018',
            '2019' => '2019'
        );

        return $data;
    }
}


function get_grading_gpa()
{
    $arr =  array(
        "A+" => "5.0",
        "A" => "4.0",
        "A-" => "3.5",
        "B" => "3.0",
        "C" => "2.0",
        "D" => "1.0",
        "F" => "0.0"
    );
    return $arr;
}


function amount_topay_array($UNIT)
{
    $arr = array(
        "A" => 1245,
        "B" => 1045
    );
    return $arr[$UNIT];
}

function get_quota_array()
{
    $arr =  array(
        "NA" => "None",
        "FF" => "Freedom Fighter",
        "PH" => "Physically Handicapped",
        "WQ" => "Ward",
        "TB" => "Tribal"
    );
    return $arr;
}

function get_district_array()
{
    $dis_array = array(
        '' => 'Select',
        'BAGERHAT' => 'BAGERHAT',
        'BANDARBAN' => 'BANDARBAN',
        'BARGUNA' => 'BARGUNA',
        'BARISAL' => 'BARISAL',
        'BHOLA' => 'BHOLA',
        'BOGRA' => 'BOGRA',
        'BRAHMANBARIA' => 'BRAHMANBARIA',
        'CHANDPUR' => 'CHANDPUR',
        'CHAPAINABABGANJ' => 'CHAPAINABABGANJ',
        'CHITTAGONG' => 'CHITTAGONG',
        'CHUADANGA' => 'CHUADANGA',
        'COMILLA' => 'COMILLA',
        'COXS BAZAR' => 'COXS BAZAR',
        'DHAKA' => 'DHAKA',
        'DINAJPUR' => 'DINAJPUR',
        'FARIDPUR' => 'FARIDPUR',
        'FENI' => 'FENI',
        'GAIBANDHA' => 'GAIBANDHA',
        'GAZIPUR' => 'GAZIPUR',
        'GOPALGANJ' => 'GOPALGANJ',
        'HABIGANJ' => 'HABIGANJ',
        'JAMALPUR' => 'JAMALPUR',
        'JESSORE' => 'JESSORE',
        'JHALOKATI' => 'JHALOKATI',
        'JHENAIDAH' => 'JHENAIDAH',
        'JOYPURHAT' => 'JOYPURHAT',
        'KHAGRACHHARI' => 'KHAGRACHHARI',
        'KHULNA' => 'KHULNA',
        'KISHOREGONJ' => 'KISHOREGONJ',
        'KURIGRAM' => 'KURIGRAM',
        'KUSHTIA' => 'KUSHTIA',
        'LAKSHMIPUR' => 'LAKSHMIPUR',
        'LALMONIRHAT' => 'LALMONIRHAT',
        'MADARIPUR' => 'MADARIPUR',
        'MAGURA' => 'MAGURA',
        'MANIKGANJ' => 'MANIKGANJ',
        'MAULVIBAZAR' => 'MAULVIBAZAR',
        'MEHERPUR' => 'MEHERPUR',
        'MUNSHIGANJ' => 'MUNSHIGANJ',
        'MYMENSINGH' => 'MYMENSINGH',
        'NAOGAON' => 'NAOGAON',
        'NARAIL' => 'NARAIL',
        'NARAYANGANJ' => 'NARAYANGANJ',
        'NARSINGDI' => 'NARSINGDI',
        'NATORE' => 'NATORE',
        'NETRAKONA' => 'NETRAKONA',
        'NILPHAMARI' => 'NILPHAMARI',
        'NOAKHALI' => 'NOAKHALI',
        'PABNA' => 'PABNA',
        'PANCHAGARH' => 'PANCHAGARH',
        'PATUAKHALI' => 'PATUAKHALI',
        'PIROJPUR' => 'PIROJPUR',
        'RAJBARI' => 'RAJBARI',
        'RAJSHAHI' => 'RAJSHAHI',
        'RANGAMATI' => 'RANGAMATI',
        'RANGPUR' => 'RANGPUR',
        'SATKHIRA' => 'SATKHIRA',
        'SHARIATPUR' => 'SHARIATPUR',
        'SHERPUR' => 'SHERPUR',
        'SIRAJGANJ' => 'SIRAJGANJ',
        'SUNAMGANJ' => 'SUNAMGANJ',
        'SYLHET' => 'SYLHET',
        'TANGAIL' => 'TANGAIL',
        'THAKURGAON' => 'THAKURGAON',
    );
    return $dis_array;
}

function count_applicaton_by_unit($unit)
{
    //unique by applicant_id
    $CI = &get_instance();
    $CI->load->model('admin_report_model');
    return $CI->admin_report_model->count_applicaton_by_unit($unit);
}

function get_paid_count()
{
    //unique by applicant_id
    $CI = &get_instance();
    $CI->load->model('admin_report_model');
    return $CI->admin_report_model->get_paid_count();
}


function count_total_applicant()
{
    //unique by applicant_id
    $CI = &get_instance();
    $CI->load->model('admin_report_model');
    return $CI->admin_report_model->count_total_applicant();
}
function get_photo_path($regid)
{
    $ci = &get_instance();

    if ($regid) {
        $query = $ci->db->select('IMGUPLOAD_PATH');
        $query = $ci->db->where('REGISID', $regid);
        $query = $ci->db->get('tbl_registration_pust');
        if ($query->num_rows() > 0) {
            $r = $query->row();
            return $r->IMGUPLOAD_PATH;
        }
    } else {
        return false;
    }
}

function generate_random_password($length = 6)
{
    return substr(str_shuffle("123456789ABCDEFGHIJKLMNPQRSTUVWXYZ"), 0, $length);
}

function get_all_subject_code_name()
{
    $ci = &get_instance();

    $arr = array();

    $query = $ci->db->get('tbl_subject');
    if ($query->num_rows() > 0) {
        $r = $query->result_array();
        foreach ($r as $key => $value) {
            $arr[$value['sub_code']] = $value['sub_name'];
        }
    }
    return $arr;
}


function get_all_subject_faculty_name()
{
    $ci = &get_instance();

    $arr = '';

    $query = $ci->db->get('tbl_subject');
    if ($query->num_rows() > 0) {
        $r = $query->result_array();
        foreach ($r as $key => $value) {
            $arr[$value['sub_code']] = $value['faculty_name'];
        }
    }
    return $arr;
}

function get_all_subject_dep_name()
{
    $ci = &get_instance();

    $arr = '';

    $query = $ci->db->get('tbl_subject');
    if ($query->num_rows() > 0) {
        $r = $query->result_array();
        foreach ($r as $key => $value) {
            $arr[$value['sub_code']] = $value['dep_name'];
        }
    }
    return $arr;
}

function get_all_course_name()
{
    $ci = &get_instance();

    $arr = '';

    $query = $ci->db->get('tbl_subject');
    if ($query->num_rows() > 0) {
        $r = $query->result_array();
        foreach ($r as $key => $value) {
            $arr[$value['sub_code']] = $value['course'];
        }
    }
    return $arr;
}

function get_my_saved_subject_list($unique_id, $exam_roll, $unit_name)
{
    $CI = &get_instance();
    $CI->load->model('dashboard_model');
    return $CI->dashboard_model->get_saved_subject_list($unique_id, $exam_roll, $unit_name);
}

function is_own_application($unique_id, $exam_roll, $unit_name)
{
    $ci = &get_instance();

    $student_login_regisid = $ci->session->userdata('student_login_regisid');

    $query = $ci->db->where('unique_id', $unique_id);
    $query = $ci->db->where('exam_roll', $exam_roll);
    $query = $ci->db->where('unit', $unit_name);
    $query = $ci->db->where('system_reg_id', $student_login_regisid);

    $query = $ci->db->get('tbl_merit_list');

    if ($query->num_rows() > 0) {
        return 'yes';
    } else {
        return 'no';
    }
}

function choice_on_off_status()
{
    $arr =  array(
        'on_off' => 'OFF', //ON/OFF
        'msg' => 'Make your subject choice preference and fill up the following fields carefully. You can edit this form several times till 05.12.2021'
    );
    return $arr;
}

function admission_payment_messg()
{
    $arr =  array(
        'msg' => 'Please pay taka 8200/= using Tracking ID as Bill number to Biller ID 253 using DBBL Rocket.After completing payment you will get option to download admission form'
    );
    return $arr;
}

function get_religion()
{
    $arr =  array(
        '' => 'Select Option',
        'Islam' => 'Islam',
        'Christianity ' => 'Christianity',
        'Hinduism' => 'Hinduism',
        'Buddhism ' => 'Buddhism',
        'Others ' => 'Others '
    );

    return $arr;
}

function get_scout()
{
    $arr =  array(
        '' => 'Select Option',
        'None' => 'None',
        'BNCC' => 'BNCC',
        'Rover Scout' => 'Rover Scout',
    );

    return $arr;
}

function get_subject_gpa_for_b()
{
    $ci = &get_instance();
    $student_login_regisid = $ci->session->userdata('student_login_regisid');

    $query = $ci->db->select('mathematics_gpa_score,physics_gpa_score,chemistry_gpa_score,biology_gpa_score,statistics_gpa_score');
    $query = $ci->db->where('UNIT', 'A');
    $query = $ci->db->where('REGISID', $student_login_regisid);

    $query = $ci->db->get('tbl_pabna_data_final');

    if ($query->num_rows() > 0) {
        $r = $query->row();
        $arr =  array(
            'mathematics_gpa_score' => $r->mathematics_gpa_score,
            'physics_gpa_score' => $r->physics_gpa_score,
            'chemistry_gpa_score' => $r->chemistry_gpa_score,
            'biology_gpa_score' => $r->biology_gpa_score,
            'statistics_gpa_score' => $r->statistics_gpa_score,
        );

        return $arr;
    } else {
        return null;
    }
}

function is_in_waiting()
{
    $ci = &get_instance();
    $student_login_regisid = $ci->session->userdata('student_login_regisid');
    if ($student_login_regisid != '') {

        $ci->db->where('system_reg_id', $student_login_regisid);
        $ci->db->where('sub_choice_done', 'YES');
        $ci->db->where('is_prent_in_viva', 'YES');

        $query = $ci->db->get('tbl_merit_list');

        if ($query->num_rows() > 0) {
            return "in_waiting";
        } else {
            return null;
        }
    } else {
        return null;
    }
}



function get_paid_count_agent($agent)
{
    $ci = &get_instance();
    if ($agent == "rocket") {
        $sql = "SELECT ID  FROM tbl_application_pust WHERE `PAID_FRM_DBBL`='Y'";
    } else {
        $sql = "SELECT ID  FROM tbl_application_pust WHERE `PAID_FRM_SCASH`='Y'";
    }

    $query_all = $ci->db->query($sql);
    return $query_all->num_rows();
}


function count_paid_applicaton_by_unit($unit)
{
    //unique by applicant_id
    $ci = &get_instance();
    $sql = "SELECT ID  FROM tbl_application_pust WHERE `UNIT`='" . $unit . "' AND PMTSTATUS='P' GROUP BY `appid`";
    $query_all = $ci->db->query($sql);
    return $query_all->num_rows();
}


function can_give_waiting_choise()
{
    $ci = &get_instance();
    $student_login_regisid = $ci->session->userdata('student_login_regisid');
    if ($student_login_regisid != '') {

        $sql = "SELECT id  FROM tbl_merit_list WHERE `system_reg_id`='" . $student_login_regisid . "'  and sub_choice_done= 'YES'  and is_prent_in_viva =  'YES'  and `allocated_subject` is null";
        $query = $ci->db->query($sql);


        if ($query->num_rows() > 0) {
            return "YES";
        } else {
            return "NO";
        }
    } else {
        return "NO";
    }
}

function wants_to_admit_frm_waiting()
{
    $ci = &get_instance();
    $student_login_regisid = $ci->session->userdata('student_login_regisid');
    if ($student_login_regisid != '') {

        $sql = "SELECT wants_to_admit_frm_waiting  FROM tbl_merit_list WHERE `system_reg_id`='" . $student_login_regisid . "' and sub_choice_done= 'YES'  and is_prent_in_viva =  'YES' and `allocated_subject` is null";
        $query = $ci->db->query($sql);


        if ($query->num_rows() > 0) {
            $r = $query->row_array();
            return $r['wants_to_admit_frm_waiting'];
        }
    }
}
if (!function_exists('dd')) {
    function dd()
    {
        echo "<pre>";
        foreach (func_get_args() as $x) {
            echo "<br>";
            print_r($x);
        }
        die;
    }
}
if (!function_exists('getSubjectGPA')) {
    function getSubjectGPA($hsc_result, $hsc_board_name, $hsc_subject_code)
    {
        $gpa_scale = get_grading_gpa();
        $gpa = 0.00;
        $no_of_subject_code = 0;
        $total_gpa = 0.00;
        $board_name = substr(strtolower(trim($hsc_board_name)), 0, 3);
        $subject_code = $board_name == 'tec' ? $hsc_subject_code['technical'] : ($board_name == 'mad' ? $hsc_subject_code['madrasah'] : ($board_name == 'gce' ? $hsc_subject_code['gce'] : $hsc_subject_code['general']));
        if (!empty($subject_code)) {
            foreach ($hsc_result as $code => $result) {
                if (in_array($code, $subject_code) && isset($result->g)) {
                    // echo 'start>>' . $code;
                    // echo '<br>';
                    if (is_numeric(trim($result->g))) {
                        // echo 'is_numeric>>' . $result->g;
                        // echo '<br>';
                        $no_of_subject_code += 1;
                        $total_gpa += $result->g;
                    } elseif (isset($gpa_scale[trim($result->g)])) {
                        // echo 'not_numeric>>' . $result->g;
                        // echo '<br>';
                        $no_of_subject_code += 1;
                        $total_gpa += $gpa_scale[trim($result->g)];
                    }
                }
            }
        }
        // dd($no_of_subject_code, $total_gpa);
        if ($no_of_subject_code) {
            $gpa = ($total_gpa / $no_of_subject_code);
        }
        return $gpa;
    }
}
if (!function_exists('asset_url')) {
    function asset_url($path = '')
    {
        $end_point = 'http://147.182.129.154/';
        return $end_point . $path;
    }
}
