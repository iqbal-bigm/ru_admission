<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Form Validatoin error prefix and suffix element and class
|--------------------------------------------------------------------------
*/

$config['error_prefix'] = '<div class="validation-error"> <i class="fa fa-exclamation-triangle pr-5"></i>';
$config['error_suffix'] = '</div>';