<?php

/**
 * Created by PhpStorm.
 * User: BigM Resources Ltd
 * Date: 4/23/2017
 * Time: 3:18 PM
 */
class MY_Controller extends CI_Controller
{
    private $data = [];
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
    }
    protected function VivaPaymentStatus()
    {
        $regisid = $this->session->userdata('student_login_regisid');
        $query = $this->db->where('regisid', $regisid)->where('pmtstatus', 'P')->get('tbl_application_pust');
        if ($query->num_rows() >= 1) {
            return true;
        } else {
            return false;
        }
    }
    protected function WithArchitectureStatus()
    {
        $regisid = $this->session->userdata('student_login_regisid');
        $query = $this->db->where('regisid', $regisid)->where('with_architecture', 'YES')->get('tbl_application_pust');
        if ($query->num_rows() >= 1) {
            return true;
        } else {
            return false;
        }
    }
}
