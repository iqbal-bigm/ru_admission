<?php

/**
 * Created by PhpStorm.
 * User: BigM Resources Ltd
 * Date: 4/24/2017
 * Time: 1:35 PM
 */
class Registration_model extends CI_Model{

    protected $return;
    private $_RESG_TABLE;
    private $_APPLY_TABLE;
    private $_PAYMENT_TABLE;
    private $orginal_image_path;

    public function __construct(){
        parent:: __construct();

        $this->_RESG_TABLE = 'tbl_registration_pust';
        $this->_APPLY_TABLE = 'tbl_application_pust';
        $this->_PAYMENT_TABLE = 'tbl_payment_pust';
        $this->orginal_image_path = realpath(APPPATH . '../uploads');

        $this->return = array('success' => FALSE, 'msg' => NULL, 'content' => NULL, 'reg_status' => FALSE, 'registered_id' => 0);
    }

    function check_mobile_number($mobile_no) {
            $this->db->where('mobile_no', $mobile_no);

            $query = $this->db->get('tbl_login');

            if ($query->num_rows() > 0) {
                return "used";
            }else{
                return "not_used";
            }
    }

    /**
     * HSC Inforamation BY Board, Pass Year , ROLL && Reg no
     * check in final data table
     */

    public function check_valid_hsc_info($hsc_board = '', $hsc_passing_year='', $hsc_roll='', $hsc_registration=''){
        if ($hsc_board != '' && $hsc_passing_year != '' && $hsc_roll != '' && $hsc_registration !=''){

            $this->db->where('HBOARD_NAME', $hsc_board);
            $this->db->where('HPASS_YEAR', $hsc_passing_year);
            $this->db->where('HROLL_NO', $hsc_roll);
            $this->db->where('HREG_NO', $hsc_registration);

            $query = $this->db->get('tbl_pabna_data_final');

            if ($query->num_rows() > 0) {
                return $query->row_array();
            }else{
				return null;
			}
        }else{
				return null;
		}
    }

    /**
     * HSC Inforamation BY Board, Pass Year , ROLL && Reg no
     * check in login table
     */

    public function check_hsc_info_login($hsc_board = '', $hsc_passing_year='', $hsc_roll='', $hsc_registration=''){
        if ($hsc_board != '' && $hsc_passing_year != '' && $hsc_roll != '' && $hsc_registration !=''){

            $this->db->where('hsc_board', $hsc_board);
            $this->db->where('hsc_year', $hsc_passing_year);
            $this->db->where('hsc_reg', $hsc_registration);
            $this->db->where('hsc_roll', $hsc_roll);

            $query = $this->db->get('tbl_login');

            if ($query->num_rows() > 0) {
                return true;
            }else{
                return null;
            }
        }else{
                return null;
        }
    }

    function save_hsc_info($system_regid)
    {

        $password = generate_random_password(6);
        $mob_no = $this->input->post('mobile_no', TRUE);

        $GSM = '88'.$mob_no;
        $current_date_time =  date('Y-m-d H:i:s');
        $data = array(
            'system_regid' =>$system_regid,
            'hsc_board' => $this->input->post('hsc_board', TRUE),
            'hsc_year' => $this->input->post('hsc_passing_year', TRUE),
            'hsc_reg' => $this->input->post('hsc_registration', TRUE),
            'hsc_roll' => $this->input->post('hsc_roll', TRUE),
            'mobile_no' => $this->input->post('mobile_no', TRUE),
            'password' => $password,
            'creation_dt' => $current_date_time,
			'is_secnd_meirt' => 'YES'
        );

        $result = $this->db->insert('tbl_login', $data);

        if ($result) {
            //send SMS
         //   $sms_text = urlencode("Login Information for PUST admission website. Mobile Number(Login id): $mob_no  Password: $password");
        //    file_get_contents("http://api.kushtia.com/sendsms/plain?user=BMRL&password=62Slrfcq&sender=PUST&SMSText=".$sms_text."&GSM=".$GSM);

            return TRUE;
        } else {
            return FALSE;
        }

    }




    /**
     * @param $reg_id
     * @param $result
     * Create New Registration After Checking From @validation_input_data
     */
    public function create_new_registration($reg_id, $ssc_result, $hsc_result){

        $dt =  date('Y-m-d H:i:s');

         $ssc_board = $this->input->post('ssc_board', TRUE);

     /*    if($ssc_board == "CHITTAGONG" || $ssc_board == "MADRASAH"){
           $ssc_group  =   $ssc_result['STUD_GROUP'];
         }else{
             $ssc_group  =   $ssc_result['SSC_GROUP'];
         }
       */

        $ssc_group  =   $ssc_result['SSC_GROUP'];

        $this->db->set('REGISID', $reg_id);

        $this->db->set('SNAME', $ssc_result['NAME']);
        $this->db->set('SFNAME',$ssc_result['FNAME']);
        $this->db->set('SMNAME', $ssc_result['MNAME']);
        $this->db->set('SBOARD_NAME', $ssc_result['BOARD_NAME']);
        $this->db->set('SPASS_YEAR', $ssc_result['PASS_YEAR']);
        $this->db->set('SROLL_NO', $ssc_result['ROLL_NO']);
        $this->db->set('SREG_NO', $ssc_result['REGNO']);
        $this->db->set('SGROUP', $ssc_group);
        $this->db->set('SLTRGRD', $ssc_result['LTRGRD']);
        $this->db->set('SGPA', $ssc_result['GPA']);
        $this->db->set('SRESULT', $ssc_result['RESULT']);

        $this->db->set('HNAME', $ssc_result['NAME']);
        $this->db->set('HFNAME',$ssc_result['FNAME']);
        $this->db->set('HMNAME', $ssc_result['MNAME']);
        $this->db->set('HBOARD_NAME', $hsc_result['BOARD_NAME']);
        $this->db->set('HPASS_YEAR', $hsc_result['PASS_YEAR']);
        $this->db->set('HROLL_NO', $hsc_result['ROLL_NO']);
        $this->db->set('HREG_NO', $hsc_result['REGNO']);
        $this->db->set('HGROUP', $hsc_result['HSC_GROUP']);
        $this->db->set('HLTRGRD', $hsc_result['LTRGRD']);
        $this->db->set('HGPA', $hsc_result['GPA']);
        $this->db->set('HRESULT', $hsc_result['RESULT']);
        $this->db->set('CLIENTIP', $_SERVER['REMOTE_ADDR']);
        $this->db->set('ENTRY_DT', $dt);

        if ($this->db->insert($this->_RESG_TABLE)){
            $this->return['registered_id'] = $this->db->insert_id();
            $this->return['success'] = TRUE;
            $this->return['msg'] = 'Congrats! You are Successfully Registered.';
            $this->return['reg_status'] = TRUE;
        }
    }



    /**
     * @param $reg_id
     * Get  Registration details
     */
    public function get_registration_details($reg_id){

        $this->db->where('REGISID', $reg_id);
        $query = $this->db->get($this->_RESG_TABLE);

        if ($query->num_rows() > 0) {

            return $query->row_array();
        }else{
            return null;
        }
    }


}
