<?php

/**
 * Created by PhpStorm.
 * User: BigM Resources Ltd
 * Date: 4/24/2017
 * Time: 1:35 PM
 */
class Dashboard_model extends CI_Model
{

    protected $return;
    private $_RESG_TABLE;
    private $_APPLY_TABLE;
    private $_PAYMENT_TABLE;
    private $orginal_image_path;

    public function __construct()
    {
        parent::__construct();

        $this->_RESG_TABLE = 'tbl_registration_pust';
        $this->_APPLY_TABLE = 'tbl_application_pust';
        $this->_PAYMENT_TABLE = 'tbl_payment_pust';
        $this->orginal_image_path = realpath(APPPATH . '../uploads');

        $this->return = array('success' => FALSE, 'msg' => NULL, 'content' => NULL, 'reg_status' => FALSE, 'registered_id' => 0);
    }

    /**
     * HSC Inforamation BY Board, Pass Year , ROLL && Reg no
     * check in final data table
     */

    public function get_student_details($hsc_board = '', $hsc_passing_year = '', $hsc_roll = '', $hsc_registration = '')
    {
        if ($hsc_board != '' && $hsc_passing_year != '' && $hsc_roll != '' && $hsc_registration != '') {

            $this->db->where('HBOARD_NAME', $hsc_board);
            $this->db->where('HPASS_YEAR', $hsc_passing_year);
            $this->db->where('HROLL_NO', $hsc_roll);
            $this->db->where('HREG_NO', $hsc_registration);

            $query = $this->db->get('tbl_pabna_data_final');

            if ($query->num_rows() > 0) {
                return $query->row_array();
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    public function get_eligible_unit_list($student_login_regisid = '')
    {
        if ($student_login_regisid != '') {

            $this->db->where('system_reg_id', $student_login_regisid);

            $query = $this->db->get('tbl_merit_list');

            if ($query->num_rows() > 0) {
                return $query->result_array();
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    public function get_eligible_unit_details($unique_id, $exam_roll, $unit_name, $student_login_regisid = '')
    {
        if ($student_login_regisid != '') {

            $this->db->where('unique_id', $unique_id);
            $this->db->where('exam_roll', $exam_roll);
            $this->db->where('unit', $unit_name);
            $this->db->where('system_reg_id', $student_login_regisid);

            $query = $this->db->get('tbl_merit_list');

            if ($query->num_rows() > 0) {
                return $query->row_array();
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    public function get_unit_wise_subject_list($unit_name)
    {

        $this->db->where('unit', $unit_name);

        $query = $this->db->get('tbl_subject');
        $rowcount = $query->num_rows();
        if ($rowcount > 0) {
            return $query->result_array();
        } else {
            return null;
        }
    }

    public function get_std_dtl_from_login_tbl($hsc_board = '', $hsc_passing_year = '', $hsc_roll = '', $hsc_registration = '')
    {
        if ($hsc_board != '' && $hsc_passing_year != '' && $hsc_roll != '' && $hsc_registration != '') {

            $this->db->where('hsc_board', $hsc_board);
            $this->db->where('hsc_year', $hsc_passing_year);
            $this->db->where('hsc_reg', $hsc_registration);
            $this->db->where('hsc_roll', $hsc_roll);

            $query = $this->db->get('tbl_login');

            if ($query->num_rows() > 0) {
                return $query->row_array();
            } else {
                return null;
            }
        } else {
            return null;
        }
    }



    function my_subject_option_save_data()
    {


        $current_date_time =  date('Y-m-d H:i:s');

        $login_table_auto_id = $this->input->post('login_table_auto_id', TRUE);

        // data upadte into login table
        $per_data = array(
            'dob' => $this->input->post('dob', TRUE),
            //  'gender' => $this->input->post('gender', TRUE),
            'maritlal_status' => $this->input->post('maritlal_status', TRUE),
            'father_occupation' => $this->input->post('father_occupation', TRUE),
            'mother_occupation' => $this->input->post('mother_occupation', TRUE),
            'family_income' => $this->input->post('family_income', TRUE),
            'scout' => $this->input->post('scout', TRUE),
            'local_gurdian_name' => $this->input->post('local_gurdian_name', TRUE),
            'local_gurdian_realation' => $this->input->post('local_gurdian_realation', TRUE),
            'local_gurdian_phone' => $this->input->post('local_gurdian_phone', TRUE),
            'local_gurdian_address' => $this->input->post('local_gurdian_address', TRUE),
            'religion' => $this->input->post('religion', TRUE),
            // 'permanent_address' => $this->input->post('permanent_address', TRUE),
            'padd_village' => $this->input->post('padd_village', TRUE),
            'padd_house' => $this->input->post('padd_house', TRUE),
            'padd_road' => $this->input->post('padd_road', TRUE),
            'padd_post_office' => $this->input->post('padd_post_office', TRUE),
            'padd_police_station' => $this->input->post('padd_police_station', TRUE),
            'padd_district' => $this->input->post('padd_district', TRUE),
            'mailing_address' => '', //$this->input->post('mailing_address', TRUE),
            'district' => $this->input->post('district', TRUE),
            'prad_village' => $this->input->post('prad_village', TRUE),
            'prad_house' => $this->input->post('prad_house', TRUE),
            'prad_road' => $this->input->post('prad_road', TRUE),
            'prad_post_office' => $this->input->post('prad_post_office', TRUE),
            'prad_police_station' => $this->input->post('prad_police_station', TRUE),
            'prad_district' => $this->input->post('prad_district', TRUE)
        );

        $this->db->where('id', $login_table_auto_id);
        $result = $this->db->update('tbl_login', $per_data);

        $unique_id = $this->input->post('unique_id', TRUE);


        $arr_sub_choice_done = array(
            'sub_choice_done' => 'YES'
        );

        $this->db->where('unique_id', $unique_id);
        $this->db->update('tbl_merit_list', $arr_sub_choice_done);


        // end data upadte into login table

        if ($result) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function get_saved_subject_list($unique_id, $exam_roll, $unit_name)
    {
        $this->db->where('unique_id', $unique_id);
        $this->db->where('roll_number', $exam_roll);
        $this->db->where('unit_name', $unit_name);
        $this->db->where('with_architecture !=', 'YES');
        $this->db->order_by('choise_order', 'ASC');

        $query = $this->db->get('tbl_subject_choise');
        $rowcount = $query->num_rows();
        if ($rowcount > 0) {
            return $query->result_array();
        } else {
            return null;
        }
    }


    public function get_student_all__details($sys_reg_id)
    {


        $this->db->where('REGISID', $sys_reg_id);
        $this->db->limit(1);
        $query = $this->db->get('tbl_pabna_data_final');

        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else {
            return null;
        }
    }

    function get_result_subject_list($eligible_unit_list)
    {
        $arr = '';
        if (!empty($eligible_unit_list)) {
            foreach ($eligible_unit_list as $key => $value) {

                $my_unique_id = $value['unique_id'];

                $this->db->where('unique_id_ref', $my_unique_id);

                $query = $this->db->get('tbl_result');

                if ($query->num_rows() > 0) {
                    $arr[] =  $query->row_array();
                }
            }
        }

        return $arr;
    }

    function get_admission_status($sys_reg_id)
    {

        $this->db->where('system_reg_id_ref', $sys_reg_id);
        $this->db->where('paid_status', 'P');
        $query = $this->db->get('tbl_result');

        if ($query->num_rows() > 0) {
            $row = $query->row();
            $arr['admission_done'] =   'admission_done';
            $arr['migration_on_off'] =   $row->migration_on_off;
        } else {
            $arr['admission_done'] =   'did_not_admit';
            $arr['migration_on_off'] =   '';
        }

        return $arr;
    }

    function get_result_subject_details($unique_id)
    {
        $this->db->where('unique_id_ref', $unique_id);

        $query = $this->db->get('tbl_result');

        if ($query->num_rows() > 0) {
            return  $query->row_array();
        } else {
            return null;
        }
    }

    function get_result_cancel_request($system_reg_id_ref)
    {
        $this->db->where('system_reg_id_ref', $system_reg_id_ref);
        $this->db->where('cancel_status', 'cancel_apply');
        $query = $this->db->get('tbl_result');

        if ($query->num_rows() > 0) {
            return  $query->result_array();
        } else {
            return null;
        }
    }
    public function checkIsApply($regisid)
    {
        // return false;
        if ($this->db->where('regisid', $regisid)->get('tbl_application_pust')->num_rows() >= 1) {
            return true;
        } else {
            return false;
        }
    }
    public function get_unit_wise_available_subject_list($unit_name)
    {
        if ($unit_name == 'A') {
            $this->db->where_in('unit', ['A', 'B', 'C']);
        } elseif ($unit_name == 'B') {
            $this->db->where_in('unit', ['B', 'C']);
        } elseif ($unit_name == 'C') {
            $this->db->where('unit', 'C');
        } else {
            $this->db->where('unit', $unit_name);
        }
        // $this->db->order_by('sub_name', 'ASC');
        $query = $this->db->get('tbl_subject');
        $rowcount = $query->num_rows();
        if ($rowcount > 0) {
            return $query->result_array();
        } else {
            return null;
        }
    }
}
