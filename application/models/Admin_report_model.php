<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Admin_report
 *
 * @author Mhabub
 */
class Admin_report_model extends CI_Model
{
    private $_RESG_TABLE;
    private $_APPLY_TABLE;
    private $_PAYMENT_TABLE;

    public function __construct()
    {
        parent::__construct();
        $this->_RESG_TABLE = 'tbl_registration_pust';
        $this->_APPLY_TABLE = 'tbl_application_pust';
        $this->_PAYMENT_TABLE = 'tbl_payment_pust';
    }

    function count_total_applicant()
    { //unique by applicant_id
        $this->db->from($this->_APPLY_TABLE);
        $this->db->group_by('appid');
        $query = $this->db->get();
        return $query->num_rows();
    }
    function count_applicaton_by_unit($unit)
    {
        //unique by applicant_id
        $this->db->from($this->_APPLY_TABLE);
        $this->db->where('UNIT', $unit);
        $this->db->group_by('appid');
        $query = $this->db->get();
        return $query->num_rows();
    }

    function applicaton_by_unit_list($unit)
    {
        $this->db->from($this->_APPLY_TABLE);
        $this->db->where('UNIT', $unit);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return null;
        }
    }

    function get_paid_count()
    {
        //unique by applicant_id
        $sql = "SELECT * FROM $this->_APPLY_TABLE WHERE `TXNID` IS NOT NULL AND `PMTSTATUS` ='P' GROUP BY `appid`";
        $query_all = $this->db->query($sql);
        return $query_all->num_rows();
    }


    function get_admission_cancel_pending_list()
    {
        $this->db->where('cancel_status', 'cancel_apply');
        $this->db->where('cancel_paid_status', 'P');
        $this->db->order_by('cancel_apply_date', 'DESC');
        //$this->db->order_by('cancel_payment_date','DESC');
        $this->db->from('tbl_result');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return null;
        }
    }

    public function get_std_dtl_from_login($system_regid)
    {


        $this->db->where('system_regid', $system_regid);

        $query = $this->db->get('tbl_login');

        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else {
            return null;
        }
    }
}
