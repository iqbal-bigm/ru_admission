<?php

/**
 * Created by PhpStorm.
 * User: BigM Resources Ltd
 * Date: 4/24/2017
 * Time: 1:35 PM
 */
class Home_model extends CI_Model{

    protected $return;
    private $_RESG_TABLE;
    private $_APPLY_TABLE;
    private $_PAYMENT_TABLE;
    private $orginal_image_path;

    public function __construct(){
        parent:: __construct();

        $this->_RESG_TABLE = 'tbl_registration_pust';
        $this->_APPLY_TABLE = 'tbl_application_pust';
        $this->_PAYMENT_TABLE = 'tbl_payment_pust';
        $this->orginal_image_path = realpath(APPPATH . '../uploads');

        $this->return = array('success' => FALSE, 'msg' => NULL, 'content' => NULL, 'reg_status' => FALSE, 'registered_id' => 0);
    }

    public function check_registration_status($reg_id){
        $this->db->where('REGISID', $reg_id);
        $query = $this->db->get($this->_RESG_TABLE);

        if ($query->num_rows() > 0) {
            $this->return['success'] = TRUE;
            $this->return['reg_status'] = TRUE;
            $this->return['content'] = $query->row_array();
            $this->return['registered_id'] = $this->return['content']['REGISID'];
            $this->return['msg'] = 'You Are Already Registered.';
        }else{
            $this->return['msg'] = 'Student Registration Information Not Found.';
            $this->return['reg_status'] = FALSE;
        }

        return $this->return;
    }



    /**
     * SSC Inforamation BY Board, Pass Year , ROLL && Reg no
     */

    public function ssc_info($ssc_board = '', $ssc_passing_year='', $ssc_roll='', $ssc_registration=''){
        if ($ssc_board != '' && $ssc_passing_year != '' && $ssc_roll != '' && $ssc_registration !=''){
          //  $this->db->where('BOARD_NAME', $ssc_board);
            $this->db->where('PASS_YEAR', $ssc_passing_year);
            $this->db->where('ROLL_NO', $ssc_roll);
            $this->db->where('REGNO', $ssc_registration);

            $ssc_table = table_name($ssc_board, $ssc_passing_year, 's');

            $query = $this->db->get($ssc_table);

            if ($query->num_rows() > 0) {
                return $query->row_array();
            }else{
				return null;
			}
        }else{
				return null;
			}
    }

    /**
     * HSC Inforamation BY Board, Pass Year , ROLL && Reg no
     */

    public function hsc_info($hsc_board = '', $hsc_passing_year='', $hsc_roll='', $hsc_registration=''){
        if ($hsc_board != '' && $hsc_passing_year != '' && $hsc_roll != '' && $hsc_registration !=''){
           // $this->db->where('BOARD_NAME', $hsc_board);
            $this->db->where('PASS_YEAR', $hsc_passing_year);
            $this->db->where('ROLL_NO', $hsc_roll);
            $this->db->where('REGNO', $hsc_registration);

           $hsc_table = table_name($hsc_board, $hsc_passing_year, 'h');

            $query = $this->db->get($hsc_table);

            if ($query->num_rows() > 0) {
                return $query->row_array();
            }else{
				return null;
			}
        }else{
				return null;
		}
    }




    /**
     * @param $reg_id
     * @param $result
     * Create New Registration After Checking From @validation_input_data
     */
    public function create_new_registration($reg_id, $ssc_result, $hsc_result){

        $dt =  date('Y-m-d H:i:s');

         $ssc_board = $this->input->post('ssc_board', TRUE);

        $ssc_group  =   $ssc_result['SSC_GROUP'];

        $this->db->set('REGISID', $reg_id);

        $this->db->set('SNAME', $ssc_result['name']);
        $this->db->set('SFNAME',$ssc_result['father']);
        $this->db->set('SMNAME', $ssc_result['mother']);
        $this->db->set('SBOARD_NAME', $ssc_result['board']);
        $this->db->set('SPASS_YEAR', $ssc_result['passyear']);
        $this->db->set('SROLL_NO', $ssc_result['rollno']);
        $this->db->set('SREG_NO', $ssc_result['regno']);
        $this->db->set('SGROUP', $ssc_result['stud_group']);
        $this->db->set('SLTRGRD', $ssc_result['ltrgd']);
        $this->db->set('SGPA', $ssc_result['gpa']);
        $this->db->set('SRESULT', $ssc_result['result']);
        $this->db->set('STD_GENDER', $ssc_result['gender']);

        $this->db->set('HNAME', $ssc_result['name']);
        $this->db->set('HFNAME',$ssc_result['father']);
        $this->db->set('HMNAME', $ssc_result['mother']);
        $this->db->set('HBOARD_NAME', $hsc_result['board']);
        $this->db->set('HPASS_YEAR', $hsc_result['passyear']);
        $this->db->set('HROLL_NO', $hsc_result['rollno']);
        $this->db->set('HREG_NO', $hsc_result['regno']);
        $this->db->set('HGROUP', $hsc_result['stud_group']);
        $this->db->set('HLTRGRD', $hsc_result['ltrgd']);
        $this->db->set('HGPA', $hsc_result['gpa']);
        $this->db->set('HRESULT', $hsc_result['result']);
		
		$this->db->set('FORTH_SUB_CODE', $hsc_result['4th_sub_code']);
				        		
        $this->db->set('CLIENTIP', $_SERVER['REMOTE_ADDR']);
        $this->db->set('ENTRY_DT', $dt);

		$this->db->set('STD_GENDER', $ssc_result['SEX']);

        if ($this->db->insert($this->_RESG_TABLE)){
            $this->return['registered_id'] = $this->db->insert_id();
            $this->return['success'] = TRUE;
            $this->return['msg'] = 'Congrats! You are Successfully Registered.';
            $this->return['reg_status'] = TRUE;
        }
    }



    /**
     * @param $reg_id
     * Get  Registration details
     */
    public function get_registration_details($reg_id){

        $this->db->where('REGISID', $reg_id);
        $query = $this->db->get($this->_RESG_TABLE);

        if ($query->num_rows() > 0) {

            return $query->row_array();
        }else{
            return null;
        }
    }


function get_student_all_grade($REGISID) {
        $this->db->where('REGISID', $REGISID);
        $this->db->from($this->_RESG_TABLE);

        $query = $this->db->get();
        $rowcount = $query->num_rows();
        if($rowcount > 0){
           $r =  $query->row();
           return $r->HLTRGRD;
        }else{
            return null;
        }
}


function get_already_applied_unit($REGISID) {
        $this->db->where('REGISID', $REGISID);
        $this->db->from($this->_APPLY_TABLE);

        $query = $this->db->get();
        $rowcount = $query->num_rows();
        if($rowcount > 0){
           return $query->result_array();
        }else{
            return null;
        }
}

    /**
     * @param $array
     * Apply for the Unit
     */
    public function apply_unit($array){

        $app_id = '';

        $this->db->select('APPID');
        $this->db->from($this->_APPLY_TABLE);
        $this->db->where('UNIT', $array['unitcode']);
        $query = $this->db->get();
        $rowcount = $query->num_rows();

        $serialNo = 10000 + $rowcount + 1;
        $app_id = $array['unitcode']."19". $serialNo;

        $amount_to_pay = amount_topay_array($array['unitcode']);
		
		if($array['with_arch'] == "YES" &&  $array['unitcode'] =="A" ){
			$amount_to_pay = 1345; //$amount_to_pay + 100;
		}

        $query_TEMP = $this->db->where('APPID', $app_id);
        $query_TEMP = $this->db->from($this->_APPLY_TABLE);
        $query_TEMP = $this->db->get();
        $rowcount_TEMP = $query_TEMP->num_rows();

        if($rowcount_TEMP <= 0){

            $arr =  array(
                        'APPID' => $app_id,
                        'REGISID' => $array['reg_id'],
                        'UNIT' =>  $array['unitcode'],
                        'QNAME' => $array['qname'],
                        'MOBNUMBER' => $array['mobile'],
                        'APPLYDATE' => date('Y-m-d H:i:s'),
                        'PMTSTATUS' => 'U',//unpaid
                        'AMOUNT_TO_PAY' => $amount_to_pay,
						'HDISTRICT' => $array['district'],
						'EXAM_VERSION' => $array['exam_question_tpe'],
						'WITH_ARCHITECTURE' => $array['with_arch'],
						'EDIT_ID' => $array['reg_id'].'_'.time(),
                    );

             $this->db->insert($this->_APPLY_TABLE,$arr);
             return $app_id;

        }else {
            return  null;
        }
  }


  function get_application_data($app_id,$reg_id) {
        $this->db->where('REGISID', $reg_id);
        $this->db->where('APPID', $app_id);
        $this->db->from($this->_APPLY_TABLE);

        $query = $this->db->get();
        $rowcount = $query->num_rows();
        if($rowcount > 0){
           return $query->row_array();
        }else{
            return null;
        }
  }
  
  
  
    function get_application_data_by_regid($reg_id) {
        $this->db->where('REGISID', $reg_id);
        $this->db->from($this->_APPLY_TABLE);

        $query = $this->db->get();
        $rowcount = $query->num_rows();
        if($rowcount > 0){
           return $query->row_array();
        }else{
            return null;
        }
  }

    /**
     * @param string
     * upload photo and return photo url
     * @return string
     */
    function upload_photo($reg_id_clean) {

       $is_valid_info =  $this->get_valid_registration($reg_id_clean);

       if($is_valid_info == 'yes'){ //upload if only valid

            $month_day = date('n').'_'.date('j');
            $upload_loc_final = $this->orginal_image_path . '/'.$month_day.'/';
            $config = array(
                 'allowed_types' => 'jpg|jpeg',
                 'file_name' => $reg_id_clean,
                 'upload_path' => $upload_loc_final,
                'overwrite' => true,
                 'max_size' => 1024
             );

             $this->load->library('upload', $config);
             $this->upload->do_upload('userImage');
             $image_data = $this->upload->data();
             $orginal_image_name = $image_data['file_name'];

              $IMGUPLOAD_PATH = $month_day;

              //update in database
               $sql = "UPDATE  ".$this->_RESG_TABLE." set IMGUPLOAD='Y', IMGUPLOAD_PATH='".$IMGUPLOAD_PATH."', IMGUPLOA_NO_TIME = IMGUPLOA_NO_TIME+1  WHERE REGISID = ?";
               $this->db->query($sql, array($reg_id_clean));

             $img = '<img src="'.base_url().'uploads/'.$month_day.'/'.$orginal_image_name.'?'.time().'" height="220px;" width="180px;" alt="img" />';
             return $img;

       }else{
           return false;
       }
    }

    public function get_valid_registration($reg_id){

        $this->db->where('REGISID', $reg_id);
        $query = $this->db->get($this->_RESG_TABLE);

        if ($query->num_rows() > 0) {
            return "yes";
        }else{
            return "no";
        }
    }

function get_student_all_grade_ssc($REGISID) {
        $this->db->where('REGISID', $REGISID);
        $this->db->from($this->_RESG_TABLE);

        $query = $this->db->get();
        $rowcount = $query->num_rows();
        if($rowcount > 0){
           $r =  $query->row();
           return $r->SLTRGRD;
        }else{
            return null;
        }
}
}
