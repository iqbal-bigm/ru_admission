<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Admitcard_model
 *
 * @author Mhabub
 */
class Admitcard_model extends CI_Model{

    protected $return;
    private $_RESG_TABLE;
    private $_APPLY_TABLE;
    private $_PAYMENT_TABLE;


    public function __construct(){
        parent:: __construct();

        $this->_RESG_TABLE = 'tbl_registration_pust';
        $this->_APPLY_TABLE = 'tbl_application_pust';
        $this->_PAYMENT_TABLE = 'tbl_payment_pust';
    }
    
    function get_payment_status_for_admit($appid) {
        
        $this->db->select('APPID,PMTDATE,PMTSTATUS,PMTAMOUNT,TXNID');       
        $this->db->where('APPID', $appid);       
        $this->db->where('PMTSTATUS', 'P');
	$this->db->where('ROLLNO_FINAL >', 0);
        $this->db->from($this->_APPLY_TABLE);
        $query = $this->db->get();
        $rowcount = $query->num_rows();

        if ($rowcount > 0) { //paid
            return true;
        }else{
            return false;
        }        
    }     
    
     function get_admitcard_application_data($app_id) {
        $this->db->where('APPID', $app_id);
        $this->db->from($this->_APPLY_TABLE);
        
        $query = $this->db->get();
        $rowcount = $query->num_rows();    
        if($rowcount > 0){
           return $query->row_array();
        }else{
            return null;
        }      
  }
}