<?php

/**
 * Created by PhpStorm.
 * User: BigM Resources Ltd
 * Date: 4/24/2017
 * Time: 1:35 PM
 */
class Login_model extends CI_Model
{

    protected $return;
    private $_RESG_TABLE;
    private $_APPLY_TABLE;
    private $_PAYMENT_TABLE;
    private $orginal_image_path;

    public function __construct()
    {
        parent::__construct();

        $this->_RESG_TABLE = 'tbl_registration_pust';
        $this->_APPLY_TABLE = 'tbl_application_pust';
        $this->_PAYMENT_TABLE = 'tbl_payment_pust';
        $this->orginal_image_path = realpath(APPPATH . '../uploads');

        $this->return = array('success' => FALSE, 'msg' => NULL, 'content' => NULL, 'reg_status' => FALSE, 'registered_id' => 0);
    }

    /**
     * HSC Inforamation BY Board, Pass Year , ROLL && Reg no
     * check in final data table
     */

    public function check_login_info($mobile_no = '', $applicant_id = '', $admission_roll)
    {

        if ($mobile_no != '' && $applicant_id != '' && $admission_roll != '') {

            $this->db->where('mobile_no', $mobile_no);
            $this->db->where('applicant_id', $applicant_id);
            $this->db->where('admission_roll', $admission_roll);
            $query = $this->db->get('tbl_login');

            if ($query->num_rows() > 0) {
                //update last login field

                $arr_log = array(
                    'last_login_dt' => date('Y-m-d H:i:s')
                );

                $this->db->where('mobile_no', $mobile_no);
                $this->db->update('tbl_login', $arr_log);

                return $query->row();
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }

    /**
     * HSC Inforamation BY Board, Pass Year , ROLL && Reg no
     * collect data
     */

    public function get_info_by_hsc_data($hsc_board = '', $hsc_passing_year = '', $hsc_roll = '', $hsc_registration = '')
    {
        if ($hsc_board != '' && $hsc_passing_year != '' && $hsc_roll != '' && $hsc_registration != '') {


            $this->db->where('HBOARD_NAME', $hsc_board);
            $this->db->where('HPASS_YEAR', $hsc_passing_year);
            $this->db->where('HROLL_NO', $hsc_roll);
            $this->db->where('HREG_NO', $hsc_registration);

            $query = $this->db->get('tbl_pabna_data_final');

            if ($query->num_rows() > 0) {
                return $query->row_array();
            } else {
                return null;
            }
        } else {
            return null;
        }
    }
    public function get_info_by_regisid($regisid)
    {
        if ($regisid) {


            $this->db->where('regisid', $regisid);

            $query = $this->db->get('tbl_pabna_data_final');

            if ($query->num_rows() > 0) {
                return $query->row_array();
            } else {
                return null;
            }
        } else {
            return null;
        }
    }
    public function store_gst_data($gst_data)
    {
        $login_table_data = [
            'system_regid' => substr($gst_data->hsc_board, 0, 2) . substr($gst_data->hsc_pass_year, 2, 2) . trim($gst_data->hsc_regi) . trim($gst_data->hsc_roll),
            'applicant_id' => $gst_data->applicant_id,
            'admission_roll' => $gst_data->admission_roll,
            'hsc_year' => $gst_data->hsc_pass_year,
            'hsc_board' => $gst_data->hsc_board,
            'hsc_reg' => $gst_data->hsc_regi,
            'hsc_roll' => $gst_data->hsc_roll,
            'mobile_no' => $gst_data->cell_phone,
            'password' => rand(100000, 999999),
            'dob' => date('d-m-Y', strtotime(str_replace('/', '-', $gst_data->dob))),
            'gender' => $gst_data->gender,
            // 'maritlal_status' => null,
            // 'father_occupation' => null,
            // 'mother_occupation' => null,
            // 'family_income' => null,
            // 'padd_village' => null,
            // 'padd_house' => null,
            // 'padd_road' => null,
            // 'padd_post_office' => null,
            // 'padd_police_station' => null,
            // 'padd_district' => null,
            // 'local_gurdian_name' => null,
            // 'local_gurdian_realation' => null,
            // 'local_gurdian_phone' => null,
            // 'local_gurdian_address' => null,
            // 'religion' => null,
            // 'scout' => null,
            // 'permanent_address' => null,
            // 'prad_village' => null,
            // 'prad_house' => null,
            // 'prad_road' => null,
            // 'prad_post_office' => null,
            // 'prad_police_station' => null,
            // 'prad_district' => null,
            // 'mailing_address' => null,
            // 'district' => null,
            // 'creation_dt' => null,
            // 'last_login_dt' => null,
            // 'is_secnd_meirt' => null,
            // 'status' => null,
            // 'mob_no_error' => null
        ];
        $registration_table_data = [
            'REGISID' => $login_table_data['system_regid'],
            'SNAME' => $gst_data->name,
            'SFNAME' =>  $gst_data->fname,
            'SMNAME' => $gst_data->mname,
            'SBOARD_NAME' => $gst_data->ssc_board,
            'SPASS_YEAR' => $gst_data->ssc_pass_year,
            'SROLL_NO' => $gst_data->ssc_roll,
            'SREG_NO' => $gst_data->ssc_regi,
            'SGROUP' => $gst_data->ssc_study_group,
            'SGPA' => $gst_data->ssc_gpa,
            // 'SLTRGRD' => null,
            // 'SRESULT' => null,
            'HNAME' => $gst_data->name,
            'HFNAME' => $gst_data->fname,
            'HMNAME' => $gst_data->mname,
            'HBOARD_NAME' => $gst_data->hsc_board,
            'HPASS_YEAR' => $gst_data->hsc_pass_year,
            'HROLL_NO' => $gst_data->hsc_roll,
            'HREG_NO' => $gst_data->hsc_regi,
            'HGROUP' => $gst_data->hsc_study_group,
            'HGPA' => $gst_data->hsc_gpa,
            // 'HLTRGRD' => null,
            // 'HRESULT' => null,
            // 'FORTH_SUB_CODE' => null,
            // 'IMGUPLOAD' => null,
            // 'IMGUPLOAD_PATH' => null,
            // 'IMGUPLOA_NO_TIME' => null,
            'STD_GENDER' => $gst_data->gender,
            // 'CLIENTIP' => $gst_data->ip_address,
            'CLIENTIP' => $this->input->ip_address(),
            // 'ENTRY_DT' => null

        ];
        $final_data = [
            'REGISID' => $login_table_data['system_regid'],
            'SNAME' => $gst_data->name,
            'SFNAME' =>  $gst_data->fname,
            'SMNAME' => $gst_data->mname,
            'APPID' => $gst_data->applicant_id,
            'UNIT' => $gst_data->apply_unit,
            // 'QNAME' => null,
            'MOBNUMBER' => $login_table_data['mobile_no'],
            // 'APPSTATUS' => null,
            // 'APPLYDATE' => date('Y-m-d h:i:s', strtotime(str_replace('/', '-', $gst_data->entry_date))),
            // 'PMTDATE' => null,
            // 'PMTSTATUS' => null,
            // 'PMTAMOUNT' => null,
            // 'TXNID' => null,
            'ROLLNO_FINAL' => $gst_data->admission_roll,
            // 'EXAM_VERSION' => null,
            // 'WITH_ARCHITECTURE' => null,
            //'HDISTRICT' => $gst_data->hsc_college_district,
            'SBOARD_NAME' => $gst_data->ssc_board,
            'SPASS_YEAR' => $gst_data->ssc_pass_year,
            'SROLL_NO' => $gst_data->ssc_roll,
            'SREG_NO' => $gst_data->ssc_regi,
            'SGROUP' => $gst_data->ssc_study_group,
            'SGPA' => $gst_data->ssc_gpa,
            // 'SLTRGRD' => null,
            'HBOARD_NAME' => $gst_data->hsc_board,
            'HPASS_YEAR' => $gst_data->hsc_pass_year,
            'HROLL_NO' => $gst_data->hsc_roll,
            'HREG_NO' => $gst_data->hsc_regi,
            'HGROUP' => $gst_data->hsc_study_group,
            'HGPA' => $gst_data->hsc_gpa,
            // 'HLTRGRD' => null,
            // 'IMGUPLOAD' => null,
            // 'IMGUPLOAD_PATH' => null,
            'STD_GENDER' => $gst_data->gender,
            // 'CLIENTIP' => $gst_data->ip_address,
            'CLIENTIP' => $this->input->ip_address(),
            'english_gpa_score' => getSubjectGPA(json_decode($gst_data->hsc_subject_data), $gst_data->hsc_board, ['general' => ['107', '108', '107-108', '107+108'], 'madrasah' => ['238', '239', '238-239', '238+239'], 'technical' => [1542, 1122, 1622, 1812, 1832, 1811, 1812], 'gce' => ['English']]), //English
            'mathematics_gpa_score' => getSubjectGPA(json_decode($gst_data->hsc_subject_data), $gst_data->hsc_board, ['general' => ['265', '266', '265-266', '265+266'], 'madrasah' => ['228', '229', '228-229', '228+229'], 'technical' => [1421], 'gce' => ['Mathematics']]),
            'physics_gpa_score' => getSubjectGPA(json_decode($gst_data->hsc_subject_data), $gst_data->hsc_board, ['general' => ['174', '175', '174-175', '174+175'], 'madrasah' => ['224', '225', '224-225', '224+225'], 'technical' => [1422], 'gce' => ['Physics']]),
            'chemistry_gpa_score' => getSubjectGPA(json_decode($gst_data->hsc_subject_data), $gst_data->hsc_board, ['general' => ['176', '177', '176-177', '176+177'], 'madrasah' => ['226', '227', '226-227', '226+227'], 'technical' => [1423], 'gce' => ['Chemistry']]),
            'biology_gpa_score' => getSubjectGPA(json_decode($gst_data->hsc_subject_data), $gst_data->hsc_board, ['general' => ['178', '179', '178-179', '178+179'], 'madrasah' => ['230', '231', '230-231', '230+231'], 'technical' => [1324], 'gce' => ['Biology']]),
            'geography_gpa_score' => getSubjectGPA(json_decode($gst_data->hsc_subject_data), $gst_data->hsc_board, ['general' => ['125', '126', '125-126', '125+126'], 'madrasah' => [], 'technical' => [], 'gce' => ['Geography']]), //Geography And Environment
            'statistics_gpa_score' => getSubjectGPA(json_decode($gst_data->hsc_subject_data), $gst_data->hsc_board, ['general' => ['129', '130', '129-130', '129+130'], 'madrasah' => [], 'technical' => [], 'gce' => []]), //statistics
            'economics_gpa_score' => getSubjectGPA(json_decode($gst_data->hsc_subject_data), $gst_data->hsc_board, ['general' => ['109', '110', '109-110', '109+110'], 'madrasah' => ['213', '214', '213-214', '213+214'], 'technical' => [], 'gce' => ['Economics']]),
            // 'english_gpa_score' => null,
            // 'mathematics_gpa_score' => null,
            // 'physics_gpa_score' => null,
            // 'chemistry_gpa_score' => null,
            // 'biology_gpa_score' => null,
            // 'statistics_gpa_score' => null,
            // 'is_eligible' => null,
            // 'building_name' => null,
            // 'exam_centre' => null,
            // 'room_no' => null,
            // 'is_in_merit_list' => null,
            // 'score' => null,
            'gst_score' => $gst_data->total,
            // 'merit_position' => null,
            // 'is_quota' => null,
            // 'quota_name' => null,
            // 'sms_sent' => null,
            // 'sub_code' => null,
            // 'class_roll' => null,
            // 'class_reg' => null,
            // 'uni_id_no' => null,
            // 'tracking_id' => null
        ];
        // if (empty($final_data['english_gpa_score']) && $this->data['student_info']->hsc_board != 'gce') {
        //     redirect('errordata');
        // } 
        $this->db->trans_begin();
        $this->db->insert('tbl_login', $login_table_data);
        $this->db->insert('tbl_registration_pust', $registration_table_data);
        $this->db->insert('tbl_pabna_data_final', $final_data);
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
        } else {
            $this->db->trans_commit();
        }
    }
}
