<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Payment_model
 *
 * @author Mhabub
 */
class Payment_model extends CI_Model{

    protected $return;
    private $_RESG_TABLE;
    private $_APPLY_TABLE;
    private $_PAYMENT_TABLE;

    public function __construct(){
        parent:: __construct();

        $this->_RESG_TABLE = 'tbl_registration_pust';
        $this->_APPLY_TABLE = 'tbl_application_pust';
        $this->_PAYMENT_TABLE = 'tbl_payment_pust';     
    }
     public function getPaymentStatus($APPID) {

        $this->db->select('APPID,PMTDATE,PMTSTATUS,PMTAMOUNT,TXNID');       
        $this->db->where('APPID', $APPID);
        //$this->db->where('PMTSTATUS', 'P'); //PAID
        $this->db->from($this->_APPLY_TABLE);
        $query = $this->db->get();
        $rowcount = $query->num_rows();
        if($rowcount > 0){
            return  $query->row();
        }else{
            return null;
        }
        
    }
    
    public function makePayment($data) {

        $dateTime = date('Y-m-d H:i:s');
        $appdata = array(
            'PMTSTATUS' => 'P',
            'PMTAMOUNT' => $data['AMOUNT'],
            'TXNID' => $data['TXNID'],
            'APPSTATUS' => 6,
            'PMTDATE' => $dateTime,
        );

        $this->db->where('APPID', $data['APPID']);
        $this->db->update($this->_APPLY_TABLE, $appdata);
               
        $this->db->set('APPID', $data['APPID']);
        $this->db->set('BILLERID', $data['BILLERID']);
        $this->db->set('PAYMENTID', $data['PAYMENTID']);
        $this->db->set('AMOUNT', $data['AMOUNT']);
        $this->db->set('PAYMENTDATE', $dateTime);
        $this->db->set('TXNDATE', $data['TXNDATE']);

        $this->db->insert($this->_PAYMENT_TABLE );        
    }    
    
    function get_payment_status($appid) {
        $this->db->select('APPID,PMTDATE,PMTSTATUS,PMTAMOUNT,TXNID');
        $this->db->from($this->_APPLY_TABLE);
        $this->db->where('APPID', $appid);       
        $this->db->where('PMTSTATUS', 'P');
	$this->db->where('ROLLNO_FINAL >', 0);
        $query = $this->db->get();
        $rowcount = $query->num_rows();

        if ($rowcount > 0) { //paid
            return true;
        }else{
            return false;
        }        
    }    
}
