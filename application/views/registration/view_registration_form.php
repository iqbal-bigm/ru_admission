<?php $this->load->view('common/header'); ?>
<?php $this->load->view('common/navbar'); ?>

<div id="main">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">Registration Form</div>
                        
                    <?php
                    if(!empty($this->session->flashdata('notification_error'))){ 
                        ?>
                    <div class="panel-body">
                        <div class=" has-error">
                            <span class="pd15 bg-danger help-block"><?php echo $this->session->flashdata('notification_error'); ?></span>
                        </div>
                    </div>                    
                    <?php
                    }
                    ?>
                    <?php $msg = $this->session->flashdata('notification');

                    if( !empty($msg)): ?>
                    <div class="panel-body">
                        <div class="">
                            <span class="pd15 bg-success help-block"><?php echo $msg; ?></span>
                        </div>
                    </div>
                    <?php endif; ?>
                    <div class="panel-body">
                        <?= form_open('registration/save' ); ?>
                        <?php $this->form_validation->set_error_delimiters('<span class="help-block">', '</span>'); ?>
                        
                        <div class="row form-group">
                            <div class="col-md-3">
                                <p><strong>* Mobile Number (11 digit)</strong></p>
                                
                            </div> 
                            <div class="col-md-4">
                                <div class="form-group <?php if(form_error('mobile_no')) echo "has-error"; ?>">
                                    <?php
                                    $form_input = array(
                                        'name'  => 'mobile_no',
                                        'id'  => 'mobile_no',
                                        'value' => set_value('mobile_no', $mobile_no),
                                        'autocomplete' => 'off',
                                        'class'  => 'form-control',
                                        'placeholder'  => 'Mobile No',
                                        'maxlength' => '11'
                                    );
                                    echo form_input($form_input);
                                    if(form_error('mobile_no')) echo form_label(form_error('mobile_no'));
                                    ?>
                                    <b>Please write mobile number carefully, your login id and password will send to mobile number by SMS</b>
                                </div>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-md-3"><p><strong>* H.S.C / Equivalent Board/Institute</strong></p></div>
                            <div class="col-md-4">
                                <div class="form-group <?php if(form_error('hsc_board')) echo "has-error"; ?>">
                                    <?php
                                        echo form_dropdown('hsc_board',  /*ssc_hsc_board()*/ hsc_board(), set_value('hsc_board', $hsc_board), ['id' => 'hsc_board', 'class' => 'form-control', 'required']);
                                        if(form_error('hsc_board')) echo form_label(form_error('hsc_board'));
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-md-3"><p><strong>* H.S.C / Equivalent Passing Year</strong></p></div>
                            <div class="col-md-4">
                                <div class="form-group <?php if(form_error('hsc_passing_year')) echo "has-error"; ?>">
                                    <?php
                                        echo form_dropdown('hsc_passing_year', hsc_passing_year(), set_value('hsc_passing_year', $hsc_passing_year), ['id' => 'hsc_passing_year', 'class' => 'form-control', 'required']);
                                        if(form_error('hsc_passing_year')) echo form_label(form_error('hsc_passing_year'));
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-md-3"><p><strong>* H.S.C / Equivalent Reg. No.</strong></p></div>
                            <div class="col-md-4">
                                <div class="form-group <?php if(form_error('hsc_registration')) echo "has-error"; ?>">
                                    <?php
                                    $form_input = array(
                                        'name'  => 'hsc_registration',
                                        'id'  => 'hsc_registration',
                                        'value' => set_value('hsc_registration', $hsc_registration),
                                        'autocomplete' => 'off',
                                        'class'  => 'form-control',
                                        'placeholder'  => 'HSC Registraion No'
                                    );
                                    echo form_input($form_input);
                                    if(form_error('hsc_registration')) echo form_label(form_error('hsc_registration'));
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-md-3"><p><strong>* H.S.C / Equivalent Roll No.</strong></p></div>
                            <div class="col-md-4">
                                <div class="form-group <?php if(form_error('hsc_roll')) echo "has-error"; ?>">
                                    <?php
                                    $form_input = array(
                                        'name'  => 'hsc_roll',
                                        'id'  => 'hsc_roll',
                                        'value' => set_value('hsc_roll', $hsc_roll),
                                        'autocomplete' => 'off',
                                        'class'  => 'form-control',
                                        'placeholder'  => 'HSC Roll No'
                                    );
                                    echo form_input($form_input);
                                    if(form_error('hsc_roll')) echo form_label(form_error('hsc_roll'));
                                    ?>
                                </div>
                            </div>
                        </div>
                        
                        
                        <hr>
                        <span class="help-block"> [N.B] All Field Are Required <span class="required" aria-required="true">*</span></span>
                        <div class="col-md-7">
                            <?= form_submit('submit', 'REGISTRATION', ['class' => 'btn btn-primary pull-right']); ?>
                            <?= form_close(); ?>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('common/footer'); ?>
