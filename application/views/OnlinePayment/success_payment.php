<?php $this->load->view('common/header'); ?>
<?php $this->load->view('common/navbar'); ?>
<style>
    .unit_radio_style {
        padding-bottom: 10px;
    }

    .my_error {
        color: red;
    }
</style>

<div id="main">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">Payment Status</div>
                    <div class="panel-body">
                        <h2>Your Payment Has Been Successfully Received. <a href="<?= base_url('dashboard') ?>">Please click here to go back in Dashboard</a></h2>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<?php $this->load->view('common/footer'); ?>