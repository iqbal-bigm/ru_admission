<div>
    <table repeat_header="1" border="1" width="100%" style="border:0px solid #cacaca;border-collapse: collapse;font-size: 11px;">
        <thead>
            <tr>
                <td width="20%">
                    <img width="40px" height="60px" style="" src="<?php echo base_url('assets/images/logo.png'); ?>" />
                </td>
                <td width="80%" valign="top" colspan="5">
                    <span style="font-size:16px;text-align: center;"><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sheikh Hasina University</b></span>
                    <br>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1st Year Admission Waiting List (Session: 2019-2020)
                </td>
            </tr>
            <tr>
                <td colspan="6" style="text-align: left;font-size:12px;padding-top: 20px;padding-bottom: 20px;">Unit: <?php echo $UNIT; ?> </td>
            </tr>
            <tr>
                <td colspan="6" style="text-align: center;font-size:12px;"><b>First Waiting List</b></td>
            </tr>
            <tr>
                <td width="2%"><b>Sl.</b></td>
                <td width="8%"><b>Tracking ID</b></td>

                <td width="10%"><b>Roll No.</b></td>
                <td width="30%"><b>App. ID, Score & Merit</b></td>
                <td width="59%"><b>Applicant's Info</b></td>
            </tr>
        </thead>
        <tbody>

            <?php
            $arr_all_subject_code_name = get_all_subject_code_name();
            $i = 1;
            foreach ($res as $key => $value) {
            ?>
                <tr>
                    <td><?php echo $i; ?></td>
                    <td>
                        <?php
                        echo $value['tracking_id'];
                        ?>
                    </td>
                    <td>
                        <?php
                        echo $value['exam_roll'];
                        ?>
                    </td>
                    <td>
                        <?php

                        echo "Application ID: " . $value['app_id'];
                        echo "<br><br>Score: " . $value['score'];
                        echo "<br>Merit Position: " . $value['merit'];
                        ?>
                    </td>
                    <td>
                        <?php
                        echo "<br>";
                        echo "Name: " . $value['sname'];
                        echo "<br>";
                        echo "<br>Father's Name: " . $value['sfname'];
                        echo "<br>Mother's  Name: " . $value['smname'];
                        echo "<br>SSC Roll: " . $value['ssc_roll'] . ' Board:' . $value['ssc_board'];
                        echo "<br>GPA: " . $value['ssc_gpa'] . " Reg. No." . $value['ssc_reg'];
                        echo "<br>HSC Roll: " . $value['hsc_roll'] . ' Board:' . $value['hsc_board'];
                        echo "<br>GPA: " . $value['hsc_gpa'] . " Reg. No." . $value['hsc_reg'];
                        if ($UNIT == "B") {
                            echo "<br>HSC Group: " . $value['c_section_hsc_group'];
                        }

                        if ($UNIT == "A") {
                            echo "<br>(" . $value['optional'] . ")";
                            echo "<br>";
                            if (!empty($value['res_my_sub_choice'])) {
                                $_indexer = 1;
                                foreach ($value['res_my_sub_choice'] as $key_ => $value_) {
                                    echo $_indexer . ". " . $arr_all_subject_code_name[$value_['sub_code_ref']] . " ";
                                    $_indexer++;
                                }
                            }
                        }
                        echo  "<br>";
                        ?>
                    </td>
                </tr>
            <?php
                $i++;
            }
            ?>
        </tbody>
    </table>
</div>