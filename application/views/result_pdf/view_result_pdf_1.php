<div>
    <table repeat_header="1" border="1" width="100%" style="border:0px solid #cacaca;border-collapse: collapse;font-size: 11px;">
        <thead>
            <tr>
                <td width="20%">
                    <img width="40px" height="60px" style="" src="<?php echo base_url('assets/images/logo.png'); ?>" />
                </td>
                <td width="80%" valign="top" colspan="5">
                    <span style="font-size:16px;text-align: center;"><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sheikh Hasina University</b></span>
                    <br>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1st Year Admission Result Sheet (Session: 2017-2018)
                </td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: left;font-size:12px;">Unit: <?php echo $UNIT;
                                                                                echo " (" . $group_c . ")"; ?> </td>
                <td style="text-align: left;font-size:12px;">Subject Code: <?php echo $sub_code; ?></td>
                <td style="text-align: left;font-size:12px;">Subject Name: <?php echo $sub_name; ?> </td>
                <td style="text-align: left;font-size:12px;">Faculty Name: <?php echo $faculty_name; ?></td>
                <td style="text-align: left;font-size:12px;">
                    Total Seat: <?php echo $total_seat; ?>
                    <br>
                    Total Seat For <?php echo $group_c; ?> : <?php echo count($res); ?>
                </td>
            </tr>
            <tr>
                <td colspan="6" style="text-align: center;font-size:12px;"><b>Result Sheet</b></td>
            </tr>
            <tr>
                <td width="5%"><b>Sl.</b></td>
                <td width="12%"><b>Tracking ID</b></td>
                <td width="10%"><b>Roll No.</b></td>
                <td width="16%"><b>App. ID, Score & Merit</b></td>
                <td width="40%"><b>Applicant's Info</b></td>
                <td width="17%"><b>Photo</b></td>
            </tr>
        </thead>
        <tbody>

            <?php
            $i = 1;
            foreach ($res as $key => $value) {
            ?>
                <tr>
                    <td><?php echo $i; ?></td>
                    <td>
                        <?php
                        echo $value['tracking_id'];
                        ?>
                    </td>
                    <td>
                        <?php
                        echo $value['exam_roll'];
                        ?>
                    </td>
                    <td>
                        <?php

                        echo "Application ID: " . $value['app_id'];
                        echo "<br><br>Score: " . $value['score'];
                        echo "<br>Merit Position: " . $value['merit'];
                        ?>
                    </td>
                    <td>
                        <?php
                        echo "<br>";
                        echo "Name: " . $value['sname'];
                        echo "<br>";
                        echo "<br>Father's Name: " . $value['sfname'];
                        echo "<br>Mother's  Name: " . $value['smname'];
                        echo "<br>SSC Roll: " . $value['ssc_roll'] . ' Board:' . $value['ssc_board'];
                        echo "<br>GPA: " . $value['ssc_gpa'] . " Reg. No." . $value['ssc_reg'];
                        echo "<br>HSC Roll: " . $value['hsc_roll'] . ' Board:' . $value['hsc_board'];
                        echo "<br>GPA: " . $value['hsc_gpa'] . " Reg. No." . $value['hsc_reg'];
                        if ($UNIT == "C") {
                            echo "<br>HSC Group: " . $value['c_section_hsc_group'];
                        }
                        echo "<br>";
                        //$IMGUPLOAD_PATH = $value['img_path']; //get_photo_path($value->REGISID);
                        $IMGUPLOAD_PATH = get_photo_path($value['system_reg_id']);
                        ?>
                    </td>
                    <td>
                        <img src="<?php echo 'http://admission1718.pust.ac.bd/uploads/' . $IMGUPLOAD_PATH . '/' . $value['system_reg_id']  . '.jpg'; ?>" alt="photo" style="width:80px;height: 90px" />
                    </td>

                </tr>
            <?php
                $i++;
            }
            ?>
        </tbody>
    </table>
</div>