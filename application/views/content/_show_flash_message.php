<?php if ($message = $this->session->flashdata('message')) : ?>
    <?php if ($message['type'] == 'success') : ?>
        <p class="bg-success"><?= $message['message'] ?></p>
    <?php elseif ($message['type'] == 'error') : ?>
        <p class="bg-danger"><?= $message['message'] ?></p>
    <?php elseif ($message['type'] == 'info') : ?>
        <p class="bg-info"><?= $message['message'] ?></p>
    <?php elseif ($message['type'] == 'primary') : ?>
        <p class="bg-primary"><?= $message['message'] ?></p>
    <?php endif; ?>
<?php endif; ?>