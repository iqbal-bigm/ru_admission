<?php $this->load->view('common/header'); ?>
<?php $this->load->view('common/navbar'); ?>

<div id="main">
    <div class="container">

<div class="row">
            <div class="col-md-12">
 <div class="panel panel-primary">
                    <div class="panel-heading">10TH Admission Notice</div>

                    <div class="panel-body">
                        <div class="row">
                            <div style="text-align: center;">
                                <h3><a target="_blank" href="<?php echo base_url(); ?>migration_files/10thnotice.pdf">10TH Admission Notice</a></h3>
                            </div>
                        </div>
                    </div>
                </div>			
<div class="panel panel-primary">
                    <div class="panel-heading"><h3 >10TH Result: Admission 2018-19</h3></div>
                    <div class="panel-body">
                        <table class="table table-responsive table-bordered">
                            <tr>
                                <th colspan="2"><h3 style="color: #0F0FFF;">Unit A</h3></th>
                            </tr>

                            <tr>
                                <td><h4>Geography and Environment</h4></td><td><a href="<?php echo base_url('resultpdf/10th/09_GeographyandEnvironment_result_10th_.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
                            </tr>
                        </table>
                    </div>
                </div>
				
	<div class="panel panel-primary">
                    <div class="panel-heading">How to Pay Admission Fee</div>

                    <div class="panel-body">
                        <div class="row">
                            <div style="text-align: center;">
                                <h3><a target="_blank" href="http://admission1819.pust.ac.bd/migration_files/PUST_Admission_Guideline_2018-2019.pdf">Admission Guideline and Payment</a></h3>
				<img width="80%;" src="http://admission1819.pust.ac.bd/migration_files/Screenshot_1.jpg" />
                            </div>
                        </div>
                        <hr>
                    </div>
                </div>				
				
    <div class="panel panel-primary">
                    <div class="panel-heading">9TH Admission Notice</div>

                    <div class="panel-body">
                        <div class="row">
                            <div style="text-align: center;">
                                <h3><a target="_blank" href="<?php echo base_url(); ?>migration_files/9th_Admission_Notice_24.02.19.pdf">9TH Admission Notice</a></h3>
                            </div>
                        </div>
                    </div>
                </div>
				

				
<div class="panel panel-primary">
                    <div class="panel-heading"><h3 >9TH Result: Admission 2018-19</h3></div>
                    <div class="panel-body">
                        <table class="table table-responsive table-bordered">
                            <tr>
                                <th colspan="2"><h3 style="color: #0F0FFF;">Unit A</h3></th>
                            </tr>

                            <tr>
                                <td><h4>Geography and Environment</h4></td><td><a href="<?php echo base_url('resultpdf/9th/09_GeographyandEnvironment_result_9th.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
                            </tr>
                        </table>
                    </div>
                </div>

 <div class="panel panel-primary">
                    <div class="panel-heading">9TH Waiting List</div>

                    <div class="panel-body">
                        <div class="row">
                            <div style="text-align: center;">
                                <a target="_blank" href="http://admission1819.pust.ac.bd/waiting/A_waiting_10.pdf">Unit A</a>
                            </div>
                        </div>
                        <hr>
                    </div>
</div>


            <div class="panel panel-primary">
                    <div class="panel-heading">8th Migration Result</div>

                    <div class="panel-body">
                        <div class="row">
                            <div style="text-align: center;">
                                <a target="_blank" href="http://admission1819.pust.ac.bd/migration_files/8th_migration.pdf">Download 8th Migration Result</a>
                            </div>
                        </div>
                        <hr>
                    </div>
                </div>

    <div class="panel panel-primary">
                    <div class="panel-heading">8TH Admission Notice</div>

                    <div class="panel-body">
                        <div class="row">
                            <div style="text-align: center;">
                                <h3><a target="_blank" href="<?php echo base_url(); ?>migration_files/8th_admission_notice.pdf">8TH Admission Notice</a></h3>
                            </div>
                        </div>
                    </div>
                </div>

 <div class="panel panel-primary">
                    <div class="panel-heading"><h3 >8TH Result: Admission 2018-19</h3></div>
                    <div class="panel-body">
                        <table class="table table-responsive table-bordered">
                            <tr>
                                <th colspan="2"><h3 style="color: #0F0FFF;">Unit A</h3></th>
                            </tr>

                            <tr>
                                <td><h4>Geography and Environment</h4></td><td><a href="<?php echo base_url('resultpdf/8th/09_GeographyandEnvironment_result_8th.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
                            </tr>
                            <tr>
                                <td><h4>Statistics</h4></td><td><a href="<?php echo base_url('resultpdf/8th/16_Statistics_result_8th.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
                            </tr>

                            <tr>
                                <th colspan="2"><h3 style="color: #0F0FFF;">Unit B</h3></th>
                            </tr>
                            <tr>
                                <td><h4>History and Bangladesh Studies</h4></td><td><a href="<?php echo base_url('resultpdf/8th/20_HistoryandBangladeshStudies_result_8th.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
                            </tr>
                        </table>
                    </div>
                </div>

 <div class="panel panel-primary">
                    <div class="panel-heading">8TH Waiting List</div>

                    <div class="panel-body">
                        <div class="row">
                            <div style="text-align: center;">
                                <a target="_blank" href="http://admission1819.pust.ac.bd/waiting/A_waiting_9.pdf">Unit A</a>
                            </div>
                            <div style="text-align: center;">
                                <a target="_blank" href="http://admission1819.pust.ac.bd/waiting/B_science_waiting_9.pdf">Unit B Science</a>
                            </div>
                                <div style="text-align: center;">
                                <a target="_blank" href="http://admission1819.pust.ac.bd/waiting/B_humanities_waiting_9.pdf">Unit B Humanities</a>
                            </div>
                        </div>
                        <hr>
                    </div>
                </div>

            <div class="panel panel-primary">
                    <div class="panel-heading">7th Migration Result</div>

                    <div class="panel-body">
                        <div class="row">
                            <div style="text-align: center;">
                                <a target="_blank" href="http://admission1819.pust.ac.bd/migration_files/7th_migration.pdf">Download 7th Migration Result</a>
                            </div>
                        </div>
                        <hr>
                    </div>
                </div>

                <div class="panel panel-primary" style="display: none;">
                    <div class="panel-heading">How to Stop Auto Migration</div>

                    <div class="panel-body">
                        <div class="row">
                            <div style="text-align: center;">
<!--                                <h4>Last date of Stop Auto Migration is 08.02.2019 12:00 PM</h4>-->
                                <b><a target="_blank" href="http://admission1819.pust.ac.bd/migration_files/How_to_stop_auto_migration.pdf">Stop Auto Migration Guideline</a></b>
                            </div>
                        </div>

                    </div>
                </div>

               <div class="panel panel-primary">
                    <div class="panel-heading">7TH Admission Notice</div>

                    <div class="panel-body">
                        <div class="row">
                            <div style="text-align: center;">
                                <h3><a target="_blank" href="<?php echo base_url(); ?>migration_files/7th_admission_notice.pdf">7TH Admission Notice</a></h3>
                            </div>
                        </div>
                    </div>
                </div>

 <div class="panel panel-primary">
                    <div class="panel-heading"><h3 >7TH Result: Admission 2018-19</h3></div>
                    <div class="panel-body">
                        <table class="table table-responsive table-bordered">
                            <tr>
                                <th colspan="2"><h3 style="color: #0F0FFF;">Unit A</h3></th>
                            </tr>

                            <tr>
                                <td><h4>Geography and Environment</h4></td><td><a href="<?php echo base_url('resultpdf/7th/09_GeographyandEnvironment_result_7th.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
                            </tr>
                            <tr>
                                <td><h4>Statistics</h4></td><td><a href="<?php echo base_url('resultpdf/7th/16_Statistics_result_7th.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
                            </tr>

                            <tr>
                                <th colspan="2"><h3 style="color: #0F0FFF;">Unit A ARCH</h3></th>
                            </tr>
                            <tr>
                                <td><h4>Architecture</h4></td><td><a href="<?php echo base_url('resultpdf/7th/12_Architecture_result_7th.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
                            </tr>


                            <tr>
                                <th colspan="2"><h3 style="color: #0F0FFF;">Unit B</h3></th>
                            </tr>
                            <tr>
                                <td><h4>History and Bangladesh Studies</h4></td><td><a href="<?php echo base_url('resultpdf/7th/20_HistoryandBangladeshStudies_result_7th.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
                            </tr>
                        </table>
                    </div>
                </div>

                <div class="panel panel-primary">
                    <div class="panel-heading">How to Pay Admission Fee</div>

                    <div class="panel-body">
                        <div class="row">
                            <div style="text-align: center;">
                                <h3><a target="_blank" href="http://admission1819.pust.ac.bd/migration_files/PUST_Admission_Guideline_2018-2019.pdf">Admission Guideline and Payment</a></h3>
				<img width="80%;" src="http://admission1819.pust.ac.bd/migration_files/Screenshot_1.jpg" />
                            </div>
                        </div>
                        <hr>
                    </div>
                </div>

        <div class="panel panel-primary">
                    <div class="panel-heading">7TH Waiting List</div>

                    <div class="panel-body">
                        <div class="row">
                            <div style="text-align: center;">
                                <a target="_blank" href="http://admission1819.pust.ac.bd/waiting/A_waiting_8.pdf">Unit A</a>
                            </div>
                            <div style="text-align: center;">
                                <a target="_blank" href="http://admission1819.pust.ac.bd/waiting/A_ARCH_waiting_8.pdf">Unit A ARCH</a>
                            </div>
                            <div style="text-align: center;">
                                <a target="_blank" href="http://admission1819.pust.ac.bd/waiting/B_science_waiting_8.pdf">Unit B Science</a>
                            </div>
                                <div style="text-align: center;">
                                <a target="_blank" href="http://admission1819.pust.ac.bd/waiting/B_humanities_waiting_8.pdf">Unit B Humanities</a>
                            </div>
                        </div>
                        <hr>
                    </div>
                </div>

            <div class="panel panel-primary">
                    <div class="panel-heading">6th Migration Result</div>

                    <div class="panel-body">
                        <div class="row">
                            <div style="text-align: center;">
                                <a target="_blank" href="http://admission1819.pust.ac.bd/migration_files/6th_migration.pdf">Download 6th Migration Result</a>
                            </div>
                        </div>
                        <hr>
                    </div>
                </div>


               <div class="panel panel-primary">
                    <div class="panel-heading">6TH Admission Notice</div>

                    <div class="panel-body">
                        <div class="row">
                            <div style="text-align: center;">
                                <h3><a target="_blank" href="<?php echo base_url(); ?>migration_files/6th_Admission_Notice.pdf">6th Admission Notice</a></h3>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-primary">
                    <div class="panel-heading"><h3 >6TH Result: Admission 2018-19</h3></div>
                    <div class="panel-body">
                        <table class="table table-responsive table-bordered">
                            <tr>
                                <th colspan="2"><h3 style="color: #0F0FFF;">Unit A</h3></th>
                            </tr>

                            <tr>
                                <td><h4>Urban and Regional Planning</h4></td><td><a href="<?php echo base_url('resultpdf/6th/17_UrbanandRegionalPlanning_result_6th.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
                            </tr>

                            <tr>
                                <td><h4>Geography and Environment</h4></td><td><a href="<?php echo base_url('resultpdf/6th/09_GeographyandEnvironment_result_6th.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
                            </tr>
                            <tr>
                                <td><h4>Statistics</h4></td><td><a href="<?php echo base_url('resultpdf/6th/16_Statistics_result_6th.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
                            </tr>
                            <tr>
                                <td><h4>Mathematics</h4></td><td><a href="<?php echo base_url('resultpdf/6th/03_Mathematics_result_6th.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
                            </tr>

                            <tr>
                                <th colspan="2"><h3 style="color: #0F0FFF;">Unit A ARCH</h3></th>
                            </tr>
                            <tr>
                                <td><h4>Architecture</h4></td><td><a href="<?php echo base_url('resultpdf/6th/12_Architecture_result_6th.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
                            </tr>


                            <tr>
                                <th colspan="2"><h3 style="color: #0F0FFF;">Unit B</h3></th>
                            </tr>
                            <tr>
                                <td><h4>History and Bangladesh Studies</h4></td><td><a href="<?php echo base_url('resultpdf/6th/20_HistoryandBangladeshStudies_result_6th.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
                            </tr>
                        </table>
                    </div>
                </div>



        <div class="panel panel-primary">
                    <div class="panel-heading">6TH Waiting List</div>

                    <div class="panel-body">
                        <div class="row">
                            <div style="text-align: center;">
                                <a target="_blank" href="http://admission1819.pust.ac.bd/waiting/A__waiting_7.pdf">Unit A</a>
                            </div>
                            <div style="text-align: center;">
                                <a target="_blank" href="http://admission1819.pust.ac.bd/waiting/A_ARCH__waiting_7.pdf">Unit A ARCH</a>
                            </div>
                            <div style="text-align: center;">
                                <a target="_blank" href="http://admission1819.pust.ac.bd/waiting/B_science_waiting_7.pdf">Unit B Science</a>
                            </div>
                                <div style="text-align: center;">
                                <a target="_blank" href="http://admission1819.pust.ac.bd/waiting/B_humanities_waiting_7.pdf">Unit B Humanities</a>
                            </div>
                        </div>
                        <hr>
                    </div>
                </div>


                <div class="panel panel-primary">
                    <div class="panel-heading">5th Migration Result</div>

                    <div class="panel-body">
                        <div class="row">
                            <div style="text-align: center;">
                                <a target="_blank" href="http://admission1819.pust.ac.bd/migration_files/5th_migration.pdf">Download 5th Migration Result</a>
                            </div>
                        </div>
                        <hr>
                    </div>
                </div>

                <div class="panel panel-primary" style="display: none;">
                    <div class="panel-heading">How to Stop Auto Migration</div>

                    <div class="panel-body">
                        <div class="row">
                            <div style="text-align: center;">
                                <h4>Last date of Stop Auto Migration is 08.02.2019 12:00 PM</h4>
                                <b><a target="_blank" href="http://admission1819.pust.ac.bd/migration_files/How_to_stop_auto_migration.pdf">Stop Auto Migration Guideline</a></b>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="panel panel-primary">
                    <div class="panel-heading">Important Notice for A Unit</div>

                    <div class="panel-body">
                        <div class="row">
                            <div style="text-align: center;">
                                        <style>
                                        .blink_me {
                                          animation: blinker 1s linear infinite;
                                        }

                                        @keyframes blinker {
                                          50% {
                                            opacity: 0;
                                          }
                                        }
                                        </style>
                                    <h3 class="" style="color:red;">Merit Position 1313 to 2000 (370 Applicants) from 5th Waiting List</h3>
                                    <br>
                                    <span style="color:red;font-size: 20px;"> If you are interested to take admission from waiting list, please login and apply within 06-Feb-2019. Subject will be allocated based on merit position and seat availability.  Please check your phone message for Password. If you are failed to apply within the time, you will never  selected for admission.</span>

                                    <h3><a target="_blank" href="<?php echo base_url(); ?>migration_files/notice_of_A_unit_1313_2000.pdf">Download Notice</a></h3>
                            </div>
                        </div>
                    </div>
                </div>

               <div class="panel panel-primary">
                    <div class="panel-heading">5TH Admission Notice</div>

                    <div class="panel-body">
                        <div class="row">
                            <div style="text-align: center;">
                                <h3><a target="_blank" href="<?php echo base_url(); ?>migration_files/5th_admission_notice.pdf">5th Admission Notice</a></h3>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-primary" style="display: none;">
                    <div class="panel-heading">5th Migration Result</div>

                    <div class="panel-body">
                        <div class="row">
                            <div style="text-align: center;">
                                <a target="_blank" href="http://admission1819.pust.ac.bd/migration_files/4th_migration.pdf">Download 4th Migration Result</a>
                            </div>
                        </div>
                        <hr>
                    </div>
                </div>
                                   <div class="panel panel-primary">
                            <div class="panel-heading"><h3 >6TH Result: Admission 2018-19</h3></div>
                    				<div class="panel-body">
						<table class="table table-responsive table-bordered">
						<tr>
                                     <th colspan="2"><h3 style="color: #0F0FFF;">Unit A</h3></th>
						</tr>

						<tr>
						<td><h4>Urban and Regional Planning</h4></td><td><a href="<?php echo  base_url('resultpdf/5th/17_UrbanandRegionalPlanning_result_5th.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
						</tr>

						<tr>
						<td><h4>Geography and Environment</h4></td><td><a href="<?php echo  base_url('resultpdf/5th/09_GeographyandEnvironment_result_5th.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
						</tr>
						<tr>
						<td><h4>Statistics</h4></td><td><a href="<?php echo  base_url('resultpdf/5th/16_Statistics_result_5th.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
						</tr>
						<tr>
						<td><h4>Mathematics</h4></td><td><a href="<?php echo  base_url('resultpdf/5th/03_Mathematics_result_5th.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
						</tr>

                                                <tr>
						<th colspan="2"><h3 style="color: #0F0FFF;">Unit A ARCH</h3></th>
						</tr>
						<tr>
						<td><h4>Architecture</h4></td><td><a href="<?php echo  base_url('resultpdf/5th/12_Architecture_result_5th.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
						</tr>


						<tr>
						<th colspan="2"><h3 style="color: #0F0FFF;">Unit B</h3></th>
						</tr>
						<tr>
						<td><h4>History and Bangladesh Studies</h4></td><td><a href="<?php echo  base_url('resultpdf/5th/20_HistoryandBangladeshStudies_result_5th.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
						</tr>
						</table>
						</div>
					</div>

             <div class="panel panel-primary">
                    <div class="panel-heading">How to Pay Admission Fee</div>

                    <div class="panel-body">
                        <div class="row">
                            <div style="text-align: center;">
                                <h3><a target="_blank" href="http://admission1819.pust.ac.bd/migration_files/PUST_Admission_Guideline_2018-2019.pdf">Admission Guideline and Payment</a></h3>
								<img width="80%;" src="http://admission1819.pust.ac.bd/migration_files/Screenshot_1.jpg" />
                            </div>
                        </div>
                        <hr>
                    </div>
                </div>


    <div class="panel panel-primary">
                    <div class="panel-heading">5TH Waiting List</div>

                    <div class="panel-body">
                        <div class="row">
                            <div style="text-align: center;">
                                <a target="_blank" href="http://admission1819.pust.ac.bd/waiting/A_waiting_6.pdf">Unit A</a>
                            </div>
                            <div style="text-align: center;">
                                <a target="_blank" href="http://admission1819.pust.ac.bd/waiting/A_ARCH_waiting_6.pdf">Unit A ARCH</a>
                            </div>
                            <div style="text-align: center;">
                                <a target="_blank" href="http://admission1819.pust.ac.bd/waiting/B_science_waiting_6.pdf">Unit B Science</a>
                            </div>
                                <div style="text-align: center;">
                                <a target="_blank" href="http://admission1819.pust.ac.bd/waiting/B_humanities_waiting_6.pdf">Unit B Humanities</a>
                            </div>
                        </div>
                        <hr>
                    </div>
                </div>


                <div class="panel panel-primary">
                    <div class="panel-heading">4th Migration Result</div>

                    <div class="panel-body">
                        <div class="row">
                            <div style="text-align: center;">
                                <a target="_blank" href="http://admission1819.pust.ac.bd/migration_files/4th_migration.pdf">Download 4th Migration Result</a>
                            </div>
                        </div>
                        <hr>
                    </div>
                </div>

				<div class="panel panel-primary">
                    <div class="panel-heading">How to Stop Auto Migration</div>

                    <div class="panel-body">
                        <div class="row">
                            <div style="text-align: center;">
							     <h4>Last date of Stop Auto Migration is 01.02.2019 12:00 PM</h4>
                                <b><a target="_blank" href="http://admission1819.pust.ac.bd/migration_files/How_to_stop_auto_migration.pdf">Stop Auto Migration Guideline</a></b>
                            </div>
                        </div>

                    </div>
                </div>

               <div class="panel panel-primary">
                    <div class="panel-heading">4TH Admission Notice</div>

                    <div class="panel-body">
                        <div class="row">
                            <div style="text-align: center;">
                                <h3><a target="_blank" href="<?php echo base_url(); ?>migration_files/4th_admission_notice_update.pdf">4th Admission Notice</a></h3>
                            </div>
                        </div>
                    </div>
                </div>

                    <div class="panel panel-primary">
                            <div class="panel-heading"><h3 >4TH Result: Admission 2018-19</h3></div>
                    				<div class="panel-body">
						<table class="table table-responsive table-bordered">
						<tr>
                                  <th colspan="2"><h3 style="color: #0F0FFF;">Unit A</h3></th>
						</tr>

						<tr>
						<td><h4>Urban and Regional Planning</h4></td><td><a href="<?php echo  base_url('resultpdf/4th/17_UrbanandRegionalPlanning_result_4th.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
						</tr>

						<tr>
						<td><h4>Geography and Environment</h4></td><td><a href="<?php echo  base_url('resultpdf/4th/09_GeographyandEnvironment_result_4th.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
						</tr>
						<tr>
						<td><h4>Statistics</h4></td><td><a href="<?php echo  base_url('resultpdf/4th/16_Statistics_result_4th.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
						</tr>
						<tr>
						<td><h4>Mathematics</h4></td><td><a href="<?php echo  base_url('resultpdf/4th/03_Mathematics_result_4th.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
						</tr>

                                                <tr>
						<th colspan="2"><h3 style="color: #0F0FFF;">Unit A ARCH</h3></th>
						</tr>
						<tr>
						<td><h4>Architecture</h4></td><td><a href="<?php echo  base_url('resultpdf/4th/12_Architecture_result_4th.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
						</tr>


						<tr>
						<th colspan="2"><h3 style="color: #0F0FFF;">Unit B</h3></th>
						</tr>
						<tr>
						<td><h4>History and Bangladesh Studies</h4></td><td><a href="<?php echo  base_url('resultpdf/4th/20_HistoryandBangladeshStudies_result_4th.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
						</tr>
						</table>
						</div>
					</div>


    <div class="panel panel-primary">
                    <div class="panel-heading">4TH Waiting List</div>

                    <div class="panel-body">
                        <div class="row">
                            <div style="text-align: center;">
                                <a target="_blank" href="http://admission1819.pust.ac.bd/waiting/A_waiting_5.pdf">Unit A</a>
                            </div>
                            <div style="text-align: center;">
                                <a target="_blank" href="http://admission1819.pust.ac.bd/waiting/A_ARCH_waiting_5.pdf">Unit A ARCH</a>
                            </div>
                            <div style="text-align: center;">
                                <a target="_blank" href="http://admission1819.pust.ac.bd/waiting/B_science_waiting_5.pdf">Unit B Science</a>
                            </div>
                                <div style="text-align: center;">
                                <a target="_blank" href="http://admission1819.pust.ac.bd/waiting/B_humanities_waiting_5.pdf">Unit B Humanities</a>
                            </div>
                        </div>
                        <hr>
                    </div>
                </div>


                <div class="panel panel-primary">
                    <div class="panel-heading">3rd Migration Result</div>

                    <div class="panel-body">
                        <div class="row">
                            <div style="text-align: center;">
                                <a target="_blank" href="http://admission1819.pust.ac.bd/migration_files/3rd_migration_list.pdf">Download 3rd Migration Result</a>
                            </div>
                        </div>
                        <hr>
                    </div>
                </div>

               <div class="panel panel-primary">
                    <div class="panel-heading">3rd Admission Notice</div>

                    <div class="panel-body">
                        <div class="row">
                            <div style="text-align: center;">
                                <h3><a target="_blank" href="<?php echo base_url(); ?>migration_files/3rd_admission_notice_19.01.19.pdf">3rd Admission Notice</a></h3>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-primary">
                    <div class="panel-heading">Quota Admission Notice</div>

                    <div class="panel-body">
                        <div class="row">
                            <div style="text-align: center;">
                                <h3><a target="_blank" href="<?php echo base_url(); ?>migration_files/Admission Notice Quota.pdf">Quota Admission Notice</a></h3>
                            </div>
                        </div>
                    </div>
                </div>
                        <div class="panel panel-primary">
                            <div class="panel-heading"><h3 >3RD Result: Admission 2018-19</h3></div>
                    				<div class="panel-body">
						<table class="table table-responsive table-bordered">
						<tr>
                                  <th colspan="2"><h3 style="color: #0F0FFF;">Unit A</h3></th>
						</tr>

						<tr>
						<td><h4>Urban and Regional Planning</h4></td><td><a href="<?php echo  base_url('resultpdf/3rd/17_UrbanandRegionalPlanning_result_3rd.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
						</tr>

						<tr>
						<td><h4>Geography and Environment</h4></td><td><a href="<?php echo  base_url('resultpdf/3rd/09_GeographyandEnvironment_result_3rd.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
						</tr>
						<tr>
						<td><h4>Statistics</h4></td><td><a href="<?php echo  base_url('resultpdf/3rd/16_Statistics_result_3rd.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
						</tr>
						<tr>
						<td><h4>Mathematics</h4></td><td><a href="<?php echo  base_url('resultpdf/3rd/03_Mathematics_result_3rd.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
						</tr>

                                                <tr>
						<th colspan="2"><h3 style="color: #0F0FFF;">Unit A ARCH</h3></th>
						</tr>
						<tr>
						<td><h4>Architecture</h4></td><td><a href="<?php echo  base_url('resultpdf/3rd/12_Architecture_result_3rd.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
						</tr>


						<tr>
						<th colspan="2"><h3 style="color: #0F0FFF;">Unit B</h3></th>
						</tr>
						<tr>
						<td><h4>History and Bangladesh Studies</h4></td><td><a href="<?php echo  base_url('resultpdf/3rd/20_HistoryandBangladeshStudies_result_3rd.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
						</tr>
						</table>
						</div>
					</div>

            <div class="panel panel-primary">
                    <div class="panel-heading"><h3 >Quota Result</h3 ></div>

                    <div class="panel-body">
                        <div class="row">
                            <div style="text-align: center;">
                               <h4> <a target="_blank" href="http://admission1819.pust.ac.bd/resultpdf/quota/FF_A_result_quota.pdf">Unit A, Freedom Fighter</a></h4>
                            </div>
                            <div style="text-align: center;">
                               <h4> <a target="_blank" href="http://admission1819.pust.ac.bd/resultpdf/quota/PH_A_result_quota.pdf">Unit A, Physically Handicapped</a></h4>
                            </div>
                            <div style="text-align: center;">
                            <h4><a target="_blank" href="http://admission1819.pust.ac.bd/resultpdf/quota/FF_B_result_quota.pdf">Unit B, Freedom Fighter Result</a></h4>
                            </div>
                        </div>
                        <hr>
                    </div>
                </div>

		<div class="panel panel-primary">
                    <div class="panel-heading">2ND Migration Result</div>

                    <div class="panel-body">
                        <div class="row">
                            <div style="text-align: center;">
                                <a target="_blank" href="http://admission1819.pust.ac.bd/migration_files/2nd_migration_19.01.2019.pdf">Download 2ND Migration Result</a>
                            </div>
                        </div>
                        <hr>
                    </div>
                </div>

            <div class="panel panel-primary">
                    <div class="panel-heading">3RD Waiting List</div>

                    <div class="panel-body">
                        <div class="row">
                            <div style="text-align: center;">
                                <a target="_blank" href="http://admission1819.pust.ac.bd/waiting/A_waiting_4.pdf">Unit A</a>
                            </div>
                            <div style="text-align: center;">
                                <a target="_blank" href="http://admission1819.pust.ac.bd/waiting/A_ARCH_waiting_4.pdf">Unit A ARCH</a>
                            </div>
                            <div style="text-align: center;">
                                <a target="_blank" href="http://admission1819.pust.ac.bd/waiting/B_science_waiting_4.pdf">Unit B Science </a>
                            </div>
                                <div style="text-align: center;">
                                <a target="_blank" href="http://admission1819.pust.ac.bd/waiting/B_humanities_waiting_4.pdf">Unit B Humanities  </a>
                            </div>
                        </div>
                        <hr>
                    </div>
                </div>


		<div class="panel panel-primary">
                    <div class="panel-heading">How to Stop Auto Migration</div>

                    <div class="panel-body">
                        <div class="row">
                            <div style="text-align: center;">
							     <h4>Last date of Stop Auto Migration is 18.01.2019 12:00 PM</h4>
                                <b><a target="_blank" href="http://admission1819.pust.ac.bd/migration_files/How_to_stop_auto_migration.pdf">Stop Auto Migration Guideline</a></b>
                            </div>
                        </div>

                    </div>
                </div>
			        <div class="panel panel-primary">
                    <div class="panel-heading">2ND Admission Notice</div>

                    <div class="panel-body">
                        <div class="row">
                            <div style="text-align: center;">
                                <h3><a target="_blank" href="<?php echo base_url(); ?>migration_files/2nd_Admission_Notice_.pdf">2ND Admission Notice</a></h3>
                            </div>
                        </div>
                        <hr>
                    </div>
                </div>

 <div class="panel panel-primary">
                    <div class="panel-heading"><h3 >2ND Result: Admission 2018-19</h3></div>
						<div class="panel-body">
						<table class="table table-responsive table-bordered">
						<tr>
                          <th colspan="2"><h3 style="color: #0F0FFF;">Unit A</h3></th>
						</tr>

						<tr>
						<td><h4>Urban and Regional Planning</h4></td><td><a href="<?php echo  base_url('resultpdf/2nd/17_UrbanandRegionalPlanning_result_2nd.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
						</tr>

						<tr>
						<td><h4>Geography and Environment</h4></td><td><a href="<?php echo  base_url('resultpdf/2nd/09_GeographyandEnvironment_result_2nd.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
						</tr>
						<tr>
						<td><h4>Statistics</h4></td><td><a href="<?php echo  base_url('resultpdf/2nd/16_Statistics_result_2nd.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
						</tr>
						<td><h4>Physics</h4></td><td><a href="<?php echo  base_url('resultpdf/2nd/07_Physics_result_2nd.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
						</tr>
						<tr>
						<td><h4>Mathematics</h4></td><td><a href="<?php echo  base_url('resultpdf/2nd/03_Mathematics_result_2nd.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
						</tr>

                                                <tr>
						<th colspan="2"><h3 style="color: #0F0FFF;">Unit A ARCH</h3></th>
						</tr>
						<tr>
						<td><h4>Architecture</h4></td><td><a href="<?php echo  base_url('resultpdf/2nd/12_Architecture_result_2nd.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
						</tr>


						<tr>
						<th colspan="2"><h3 style="color: #0F0FFF;">Unit B</h3></th>
						</tr>
						<tr>
						<td><h4>Tourism and Hospitality Management</h4></td><td><a href="<?php echo  base_url('resultpdf/2nd/21_TourismandHospitalityManagement_result_2nd.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
						</tr>
						<tr>
						<td><h4>Social Work</h4></td><td><a href="<?php echo  base_url('resultpdf/2nd/15_SocialWork_result_2nd.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
						</tr>
						<tr>
						<td><h4>Bangla</h4></td><td><a href="<?php echo  base_url('resultpdf/2nd/10_Bangla_result_2nd.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
						</tr>
						<tr>
						<td><h4>History and Bangladesh Studies</h4></td><td><a href="<?php echo  base_url('resultpdf/2nd/20_HistoryandBangladeshStudies_result_2nd.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
						</tr>
						</table>
						</div>
					</div>

<div class="panel panel-primary">
                    <div class="panel-heading">First Migration Result</div>

                    <div class="panel-body">
                        <div class="row">
                            <div style="text-align: center;">
                                <a target="_blank" href="http://admission1819.pust.ac.bd/migration_files/1st_migration_data_13_jan_2018.pdf">Download First Migration Result</a>
                            </div>
                        </div>
                        <hr>
                    </div>
                </div>

  <div class="panel panel-primary">
                    <div class="panel-heading">2ND Waiting List</div>

                    <div class="panel-body">
                        <div class="row">
                            <div style="text-align: center;">
                                <a target="_blank" href="http://admission1819.pust.ac.bd/waiting/A_waiting_2.pdf">Unit A</a>
                            </div>
                            <div style="text-align: center;">
                                <a target="_blank" href="http://admission1819.pust.ac.bd/waiting/A_ARCH_waiting_2.pdf">Unit A ARCH</a>
                            </div>
                            <div style="text-align: center;">
                                <a target="_blank" href="http://admission1819.pust.ac.bd/waiting/B_science_waiting_2.pdf">Unit B Science </a>
                            </div>
                                <div style="text-align: center;">
                                <a target="_blank" href="http://admission1819.pust.ac.bd/waiting/B_humanities_waiting_2.pdf">Unit B Humanities  </a>
                            </div>
                        </div>
                        <hr>
                    </div>
                </div>


			<div class="panel panel-primary">
                    <div class="panel-heading">How to Stop Auto Migration</div>

                    <div class="panel-body">
                        <div class="row">
                            <div style="text-align: center;">
							     <h4>Last date of Stop Auto Migration is 12.01.2019 12:00 PM</h4>
                                <b><a target="_blank" href="http://admission1819.pust.ac.bd/migration_files/How_to_stop_auto_migration.pdf">Stop Auto Migration Guideline</a></b>
                            </div>
                        </div>

                    </div>
                </div>

				<div class="panel panel-primary">
                    <div class="panel-heading">How to Cancel Admission</div>

                    <div class="panel-body">
                        <div class="row">
                            <div style="text-align: center;">
                                <b><a target="_blank" href="http://admission1819.pust.ac.bd/migration_files/PUST_Admission_Cancellation_Guideline.pdf">Admission Cancellation Guideline</a></b>
                            </div>
                        </div>

                    </div>
                </div>



			<div class="panel panel-primary">
                    <div class="panel-heading">Quota Notice</div>

                    <div class="panel-body">
                        <div class="row">
                            <div style="text-align: center;">
                                <h3><a target="_blank" href="<?php echo base_url(); ?>migration_files/Emargency_notice_of_quota_18.12.18.pdf">Quota Notice</a></h3>
                            </div>
                        </div>
                        <hr>
                    </div>
                </div>

        <div class="panel panel-primary">
                    <div class="panel-heading">First Admission Notice</div>

                    <div class="panel-body">
                        <div class="row">
                            <div style="text-align: center;">
                                <h3><a target="_blank" href="<?php echo base_url(); ?>migration_files/First Admission Notice.pdf">First Admission Notice</a></h3>
                            </div>
                        </div>
                        <hr>
                    </div>
                </div>

                <div class="panel panel-primary">
                    <div class="panel-heading"><h3 >Result: Admission 2018-19</h3></div>
						<div class="panel-body">
						<table class="table table-responsive table-bordered">
						<tr>
                                                    <th colspan="2"><h3 style="color: #0F0FFF;">Unit A</h3></th>
						</tr>
						<tr>
						<td><h4>Computer Science and Engineering</h4></td><td><a href="<?php echo  base_url('resultpdf/01_ComputerScienceandEngineering_result.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
						</tr>
						<tr>
						<td><h4>Civil Engineering</h4></td><td><a href="<?php echo  base_url('resultpdf/11_CivilEngineering_result.pdf'); ?>" target="_blank">DOWNLOAD</td>
						</tr>
						<tr>
						<td><h4>Information and Communication Engineering</h4></td><td><a href="<?php echo  base_url('resultpdf/06_InformationandCommunicationEngineering_result.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
						</tr>
						<tr>
						<td><h4>Electronic and Telecommunication Engineering</h4></td><td><a href="<?php echo  base_url('resultpdf/05_ElectronicandTelecommunicationEngineering_result.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
						</tr>
						<tr>
						<td><h4>Electrical and Electronic Engineering</h4></td><td><a href="<?php echo  base_url('resultpdf/02_ElectricalandElectronicEngineering_result.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
						</tr>
						<tr>
						<td><h4>Urban and Regional Planning</h4></td><td><a href="<?php echo  base_url('resultpdf/17_UrbanandRegionalPlanning_result.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
						</tr>

						<tr>
						<td><h4>Geography and Environment</h4></td><td><a href="<?php echo  base_url('resultpdf/09_GeographyandEnvironment_result.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
						</tr>
						<tr>
						<td><h4>Statistics</h4></td><td><a href="<?php echo  base_url('resultpdf/16_Statistics_result.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
						</tr>
						<tr>
						<td><h4>Chemistry</h4></td><td><a href="<?php echo  base_url('resultpdf/14_Chemistry_result.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
						</tr>
						<tr>
						<td><h4>Pharmacy</h4></td><td><a href="<?php echo  base_url('resultpdf/13_Pharmacy_result.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
						</tr>
						<tr>
						<td><h4>Physics</h4></td><td><a href="<?php echo  base_url('resultpdf/07_Physics_result.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
						</tr>
						<tr>
						<td><h4>Mathematics</h4></td><td><a href="<?php echo  base_url('resultpdf/03_Mathematics_result.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
						</tr>

                                                <tr>
						<th colspan="2"><h3 style="color: #0F0FFF;">Unit A ARCH</h3></th>
						</tr>
						<tr>
						<td><h4>Architecture</h4></td><td><a href="<?php echo  base_url('resultpdf/12_Architecture_result.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
						</tr>


						<tr>
						<th colspan="2"><h3 style="color: #0F0FFF;">Unit B</h3></th>
						</tr>
						<tr>
						<td><h4>Tourism and Hospitality Management</h4></td><td><a href="<?php echo  base_url('resultpdf/21_TourismandHospitalityManagement_result.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
						</tr>
						<tr>
						<td><h4>Public Administration</h4></td><td><a href="<?php echo  base_url('resultpdf/19_PublicAdministration_result.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
						</tr>
						<tr>
						<td><h4>Social Work</h4></td><td><a href="<?php echo  base_url('resultpdf/15_SocialWork_result.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
						</tr>
						<tr>
						<td><h4>English</h4></td><td><a href="<?php echo  base_url('resultpdf/18_English_result.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
						</tr>
						<tr>
						<td><h4>Bangla</h4></td><td><a href="<?php echo  base_url('resultpdf/10_Bangla_result.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
						</tr>
						<tr>
						<td><h4>Economics</h4></td><td><a href="<?php echo  base_url('resultpdf/08_Economics_result.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
						</tr>
						<tr>
						<td><h4>Business Administration</h4></td><td><a href="<?php echo  base_url('resultpdf/04_BusinessAdministration_result.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
						</tr>
						<tr>
						<td><h4>History and Bangladesh Studies</h4></td><td><a href="<?php echo  base_url('resultpdf/20_HistoryandBangladeshStudies_result.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
						</tr>
						</table>
						</div>
					</div>

        <div class="panel panel-primary">
                    <div class="panel-heading">Waiting List</div>

                    <div class="panel-body">
                        <div class="row">
                            <div style="text-align: center;">
                                <a target="_blank" href="http://admission1819.pust.ac.bd/waiting/A_waiting.pdf">Unit A</a>
                            </div>
                            <div style="text-align: center;">
                                <a target="_blank" href="http://admission1819.pust.ac.bd/waiting/A_ARCH_waiting.pdf">Unit A ARCH</a>
                            </div>
                            <div style="text-align: center;">
                                <a target="_blank" href="http://admission1819.pust.ac.bd/waiting/B_science_waiting.pdf">Unit B Science </a>
                            </div>
                                <div style="text-align: center;">
                                <a target="_blank" href="http://admission1819.pust.ac.bd/waiting/B_humanities_waiting.pdf">Unit B Humanities  </a>
                            </div>
                        </div>
                        <hr>
                    </div>
                </div>
		    </div>
		</div>

        <div class="row">
            <div class="col-md-12">

 <div class="panel panel-primary">

        <div class="panel panel-primary">
                    <div class="panel-heading">Admission 2018-2019</div>

                    <div class="panel-body">
                        <div class="row">
                            <div style="text-align: center;">
                                 <h4>Students who are in merit list, has already received login information (Login id and Password) through SMS in Mobile number which was provided during application</h4>
                                 <h4>মেধা তালিকায় থাকা শিক্ষার্থীরা ইতোমধ্যেই মোবাইল নম্বরটিতে লগইন তথ্য (Login id and Password) পেয়েছে, যা আবেদন করার সময় প্রদান করেছিল </h4>
                                <h3><a  href="<?php echo base_url(); ?>login">Click Here For Login</a></h3>
                            </div>
                        </div>
                        <hr>
                    </div>
                </div>

                <div class="panel panel-primary">
                    <div class="panel-heading">Admission 2018-2019: Notice</div>

                    <div class="panel-body">
                        <div class="row">
                            <div style="padding-left: 200px;">

									<h3><a targer="_blank" href="<?php echo base_url(); ?>migration_files/Notice_for_choice_form_fill_up_2018-19.pdf">Download</a></h3>
									<div>
									<br>
									<img src="<?php echo base_url(); ?>migration_files/notice_1.jpg">
									</div>

                            </div>
                        </div>
                        <hr>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('common/footer'); ?>
