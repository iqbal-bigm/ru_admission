<?php $this->load->view('common/header'); ?>
<?php $this->load->view('common/navbar'); ?>

<div id="main">
    <div class="container">


        <div class="row">
            <div class="col-md-8">

 <div class="panel panel-primary">
                <div class="panel panel-primary">
                    <div class="panel-heading">Admission 2019-2020: Result and Guideline</div>

                    <div class="panel-body">
               							<div>						
										<h4>Admission guideline  : <a targer="_blank" href="<?php echo base_url(); ?>migration_files/PUST_Admission_Guideline_2019-2020.pdf">Download</a></h4>
										<h4>How to stop auto migration : <a targer="_blank" href="<?php echo base_url(); ?>migration_files/How_to_stop_auto_migration_2019-2020.pdf">Download</a></h4>
										<h4>Admission cancellation guideline  : <a targer="_blank" href="<?php echo base_url(); ?>migration_files/PUST_Admission_Cancellation_Guideline_2019-2020.pdf">Download</a></h4>
										</div>	
										
								<div>						
								<h3>7th Result</h3>
										<table class="table table-responsive table-bordered">
										<tr>
											<th colspan="2"><h4 style="color: #0F0FFF;">Unit A</h4></th>
										</tr>
	
										<tr>
											<td><h4>Geography and Environment</h4></td><td><a href="<?php echo base_url('resultpdf/7th/09_GeographyandEnvironment_result_7th.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
										</tr>
										<tr>
											<td><h4>Statistics</h4></td><td><a href="<?php echo base_url('resultpdf/7th/16_Statistics_result_7th.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
										</tr>
										
									</table>
																	
							</div>										
		
								<div>						
								<h3>6th Result</h3>
										<table class="table table-responsive table-bordered">
										<tr>
											<th colspan="2"><h4 style="color: #0F0FFF;">Unit A</h4></th>
										</tr>
	
										<tr>
											<td><h4>Geography and Environment</h4></td><td><a href="<?php echo base_url('resultpdf/6th/09_GeographyandEnvironment_result_6th.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
										</tr>
										<tr>
											<td><h4>Statistics</h4></td><td><a href="<?php echo base_url('resultpdf/6th/16_Statistics_result_6th.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
										</tr>
										
									</table>
																	
							</div>		

				<div class="panel-body">
               
                            <div>						
								<h3>5th Migration List</h3>
										<table class="table table-responsive table-bordered">
										<tr>
											<th width="50%;">5th Migration </th> <th><a href="<?php echo base_url('5th_migration_19.02.2020.pdf'); ?>" target="_blank">DOWNLOAD</a></th>  
										</tr>										
									</table>
							</div>
                    </div>								
										
								<div>						
								<h3>5th Result</h3>
										<table class="table table-responsive table-bordered">
										<tr>
											<th colspan="2"><h4 style="color: #0F0FFF;">Unit A</h4></th>
										</tr>
	
										<tr>
											<td><h4>Mathematics</h4></td><td><a href="<?php echo base_url('resultpdf/5th/03_Mathematics_result_5th.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
										</tr>
										<tr>
											<td><h4>Physics</h4></td><td><a href="<?php echo base_url('resultpdf/5th/07_Physics_result_5th.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
										</tr>
										<tr>
											<td><h4>Geography and Environment</h4></td><td><a href="<?php echo base_url('resultpdf/5th/09_GeographyandEnvironment_result_5th.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
										</tr>
										<tr>
											<td><h4>Statistics</h4></td><td><a href="<?php echo base_url('resultpdf/5th/16_Statistics_result_5th.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
										</tr>
										
									</table>
									
										<table class="table table-responsive table-bordered">
										<tr>
											<th colspan="2"><h4 style="color: #0F0FFF;">Unit A ARCH</h4></th>
										</tr>
										<tr>
											<td><h4>Architecture</h4></td><td><a href="<?php echo base_url('resultpdf/5th/12_Architecture_result_5th.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
										</tr>

									</table>

										<table class="table table-responsive table-bordered">
										<tr>
											<th colspan="2"><h4 style="color: #0F0FFF;">Unit B</h4></th>
										</tr>
										<tr>
											<td><h4>History and Bangladesh Studies</h4></td><td><a href="<?php echo base_url('resultpdf/5th/20_HistoryandBangladeshStudies_result_5th.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
										</tr>
									</table>									
							</div>
							
				<div class="panel-body">
               
                            <div>						
								<h3 >5th Waiting List</h3>
										<table class="table table-responsive table-bordered">
										<tr>
											<th width="50%;">Unit A </th> <th><a href="<?php echo base_url('waiting/A_waiting_5th.pdf'); ?>" target="_blank">DOWNLOAD</a></th>  
										</tr>
									</table>
									
										<table class="table table-responsive table-bordered">
										<tr>
											<th>Unit A ARCH</th> <th><a href="<?php echo base_url('waiting/A_ARCH_waiting_5th.pdf'); ?>" target="_blank">DOWNLOAD</a></th>  
										</tr>

									</table>

										<table class="table table-responsive table-bordered">
										<tr>
											<th  width="50%;">Unit B Science</th> <th><a href="<?php echo base_url('waiting/B_science_waiting_5th.pdf'); ?>" target="_blank">DOWNLOAD</a></th>  
										</tr>
										<tr>
											<th  width="50%;">Unit B Humanities</th> <th><a href="<?php echo base_url('waiting/B_humanities_waiting_5th.pdf'); ?>" target="_blank">DOWNLOAD</a></th>  
										</tr>										
									</table>	
							
							</div>
                    </div>							
							
							
				<div class="panel-body">
               
                            <div>						
								<h3>4th Migration List</h3>
										<table class="table table-responsive table-bordered">
										<tr>
											<th width="50%;">4th Migration </th> <th><a href="<?php echo base_url('4th_migration_12.02.2020.pdf'); ?>" target="_blank">DOWNLOAD</a></th>  
										</tr>										
									</table>
							</div>
                    </div>	
					
							<div>						
								<h3 >4th Result</h3>
										<table class="table table-responsive table-bordered">
										<tr>
											<th colspan="2"><h4 style="color: #0F0FFF;">Unit A</h4></th>
										</tr>
	
										<tr>
											<td><h4>Mathematics</h4></td><td><a href="<?php echo base_url('resultpdf/4th/03_Mathematics_result_4th.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
										</tr>
										<tr>
											<td><h4>Physics</h4></td><td><a href="<?php echo base_url('resultpdf/4th/07_Physics_result_4th.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
										</tr>
										<tr>
											<td><h4>Geography and Environment</h4></td><td><a href="<?php echo base_url('resultpdf/4th/09_GeographyandEnvironment_result_4th.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
										</tr>
										<tr>
											<td><h4>Statistics</h4></td><td><a href="<?php echo base_url('resultpdf/4th/16_Statistics_result_4th.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
										</tr>
										
									</table>
									
										<table class="table table-responsive table-bordered">
										<tr>
											<th colspan="2"><h4 style="color: #0F0FFF;">Unit A ARCH</h4></th>
										</tr>
										<tr>
											<td><h4>Architecture</h4></td><td><a href="<?php echo base_url('resultpdf/4th/12_Architecture_result_4th.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
										</tr>

									</table>

										<table class="table table-responsive table-bordered">
										<tr>
											<th colspan="2"><h4 style="color: #0F0FFF;">Unit B</h4></th>
										</tr>
										<tr>
											<td><h4>History and Bangladesh Studies</h4></td><td><a href="<?php echo base_url('resultpdf/4th/20_HistoryandBangladeshStudies_result_4th.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
										</tr>
										<tr>
											<td><h4>Geography and Environment</h4></td><td><a href="<?php echo base_url('resultpdf/4th/22_GeographyandEnvironment_result_4th.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
										</tr>

									</table>									
							</div>						

				<div class="panel-body">
               
                            <div>						
								<h3>3rd Migration List</h3>
										<table class="table table-responsive table-bordered">
										<tr>
											<th width="50%;">3rd Migration </th> <th><a href="<?php echo base_url('3rd_Migration_28.01.2020.pdf'); ?>" target="_blank">DOWNLOAD</a></th>  
										</tr>
										<tr>
											<th width="50%;">Quota Migration </th> <th><a href="<?php echo base_url('Quota_2nd_Migration_28.01.2020.pdf'); ?>" target="_blank">DOWNLOAD</a></th>  
										</tr>										
									</table>
							</div>
                    </div>		


					<div class="panel-body">
               
                            <div>						
								<h3 >4th Waiting List</h3>
										<table class="table table-responsive table-bordered">
										<tr>
											<th width="50%;">Unit A </th> <th><a href="<?php echo base_url('waiting/A_waiting_4th.pdf'); ?>" target="_blank">DOWNLOAD</a></th>  
										</tr>
									</table>
									
										<table class="table table-responsive table-bordered">
										<tr>
											<th>Unit A ARCH</th> <th><a href="<?php echo base_url('waiting/A_ARCH_waiting_4th.pdf'); ?>" target="_blank">DOWNLOAD</a></th>  
										</tr>

									</table>

										<table class="table table-responsive table-bordered">
										<tr>
											<th  width="50%;">Unit B Science</th> <th><a href="<?php echo base_url('waiting/B_science_waiting_4th.pdf'); ?>" target="_blank">DOWNLOAD</a></th>  
										</tr>
										<tr>
											<th  width="50%;">Unit B Humanities</th> <th><a href="<?php echo base_url('waiting/B_humanities_waiting_4th.pdf'); ?>" target="_blank">DOWNLOAD</a></th>  
										</tr>										
									</table>	
							
							</div>
                    </div>						
									
							<div>						
								<h3 >3rd Result</h3>
										<table class="table table-responsive table-bordered">
										<tr>
											<th colspan="2"><h4 style="color: #0F0FFF;">Unit A</h4></th>
										</tr>
	
										<tr>
											<td><h4>Mathematics</h4></td><td><a href="<?php echo base_url('resultpdf/3rd/03_Mathematics_result_3rd.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
										</tr>
										<tr>
											<td><h4>Physics</h4></td><td><a href="<?php echo base_url('resultpdf/3rd/07_Physics_result_3rd.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
										</tr>
										<tr>
											<td><h4>Geography and Environment</h4></td><td><a href="<?php echo base_url('resultpdf/3rd/09_GeographyandEnvironment_result_3rd.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
										</tr>
										<tr>
											<td><h4>Statistics</h4></td><td><a href="<?php echo base_url('resultpdf/3rd/16_Statistics_result_3rd.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
										</tr>
										
									</table>
									
										<table class="table table-responsive table-bordered">
										<tr>
											<th colspan="2"><h4 style="color: #0F0FFF;">Unit A ARCH</h4></th>
										</tr>
										<tr>
											<td><h4>Architecture</h4></td><td><a href="<?php echo base_url('resultpdf/3rd/12_Architecture_result_3rd.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
										</tr>

									</table>

										<table class="table table-responsive table-bordered">
										<tr>
											<th colspan="2"><h4 style="color: #0F0FFF;">Unit B</h4></th>
										</tr>
										<tr>
											<td><h4>History and Bangladesh Studies</h4></td><td><a href="<?php echo base_url('resultpdf/3rd/20_HistoryandBangladeshStudies_result_3rd.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
										</tr>
										<tr>
											<td><h4>Geography and Environment</h4></td><td><a href="<?php echo base_url('resultpdf/3rd/22_GeographyandEnvironment_result_3rd.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
										</tr>

									</table>									
							</div>
							
				<div class="panel-body">
               
                            <div>						
								<h3>2nd Migration List</h3>
										<table class="table table-responsive table-bordered">
										<tr>
											<th width="50%;">2nd Migration </th> <th><a href="<?php echo base_url('2nd_migration 21.01.2020.pdf'); ?>" target="_blank">DOWNLOAD</a></th>  
										</tr>
									</table>
							</div>
                    </div>								
							
					<div class="panel-body">
               
                            <div>						
								<h3 >3rd Waiting List</h3>
										<table class="table table-responsive table-bordered">
										<tr>
											<th width="50%;">Unit A </th> <th><a href="<?php echo base_url('waiting/A_waiting_3rd.pdf'); ?>" target="_blank">DOWNLOAD</a></th>  
										</tr>
									</table>
									
										<table class="table table-responsive table-bordered">
										<tr>
											<th>Unit A ARCH</th> <th><a href="<?php echo base_url('waiting/A_ARCH_waiting_3rd.pdf'); ?>" target="_blank">DOWNLOAD</a></th>  
										</tr>

									</table>

										<table class="table table-responsive table-bordered">
										<tr>
											<th  width="50%;">Unit B Science</th> <th><a href="<?php echo base_url('waiting/B_science_waiting_3rd.pdf'); ?>" target="_blank">DOWNLOAD</a></th>  
										</tr>
										<tr>
											<th  width="50%;">Unit B Humanities</th> <th><a href="<?php echo base_url('waiting/B_humanities_waiting_3rd.pdf'); ?>" target="_blank">DOWNLOAD</a></th>  
										</tr>										
									</table>	
							
							</div>
                    </div>							
							
								<div>						
								<h3 >2nd Result</h3>
										<table class="table table-responsive table-bordered">
										<tr>
											<th colspan="2"><h4 style="color: #0F0FFF;">Unit A</h4></th>
										</tr>
	
										<tr>
											<td><h4>Mathematics</h4></td><td><a href="<?php echo base_url('resultpdf/2nd/03_Mathematics_result_2nd.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
										</tr>
										<tr>
											<td><h4>Physics</h4></td><td><a href="<?php echo base_url('resultpdf/2nd/07_Physics_result_2nd.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
										</tr>
										<tr>
											<td><h4>Geography and Environment</h4></td><td><a href="<?php echo base_url('resultpdf/2nd/09_GeographyandEnvironment_result_2nd.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
										</tr>
										<tr>
											<td><h4>Chemistry</h4></td><td><a href="<?php echo base_url('resultpdf/2nd/14_Chemistry_result_2nd.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
										</tr>
										<tr>
											<td><h4>Urban and Regional Planning</h4></td><td><a href="<?php echo base_url('resultpdf/2nd/17_UrbanandRegionalPlanning_result_2nd.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
										</tr>
										<tr>
											<td><h4>Statistics</h4></td><td><a href="<?php echo base_url('resultpdf/2nd/16_Statistics_result_2nd.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
										</tr>
										
									</table>
									
										<table class="table table-responsive table-bordered">
										<tr>
											<th colspan="2"><h4 style="color: #0F0FFF;">Unit A ARCH</h4></th>
										</tr>
										<tr>
											<td><h4>Architecture</h4></td><td><a href="<?php echo base_url('resultpdf/2nd/12_Architecture_result_2nd.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
										</tr>

									</table>

										<table class="table table-responsive table-bordered">
										<tr>
											<th colspan="2"><h4 style="color: #0F0FFF;">Unit B</h4></th>
										</tr>
										<tr>
											<td><h4>Bangla</h4></td><td><a href="<?php echo base_url('resultpdf/2nd/10_Bangla_result_2nd.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
										</tr>
										<tr>
											<td><h4>History and Bangladesh Studies</h4></td><td><a href="<?php echo base_url('resultpdf/2nd/20_HistoryandBangladeshStudies_result_2nd.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
										</tr>
										<tr>
											<td><h4>Social Work</h4></td><td><a href="<?php echo base_url('resultpdf/2nd/15_SocialWork_result_2nd.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
										</tr>
										<tr>
											<td><h4>Geography and Environment</h4></td><td><a href="<?php echo base_url('resultpdf/2nd/22_GeographyandEnvironment_result_2nd.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
										</tr>

									</table>									
							</div>
							

                            <div>						
								<h3>2nd Quota Result</h3>
									<table class="table table-responsive table-bordered">
										<tr>
											<th colspan="2"><h4 style="color: #0F0FFF;">Unit B</h4></th>
										</tr>	
										<tr>
											<td><h4>Freedom Fighter</h4></td><td><a href="<?php echo base_url('resultpdf/quota/FF_B_result_quota_2nd.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
										</tr>
										
									</table>
								</div>								

				<div class="panel-body">
               
                            <div>						
								<h3>First Migration List</h3>
										<table class="table table-responsive table-bordered">
										<tr>
											<th width="50%;">First Migration </th> <th><a href="<?php echo base_url('First_Migration 16.01.2020.pdf'); ?>" target="_blank">DOWNLOAD</a></th>  
										</tr>
										<tr>
											<th width="50%;">First Migration Quota</th> <th><a href="<?php echo base_url('Quota_First_Migration.pdf'); ?>" target="_blank">DOWNLOAD</a></th>  
										</tr>										
									</table>
							</div>
                    </div>							

							
                    <div class="panel-body">
               
                            <div>						
								<h3 >2nd Waiting List</h3>
										<table class="table table-responsive table-bordered">
										<tr>
											<th width="50%;">Unit A </th> <th><a href="<?php echo base_url('waiting/A_waiting_2nd.pdf'); ?>" target="_blank">DOWNLOAD</a></th>  
										</tr>
									</table>
									
										<table class="table table-responsive table-bordered">
										<tr>
											<th>Unit A ARCH</th> <th><a href="<?php echo base_url('waiting/A_ARCH_waiting_2nd.pdf'); ?>" target="_blank">DOWNLOAD</a></th>  
										</tr>

									</table>

										<table class="table table-responsive table-bordered">
										<tr>
											<th  width="50%;">Unit B Science</th> <th><a href="<?php echo base_url('waiting/B_humanities_waiting_2nd.pdf'); ?>" target="_blank">DOWNLOAD</a></th>  
										</tr>
										<tr>
											<th  width="50%;">Unit B Humanities</th> <th><a href="<?php echo base_url('waiting/B_science_waiting_2nd.pdf'); ?>" target="_blank">DOWNLOAD</a></th>  
										</tr>										
									</table>	

										<h3 >2nd Waiting List Quota: B unit</h3>
										<table class="table table-responsive table-bordered">
										<tr>
											<td><h4>Freedom Fighter</h4></td><td><a href="<?php echo base_url('waiting/quota/B_waiting_first_quota_2nd.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
										</tr>	</table>								
							</div>
                    </div>							
							
                            <div>						
								<h3 >Quota Result</h3>
									<table class="table table-responsive table-bordered">
										<tr>
											<th colspan="2"><h4 style="color: #0F0FFF;">Unit A</h4></th>
										</tr>	
										<tr>
											<td><h4>Freedom Fighter</h4></td><td><a href="<?php echo base_url('resultpdf/quota/FF_A_result_quota.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
										</tr>
										<tr>
											<td><h4>Physically Handicap</h4></td><td><a href="<?php echo base_url('resultpdf/quota/PH_A_result_quota.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
										</tr>
										
										<tr>
											<th colspan="2"><h4 style="color: #0F0FFF;">Unit B</h4></th>
										</tr>	
										<tr>
											<td><h4>Freedom Fighter</h4></td><td><a href="<?php echo base_url('resultpdf/quota/FF_B_result_quota.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
										</tr>
										<tr>
											<td><h4>Physically Handicap</h4></td><td><a href="<?php echo base_url('resultpdf/quota/PH_B_result_quota.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
										</tr>										
										
									</table>
								</div>	

							<div>						
								<h3 >Quota Waiting</h3>
									<table class="table table-responsive table-bordered">
										<tr>
											<th colspan="2"><h4 style="color: #0F0FFF;">Unit A</h4></th>
										</tr>	
										<tr>
											<td><h4>Freedom Fighter</h4></td><td><a href="<?php echo base_url('waiting/quota/A_waiting_first_quota.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
										</tr>
										
										<tr>
											<th colspan="2"><h4 style="color: #0F0FFF;">Unit B</h4></th>
										</tr>	
										<tr>
											<td><h4>Freedom Fighter</h4></td><td><a href="<?php echo base_url('waiting/quota/B_waiting_first_quota.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
										</tr>
										
									</table>
								</div>								
                            <div>						
								<h3 >First Result</h3>
										<table class="table table-responsive table-bordered">
										<tr>
											<th colspan="2"><h4 style="color: #0F0FFF;">Unit A</h4></th>
										</tr>
										<?php 
										if($a_res){
										 foreach ($a_res as $key => $value) {
											  $sub_name_pdf = str_replace(" ","",$value['sub_name']);
										?>		
										<tr>
											<td><h4><?php echo $value['sub_name']; ?></h4></td><td><a href="<?php echo base_url('resultpdf/1st/'.$value['sub_code']."_".$sub_name_pdf.'_result.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
										</tr>
										<?php } } ?>
									</table>
									
										<table class="table table-responsive table-bordered">
										<tr>
											<th colspan="2"><h4 style="color: #0F0FFF;">Unit A ARCH</h4></th>
										</tr>
										<?php 
										if($a_arch_res){
										 foreach ($a_arch_res as $key => $value) {
											  $sub_name_pdf_arc = str_replace(" ","",$value['sub_name']);
										?>		
										<tr>
											<td><h4><?php echo $value['sub_name']; ?></h4></td><td><a href="<?php echo base_url('resultpdf/1st/'.$value['sub_code']."_".$sub_name_pdf_arc.'_result.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
										</tr>
										<?php } } ?>
									</table>

										<table class="table table-responsive table-bordered">
										<tr>
											<th colspan="2"><h4 style="color: #0F0FFF;">Unit B</h4></th>
										</tr>
										<?php 
										if($b_res){
										 foreach ($b_res as $key => $value) {
											  $sub_name_pdf_b = str_replace(" ","",$value['sub_name']);
										?>		
										<tr>
											<td><h4><?php echo $value['sub_name']; ?></h4></td><td><a href="<?php echo base_url('resultpdf/1st/'.$value['sub_code']."_".$sub_name_pdf_b.'_result.pdf'); ?>" target="_blank">DOWNLOAD</a></td>
										</tr>
										<?php } } ?>
									</table>									
							</div>
                    </div>
					
                    <div class="panel-body">
               
                            <div>						
								<h3 >First Waiting List</h3>
										<table class="table table-responsive table-bordered">
										<tr>
											<th width="50%;">Unit A </th> <th><a href="<?php echo base_url('waiting/A_waiting_first.pdf'); ?>" target="_blank">DOWNLOAD</a></th>  
										</tr>
									</table>
									
										<table class="table table-responsive table-bordered">
										<tr>
											<th>Unit A ARCH</th> <th><a href="<?php echo base_url('waiting/A_ARCH_waiting_first.pdf'); ?>" target="_blank">DOWNLOAD</a></th>  
										</tr>

									</table>

										<table class="table table-responsive table-bordered">
										<tr>
											<th  width="50%;">Unit B Science</th> <th><a href="<?php echo base_url('waiting/B_humanities_waiting_first.pdf'); ?>" target="_blank">DOWNLOAD</a></th>  
										</tr>
										<tr>
											<th  width="50%;">Unit B Humanities</th> <th><a href="<?php echo base_url('waiting/B_science_waiting_first.pdf'); ?>" target="_blank">DOWNLOAD</a></th>  
										</tr>										
									</table>									
							</div>
                    </div>					
					
                </div>
            </div>
        </div>		
            <div class="col-md-4">

 <div class="panel panel-primary">


                <div class="panel panel-primary">
                    <div class="panel-heading">Admission 2019-2020: Notice</div>

                    <div class="panel-body">
                        <div class="row">
                            <div style="padding-left: 20px;">		
							<h4 style="color:red;">7th Admission Notice  : <a targer="_blank" href="<?php echo base_url(); ?>migration_files/7th_Admission_Notice_26.02.2020.pdf">Download</a></h4>
							<hr>																					
							<h4 style="color:red;">6th Admission Notice  : <a targer="_blank" href="<?php echo base_url(); ?>migration_files/6th_Admission Notice_23.02.2020_24.02.2020.pdf">Download</a></h4>
							<hr>														
							<h4 style="color:red;">5th Admission Notice  : <a targer="_blank" href="<?php echo base_url(); ?>migration_files/5th_admission_notice_16.02.2020_17.02.2020.pdf">Download</a></h4>
							<hr>							
							<h4 style="color:red;">Waiting List Notice  : <a targer="_blank" href="<?php echo base_url(); ?>migration_files/Noticefor4th_waitinglist.pdf">Download</a></h4>
							<hr>
								<h4 style="color:red;">4th Admission Notice  : <a targer="_blank" href="<?php echo base_url(); ?>migration_files/4th_Admissionnotice_02.02.2020_03.02.2020.pdf">Download</a></h4>
								<h4 style="color:red;">3rd Admission Notice  : <a targer="_blank" href="<?php echo base_url(); ?>migration_files/3rdadmissionnotice.pdf">Download</a></h4>
								<h4 style="color:red;">2nd Admission Notice  : <a targer="_blank" href="<?php echo base_url(); ?>migration_files/2nd_Admission_Notice_2019-20.pdf">Download</a></h4>
								<h4 style="color:red;">First Admission Notice  : <a targer="_blank" href="<?php echo base_url(); ?>migration_files/1stAdmissionNotice.pdf">Download</a></h4>
								<!--<h4>Admission Notice  : <a targer="_blank" href="<?php echo base_url(); ?>migration_files/Admissionnotice_181219.pdf">Download</a></h4>-->
								<h4>Interview schedule of Quota for Admission 2019-2020 : <a targer="_blank" href="<?php echo base_url(); ?>migration_files/Interview schedule of Quota for Admission 2019-2020.pdf">Download</a></h4>
								<hr>
								<h4>Interview/Viva Schedule A : <a targer="_blank" href="<?php echo base_url(); ?>migration_files/A_Unit_Interview_Schedule.pdf">Download</a></h4>
								<h4>Interview/Viva Schedule B : <a targer="_blank" href="<?php echo base_url(); ?>migration_files/B_Unit_Interview_Schedule.pdf">Download</a></h4>
								<br>
								<b>	Notice of fill-up the choice form : <a targer="_blank" href="<?php echo base_url(); ?>migration_files/Notice of fillup the choice form.pdf">Download</a></b>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('common/footer'); ?>
