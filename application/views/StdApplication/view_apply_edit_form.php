<?php $this->load->view('common/header'); ?>
<?php $this->load->view('common/navbar'); ?>
<style>
    .unit_radio_style {
        padding-bottom: 10px;
    }

    .my_error {
        color: red;
    }
</style>

<div id="main">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-primary">
                    <div class="panel-heading"><?= $page_title ?></div>
                    <div class="panel-body">
                        <?php $this->load->view('StdApplication/_edit_application_form'); ?>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">Student Information</div>
                    <div class="panel-body">
                        <?php $this->load->view('StdApplication/_student_info'); ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('common/footer'); ?>