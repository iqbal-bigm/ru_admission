<?php if (isset($student_info) || !empty($student_info)) : ?>

    <table class="table table-striped table-bordered">
        <tbody>
            <tr>
                <td>Admission Unit</td>
                <td><?= $student_info->apply_unit ?></td>
            </tr>
            <?php if (!empty($student_info->total)) : ?>
                <tr>
                    <td>GST Score</td>
                    <td><?= $student_info->total ?></td>
                </tr>
            <?php endif; ?>
            <?php if (!empty($student_info->gst_score)) : ?>
                <tr>
                    <td>GST Score</td>
                    <td><?= $student_info->gst_score ?></td>
                </tr>
            <?php endif; ?>
            <?php if (!empty($student_info->mobnumber)) : ?>
                <tr>
                    <td>Mobile No.</td>
                    <td><?= $student_info->mobnumber ?></td>
                </tr>
            <?php endif; ?>

            <tr>
                <td>Name</td>
                <td><?= $student_info->name ?></td>
            </tr>
            <tr>
                <td>Father Name</td>
                <td><?= $student_info->fname ?></td>
            </tr>
            <tr>
                <td>Mother Name</td>
                <td><?= $student_info->mname ?></td>
            </tr>
            <!-- <tr>
                <td>Date Of Birth</td>
                <td><?= $student_info->dob ?></td>
            </tr> -->
            <tr>
                <td>HSC Board</td>
                <td><?= strtoupper($student_info->hsc_board) ?></td>
            </tr>
            <tr>
                <td>HSC GPA</td>
                <td><?= $student_info->hsc_gpa ?></td>
            </tr>
            <tr>
                <td>HSC Study Group</td>
                <td><?= $student_info->hsc_study_group ?></td>
            </tr>
            <tr>
                <td>SSC Board</td>
                <td><?= strtoupper($student_info->ssc_board) ?></td>
            </tr>
            <tr>
                <td>SSC GPA</td>
                <td><?= $student_info->ssc_gpa ?></td>
            </tr>
            <tr>
                <td>SSC Study Group</td>
                <td><?= $student_info->ssc_study_group ?></td>
            </tr>

        </tbody>
    </table>
<?php else : ?>
    <?php $this->load->view('content/_not_found'); ?>
<?php endif; ?>