<?php $this->load->view('common/header'); ?>
<?php $this->load->view('common/navbar'); ?>
<style>
    .unit_radio_style {
        padding-bottom: 10px;
    }

    .my_error {
        color: red;
    }
</style>

<div id="main">
    <div class="container">
        <?php if ($apply_status) : ?>
            <div class="row">
                <div class="col-md-12">
                    <p class="bg-success" style="padding: 15px;font-size:18px;">You have already applied.</p>
                </div>
            </div>
        <?php else : ?>

            <div class="row">
                <div class="col-md-4">
                    <div class="panel panel-primary">
                        <div class="panel-heading">Student Information</div>
                        <div class="panel-body">
                            <?php $this->load->view('StdApplication/_student_info'); ?>

                        </div>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="panel panel-primary">
                        <div class="panel-heading"><?= $page_title ?></div>
                        <div class="panel-body">
                            <?php $this->load->view('StdApplication/_application_form'); ?>
                        </div>
                    </div>
                </div>

            </div>
        <?php endif; ?>
    </div>
</div>

<?php $this->load->view('common/footer'); ?>