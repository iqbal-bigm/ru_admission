<?= form_open('stdApplication/editApplicationForm', ['method' => 'POST']) ?>
<?= form_hidden('id', $student_info->id) ?>
<?= form_hidden('appid', $student_info->appid) ?>
<?= form_hidden('regisid', $student_info->regisid) ?>
<table class="table table-striped table-bordered">
    <!-- <thead class="thead-dark">
                                <tr>
                                    <th>Title</th>
                                    <th>Value</th>
                                </tr>
                            </thead> -->
    <tbody>
        <tr>
            <td>Unit</td>
            <td><?= $student_info->unit ?></td>
        </tr>
        <tr>
            <td>GST Application ID</td>
            <td><?= $student_info->appid ?></td>
        </tr>
        <?php if ($with_architecture_status) : ?>
            <tr>
                <td>Option (Music Subject) <br> <strong>বিঃদ্রঃ - সঙ্গীতে আবেদন করতে চাইলে এখানে সিলেক্ট করুন।</strong></td>
                <td><?= form_dropdown('with_architecture', ['NO' => 'Without Music Subject', 'YES' => 'With Music Subject'], $student_info->with_architecture, ['id' => 'option', 'class' => 'form-control', 'required' => 'required', 'onchange' => 'cal_payment_amount(this.value)']) ?></td>
            </tr>
        <?php endif; ?>
        <tr>
            <td>Select Quota</td>
            <td><?= form_dropdown('quota', get_quota_array(), $student_info->qname, ['class' => 'form-control']) ?></td>
        </tr>
        <tr>
            <td>Amount to Pay</td>
            <td><span data-payment_amount_a="<?= $payment_amount['A']  ?>" data-payment_amount_with_archt="<?= $payment_amount['with_archt']  ?>" id="payment_amount"><?= $student_info->amount_to_pay ?></span></td>
        </tr>
    </tbody>
</table>
<?= form_hidden('submit', 'apply') ?>
<button type="submit" class="btn btn-primary pull-right">Update</button>
<?= form_close() ?>
<script>
    function cal_payment_amount(with_archt) {
        var payment_amount_a = $("#payment_amount").data("payment_amount_a")
        var payment_amount_with_archt = $("#payment_amount").data("payment_amount_with_archt")
        if (with_archt == 'YES') {
            $('#payment_amount').text(payment_amount_with_archt)
        } else {
            $('#payment_amount').text(payment_amount_a)
        }
    }
</script>