<?php $this->load->view('common/header'); ?>
<?php $this->load->view('common/navbar'); ?>
<style>
    #nextstep .error {
    color: red;
        font-style: italic;
}
</style>  
<div id="main">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">Find Application ID</div>

                   <?php if(!empty($this->uri->segment(3)) && ( $this->uri->segment(3) == "noapp") || $this->uri->segment(3) == "notfound"  ){ ?>
                    <div class="panel-body">
                        <div class=" has-error">
                            <span class="pd15 bg-danger help-block">
								Invalid Information. No Application Found.
							</span>
                        </div>
                    </div>
				   <?php } ?>
                    <div class="panel-body">                       
                        <form action="<?php echo base_url('recover/find'); ?>" method="post" name="nextstep" id="nextstep">
                        <?php $this->form_validation->set_error_delimiters('<span class="help-block">', '</span>'); ?>
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table table-responsive table-bordered">
                                    <tr>
                                        <th colspan="2"><h4>H.S.C/Equivalent Information</h4></th>
                                    </tr>
                                    <tr>
                                        <th>Board/Institute</th>
                                        <td>
                                           <div class="form-group <?php if(form_error('hsc_board')) echo "has-error"; ?>">
                                                <?php
                                                    $id_hsc_board = 'class="form-control" id="hsc_board" required';
                                                    echo form_dropdown('hsc_board',   hsc_board(), set_value('hsc_board', $hsc_board= ''), $id_hsc_board);
                                                    if(form_error('hsc_board')) echo form_label(form_error('hsc_board'));
                                                ?>
                                            </div> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Passing Year</th>
                                        <td>
                                            <div class="form-group <?php if(form_error('hsc_passing_year')) echo "has-error"; ?>">
                                                <?php
                                                   $id_hsc_passing_year = 'class="form-control" id="hsc_passing_year" required';
                                                   echo form_dropdown('hsc_passing_year', hsc_passing_year(), set_value('hsc_passing_year', $hsc_passing_year=''), $id_hsc_passing_year);
                                                    if(form_error('hsc_passing_year')) echo form_label(form_error('hsc_passing_year'));
                                                ?>
                                            </div>
                                        </td>
                                    </tr>                                   
                                    <tr>
                                        <th>Roll No.</th>
                                        <td>
                                            <div class="form-group <?php if(form_error('hsc_roll')) echo "has-error"; ?>">
                                                <?php
                                                $form_input = array(
                                                    'name'  => 'hsc_roll',
                                                    'id'  => 'hsc_roll',
                                                    'value' => set_value('hsc_roll', $hsc_roll=''),
                                                    'autocomplete' => 'off',
                                                    'class'  => 'form-control',
                                                    'placeholder'  => 'HSC Roll No',
                                                    'required'=>'required'
                                                );
                                                echo form_input($form_input);
                                                if(form_error('hsc_roll')) echo form_label(form_error('hsc_roll'));
                                                ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Reg. No.</th>
                                        <td>
                                            <div class="form-group <?php if(form_error('hsc_registration')) echo "has-error"; ?>">
                                                <?php
                                                $form_input = array(
                                                    'name'  => 'hsc_registration',
                                                    'id'  => 'hsc_registration',
                                                    'value' => set_value('hsc_registration', $hsc_registration=''),
                                                    'autocomplete' => 'off',
                                                    'class'  => 'form-control',
                                                    'placeholder'  => 'HSC Registraion No',
                                                    'required'=>'required'
                                                );
                                                echo form_input($form_input);
                                                if(form_error('hsc_registration')) echo form_label(form_error('hsc_registration'));
                                                ?>
												
                                            </div>
                                        </td>
                                    </tr>

                                </table>
                            </div>
                        </div>
                        
                                           
                        <?= form_submit('submit', 'SUBMIT', ['class' => 'btn btn-primary pull-right']); ?>
                        <?= form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?= base_url('assets/js/jquery.validate.js'); ?>"></script>
<script> 
  $("#nextstep").validate();
</script>   
<?php $this->load->view('common/footer'); ?>
