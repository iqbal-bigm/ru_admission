<?php $this->load->view('common/header'); ?>
<?php //$this->load->view('admin/view_admin_navbar'); 
?>

<div id="main">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">Application Complain</div>

                    <div class="panel-body">
                        <div class="row">

                            <a href="<?php echo  base_url('admin/admin_complain/solvelist'); ?>">Solved List</a> | <a href="<?php echo  base_url('admin/admin_complain/pendinglist'); ?>">Pending List</a>
                            <table class="table table-responsive table-bordered">


                                <tr class="">
                                    <th> ID </th>
                                    <th> Name </th>
                                    <th> Mobile No and APP ID </th>
                                    <th> Student Exam Details </th>
                                    <th> Details </th>
                                    <th> Date </th>
                                    <th> IP </th>
                                    <th> Status </th>
                                </tr>
                                <?php
                                if (!empty($alldata)) {
                                    foreach ($alldata as $key => $value) {
                                ?>
                                        <tr class="">
                                            <td>
                                                <?php echo $value['id']; ?>
                                            </td>
                                            <td>
                                                <?php echo $value['name']; ?>
                                            </td>
                                            <td>
                                                Mobile No:
                                                <?php echo $value['phone']; ?>
                                                <br>
                                                GST App ID:
                                                <?php echo $value['gst_app_id']; ?>
                                            </td>
                                            <td>
                                                <?php if (!empty($value['appid'])) : ?>
                                                    <table class="table table-responsive table-bordered">
                                                        <tr>
                                                            <td colspan="2">S.S.C/Equivalent Details</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Board/Institute</td>
                                                            <td><?php echo $value['ssc_board']; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Passing Year</td>
                                                            <td><?php echo $value['ssc_year']; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Roll No.</td>
                                                            <td><?php echo $value['ssc_roll']; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Reg. No.</td>
                                                            <td><?php echo $value['ssc_reg']; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">H.S.C/Equivalent Details</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Board/Institute</td>
                                                            <td><?php echo $value['hsc_board']; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Passing Year</td>
                                                            <td><?php echo $value['hsc_year']; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Roll No.</td>
                                                            <td><?php echo $value['hsc_roll']; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Reg. No.</td>
                                                            <td><?php echo $value['hsc_reg']; ?></td>
                                                        </tr>
                                                    </table>
                                                <?php else : ?>
                                                    Applicant dose not registration yet.
                                                <?php endif; ?>
                                            </td>
                                            <td>
                                                <?php echo $value['details']; ?>
                                            </td>
                                            <td>
                                                <?php echo $value['date_time']; ?>
                                            </td>
                                            <td>
                                                <?php echo $value['ip']; ?>
                                            </td>
                                            <td>
                                                <?php echo $value['status']; ?>
                                            </td>
                                            <td>
                                                <?php if ($value['status'] == "pending") { ?>
                                                    <a class="btn btn-success" href="<?php echo base_url('admin/admin_complain/solve/' . $value['id']); ?>">Solved</a>
                                                <?php } ?>
                                            </td>
                                        </tr>
                                <?php
                                    }
                                }
                                ?>
                            </table>

                        </div>
                        <hr>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('common/footer'); ?>