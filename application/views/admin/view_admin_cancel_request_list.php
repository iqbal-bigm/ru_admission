<?php $this->load->view('common/header'); ?>
<?php $this->load->view('admin/view_admin_navbar'); ?>
<?php
                              $arr_all_subject_code_name = get_all_subject_code_name();
                              $arr_all_subject_faculty_name = get_all_subject_faculty_name();
                              $arr_all_subject_dep_name = get_all_subject_dep_name();
?>
<div id="main">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">Admission Cancel Request List</div>

                    <div class="panel-body">                        
                        <div class="row">

                          
                                <table class="table table-responsive table-bordered">

                               
                        <tr class="">
                            <th>SL.</th>
                            <th>
                              Tracking ID <br> Admissions Roll
                            </th>                            
                            <th>
                              Student Details
                            </th>
    
                             <th>
                               Subject & DEPT.
                            </th> 
                             <th>
                               Merit
                            </th>                              
                            <th>
                             Payment Details
                            </th>                               
                            <th>
                              Request Date
                            </th>             
                            <th>
                             Status
                            </th>         
                            <th>
                             Action
                            </th>                               
                        </tr>                         
                        <?php
                        if(!empty($cancel_list)){
                            $i=1;
                        foreach ($cancel_list as $key => $value) {
                            ?>
                        <tr class="">
                            <td>
                               <?php echo $i; ?> 
                            </td>                            
                            <td>
                               <?php echo $value['tracking_id']; ?> 
                                <br>
                                 <?php echo "Roll: ".$value['exam_roll_ref']; ?> 
                            </td>                            
                            <td>
                               <?php
                               $get_studen_personal_data = get_studen_personal_data($value['system_reg_id_ref']); 
                               echo "Name: ".$get_studen_personal_data['SNAME'];
                               echo "<br>";
                               echo "Father's Name: ".$get_studen_personal_data['SFNAME'];
                               echo "<br>";
                               echo "Mother's Name: ".$get_studen_personal_data['SMNAME'];
                               echo "<br>";
							    echo "<br>";
                               echo "Apply Date: ".$value['cancel_apply_date'];
                               echo "<br>";
                               echo "Approve Date: ".$value['cancel_register_approv_date'];							   
                               ?> 
                            </td>
                                
   
                            <td>
                                <?php
                                echo $arr_all_subject_code_name[$value['sub_code_1']];
                                echo "<br>";
                                echo $arr_all_subject_faculty_name[$value['sub_code_1']];
                                echo "<br>";
                                echo $arr_all_subject_dep_name[$value['sub_code_1']];
                                ?> 
                            </td>   
                            <td>
                              <?php echo $value['merit_pos_ref']; ?> 
                            </td>       
                            <td>
                                            <?php
                                            if($value['cancel_paid_status'] == "P"){
                                                echo "Status: PAID";
                                                echo "<br>";
                                                echo "Amount: ".$value['cancel_paid_amount'];
                                                echo "<br>";
                                                echo "TRANS. ID: ".$value['cancel_payment_trans_id'];    
                                                echo "<br>";
                                                echo "Date: ".$value['cancel_payment_date'];                                                  
                                            }
                                            ?>
                            </td>    
                            <td>
                              <?php echo $value['cancel_apply_date']; ?> 
                            </td>                    
 <td>
                              <?php echo $value['cancel_status']; ?> 
                            </td>                             
                            <td>
                                 <?php if( $value['cancel_paid_status'] == "P"){
                                       if($value['cancel_register_approv_status'] == "approved"){
                                     ?> 
                                            APPROVED                                    
                                       <?php }else{
                                           ?>
                                <a class="btn btn-success" href="<?php  echo base_url('admin/admin_home/approve_cancel_request/'.$value['unique_id_ref']); ?>">APPROVE</a>
                                       <?php } ?>
<!--                                <br>  <br>
                                <a class="btn btn-success" href="<?php  //echo base_url('admin/admin_home/approve_cancel_request/'.$value['id']); ?>">DETAILS</a>-->
                                <?php
                                if($value['cancel_register_approv_status'] == "approved"){
                                ?>
                                <br>  <br>
                                <a class="btn btn-success" href="<?php  echo base_url('admin/admin_home/download_cancel_confirm_pdf/'.$value['unique_id_ref'].'/'.$value['sub_code_1']); ?>">DOWNLOAD CANCEL LETTER</a>                                
                                <?php
                                }
                                ?>
                                <?php } ?>
                            </td>                              
                        </tr>                        
                        <?php
                        $i++;
                        }
                        }
                        ?>                           
                                </table>    
                         
                        </div>
                        <hr>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('common/footer'); ?>
