<?php $this->load->view('common/header'); ?>
<?php $this->load->view('admin/view_admin_navbar'); ?>

<div id="main">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">Application Report</div>

                    <div class="panel-body">
                        <div class="row">


                            <table class="table table-responsive table-bordered">

                                <tr class="">
                                    <th> UNIT </th>
                                    <th> Total Application </th>
                                    <th> Total Paid </th>
                                </tr>

                                <?php
                                $get_all_subject_arr_list = array(
                                    "A" => 'A',
                                    //  "A2" => 'A2',
                                    "B" => 'B',
                                    "C" => 'C'
                                );

                                foreach ($get_all_subject_arr_list as $key => $value) {
                                ?>
                                    <tr class="">
                                        <td style="width: 30%;">
                                            UNIT <?php echo $value; ?> :
                                        </td>
                                        <td style="width: 30%;">
                                            <?php echo count_applicaton_by_unit($value); ?>
                                        </td>
                                        <td style="width: 30%;">
                                            <?php echo count_paid_applicaton_by_unit($value); ?>
                                        </td>
                                    </tr>
                                <?php
                                }
                                ?>
                                <tr class="success">
                                    <td>
                                        Total :
                                    </td>
                                    <td>
                                        <?php echo count_total_applicant(); ?>
                                    </td>
                                    <td>
                                        <?php echo get_paid_count(); ?>
                                    </td>
                                </tr>

                                <tr class="success">
                                    <!-- <td colspan="2">
                                 PAID BY Rocket : <?php //echo get_paid_count_agent('rocket'); 
                                                    ?>
                            </td>
                            <td>
                                 PAID BY sure Cash : <?php //echo get_paid_count_agent('bkash'); 
                                                        ?>
                            </td>
                        </tr>	  -->
                            </table>
                            
                            
                             <table class="table table-responsive table-bordered">

                                <tr class="">
                                    <th> UNIT </th>
                                    <th> Total Application With Music </th>
                                    <th> Total Paid With Music</th>
                                </tr>

                                <?php
                                $get_all_subject_arr_list = array(
                                    "A" => 'A',
                                    //  "A2" => 'A2',
                                    "B" => 'B',
                                    "C" => 'C'
                                );

                                foreach ($get_all_subject_arr_list as $key => $value) {
                                ?>
                                    <tr class="">
                                        <td style="width: 30%;">
                                            UNIT <?php echo $value; ?> :
                                        </td>
                                        <td style="width: 30%;">
                                            <?php echo count_applicaton_by_unit_with_music($value); ?>
                                        </td>
                                        <td style="width: 30%;">
                                            <?php echo count_paid_applicaton_by_unit_music($value); ?>
                                        </td>
                                    </tr>
                                <?php
                                }
                                ?>

                            </table>                           
                            

                        </div>
                        <hr>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('common/footer'); ?>