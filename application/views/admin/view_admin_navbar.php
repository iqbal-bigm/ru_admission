<header>

    <div class="container text-center">
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="header">
                    <h2><img src="<?= base_url('assets/images/logo.png'); ?>" class="logo" alt="<?php echo UNIVERSITY_NAME; ?>"><?php echo UNIVERSITY_NAME; ?></h2>

                </div>
            </div>
        </div>
    </div>
</header>

<nav class="navbar ">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="navbar-nav">

<!--                <li class="active"><a href="<?= site_url('admin/admin_home'); ?>">Dashboard | </a></li>-->
                <li class="active"><a href="<?= site_url('admin/admin_report'); ?>">Application Report | </a></li>
                <!--                <li><a href="<?= site_url('admin/admin_home'); ?>">Result | </a></li>									-->
                <!--                <li><a href="<?= site_url('admin/admin_home/cancel_request_list'); ?>">Admission Cancel Request | </a></li>	-->
                <!--<li><a href="<?= site_url('admin/admin_complain'); ?>">Complain Report | </a></li>   -->
                <li><a href="<?= site_url('admin/admin_login/logout'); ?>">Logout</a></li>
            </ul>

        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>