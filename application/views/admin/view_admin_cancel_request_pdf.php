<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>PUST</title>
    <style>
        table th {
            text-align: left;
        }

        .tbl_merit th {
            background-color: #EEEEEE;
        }
    </style>
</head>

<body>

    <table border="0" style="width: 100%;border:0px solid #cacaca;border-collapse: collapse;">
        <thead>
            <tr>
                <td style="width: 25%">
                    <img style="width: 60px; height: auto" alt="<?php echo UNIVERSITY_NAME; ?>" src="<?php echo base_url(); ?>assets/images/logo.png" />
                </td>
                <td style="width: 75%;">
                    <p>
                        Office of the Registrar
                        <br />
                    <h4>Academic and Scholarship Section</h4>
                    Sheikh Hasina University
                    </p>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="width: 100%;">
                    <hr>
                </td>
            </tr>
            <tr>
                <td style="width: 25%; padding-top: 10px;">
                    Ref: pust/Academic/
                </td>
                <td style="width: 75%;padding-top: 10px;text-align: right;">
                    Date: <?php echo date('d-m-Y'); ?>
                </td>
            </tr>
        </thead>
    </table>
    <br>

    <?php
    $arr_all_subject_code_name = get_all_subject_code_name();
    $arr_all_subject_faculty_name = get_all_subject_faculty_name();
    $arr_all_subject_dep_name = get_all_subject_dep_name();
    ?>

    <table border="1" class="tbl_merit" width="100%" cellpadding="5" style="border:1px solid #cacaca;border-collapse: collapse;font-size: 11px;">
        </tr>
        <tr>
            <th colspan="1">Name</th>
            <td colspan="3"><?php echo $student_acadamic_data['SNAME']; ?></td>
            <td rowspan="7">
                <img style="width: 130px; height: auto" alt="Photo" src="<?php echo "http://admission1920.pust.ac.bd/"; //base_url(); 
                                                                            ?>uploads/<?php echo $student_login_photo_path; ?>/<?php echo $result_details['system_reg_id_ref']; ?>.jpg" />
            </td>
        </tr>

        <tr>
            <th colspan="1">Father's Name</th>
            <td colspan="3"><?php echo $student_acadamic_data['SFNAME']; ?></td>
        </tr>
        <tr>
            <th colspan="1">Subject</th>
            <td colspan="3"><?php echo $arr_all_subject_code_name[$sub_code]; ?></td>
        </tr>

        <tr>
            <th>Admission Roll</th>
            <td colspan="3"><?php echo $result_details['exam_roll_ref']; ?></td>

        </tr>

        <tr>
            <th>Merit Position</th>
            <td colspan="3"><?php echo $result_details['merit_pos_ref']; ?></td>
        </tr>
        <tr>
            <th colspan="1">Address</th>
            <td colspan="3">
                <?php
                echo "Village/Ward: " . $std_per_data['padd_village'] . ", House: " . $std_per_data['padd_house'] . ", ";
                echo "<br>";
                echo "Road: " . $std_per_data['padd_road'] . ", Post Office: " . $std_per_data['padd_post_office'] . ", ";
                echo "<br>";
                echo "Upazila: " . $std_per_data['padd_police_station'] . ", District: " . $std_per_data['padd_district'];
                ?>
            </td>
        </tr>
        <tr>
            <th colspan="1">Mobile No.</th>
            <td colspan="3"><?php echo $std_per_data['mobile_no']; ?></td>
        </tr>

    </table>
    <br>
    <table class="tbl_merit" border="0" width="100%" cellpadding="1" style="border:0px solid #cacaca;border-collapse: collapse;font-size: 12px;">
        <tr>
            <td style="padding: 15px;">

                <b> Subject: Regarding return of original papers due to admission cancellation.</b>

            </td>
        </tr>
        <tr>
            <td style="padding: 15px;">
                It is to be informed you that, in the basis of your application and recommendation of the Chairman and Hall Provost your admission have been cancelled from this university. Now you can collect your HSC and SSC original certificates and transcripts from the respective department office.
            </td>
        </tr>
        <tr>
            <td style="padding: 15px;">
                Additional Registrar <br>
                Academic Section <br>
                Sheikh Hasina University <br>
                Pabna-6600
            </td>
        </tr>
    </table>

    <br>
    <table class="tbl_merit" border="0" width="100%" cellpadding="1" style="border:0px solid #cacaca;border-collapse: collapse;font-size: 12px;">
        <tr>
            <td style="padding: 15px;">
                Ref: pust/Academic/
            </td>
            <td style="padding: 5px;text-align: right;">
                Date: <?php echo date('d-m-Y'); ?>
            </td>

        </tr>
    </table>

    <table border="0" class="tbl_merit" width="100%" cellpadding="5" style="border:0px solid #cacaca;border-collapse: collapse;font-size: 11px;">
        <tr>
            <td style="padding: 15px;">
                <u>Copy forwarded to take necessary action</u> <br>
                1. Honorable Treasurer, Sheikh Hasina University. <br>
                2. Dean, Faculty of <?php echo $arr_all_subject_faculty_name[$sub_code] ?>, PUST. <br>
                3. Chairman, <?php echo $arr_all_subject_dep_name[$sub_code] ?>, PUST. <br>
                <b>(Request you to give the original papers to the corresponding student.)</b> <br>
                4. Additional Controller of Examination, PUST <br>
                5. Office copy.

            </td>
        </tr>
        <tr>
            <td style="padding: 15px;">
                Additional Registrar <br>
                Academic Section <br>
                Sheikh Hasina University <br>
                Pabna-6600
            </td>
        </tr>
    </table>
    <br>
    <br>
    <table width="100%" style="border: 1px solid black;">
        <tr>
            <td colspan="2" style="text-align: left;font-size: 9px;text-align: center;">
                © 2019-2020; <?php echo UNIVERSITY_NAME; ?>. All rights reserved.
            </td>
        </tr>
    </table>

</body>

</html>