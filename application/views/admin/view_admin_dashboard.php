<?php $this->load->view('common/header'); ?>
<?php $this->load->view('admin/view_admin_navbar'); ?>

<div id="main">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">Admin Dashboard</div>

                    <div class="panel-body">


                        <?php /*    <tr>
                                      <th><h3>Waiting Choice </h3></th>
                                        <td><?php echo get_total_watingchoice(); ?></td>
                                    </tr>		
									 
							
					  
					
					   <div class="row">

                           <table class="table table-responsive table-bordered">
                                   <tr>
                                        <th><h3>Total Paid  6th merit </h3></th>
                                        <td><?php echo get_total_paid_6th_merit(); ?></td>
                                   </tr>						   
                                   <tr>
                                        <th><h3>Total Paid  5th merit </h3></th>
                                        <td><?php echo get_total_paid_5th_merit(); ?></td>
                                   </tr>						   
                                   <tr>
                                        <th><h3>Total Paid  4th merit </h3></th>
                                        <td><?php echo get_total_paid_4th_merit(); ?></td>
                                   </tr>						   
                                   <tr>
                                        <th><h3>Total Paid  3rd merit </h3></th>
                                        <td><?php echo get_total_paid_3rd_merit(); ?></td>
                                   </tr>
                                   <tr>
                                        <th><h3>Total Paid  2nd merit </h3></th>
                                        <td><?php echo get_total_paid_2nd_merit(); ?></td>
                                   </tr>
									
                                   <tr>
                                        <th><h3>Total Paid  </h3></th>
                                        <td><?php echo get_total_paid(); ?></td>
                                    </tr>

                               <tr><th colspan="2">Total Paid Unit Wise</th>

                                    </tr>  
                                <?php
                                             $get_all_subject_arr_list = array(
                                                "A" => 'A',
                                                "A_ARCH" => 'A_ARCH',
                                                "B" => 'B'
                                            );

                                            foreach ($get_all_subject_arr_list as $key => $value) {
										?>
												<tr>
													<th style="width: 60%;"><?php echo $value; ?> </th>
													<td><?php echo paid_count_unit($value); ?></td>
															</tr>
												<?php
														}
												?>

                                    <tr style="">
                                        <th style="width: 60%;">B  Unit Group Wise</th>
                                        <td>
										Business: <?php echo paid_count_unit_b_unit('business'); ?>
										<br>
										Humanities: <?php echo paid_count_unit_b_unit('humanities'); ?>
										<br>
										Science: <?php echo paid_count_unit_b_unit('science'); ?>
										</td>
                                    </tr>

                                    <tr style="">
                                        <th style="width: 60%;">Total Cancel : </th>
                                        <td><?php echo admission_cancel(); ?></td>
                                    </tr>
                         </table>


                            <?php
                                $sub_code_arr = array(
                                    "01" => "A, Computer Science and Engineering",
                                    "17" => "A, Urban and Regional Planning",
                                    "11" => "A, Civil Engineering",
                                    "06" => "A, Information and Communication Engineering",
                                    "05" => "A, Electronic and Telecommunication Engineering",
                                    "02" => "A, Electrical and Electronic Engineering",
                                    "12" => "A_ARCH, Architecture",
                                    "09" => "A, Geography and Environment",
                                    "16" => "A, Statistics",
                                    "14" => "A, Chemistry",
                                    "13" => "A, Pharmacy",
                                    "07" => "A, Physics",
                                    "03" => "A, Mathematics",
                                    "21" => "B, Tourism and Hospitality Management",
                                    "19" => "B, Public Administration",
                                    "18" => "B, English",
                                    "15" => "B, Social Work",
                                    "10" => "B, Bengali",
                                    "08" => "B, Economics",
                                    "04" => "B, Business Administration",
                                    "20" => "B, History and Bangladesh Studies",
									"22" => "B, Geography and Environment"
                                );
                            ?>
                           <table class="table table-responsive table-bordered">
                            <tr>
                                        <th><b>SL.</b></th>
                                        <th style="width: 40%;"><b>Unit,Subject Name,CODE</b></th>
                                        <th><b>Total Paid/Admission DONE</b></th> <!-- ( After Cancel) -->
										<th><b>Total Paid/Admission DONE, without Quota</h4></th> <!-- ( After Cancel) -->
                                        <th><b>Quota ,Total Paid/Admission DONE</b></th> <!-- ( After Cancel) -->
                                    </tr>
                                <?php
                                $indexr = 1;
                                foreach ($sub_code_arr as $key => $value) {
                                    ?>
                               <tr>
                                        <td><?php echo $indexr; ?></td>
                                        <td style="width: 40%;"><?php echo $value." , ".$key; ?> : </td>
                                        <td><?php echo get_total_subwise_paid($key); ?></td>
					<td><?php echo get_total_subwise_paid_no_quota($key); ?></td>
                                         <td><?php echo get_total_subwise_paid_quota($key); ?></td>
                                    </tr>
                               <?php
                               $indexr++;
                                }
                                ?>
                             </table>

                            <?php
                                $sub_code_arr = array(
                                    "21" => "B, Tourism and Hospitality Management",
                                    "19" => "B, Public Administration",
                                    "18" => "B, English",
                                    "15" => "B, Social Work",
                                    "10" => "B, Bengali",
                                    "08" => "B, Economics",
                                    "04" => "B, Business Administration",
                                    "20" => "B, History and Bangladesh Studies",
									"22" => "B, Geography and Environment"
                                );
                            ?>
                           <table class="table table-responsive table-bordered">
                            <tr>
                                        <th><b>SL.</b></th>
										<th><b>Subject</b></th>
                                        <th><b>Total Paid/Admission DONE</b></th>
										<th><b>Business</b></th>
										<th><b>Humanities</b></th>
										<th><b>Science</b></th>
                                    </tr>
                                <?php
                                $indexr = 1;
                                foreach ($sub_code_arr as $key => $value) {
                                    ?>
                               <tr>
                                        <td><?php echo $indexr; ?></td>
                                        <td style="width: 40%;"><?php echo $value." , ".$key; ?> : </td>
                                        <td><?php echo get_total_subwise_paid($key); ?></td>
									   <td><?php echo get_total_subwise_paid_b_group($key,'business'); ?></td>
                                       <td><?php echo get_total_subwise_paid_b_group($key,'humanities'); ?></td>
									   <td><?php echo get_total_subwise_paid_b_group($key,'science'); ?></td>
                                    </tr>
                               <?php
                               $indexr++;
                                }
                                ?>
                             </table>


                            <table class="table table-responsive table-bordered" style="display: none;">
                                    <tr>
                                        <th style="width: 60%;"><h3>Total Paid Quota and 2nd Merit : </h3></th>
                                        <td><?php echo get_total_paid_2nd_merit(); ?></td>
                                    </tr>
                                    <tr>
                                        <th style="width: 60%;">Total Paid : </th>
                                        <td><?php echo get_total_paid(); ?></td>
                                    </tr>
                                    <tr>
                                        <th style="width: 60%;">Total Registration Done : </th>
                                        <td><?php echo get_total_registation_done(); ?></td>
                                    </tr>
                                <tr>
                                        <th style="width: 60%;">Total Subject Choice Done : </th>
                                        <td><?php echo get_total_sub_choice(); ?></td>
                                    </tr>
                    <tr>
                        <th colspan="2"><hr></th>

                                    </tr>
                                <tr>
                                        <th style="width: 60%;">Total Student in  A : </th>
                                        <td><?php echo get_total_sub_choice_unit('A') / 6; ?></td>
                               </tr>
                                <tr>
                                        <th style="width: 60%;">Total Student in A ARCH : </th>
                                        <td><?php echo get_total_sub_choice_unit('A_ARCH') / 1; ?></td>
                               </tr>
                                <tr>
                                        <th style="width: 60%;">Total Student in B : </th>
                                        <td><?php echo get_total_sub_choice_unit_b('B'); ?></td>
                               </tr>
                                <tr>
                                        <th style="width: 60%;">Total Student in  B : </th>
                                        <td><?php echo get_total_sub_choice_unit('B') /8 ; ?></td>
                               </tr>
                                <?php
                                $arr_c_count =  get_total_sub_choice_unit_c_group('B')
                                ?>
                                    <tr>
                                        <th style="width: 60%;">Total Student in  B , Humanities : </th>
                                        <td><?php echo $arr_c_count['hum'] ; ?></td>
                                    </tr>
                                    <tr>
                                        <th style="width: 60%;">Total Student in  B , Business : </th>
                                        <td><?php echo $arr_c_count['bus'] ; ?></td>
                                    </tr>
                                    <tr>
                                        <th style="width: 60%;">Total Student in  B , Science : </th>
                                        <td><?php echo $arr_c_count['sci'] ; ?></td>
                                    </tr>
                             </table>
                        </div> */ ?>

                        <table class="table table-responsive table-bordered">
                            <tr>
                                <th style="width: 60%;">
                                    <h3>Total Logged In</h3>
                                </th>
                                <th>
                                    <h3><?php echo get_total_logged_in(); ?></h3>
                                </th>
                            </tr>
                            <tr>
                                <th style="width: 60%;">
                                    <h3>Subject Choice done and Personal Information Filled</h3>
                                </th>
                                <th>
                                    <h3><?php echo get_total_subject_and_address_fill_done(); ?></h3>
                                </th>
                            </tr>
                        </table>
                        <hr>
                        <div class="row">
                            <?php
                            $sub_code_arr = array(
                                "01" => "A, Computer Science and Engineering",
                                "17" => "A, Urban and Regional Planning",
                                "11" => "A, Civil Engineering",
                                "06" => "A, Information and Communication Engineering",
                                "05" => "A, Electronic and Telecommunication Engineering",
                                "02" => "A, Electrical and Electronic Engineering",
                                "09" => "A, Geography and Environment",
                                "16" => "A, Statistics",
                                "14" => "A, Chemistry",
                                "13" => "A, Pharmacy",
                                "07" => "A, Physics",
                                "03" => "A, Mathematics",
                                "12" => "A_ARCH, Architecture",
                                "21" => "B, Tourism and Hospitality Management",
                                "19" => "B, Public Administration",
                                "18" => "B, English",
                                "15" => "B, Social Work",
                                "10" => "B, Bengali",
                                "08" => "B, Economics",
                                "04" => "B, Business Administration",
                                "20" => "B, History and Bangladesh Studies",
                                "22" => "B, Geography and Environment"
                            );
                            ?>
                            <?php $get_all_subject_code_name = get_all_subject_code_name() ?>
                            <table class="table table-responsive table-bordered">
                                <tr>
                                    <th>
                                        <h3>SL</h3>
                                    </th>
                                    <th style="width: 60%;">
                                        <h3>Subject Name</h3>
                                    </th>
                                    <th>
                                        <h3>Total Choice Given</h3>
                                    </th>
                                </tr>
                                <?php $sl = 0;
                                foreach ($get_all_subject_code_name as $key => $value) {
                                    $sl += 1; ?>
                                    <tr>
                                        <td style="test-align:center"><?= $sl ?></td>
                                        <th style="width: 60%;"><?php echo $value ?> : </th>
                                        <td><?php echo get_total_subwise_choice($key); ?></td>
                                    </tr>
                                <?php } ?>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('common/footer'); ?>