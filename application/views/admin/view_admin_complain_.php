<?php $this->load->view('common/header'); ?>
<?php //$this->load->view('admin/view_admin_navbar'); 
?>

<div id="main">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">Application Complain <span class="badge"><?= count($alldata); ?></span></div>

                    <div class="panel-body">
                        <div class="row">

                            <a href="<?php echo  base_url('admin/admin_complain/solvelist'); ?>">Solved List</a> | <a href="<?php echo  base_url('admin/admin_complain/pendinglist'); ?>">Pending List</a>
                            <div class="col-md-12 table-responsive">
                                <table class="table table-bordered">


                                    <tr class="">
                                        <th> ID </th>
                                        <th> Complain info </th>
                                        <th> GST Info</th>
                                        <th> Application Info</th>
                                        <th> Student Exam Details </th>
                                        <th width="30%"> Details </th>
                                        <th> Date </th>
                                        <th> IP </th>
                                        <th> Status </th>
                                    </tr>
                                    <?php
                                    if (!empty($alldata)) {
                                        foreach ($alldata as $key => $value) {
                                    ?>
                                            <tr class="">
                                                <td>
                                                    <?php echo $value['id']; ?>
                                                </td>
                                                <td>

                                                    Name:<br>
                                                    <span style="white-space: nowrap;"><?php echo $value['name']; ?></span><br>
                                                    Mobile No:<br>
                                                    <span class="<?= ($value['phone'] != $value['gst_cell_phone']) ? 'bg-danger' : '' ?>" style="white-space: nowrap;"><?php echo $value['phone']; ?></span><br>
                                                    GST App ID:<br>
                                                    <span style="white-space: nowrap;"><?php echo $value['gst_app_id']; ?></span>
                                                </td>
                                                <td>

                                                    <?php if (!empty($value['gst_applicant_id'])) : ?>
                                                        Name: <br>
                                                        <span style="white-space: nowrap;"><?php echo $value['gst_name']; ?></span><br>
                                                        Mobile No:<br>
                                                        <span class="<?= ($value['phone'] != $value['gst_cell_phone']) ? 'bg-danger' : '' ?>" style="white-space: nowrap;"><?php echo $value['gst_cell_phone']; ?></span><br>
                                                        GST Applicant ID:<br>
                                                        <span style="white-space: nowrap;"><?php echo $value['gst_applicant_id']; ?></span><br>
                                                        GST Admission Roll:<br>
                                                        <span style="white-space: nowrap;"><?php echo $value['gst_admission_roll']; ?></span>
                                                    <?php endif; ?>
                                                </td>
                                                <td>
                                                    <?php if (!empty($value['appid'])) : ?>
                                                        Name: <br>
                                                        <span style="white-space: nowrap;"><?php echo $value['candidate_name']; ?></span><br>
                                                        Mobile No:<br>
                                                        <span class="<?= ($value['phone'] != $value['gst_cell_phone']) ? 'bg-danger' : '' ?>" style="white-space: nowrap;"><?php echo $value['mobnumber']; ?></span><br>
                                                        GST App ID:<br>
                                                        <span style="white-space: nowrap;"><?php echo $value['appid']; ?></span>
                                                    <?php else : ?>
                                                        Applicant dose not registration yet.
                                                    <?php endif; ?>
                                                </td>
                                                <td>
                                                    <?php if (!empty($value['appid'])) : ?>
                                                        <table class="table table-responsive table-bordered">
                                                            <tr>
                                                                <td colspan="2">S.S.C/Equivalent Details</td>
                                                            </tr>
                                                            <tr>
                                                                <td>Board/Institute</td>
                                                                <td><?php echo $value['ssc_board']; ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Passing Year</td>
                                                                <td><?php echo $value['ssc_year']; ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Roll No.</td>
                                                                <td><?php echo $value['ssc_roll']; ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Reg. No.</td>
                                                                <td><?php echo $value['ssc_reg']; ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="2">H.S.C/Equivalent Details</td>
                                                            </tr>
                                                            <tr>
                                                                <td>Board/Institute</td>
                                                                <td><?php echo $value['hsc_board']; ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Passing Year</td>
                                                                <td><?php echo $value['hsc_year']; ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Roll No.</td>
                                                                <td><?php echo $value['hsc_roll']; ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Reg. No.</td>
                                                                <td><?php echo $value['hsc_reg']; ?></td>
                                                            </tr>
                                                        </table>

                                                    <?php endif; ?>
                                                </td>
                                                <td>
                                                    <?php echo $value['details']; ?>
                                                </td>
                                                <td>
                                                    <?php echo $value['date_time']; ?>
                                                </td>
                                                <td>
                                                    <?php echo $value['ip']; ?>
                                                </td>
                                                <td>
                                                    <?php echo $value['status']; ?>
                                                </td>
                                                <td>
                                                    <?php if ($value['status'] == "pending") { ?>
                                                        <a class="btn btn-success" href="<?php echo base_url('admin/admin_complain/solve/' . $value['id']); ?>">Solved</a>
                                                    <?php } ?>
                                                </td>
                                            </tr>
                                    <?php
                                        }
                                    }
                                    ?>
                                </table>
                            </div>
                        </div>
                        <hr>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('common/footer'); ?>