<?php $this->load->view('common/header'); ?>


<div id="main">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-primary">
                <div class="panel-heading">Login</div>

                    <div class="panel-body">                               
                            <div>
                                <?php
                                if(!empty($validMsg)){
                                    ?>
                                <div style="color: #e73d4a;padding-left: 200px;">
                                   <h3><?php echo $validMsg; ?> </h3>
                                  </div>
                                <?php }
                                ?>
                            </div>                        
                        <div class="row">
                           <form method="post" action="<?php echo base_url().'admin/admin_login/check' ?>" class="form-group" id="formadmincard">
                                <table class="table table-responsive table-bordered">
                                    <tr>
                                        <th>ID</th>
                                        <td>
                                            <input type="text" class="form-control" placeholder="ID" id="userName" name="userName" required>
                                            <span id="app_id_msg"></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Pass</th>
                                        <td>
                                            <input type="password" class="form-control" placeholder="Password" id="password" name="password" required>
                                            <span id="app_id_msg"></span>
                                        </td>
                                    </tr>                                    
                                    <tr>
                                        <td colspan="2">
                                            <input type="submit" value="Login" name="submit_form" id="submit_form" class="btn btn-primary pull-right"/>
                                        </td>
                                    </tr>
                                </table>    
                            </form>
                        </div>
                        <hr>
                    </div>

           
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('common/footer'); ?>
