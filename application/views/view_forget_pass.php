<?php $this->load->view('common/header'); ?>
<?php $this->load->view('common/navbar'); ?>
<style>
    #nextstep .error {
    color: red;
        font-style: italic;
}
</style>  
<div id="main">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">Forget Password</div>

                   <?php if(!empty($this->uri->segment(3)) && ( $this->uri->segment(3) == "noapp") || $this->uri->segment(3) == "notfound"  ){ ?>
                    <div class="panel-body">
                        <div class=" has-error">
                            <span class="pd15 bg-danger help-block">
								Invalid Information. No Login Information Found.
							</span>
                        </div>
                    </div>
				   <?php } ?>
                    <div class="panel-body">                       
                        <form action="<?php echo base_url('recover/find_password'); ?>" method="post" name="nextstep" id="nextstep">
                        <?php $this->form_validation->set_error_delimiters('<span class="help-block">', '</span>'); ?>
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table table-responsive table-bordered">
                                    <tr>
                                        <th colspan="2"><h4>Enter bellow information to find your password</h4></th>
                                    </tr>
                                    <tr>
                                        <th>
					Login Mobile Number (11 Digit )
					</th>
                                        <td>
                                            <div class="form-group <?php if(form_error('mob_no')) echo "has-error"; ?>">
                                                <?php
                                                $form_input = array(
                                                    'name'  => 'mob_no',
                                                    'id'  => 'mob_no',
                                                    'value' => set_value('mob_no', $mob_no=''),
                                                    'autocomplete' => 'off',
                                                    'class'  => 'form-control',
                                                    'placeholder'  => 'Mobile Number',
                                                    'required'=>'required'
                                                );
                                                echo form_input($form_input);
                                                if(form_error('mob_no')) echo form_label(form_error('mob_no'));
                                                ?>
                                            </div>
                                        </td>
                                    </tr>
									
                                    <tr>
                                        <th>GST Application ID</th>
                                        <td>
                                            <div class="form-group <?php if(form_error('appid')) echo "has-error"; ?>">
                                                <?php
                                                $form_input = array(
                                                    'name'  => 'appid',
                                                    'id'  => 'appid',
                                                    'value' => set_value('appid', $hsc_roll=''),
                                                    'autocomplete' => 'off',
                                                    'class'  => 'form-control',
                                                    'placeholder'  => 'GST Application ID',
                                                    'required'=>'required'
                                                );
                                                echo form_input($form_input);
                                                if(form_error('appid')) echo form_label(form_error('appid'));
                                                ?>
                                            </div>
                                        </td>
                                    </tr>									
                                 								 
                                    <tr>
                                        <th>HSC Roll Number</th>
                                        <td>
                                            <div class="form-group <?php if(form_error('hsc_roll')) echo "has-error"; ?>">
                                                <?php
                                                $form_input = array(
                                                    'name'  => 'hsc_roll',
                                                    'id'  => 'hsc_roll',
                                                    'value' => set_value('hsc_roll', $hsc_roll=''),
                                                    'autocomplete' => 'off',
                                                    'class'  => 'form-control',
                                                    'placeholder'  => 'HSC Roll No',
                                                    'required'=>'required'
                                                );
                                                echo form_input($form_input);
                                                if(form_error('hsc_roll')) echo form_label(form_error('hsc_roll'));
                                                ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>HSC Reg. Number</th>
                                        <td>
                                            <div class="form-group <?php if(form_error('hsc_registration')) echo "has-error"; ?>">
                                                <?php
                                                $form_input = array(
                                                    'name'  => 'hsc_registration',
                                                    'id'  => 'hsc_registration',
                                                    'value' => set_value('hsc_registration', $hsc_registration=''),
                                                    'autocomplete' => 'off',
                                                    'class'  => 'form-control',
                                                    'placeholder'  => 'HSC Registraion No',
                                                    'required'=>'required'
                                                );
                                                echo form_input($form_input);
                                                if(form_error('hsc_registration')) echo form_label(form_error('hsc_registration'));
                                                ?>
												
                                            </div>
                                        </td>
                                    </tr>

                                </table>
                            </div>
                        </div>
                        
                                           
                        <?= form_submit('submit', 'SUBMIT', ['class' => 'btn btn-primary pull-right']); ?>
                        <?= form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?= base_url('assets/js/jquery.validate.js'); ?>"></script>
<script> 
  $("#nextstep").validate();
</script>   
<?php $this->load->view('common/footer'); ?>
