<?php $this->load->view('common/header'); ?>
<?php $this->load->view('common/navbar'); ?>
<style>
    .unit_radio_style {
        padding-bottom: 10px;
    }

    .my_error {
        color: red;
    }

    .fnz-12 {
        font: 12px "Helvetica Neue", Helvetica, Arial, sans-serif !important;
    }
</style>

<div id="main">
    <div class="container">
        <?php if ($reg_status && isset($login_info)) : ?>
            <!-- ============================After Registration============================ -->
            <script>
                window.onbeforeunload = function() {
                    return 'Are you sure you want to leave?';
                };
            </script>
            <div class="row">
                <!-- ============================Registration Form============================ -->
                <div class="col-md-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading"><?= $page_title ?></div>
                        <div class="panel-body">
                            <p class="bg-success" style="padding: 15px;">Registration has been completed successfully, <b>Please save your login credential and don't share it with anyone.</b></p>

                            <hr>
                            <div class="row form-group">
                                <div class="col-md-2">
                                    <p><strong>Mobile Number</strong></p>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <?= $login_info['mobile_no'] ?>
                                    </div>
                                </div>
                            </div>


                            <div class="row form-group">
                                <div class="col-md-2">
                                    <p><strong>Password</strong></p>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <?= $login_info['password'] ?>
                                    </div>
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-2">

                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <a href="<?php echo base_url('login'); ?>">Click here to go to login page</a>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================//After Registration============================ -->
        <?php else : ?>
            <div class="row">
                <!-- ============================Applicant Information Preview============================ -->

                <div class="col-md-4">
                    <?php if (isset($student_info)) : ?>
                        <div class="panel panel-primary">
                            <div class="panel-heading">Student Information</div>
                            <div class="panel-body">
                                <?php if (isset($student_info)) : ?>
                                    <?php $this->load->view('StdApplication/_student_info'); ?>
                                <?php endif; ?>

                            </div>
                        </div>
                    <?php endif; ?>
                </div>
                <!-- ============================//Applicant Information Preview============================ -->
                <!--============================//Registration Form=====================================-->
                <div class="col-md-8">
                    <div class="panel panel-primary">
                        <div class="panel-heading"><?= $page_title ?></div>
                        <div class="panel-body">

                            <?= form_open('admRegistration') ?>

                            <div class="row form-group">
                                <div class="col-md-3">
                                    <p><strong>GST Application ID</strong></p>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <?= form_input('admission_id', $admission_id, ['class' => 'form-control', 'id' => 'Admission', 'placeholder' => 'GST Application ID']) ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-md-3">
                                    <p><strong>GST Mobile No.</strong></p>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <?= form_input('mobile_no', $mobile_no, ['class' => 'form-control', 'id' => 'Mobile', 'placeholder' => 'Mobile No']) ?>
                                    </div>
                                </div>
                            </div>


                            <div class="row form-group">

                                <div class="col-md-8">
                                    <i> Note : Students must use the same mobile number that was used in GST Application</i>
                                    <br>
                                    <br>
                                    Please read <a target="_blank" href="<?php echo base_url('PUST_Application_Guideline_2020-2021.pdf'); ?>">Application Guideline</a> properly before you submit application.
                                </div>
                            </div>

                            <div class="form-group">
                                <?php if (validation_errors() && form_error('admission_id')) :
                                    echo form_label(form_error('admission_id'), '', ['class' => 'fnz-12']);
                                elseif (validation_errors() && form_error('mobile_no')) :
                                    echo form_label(form_error('mobile_no'), '', ['class' => 'fnz-12']);
                                endif;
                                ?>
                            </div>
                            <hr>
                            <?php if ($is_pass) : ?>
                                <p class="bg-success" style="padding: 15px;">You are eligible to apply, please click <b>Confirm Registration</b> button to complete the registration process.</p>
                                <?= form_hidden('submit', 'registration') ?>
                                <button type="submit" class="btn btn-primary pull-right">Confirm Registration</button>
                            <?php elseif (!empty($student_info)) : ?>
                                <p class="bg-danger" style="padding: 15px;">We are sorry, you are not eligible to apply.</p>
                            <?php endif; ?>
                            <?php if (!$is_pass) : ?>
                                <?= form_submit('submit', 'Check your Information', ['class' => 'btn btn-info', 'text' => 'hh']) ?>
                            <?php endif; ?>

                            <hr>
                            <?= form_close() ?>


                        </div>
                    </div>
                </div>
                <!--============================//Registration Form=====================================-->

            </div>
        <?php endif; ?>
    </div>
</div>

<?php $this->load->view('common/footer'); ?>