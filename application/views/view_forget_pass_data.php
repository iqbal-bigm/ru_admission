<?php $this->load->view('common/header'); ?>
<?php $this->load->view('common/navbar'); ?>
<style>
    #nextstep .error {
    color: red;
        font-style: italic;
}
</style>  
<div id="main">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">Login ID and Password</div>

                    <?php $msg = $this->session->flashdata('notification');

                    if( !empty($msg)): ?>
                    <div class="panel-body">
                        <div class=" has-error">
                            <span class="pd15 bg-danger help-block"><?= $msg; ?></span>
                        </div>
                    </div>
                    <?php endif; ?>
                    <div class="panel-body">                       
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table table-responsive table-bordered">
										<tr>
                                        <th colspan="2">Note: Do not share login information with any one</th>
                                    </tr>
                                    <tr>
                                        <td>Mobile Number(Login ID)</td><td>Password</td>
                                    </tr>

                                    <tr>
                                        <td><b><?php 
											echo $login_data['mobile_no']; 
										?></b></td>
										<td><b><?php echo $login_data['password']; ?></b></td>
                                    </tr>                                    
             
                                </table>
                            </div>
                        </div>
                        

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?= base_url('assets/js/jquery.validate.js'); ?>"></script>
<script> 
  $("#nextstep").validate();
</script>   
<?php $this->load->view('common/footer'); ?>
