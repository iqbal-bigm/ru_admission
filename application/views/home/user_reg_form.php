<?php $this->load->view('common/header'); ?>
<?php $this->load->view('common/navbar'); ?>
<style>
    #nextstep .error {
    color: red;
        font-style: italic;
}
</style>
<div id="main">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">Registration Form : Step 1</div>

                    <?php $msg = $this->session->flashdata('notification');

                    if( !empty($msg)): ?>
                    <div class="panel-body">
                        <div class=" has-error">
                            <span class="pd15 bg-danger help-block"><?= $msg; ?></span>
                        </div>
                    </div>
                    <?php endif; ?>
                    <div class="panel-body">
                        <form action="<?php echo base_url('home/nextstep'); ?>" method="post" name="nextstep" id="nextstep">
                        <?php $this->form_validation->set_error_delimiters('<span class="help-block">', '</span>'); ?>
                        <div class="row">
                            <div class="col-md-2"><h4>Exam Level</h4></div>
                            <div class="col-md-2"><h4>Board/Institute</h4></div>
                            <div class="col-md-2"><h4>Passing Year</h4></div>
                            <div class="col-md-3"><h4>Roll No.</h4></div>
                            <div class="col-md-3"><h4>Reg. No.</h4></div>
                        </div>

                        <div class="row">
                            <div class="col-md-2"><p>S.S.C/Equivalent</p></div>
                            <div class="col-md-2">
                                <div class="form-group <?php if(form_error('ssc_board')) echo "has-error"; ?>">
                                    <?php
                                        $id_ssc_board = 'class="form-control" id="ssc_board" required';
                                        echo form_dropdown('ssc_board', ssc_hsc_board(), set_value('ssc_board', $ssc_board), $id_ssc_board);
                                        //echo form_dropdown('ssc_board', ssc_hsc_board(), set_value('ssc_board', $ssc_board), ['id' => 'ssc_board', 'class' => 'form-control', 'required']);
                                        if(form_error('ssc_board')) echo form_label(form_error('ssc_board'));
                                    ?>
                                </div>
                            </div>

                            <div class="col-md-2">
                                <div class="form-group <?php if(form_error('ssc_passing_year')) echo "has-error"; ?>">
                                    <?php
                                        $id_ssc_passing_year = 'class="form-control" id="ssc_passing_year" required';
                                        echo form_dropdown('ssc_passing_year', ssc_passing_year(), set_value('ssc_passing_year', $ssc_passing_year), $id_ssc_passing_year);
                                        //echo form_dropdown('ssc_passing_year', ssc_passing_year(), set_value('ssc_passing_year', $ssc_passing_year), ['id' => 'ssc_passing_year', 'class' => 'form-control', 'required']);
                                        if(form_error('ssc_passing_year')) echo form_label(form_error('ssc_passing_year'));
                                    ?>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group <?php if(form_error('ssc_roll')) echo "has-error"; ?>">
                                    <?php
                                    $form_input = array(
                                        'name'  => 'ssc_roll',
                                        'id'  => 'ssc_roll',
                                        'value' => set_value('ssc_roll', $ssc_roll),
                                        'autocomplete' => 'off',
                                        'class'  => 'form-control',
                                        'placeholder'  => 'SSC Roll No',
                                         'required'=>'required'
                                    );
                                    echo form_input($form_input);
                                    if(form_error('ssc_roll')) echo form_label(form_error('ssc_roll'));
                                    ?>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group <?php if(form_error('ssc_registration')) echo "has-error"; ?>">
                                    <?php
                                    $form_input = array(
                                        'name'  => 'ssc_registration',
                                        'id'  => 'ssc_registration',
                                        'value' => set_value('ssc_registration', $ssc_registration),
                                        'autocomplete' => 'off',
                                        'class'  => 'form-control',
                                        'placeholder'  => 'SSC Registraion No',
                                        'required'=>'required'
                                    );
                                    echo form_input($form_input);
                                    if(form_error('ssc_registration')) echo form_label(form_error('ssc_registration'));
                                    ?>
                                </div>
                            </div>
                        </div>

                        <div class="row form-group">
                            <div class="col-md-2"><p>H.S.C/Equivalent</p></div>
                            <div class="col-md-2">
                                <div class="form-group <?php if(form_error('hsc_board')) echo "has-error"; ?>">
                                    <?php
                                        $id_hsc_board = 'class="form-control" id="hsc_board" required';
                                        echo form_dropdown('hsc_board',   hsc_board(), set_value('hsc_board', $hsc_board), $id_hsc_board);
                                        //echo form_dropdown('hsc_board',  /*ssc_hsc_board()*/ hsc_board(), set_value('hsc_board', $hsc_board), ['id' => 'hsc_board', 'class' => 'form-control', 'required']);
                                        if(form_error('hsc_board')) echo form_label(form_error('hsc_board'));
                                    ?>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group <?php if(form_error('hsc_passing_year')) echo "has-error"; ?>">
                                    <?php
                                       $id_hsc_passing_year = 'class="form-control" id="hsc_passing_year" required';
                                         echo form_dropdown('hsc_passing_year', hsc_passing_year(), set_value('hsc_passing_year', $hsc_passing_year), $id_hsc_passing_year);
                                      //  echo form_dropdown('hsc_passing_year', hsc_passing_year(), set_value('hsc_passing_year', $hsc_passing_year), ['id' => 'hsc_passing_year', 'class' => 'form-control', 'required']);
                                        if(form_error('hsc_passing_year')) echo form_label(form_error('hsc_passing_year'));
                                    ?>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group <?php if(form_error('hsc_roll')) echo "has-error"; ?>">
                                    <?php
                                    $form_input = array(
                                        'name'  => 'hsc_roll',
                                        'id'  => 'hsc_roll',
                                        'value' => set_value('hsc_roll', $hsc_roll),
                                        'autocomplete' => 'off',
                                        'class'  => 'form-control',
                                        'placeholder'  => 'HSC Roll No',
                                         'required'=>'required'
                                    );
                                    echo form_input($form_input);
                                    if(form_error('hsc_roll')) echo form_label(form_error('hsc_roll'));
                                    ?>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group <?php if(form_error('hsc_registration')) echo "has-error"; ?>">
                                    <?php
                                    $form_input = array(
                                        'name'  => 'hsc_registration',
                                        'id'  => 'hsc_registration',
                                        'value' => set_value('hsc_registration', $hsc_registration),
                                        'autocomplete' => 'off',
                                        'class'  => 'form-control',
                                        'placeholder'  => 'HSC Registraion No',
                                         'required'=>'required'
                                    );
                                    echo form_input($form_input);
                                    if(form_error('hsc_registration')) echo form_label(form_error('hsc_registration'));
                                    ?>
                                </div>
                            </div>
                        </div>

                        <?php //echo $widget; ?>
                        <?php //echo $script; ?>

                        <hr>
                        <span class="help-block"> [N.B] All Field Are Required <span class="required" aria-required="true">*</span></span>

                        <?= form_submit('submit', 'NEXT', ['class' => 'btn btn-primary pull-right']); ?>
                        <?= form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?= base_url('assets/js/jquery.validate.js'); ?>"></script>
<script>
  $("#nextstep").validate();
</script>
<?php $this->load->view('common/footer'); ?>
