<?php $this->load->view('common/header'); ?>
<?php $this->load->view('common/navbar'); ?>

<div id="main">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">Circular</div>
                    <div class="panel-body">
                        <div class="row">
                            <p> <h4 style="padding: 20px;" ><a target="_blank" href="<?php echo base_url('2020-21-Addmission-Notice.pdf'); ?>">Download Circular</a></h4></p>
                        
                         <p> <h4 style="padding: 20px;" ><a target="_blank" href="<?php echo base_url('PUST_Application_Guideline_2020-2021.pdf'); ?>">Application Guideline</a></h4></p>
                        </div>
                    </div>

                </div>
            </div>
        </div>

      <div class="row">
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">Payment Process Flow</div>
                    <div class="panel-body">
                        <div class="row">
                            <p>
                               <img width="90%" src="<?php echo base_url('rocketbill-ref.png'); ?>" /><br>
                                <img width="90%" src="<?php echo base_url('how-to-pay.png'); ?>" />
                            </p>
                        </div>
                    </div>

                </div>
            </div>
        </div> 
    </div>
</div>

<?php $this->load->view('common/footer'); ?>
