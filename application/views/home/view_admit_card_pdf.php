<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>ADMIT CARD PUST</title>
    <style>

    </style>
</head>

<body>

    <div style="width: 100%; border: 2px solid #FFC000; padding: 5px 5px; font-family: sans-serif;">
        <table style="width: 100%" border="0">
            <tr>
                <td>
                    <img style="width: 100px; height: auto" alt="<?php echo UNIVERSITY_NAME; ?>" src="<?php echo base_url(); ?>assets/images/logo.png" />
                </td>
                <td style="text-align: center;" colspan="2">
                    <p style="top:0px;font-size: 23px;color:#C00000;font-weight:bold"><?php echo UNIVERSITY_NAME; ?></p>
                    <br>
                    <p style="font-size:20px;color:#0000FF;"><b>Admission Test (Session 2019-2020)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></p>
                    <br><br><br><br>
                </td>
            </tr>
            <tr>
                <td colspan="3" style="border-bottom:1px solid black"></td>
            </tr>
        </table>
        <table width="100%" border="0">
            <tr>
                <td style="color:#0F0FFF;font-size: 25px;text-align: center">
                    &nbsp;&nbsp;
                    <b>Admit Card</b>
                    <p style="font-size:15px;color:#C00000">
                        <b>(Please print this admit card in color on an A4 paper and bring two (02) copies of it.)</b>
                    </p>
                </td>

            </tr>
            <tr>
                <td><br></td>
            </tr>
        </table>


        <table style="width: 100%" border="0">
            <tr>
                <td valign="top">
                    <table border="0" cellpadding="5px;" width="100%" class="">


                        <tr>
                            <td style="text-align: left;font-size: 14px;">Roll Number</td>
                            <td>:</td>
                            <td style="font-size: 14px;"><b><?php echo $ApplicantInfo['ROLLNO_FINAL']; ?></b></td>

                        </tr>
                        <tr>
                            <td style="text-align: left;font-size: 14px;">Application ID</td>
                            <td>:</td>
                            <td style="font-size: 14px;"><b><?php echo $ApplicantInfo['APPID']; ?></b></td>

                        </tr>
                        <tr>
                            <td style="font-size: 14px;">Unit</td>
                            <td>:</td>
                            <td style="font-size: 14px;">
                                <?php
                                echo "<b>" . $ApplicantInfo['UNIT'] . "</b>";
                                if ($ApplicantInfo['UNIT'] == "A") {
                                    if ($ApplicantInfo['WITH_ARCHITECTURE'] == "YES") {
                                        echo " ( With Architecture )";
                                    } else {
                                        echo " ( Without Architecture ) ";
                                    }
                                }
                                ?>
                            </td>

                        </tr>


                        <tr>
                            <td style="font-size: 14px;">Name</td>
                            <td>:</td>
                            <td style="font-size: 14px;"><?php echo $RegistrationInfo['SNAME']; ?></td>

                        </tr>
                        <tr>
                            <td>Father's Name</td>
                            <td>:</td>
                            <td><?php echo $RegistrationInfo['SFNAME']; ?></td>

                        </tr>
                        <tr>
                            <td>Mother's Name</td>
                            <td>:</td>
                            <td><?php echo $RegistrationInfo['SMNAME']; ?></td>

                        </tr>
                        <tr>
                            <td>SSC GPA</td>
                            <td>:</td>
                            <td><?php echo $RegistrationInfo['SGPA']; ?></td>

                        </tr>
                        <tr>
                            <td>HSC GPA</td>
                            <td>:</td>
                            <td><?php echo $RegistrationInfo['HGPA']; ?></td>

                        </tr>
                        <tr>
                            <td>Question Paper Version</td>
                            <td>:</td>
                            <td><?php echo $ApplicantInfo['EXAM_VERSION']; ?></td>

                        </tr>
                    </table>
                </td>

                <td>

                    <img width="220px" height="240px" src="<?php echo base_url() . 'uploads/' . $RegistrationInfo['IMGUPLOAD_PATH'] . '/' . $RegistrationInfo['REGISID'] . '.jpg'; ?>" alt="photo" />
                </td>
            </tr>
            <tr>
                <td colspan="2"><br><br></td>
            </tr>
        </table>
        <table style="width: 100%">
            <tr>
                <td>

                    <br>
                    <br>

                    <br>
                    _____________________________<br>
                    &nbsp;&nbsp;&nbsp;&nbsp;Invigilator's Signature
                </td>
                <td style="text-align: right;">
                    <br>
                    <br>

                    <br>
                    _____________________________<br>
                    Applicant's Signature&nbsp;&nbsp;&nbsp;&nbsp;
                </td>

            </tr>
            <tr>
                <td colspan="2"><br><br><br><br></td>
            <tr>

        </table>
        <table>
            <tr>
                <td colspan="2" style="text-align: left;font-size: 14px;">
                    <p style="border-bottom:1px solid black">Instructions:</p>

                    1. Please bring two (2) copies of colored Admit Card during admission test.
                    <br>
                    2. Candidate must use black ink ball point pen to fill the OMR sheet.
                    <br>
                    3. Candidate is not allowed to carry books, bags, mobile phones, calculators, smart watches or
                    any other electronic devices in the examination hall.
                    <br>

                    4. Candidate should reach the examination hall 20 minutes before the examination.
                    <br>
                    5. Candidate must sit according to the seating arrangement.

                    <br>
                </td>
                <td>
                    <img src="<?php echo base_url() . 'admit_qr_code/' . $ApplicantInfo['ROLLNO_FINAL'] . '.png'; ?>" width="80px" alt="QR">
                </td>

            </tr>
        </table>
        <br><br><br><br>
        <table width="100%" style="border: 1px solid black;">
            <tr>
                <td colspan="2" style="text-align: left;font-size: 12px;text-align: center;">
                    © 2019-2020; <?php echo UNIVERSITY_NAME; ?>. All rights reserved.
                </td>
            </tr>
        </table>
        <table width="100%">
            <tr>
                <td colspan="2" style="font-size: 12px;text-align: center;">
                    www.pust.ac.bd
                </td>

            </tr>
        </table>
        <br> <br>
    </div>

</body>

</html>