<?php $this->load->view('common/header'); ?>
<?php $this->load->view('common/navbar'); ?>
<style>
    .unit_radio_style { padding-bottom: 10px; }
    .my_error {color: red;}
</style>
<div id="main">
    <div class="container">
        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-primary">
                    <div class="panel-heading">EDIT</div>

                    <div class="panel-body">

						<div>
						  <form method="post" action="<?php echo base_url(); ?>home/processupdate" name="application_submit_edit" id="application_submit_edit" autocomplete="off">
								<input type="hidden" name="edit_id" id="edit_id" value="<?php echo $edit_data['EDIT_ID']; ?>">
								<input type="hidden" name="edit_appid" id="edit_appid" value="<?php echo $edit_data['APPID']; ?>">
								<input type="hidden" name="edit_unit" id="edit_unit" value="<?php echo $edit_data['UNIT']; ?>">
								<input type="hidden" name="reg_id" id="reg_id" value="<?php echo $edit_data['REGISID']; ?>">
							<div class="row" id="personal_info_holder">
								<div class="col-md-12">
											<fieldset>
												<legend>Edit Personal Information For Application ID : <?php echo $edit_data['APPID']; ?> </legend>
											</fieldset>

											<table class="table table-responsive table-bordered">
												<tr>
													<th>Mobile Number(11 digit) <span class="my_error">*</span> : </th>
													<td>
														<input onkeypress="return isNumber(event)" type="number" maxlength="11" autocomplete="false" name="mobile_no" id="mobile_no"  class="form-control" value="<?php echo $edit_data['MOBNUMBER']; ?>">
														<span id="mobile_no_msg"></span>
													</td>
												</tr>
												 <tr>
													<th>Quota <span class="my_error">*</span>: </th>
													<td>
														<?php
														$quota_array = get_quota_array();
														foreach ($quota_array as $key => $value) {
															if($key == $edit_data['QNAME']){
															?>
															<input name="quota" value="<?php echo $key; ?>" checked="true" type="radio" /> <?php echo $value; ?>
														<?php
															}else{
															   ?>
															<input name="quota" value="<?php echo $key; ?>" type="radio" /> <?php echo $value; ?>
													  <?php
															}
														}
														?>
													</td>
												</tr>
											   <tr>
													<th>Home District <span class="my_error">*</span> : </th>
													<td>
														<?php
														$id_district = 'class="form-control" id="district"';
														$dis_array = get_district_array();
														echo form_dropdown('district',$dis_array,$edit_data['HDISTRICT'],$id_district);
														?>

														<span id="district_msg"></span>
													</td>
												</tr>
												<tr>
													<th>What version of question paper do you prefer in PUST admission test 2019-2020 ? <span class="my_error"> *</span>: </th>
													<td>
														<div class="form-group">
															<select class="form-control" id="exam_question_tpe" name="exam_question_tpe">
															 <option value="">Select  your preference</option>
															   <?php
																$exam_array = array('Bangla','English');

																foreach ($exam_array as $key => $value ) {
																	if($edit_data['EXAM_VERSION'] == $value ){
																		?>
																		<option selected="selected" value="<?php echo $value; ?>"><?php echo $value; ?></option>
																		<?php
																	}else{
																	?>
																	<option value="<?php echo $value; ?>"><?php echo $value; ?></option>
																	<?php
																} } ?>

														</select>
															<span id="exam_question_tpe_msg"></span>
															</div>
													</td>
												</tr>
											</table>
										</div>
								 </div>

								<div class="row">
									<div class="col-md-12">
									<hr>
									<span class="help-block">Required <span class="required" aria-required="true">*</span></span>
									<div>
										<span id="mobile_no_msg_"></span>
										<span id="photo_upload_error_"></span>
										<span id="district_msg_"></span>
										<span id="exam_question_tpe_msg_"></span>
									</div>
									<span id="submit_btn_holder">
										<input type="submit" value="SUBMIT" name="submit_form" id="submit_form" class="btn btn-primary pull-right" onclick="return validateFromDataEdit();" />
									</span>
									</div>
								</div>
					   </form>
					</div>
       
              
                </div>

            </div>
        </div>
    </div>
</div>
</div>
<?php $this->load->view('common/footer'); ?>
