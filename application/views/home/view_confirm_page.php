<?php $this->load->view('common/header'); ?>
<?php $this->load->view('common/navbar'); ?>
<div id="main">
    <div class="container">
        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-primary">
                 <div class="panel-heading">Application Confirmation</div>
                 <br>
                 <?php
                                if($application_data['PMTSTATUS']  == "U"){
                                ?>
                 <table class="table table-responsive table-bordered">
                     <tr>
                         <td  align="center"><b>
                                 <div class="bg-success" style="text-align: justify;">
                                     Dear Applicant,
                                     <br>
                                     Your Application is pending for payment.
                                     <br>
                                    1. You need to pay application fee by  Sure Cash.
                                    <br>
                                    2. Download your admit card after 25th October, 2019.
                             </b>
                             </div>
								<br>
							  <div class="bg-info" style="text-align: justify;">
							   You can edit your application one time. <a class="btn btn-primary" href="<?php echo base_url()."home/edit/".$application_data['EDIT_ID']."/".$application_data['REGISID'].""; ?>">CLICK HERE</a> to edit.
							  <br>
							   If necessary, You can also edit application informaton later. From your application list.
							  </div>
							  <br>
                         </td>
                     </tr>
                 </table>
                                <?php } ?>
                   <table class="table table-responsive table-bordered">
                    <tr>
                        <td style="width: 30%"><a href="<?php echo base_url()."home/app_download/".$application_data['APPID']."/".$application_data['REGISID'].""; ?>" class="btn btn-success">PRINT/DOWNLOAD Application Confirmation</a></td>
                        <td style="width: 70%" valign="top">
                             <table class="table table-responsive table-bordered">
                                <tr>
                                    <td colspan="3"><b>Application Information</b></td>
                                </tr>
                                <tr>
                                    <td>Photo</td>
                                    <td>:</td>
                                    <td><img height="100px;" width="80px;" src="<?php echo base_url().'uploads/'.$student_all_data['IMGUPLOAD_PATH'].'/'.$student_all_data['REGISID'].'.jpg'.'?'.time(); ?>" alt="photo" /></td>
                                </tr>
                                <tr>
                                    <td>Application ID</td>
                                    <td>:</td>
                                    <td class="btn-info"><b><?php echo $application_data['APPID']; ?></b></td>
                                </tr>

                                <tr>
                                    <td>Unit</td>
                                    <td>:</td>
                                    <td class="btn-info"><b>
                                      <?php
                                      echo $application_data['UNIT'];

                                      if($application_data['UNIT'] == "A" && $application_data['WITH_ARCHITECTURE'] == "YES"){
                                          echo " (WITH ARCHITECTURE)";
                                      }
                                      ?>
                                    </b></td>
                                </tr>

                                <tr>
                                    <td>Question Paper</td>
                                    <td>:</td>
                                    <td><?php echo $application_data['EXAM_VERSION']; ?></td>
                                </tr>

                                <tr>
                                    <td>Home District</td>
                                    <td>:</td>
                                    <td><?php echo $application_data['HDISTRICT']; ?></td>
                                </tr>

                                <tr>
                                    <td>System REG. ID</td>
                                    <td>:</td>
                                    <td><?php echo $application_data['REGISID']; ?></td>
                                </tr>
                                <tr>
                                    <td>Name</td>
                                    <td>:</td>
                                    <td><?php echo $student_all_data['HNAME']; ?></td>
                                </tr>
                                <tr>
                                    <td>Father Name</td>
                                    <td>:</td>
                                    <td><?php echo $student_all_data['HFNAME']; ?></td>
                                </tr>
                                <tr>
                                    <td>Mother Name</td>
                                    <td>:</td>
                                    <td><?php echo $student_all_data['HMNAME']; ?></td>
                                </tr>
                            </table>
                            <table class="table table-responsive table-bordered">
                                <tr>
                                    <td colspan="6"><b>Others Information</b></td>
                                </tr>

                                <tr>
                                    <td>Mobile No</td>
                                    <td>:</td>
                                    <td><?php echo $application_data['MOBNUMBER']; ?></td>
                                    <td>Quota</td>
                                    <td>:</td>
                                    <td><?php
                                    $qname_arr = get_quota_array();
                                    echo  $qname_arr[$application_data['QNAME']];
                                    ?></td>
                                </tr>
                                <tr>
                                    <td colspan="5" >
                                         <?php
                                         if($application_data['PMTSTATUS']  == "U"){
                                            echo "Amount to Pay(please ignore if already paid) :" ;
                                          }else {
                                            echo "Amount Paid :" ;
                                          } ?>
                                    </td>
                                    <td class="btn-info"><?php echo $application_data['AMOUNT_TO_PAY']; ?> TAKA</td>
                                </tr>

                            </table>
                             <table class="table table-responsive table-bordered">
                                <tr>
                                    <td colspan="6"><b>S.S.C/Equivalent</b></td>
                                </tr>
                                <tr>
                                    <td>Roll No</td>
                                    <td>:</td>
                                    <td><?php echo $student_all_data['SROLL_NO']; ?></td>
                                    <td>Reg. No</td>
                                    <td>:</td>
                                    <td><?php echo $student_all_data['SREG_NO']; ?></td>
                                </tr>
                                <tr>
                                    <td>Board</td>
                                    <td>:</td>
                                    <td><?php echo $student_all_data['SBOARD_NAME']; ?></td>
                                    <td>Passing Year</td>
                                    <td>:</td>
                                    <td><?php echo $student_all_data['SPASS_YEAR']; ?></td>
                                </tr>
                                <tr>
                                    <td>GPA</td>
                                    <td>:</td>
                                    <td><?php echo $student_all_data['SGPA']; ?></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td colspan="6"><b>H.S.C/Equivalent</b></td>
                                </tr>
                                <tr>
                                    <td>Roll No</td>
                                    <td>:</td>
                                    <td><?php echo $student_all_data['HROLL_NO']; ?></td>
                                    <td>Reg. No</td>
                                    <td>:</td>
                                    <td><?php echo $student_all_data['HREG_NO']; ?></td>
                                </tr><tr>
                                    <td>Board</td>
                                    <td>:</td>
                                    <td><?php echo $student_all_data['HBOARD_NAME']; ?></td>
                                    <td>Passing Year</td>
                                    <td>:</td>
                                    <td><?php echo $student_all_data['HPASS_YEAR']; ?></td>
                                </tr>
                                <tr>
                                    <td>GPA</td>
                                    <td>:</td>
                                    <td><?php echo $student_all_data['HGPA']; ?></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </table>


                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('common/footer'); ?>
