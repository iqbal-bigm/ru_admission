<?php $this->load->view('common/header'); ?>
<?php $this->load->view('common/navbar'); ?>
<div id="main">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-primary">
					<div class="panel-heading">Check Paymment</div>
					<div class="panel-body">
						<?php
						if ( $show_report == "yes" ) {
							?>
							 <table class="table table-responsive table-bordered">
							<tr><th>Application ID</th><th>Paymment Status</th></tr>
							<tr><td><?php echo $APPID; ?></td><td>
							<?php if($PMTSTATUS == "PAID"){
								echo "<span class=\"btn btn-success\">";
							}else{
								echo "<span class=\"btn btn-danger\">";
							} ?>
							<?php echo $PMTSTATUS; ?>
							</span>
							</td></tr>
							</table>
							<br>
							<br>
						<?php }
						?>
						<form action="<?php echo base_url('payment/validate');?>" method="post">
							<div class="form-group row">
								<label for="" class="form-label col-md-2">Application ID</label>
								<div class="col-md-4">
									<input type="text" name="appid" class="form-control"  placeholder="Application ID">
								</div>
								<div class="col-md-1">
									<input type="submit" name="submit_form" value="SUBMMIT" class="btn btn-primary pull-right" />
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php $this->load->view('common/footer'); ?>