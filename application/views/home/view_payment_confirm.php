<?php $this->load->view('common/header'); ?>
<?php $this->load->view('common/navbar'); ?>

<div id="main">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">Payment Confirm</div>

                    <div class="panel-body">                        
                        <div class="row">
                           
                                <table class="table table-responsive table-bordered">
                                    <tr>
                                        <th colspan="2">
                                            <?php
                                            if(!empty($msg)){
                                                echo $msg;
                                            }
                                            ?>
                                        </th>
                                    </tr>
        
                                </table>    

                        </div>
                        <hr>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('common/footer'); ?>
