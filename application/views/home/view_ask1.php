<?php $this->load->view('common/header'); ?>
<?php $this->load->view('common/navbar'); ?>

<div id="main">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">Submit Complain</div>

                    <div class="panel-body">
                        <div class="row">
                            <div>
                                <?php
                                if(!empty($validMsg)){
                                    ?>
                                <div style="color: blue;padding-left: 260px;">
                                   <h3><?php echo $validMsg; ?> </h3>
                                  </div>
                                <?php }
                                ?>
                            </div>

                            <div>
                                  <?php
                                  if( !empty( $this->session->flashdata('notification')) ){
                                    ?>
                                      <div style="color:red !important; padding-left: 60px;">
                                        <h2>
                                        <?php echo $this->session->flashdata('notification'); ?>
                                        </h2>
                                      </div>
                              <?php    }

                                  ?>
                            </div>

                            <form action="<?php echo base_url() . 'ask/save' ?>" method="post" id="myform" autocomplete="off">
                                <table class="table table-responsive table-bordered">
                                    <tr>
                                        <th>Name*</th>
                                        <td>
                                            <input type="text" name="name" id="name" required  class="form-control">
                                            <span id="app_id_msg" class="form-group <?php if(form_error('name')) echo "has-error"; ?>"> <?php  if(form_error('name')) echo form_label(form_error('name')); ?></span>

                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Phone Number*</th>
                                        <td>
                                            <input type="text" name="phone" id="phone"  required  class="form-control" value="<?php echo $this->session->userdata('student_login_mobile_no'); ?>">
                                            <span id="tans_id_msg" class="form-group <?php if(form_error('phone')) echo "has-error"; ?>"><?php  if(form_error('phone')) echo form_label(form_error('phone')); ?></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>
                                            Problem Details*
                                            <br>
                                            (Write your hsc REG. no. , ROLL no.  <br> passing year, board name <br> and problem in details)
                                        </th>
                                        <td>
                                            <textarea class="form-control" cols="50" rows="5" name="details" id="details"></textarea>
                                             <span id="details_msg" class="form-group <?php if(form_error('details')) echo "has-error"; ?>"><?php  if(form_error('details')) echo form_label(form_error('details')); ?></span>
                                        </td>
                                    </tr>
                            <tr>
                                        <th></th>
                                        <td>
                                          <?php echo $widget;?>
                                          <?php echo $script;?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <input type="submit" value="SUBMIT" name="submit_form" id="submit_form" class="btn btn-primary pull-right"/>
                                        </td>
                                    </tr>
                                </table>
                            </form>
                        </div>
                        <hr>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('common/footer'); ?>
