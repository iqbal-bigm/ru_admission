<?php $this->load->view('common/header'); ?>
<?php $this->load->view('common/navbar'); ?>
<style>
    .unit_radio_style { padding-bottom: 10px; }
    .my_error {color: red;}
</style>
<div id="main">
    <div class="container">
        <div class="row">
            <div class="col-md-12">




                <div class="panel panel-primary">
                    <div class="panel-heading">Student Details Information : Step 2</div>

                    <div class="panel-body">


                        <div class="row">
                            <div class="col-md-6">
                                <fieldset>
                                    <legend>S.S.C/Equivalent:</legend>
                                </fieldset>

                                <table class="table table-responsive table-bordered">
                                    <tr>
                                        <th>Name : </th>
                                        <td><?= $student_all_data['SNAME']; ?></td>
                                    </tr>
                                    <tr>
                                        <th>Father's Name : </th>
                                        <td><?= $student_all_data['SFNAME']; ?></td>
                                    </tr>
                                    <tr>
                                        <th>Mother's Name : </th>
                                        <td><?= $student_all_data['SMNAME']; ?></td>
                                    </tr>

                                    <tr>
                                        <th>Roll No : </th>
                                        <td><?= $student_all_data['SROLL_NO']; ?></td>
                                    </tr>
                                    <tr>
                                        <th>Registration No : </th>
                                        <td><?= $student_all_data['SREG_NO']; ?></td>
                                    </tr>

                                    <tr>
                                        <th>Board : </th>
                                        <td><?= $student_all_data['SBOARD_NAME']; ?></td>
                                    </tr>

                                    <tr>
                                        <th>Passing Year : </th>
                                        <td><?= $student_all_data['SPASS_YEAR']; ?></td>
                                    </tr>
                                    <tr>
                                        <th>Group : </th>
                                        <td><?= $student_all_data['SGROUP']; ?></td>
                                    </tr>
                                    <tr>
                                        <th>GPA : </th>
                                        <td><?= $student_all_data['SGPA']; ?></td>
                                    </tr>
                                    <tr>
                                        <th>Result : </th>
                                        <td><?= $student_all_data['SRESULT']; ?></td>
                                    </tr>
                                    <tr>
                                        <th>Subject GPA : </th>
                                        <td style="font-size: 11px;">
                                            <?php
                                            $sub = explode(',', $student_all_data['SLTRGRD']);
                                            $i=0;
                                            foreach($sub as $key => $value):
                                                //$subject = get_ssc_subject($sub[$i]);
                                                //echo $subject['code'].' => '. $subject['name'] . ' : ' .$subject['grade'].'<br>';
                                                echo $value.'<br>';
                                                $i++;
                                            endforeach;
                                            ?>
                                        </td>
                                    </tr>

                                </table>
                            </div>
                            <div class="col-md-6">
                                <fieldset>
                                    <legend>H.S.C/Equivalent : </legend>
                                </fieldset>

                                <table class="table table-responsive table-bordered">
                                    <tr>
                                        <th>Name : </th>
                                        <td><?= $student_all_data['HNAME']; ?></td>
                                    </tr>
                                    <tr>
                                        <th>Father's Name : </th>
                                        <td><?= $student_all_data['HFNAME']; ?></td>
                                    </tr>
                                    <tr>
                                        <th>Mother's Name : </th>
                                        <td><?= $student_all_data['HMNAME']; ?></td>
                                    </tr>
                                    <tr>
                                        <th>Roll No : </th>
                                        <td><?= $student_all_data['HROLL_NO']; ?></td>
                                    </tr>
                                    <tr>
                                        <th>Registration No : </th>
                                        <td><?= $student_all_data['HREG_NO']; ?></td>
                                    </tr>

                                    <tr>
                                        <th>Board : </th>
                                        <td><?= $student_all_data['HBOARD_NAME']; ?></td>
                                    </tr>

                                    <tr>
                                        <th>Passing Year : </th>
                                        <td><?= $student_all_data['HPASS_YEAR']; ?></td>
                                    </tr>
                                    <tr>
                                        <th>Group : </th>
                                        <td><?= $student_all_data['HGROUP']; ?></td>
                                    </tr>
                                    <tr>
                                        <th>GPA : </th>
                                        <td><?= $student_all_data['HGPA']; ?></td>
                                    </tr>
                                    <tr>
                                        <th>Result : </th>
                                        <td><?= $student_all_data['HRESULT']; ?></td>
                                    </tr>
                                    <tr>
                                        <th>Subject GPA : </th>
                                        <td style="font-size: 11px;">
                                            <?php
                                            $sub = explode(',', $student_all_data['HLTRGRD']);
                                            $i=0;
                                            foreach($sub as $key => $value):
                                              //  $subject = get_hsc_subject($sub[$i]);
                                              //  echo $subject['code'].' => '. $subject['name'] . ' : ' .$subject['grade'].'<br>';
                                                  echo $value.'<br>';
                                                $i++;
                                            endforeach;
                                            ?>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>

                        <?php                      
						if ( !empty($student_all_data['SGPA']) && $student_all_data['SGPA'] > 0  && !empty( $student_all_data['HGPA'] )  &&  $student_all_data['HGPA'] > 0   ) {
                            ?>
                        <div class="row" >
                            <div class="col-md-12">
                                        <fieldset>
                                            <legend>Already Applied Units : </legend>
                                        </fieldset>

                                <table class="table table-responsive table-bordered">
                                    <tr>
                                        <th>Application ID</th><th>Unit</th><th>Date</th><th>Payment Status</th><th>Download</th><th>Edit</th>
                                    </tr>
                                    <?php
                                    $LAST_MOB_NUM = '';
                                    $applied_units =  array();
                                    if(!empty($applied_unit)){
                                        foreach ($applied_unit as $key => $value) {
                                            $applied_units[] =  $value['UNIT'];
                                            $LAST_MOB_NUM = $value['MOBNUMBER'];
                                       ?>
                                    <tr>
                                        <td><?php echo $value['APPID']; ?></td>
                                        <td><?php
                                        echo $value['UNIT'];
                                        if($value['UNIT'] == "A" && $value['WITH_ARCHITECTURE'] == "YES"){
                                            echo " (WITH ARCHITECTURE)";
                                        }
                                        ?></td>
                                        <td><?php echo $value['APPLYDATE']; ?></td>
                                        <td><?php
                                            if($value['PMTSTATUS'] == "P"){
                                                echo "<span class=\"btn btn-success\">Paid</span>";
                                            }else{
                                                 echo "<span class=\"btn btn-danger\">Not Paid</span>";
                                            }
                                            ?></td>
                                        <td><a href="<?php echo base_url()."home/app_download/".$value['APPID']."/".$value['REGISID'].""; ?>">Download</a></td>
										<td >
										<?php
										if($value['edited'] == "no"){
										?>
										<a class="btn btn-primary" href="<?php echo base_url()."home/edit/".$value['EDIT_ID']."/".$value['REGISID'].""; ?>">EDIT</a>
										<?php } ?>
										</td>
                                    </tr>
                                    <?php
                                        }
                                    }
                                    ?>
                                </table>
                            </div>
                        </div>

                        <div class="row">
                            <form id="photoUpload" name="photoUpload" action="#" method="post" enctype="multipart/form-data">
                                    <input type="hidden" name="REGISID_PHOTO" id="REGISID_PHOTO" value="<?php echo $REGISID; ?>" />
                                    <div class="col-md-6">
                                            <fieldset>
                                                <legend>Your Photo <span class="my_error">*</span>:</legend>
                                            </fieldset>

                                            <div class="">
											<?php
												$CSS_photo_holder = 'display: none;';
											  if($student_all_data['IMGUPLOAD'] == "Y"){
												  $CSS_photo_holder = '';
											  }
											?>
                                                <div id="photo_holder" style="<?php echo $CSS_photo_holder; ?>  background-color :#E3E3E3; height:220px;width:180px;">
                                                <?php
                                                 $CSS_photo = '';
                                                 $CSS_change_photo = ' display: none;';
												 $CSS_photo_holder_realtime = '';
                                                    if( !empty($student_all_data) && $student_all_data['IMGUPLOAD'] == "Y" ){
                                                        $CSS_photo = ' display:none;';
														$CSS_photo_holder_realtime = ' display:none;';
                                                        $CSS_change_photo = '';
                                                        ?>
                                                      <img height="240px;" width="180px;" src="<?php echo base_url().'uploads/'.$student_all_data['IMGUPLOAD_PATH'].'/'.$student_all_data['REGISID'].'.jpg'.'?'.time(); ?>" alt="photo" />
                                                    <?php
                                                    } ?>
                                                </div>

												<div id="photo_holder_realtime" style=" <?php echo $CSS_photo_holder_realtime; ?> background-color :#E3E3E3; height:240px;width:180px;">
												PREVIEW & UPLOAD
												<img id="image_upload_preview" width="180px"  height="240px;" src="http://placehold.it/180x240" alt="your image" />
												</div>

                                                <?php

                                                 if( !empty($student_all_data) && ( $student_all_data['IMGUPLOAD'] <> "Y" ||  $student_all_data['IMGUPLOA_NO_TIME'] < 2 ) ){
                                                ?>
                                              <div id="upload_button_again" style="<?php  echo $CSS_change_photo; ?> padding-top:20px;">
                                                    <a href="javascript:void" class="btn btn-link" onclick="return showUploadAgain();">Change Photo</a>
                                              </div>
                                            <?php } ?>
                                                <div id="photo_upload_option" style="<?php  echo $CSS_photo; ?>">
                                                    <input name="userImage" id="userImage" type="file" onchange="ValidatePhotoInput(this, '200');" />

                                                    <p style="font-size: 11px;color: blue;">
                                                    Image Size: width:180px,  height:240px
                                                    <br>
                                                    Only '.jpg' Image Format
                                                    <br>
                                                    Max file size 200KB
                                                    </p>
                                                    <span id="photo_upload_error"></span>
                                                    <input type="submit" name="uploadphoto" class="btn btn-success" id="uploadphoto" value="UPLOAD"  />

                                                </div>

                                            </div>

                                    </div>
                            </form>
                    </div>

                        <br><br>
                     <?php
							$stopAPPLY = 'no';
                            if($stopAPPLY == 'no'){
                     ?>
                     <div>
                      <form method="post" action="<?php echo base_url(); ?>home/process" name="application_submit" id="application_submit" autocomplete="off">
                        <input id="s_group" name="s_group" type="hidden" value="<?php echo $student_all_data['SGROUP']; ?>" />
                        <input id="s_gpa" name="s_gpa" type="hidden" value="<?php echo $student_all_data['SGPA']; ?>" />
                        <input id="h_group" name="h_group" type="hidden" value="<?php echo $student_all_data['HGROUP']; ?>" />
                        <input id="h_gpa" name="h_gpa" type="hidden" value="<?php echo $student_all_data['HGPA']; ?>" />
                        <?php
                         if( !empty($student_all_data) && $student_all_data['IMGUPLOAD'] == "Y" ){
                             ?>
                        <input type="hidden" name="is_upload_uploaded" id="is_upload_uploaded" value="yes" />
                         <?php }else{ ?>
                        <input type="hidden" name="is_upload_uploaded" id="is_upload_uploaded" value="no" />
                         <?php } ?>

                        <?php
                         $TGPA = $student_all_data['SGPA'] + $student_all_data['HGPA'];
                        ?>
                        <input id="t_gpa" name="t_gpa" type="hidden" value="<?php echo $TGPA; ?>" />
                                <div class="row">
                                    <div class="col-md-6">
                                        <fieldset>
                                            <legend>Select Unit To Apply <span class="my_error">*</span> : <img id="ajax_loader" src="<?php echo base_url('assets/images/ajax-loader.gif'); ?>" style="display: none;" /> </legend>
                                            <input type="hidden" name="REGISID" id="REGISID" value="<?php echo $REGISID; ?>" />
                                        </fieldset>

                                        <div class=" has-error">
                                            <span class="pd15 bg-success help-block" id="msg_holder_cannot_apply" style="display: none;"></span>
                                        </div>

                                        <div>
                                            <span class="pd15 bg-success help-block" id="msg_holder_can_apply" style="display: none;"></span>
                                        </div>
                                            <?php
                                            $arr_hsc_technical =  array();

                                            if($student_all_data['HBOARD_NAME'] <> 'TEC'){

                                                    $arr_hsc_technical = array(
                                                    'Secretarial Science',
                                                    'Banking',
                                                    'Accounting',
                                                    'Entrepreneurship Development',
                                                    'Shorthand',
                                                    'Machine Tools Operation and Maintenance',
                                                    'Electrical Works and Maintenance',
                                                    'Computer Operation and Maintenance',
                                                    'Agro Machinery',
                                                    'Refrigeration and Air-conditioning',
                                                    'Welding and Fabrication',
                                                    'Automobile',
                                                    'Clothing and Garments Finishing',
                                                    'Drafting Civil',
                                                    'Building Maintenance and Construction',
                                                    'Electronic Control and Communication',
                                                    'Fish Culture and Breeding',
                                                    'Industrial Wood Working',
                                                    'Poultry Rearing and Farming'
                                                );
                                              }

                                        $arr_ssc_technical =array(
                                                'Machine Tools Operation',
                                                'Food Processing and Preservation',
                                                'Refrigeration and Air Conditioning',
                                                'Building Maintenance',
                                                'Computer & Information Technology',
                                                'Civil Construction',
                                                'Automotive',
                                                'Dying, Printing and Finishing',
                                                'General Mechanics',
                                                'Dress Making',
                                                'General Electronics',
                                                'General Electrical Works',
                                                'Electrical Maintenance Works',
                                                'Architectural Drafting with AutoCAD',
                                                'Welding  and Fabrication',
                                                'Knitting',
                                                'Poultry Rearing and Farming',
                                                'Fish Culture and Breeding',
                                                'Wood Working',
                                                'Farm Machinery',
                                                'Agro Based Food',
                                                'Fruit and Vegetable Cultivation',
                                                'Dress Making and Tailoring',
                                                'Civil Drafting with CAD',
                                                'Livestock Rearing and Farming'
                                        );

                                         ?>

                                        <?php if(($student_all_data['HGROUP'] == "SCIENCE" || in_array($student_all_data['HGROUP'], $arr_hsc_technical)) && ($student_all_data['SGROUP'] == "SCIENCE" || in_array($student_all_data['SGROUP'], $arr_ssc_technical))){ ?>

                                            <?php if(!in_array('A', $applied_units)){ ?>
											
											 <table class="table table-responsive table-bordered">
											  <tr><td colspan="3" style="color:red;">
											  A ইউনিট এ আবেদন করার সময় বিশেষ সতর্কতা অবলম্বন করতে বলা হচ্ছে।
											  <br>
											  Without Architecture or With Architecture অপশন পরবর্তী তে পরিবর্তন করার সুযোগ  থাকবে না </td></tr>	
											 <tr>											                                            
											<td style="width:20%;">
                                               <div class="unit_radio_style" style=" font-size:16px;">
                                                    <input type="radio" name="application_unit" id="application_unit" value="A" />
													</div>
											 </td>
											<td  style="width:40%;">  <label>
                                                    UNIT A  															
                                                </label>
												
											
														<div id="arch_showhide" class="radio unit_radio_style" style="padding-left:40px;">
															<input type="radio" name="with_arch" value="NO" checked> Without  Architecture
															<br>
															<input type="radio" name="with_arch" value="YES"> With Architecture																													
														</div>
											</td>			
												<td  style="width:40%;">
												<b> Application Fee </b> <br>   Without  Architecture : Tk.1245 <br> With Architecture : Tk.1345
												</td>		
                                            
											</tr>
											</table>
											
                                            <?php } ?>
                                          <?php  } ?>


                                             <?php if(!in_array('B', $applied_units)){ ?>
											 <table class="table table-responsive table-bordered">
											 <tr>											                                            
											<td style="width:20%;">											 
                                            <div class="unit_radio_style" style=" font-size:16px;">
                                                
                                                    <input type="radio" name="application_unit" id="application_unit" value="B" />
													 
                                                
											 </div>	
											</td>	
											<td  style="width:40%;"> <label>UNIT  B </label>
											</td>
											<td  style="width:40%;">
											Application Fee: Tk.1045
											</td>
											</tr>
											</table>											
                                           
                                            <?php } ?>



                                    </div>
                            </div>

                        <div class="row" id="personal_info_holder" style="display: none;">
                            <div class="col-md-12">
                                        <fieldset>
                                            <legend>Enter Personal Information:</legend>
                                        </fieldset>

                                        <table class="table table-responsive table-bordered">
                                            <tr>
                                                <th>Mobile Number(11 digit) <span class="my_error">*</span> : </th>
                                                <td>
                                                    <input onkeypress="return isNumber(event)" type="number" maxlength="11" autocomplete="false" name="mobile_no" id="mobile_no"  class="form-control" value="<?php echo $LAST_MOB_NUM; ?>">
                                                    <span id="mobile_no_msg"></span>
                                                </td>
                                            </tr>
                                             <tr>
                                                <th>Quota <span class="my_error">*</span>: </th>
                                                <td>
                                                    <?php
                                                    $quota_array = get_quota_array();
                                                    foreach ($quota_array as $key => $value) {
                                                        if($key == "NA"){
                                                        ?>
                                                        <input name="quota" value="<?php echo $key; ?>" checked="true" type="radio" /> <?php echo $value; ?>
                                                    <?php
                                                        }else{
                                                           ?>
                                                        <input name="quota" value="<?php echo $key; ?>" type="radio" /> <?php echo $value; ?>
                                                  <?php
                                                        }
                                                    }
                                                    ?>
                                                </td>
                                            </tr>
                                           <tr>
                                                <th>Home District <span class="my_error">*</span> : </th>
                                                <td>
                                                    <?php
                                                    $id_district = 'class="form-control" id="district"';
                                                    $dis_array = get_district_array();
                                                    echo form_dropdown('district',$dis_array,'',$id_district);
                                                    ?>

													<span id="district_msg"></span>
                                                </td>
                                            </tr>
											<tr>
                                                <th>What version of question paper do you prefer in PUST admission test 2019-2020 ? <span class="my_error"> *</span>: </th>
                                                <td>
                                                    <div class="form-group">
                                                        <select class="form-control" id="exam_question_tpe" name="exam_question_tpe">
                                                         <option value="">Select  your preference</option>
														   <?php
															$exam_array = array('Bangla','English');

															foreach ($exam_array as $key => $value ) {
																?>
																<option value="<?php echo $value; ?>"><?php echo $value; ?></option>
																<?php
															  }  ?>

                                                    </select>
                                                        <span id="exam_question_tpe_msg"></span>
                                                        </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                             </div>

                            <div class="row">
                                <div class="col-md-12">
                                <hr>
                                <span class="help-block">Required <span class="required" aria-required="true">*</span></span>
                                <div>
                                    <span id="mobile_no_msg_"></span>
                                    <span id="photo_upload_error_"></span>
									<span id="district_msg_"></span>
									<span id="exam_question_tpe_msg_"></span>
                                </div>
                                <span id="submit_btn_holder" style="display: none;">
                                    <input type="submit" value="SUBMIT" name="submit_form" id="submit_form" class="btn btn-primary pull-right" onclick="return validateFromData();" />
                                </span>
                                </div>
                            </div>
                   </form>
                 </div>
               <?php } ?>
              <?php } ?>
                </div>

            </div>
        </div>
    </div>
</div>
</div>
<?php $this->load->view('common/footer'); ?>
