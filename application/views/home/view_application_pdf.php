<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title><?= UNIVERSITY_NAME ?></title>

    <style type="text/css">
        ::selection {
            background-color: #E13300;
            color: white;
        }

        ::-moz-selection {
            background-color: #E13300;
            color: white;
        }

        body {
            background-color: #fff;
            margin: 40px;
            font: 13px/20px normal Helvetica, Arial, sans-serif;
            color: #4F5155;
        }

        a {
            color: #003399;
            background-color: transparent;
            font-weight: normal;
        }

        h1 {
            color: #444;
            background-color: transparent;
            border-bottom: 1px solid #D0D0D0;
            font-size: 19px;
            font-weight: normal;
            margin: 0 0 14px 0;
            padding: 14px 15px 10px 15px;
        }

        code {
            font-family: Consolas, Monaco, Courier New, Courier, monospace;
            font-size: 12px;
            background-color: #f9f9f9;
            border: 1px solid #D0D0D0;
            color: #002166;
            display: block;
            margin: 14px 0 14px 0;
            padding: 12px 10px 12px 10px;
        }

        #body {
            margin: 0 15px 0 15px;
        }

        p.footer {
            text-align: right;
            font-size: 11px;
            border-top: 1px solid #D0D0D0;
            line-height: 32px;
            padding: 0 10px 0 10px;
            margin: 20px 0 0 0;
        }

        #container {
            margin: 10px;
            border: 1px solid #D0D0D0;
            box-shadow: 0 0 8px #D0D0D0;
        }

        table td {
            font-size: 13px;
            padding: 5px;
        }
    </style>
    <link rel="stylesheet" href="<?= base_url('assets/bootstrap/css/bootstrap.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/font-awesome/css/font-awesome.min.css'); ?>">
</head>

<body>

    <div id="container">
        <div style="text-align:center;border-bottom: 1px solid #D0D0D0;">
            <h2><?php echo UNIVERSITY_NAME; ?></h2>
            <h3>Session: 2020-2021</h3>
        </div>

        <div style="font-size:14px;padding:5px;">
            <?php
            if ($application_data['PMTSTATUS']  == "U") {
                echo "Application Confirmation Status: NOT PAID";
            } else {
                echo "Application Confirmation Status: PAID";
            }
            ?>
        </div>
        <div style="width: 100%;">
            <table class="table table-responsive table-bordered" cellpadding="3">
                <tr>
                    <td valign="top" style="border: 0px;">
                        <table class="table table-responsive table-bordered" cellpadding="3">
                            <tr>
                                <td colspan="3"><b> Application Information</b></td>
                            </tr>
                            <tr>
                                <td>Applicant's Photo</td>
                                <td></td>
                                <td>
                                    <?php
                                    $path = 'photos/' . strtolower($application_data['UNIT'])  . '/images_' . strtolower($application_data['UNIT']) . '/' . $application_data['ROLLNO_FINAL'] . '.jpg';
                                    $applicant_photo_url = asset_url($path); ?>
                                    <img height="100px;" width="80px;" src="<?php echo $applicant_photo_url . '?' . time(); ?>" alt="photo" />
                                </td>
                            </tr>
                            <tr>
                                <td>Application ID</td>
                                <td></td>
                                <td><b><?php echo $application_data['APPID']; ?></b></td>
                            </tr>
                            <?php if (!empty($application_data['ROLLNO'])) : ?>
                                <tr>
                                    <td>RUB Roll Number</td>
                                    <td></td>
                                    <td><b>rub<?php echo $application_data['ROLLNO']; ?></b></td>
                                </tr>
                            <?php endif; ?>

                            <tr>
                                <td>Unit</td>
                                <td></td>
                                <td><b>
                                        <?php
                                        echo $application_data['UNIT'];

                                        if ($application_data['WITH_ARCHITECTURE'] == "YES") {
                                            echo " (With Music Subject)";
                                        }

                                        ?>
                                    </b></td>
                            </tr>
                            <!-- <tr>
                                <td>Question Paper Version</td>
                                <td></td>
                                <td><?php echo $application_data['EXAM_VERSION']; ?></td>
                            </tr>

                            <tr>
                                <td>Home District</td>
                                <td></td>
                                <td><?php echo $application_data['HDISTRICT']; ?></td>
                            </tr>

                            <tr>
                                <td>REG. ID</td>
                                <td></td>
                                <td><?php echo $application_data['REGISID']; ?></td>
                            </tr> -->
                            <tr>
                                <td>Applicant's Name</td>
                                <td></td>
                                <td><?php echo $student_all_data['HNAME']; ?></td>
                            </tr>
                            <tr>
                                <td>Father's Name</td>
                                <td></td>
                                <td><?php echo $student_all_data['HFNAME']; ?></td>
                            </tr>
                            <tr>
                                <td>Mother's Name</td>
                                <td></td>
                                <td><?php echo $student_all_data['HMNAME']; ?></td>
                            </tr>

                            <tr>
                                <td>Quota</td>
                                <td></td>
                                <td><?php
                                    $qname_arr = get_quota_array();
                                    echo  $qname_arr[$application_data['QNAME']];
                                    ?></td>
                            </tr>
                            <tr>
                                <td>Applicant's Mobile No</td>
                                <td></td>
                                <td><?php echo $application_data['MOBNUMBER']; ?></td>
                            </tr>
                        </table>

                        <table class="table table-responsive table-bordered" cellpadding="3">
                            <tr>
                                <td colspan="6"><b>S.S.C/Equivalent</b></td>
                            </tr>
                            <tr>
                                <td>Roll No</td>
                                <td></td>
                                <td><?php echo $student_all_data['SROLL_NO']; ?></td>
                                <td>Reg. No</td>
                                <td></td>
                                <td><?php echo $student_all_data['SREG_NO']; ?></td>
                            </tr>
                            <tr>
                                <td>Board</td>
                                <td></td>
                                <td><?php echo $student_all_data['SBOARD_NAME']; ?></td>
                                <td>Passing Year</td>
                                <td></td>
                                <td><?php echo $student_all_data['SPASS_YEAR']; ?></td>
                            </tr>
                            <tr>
                                <td>GPA</td>
                                <td></td>
                                <td><?php echo $student_all_data['SGPA']; ?></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td colspan="6"><b>H.S.C/Equivalent</b></td>
                            </tr>
                            <tr>
                                <td>Roll No</td>
                                <td></td>
                                <td><?php echo $student_all_data['HROLL_NO']; ?></td>
                                <td>Reg. No</td>
                                <td></td>
                                <td><?php echo $student_all_data['HREG_NO']; ?></td>
                            </tr>
                            <tr>
                                <td>Board</td>
                                <td></td>
                                <td><?php echo $student_all_data['HBOARD_NAME']; ?></td>
                                <td>Passing Year</td>
                                <td></td>
                                <td><?php echo $student_all_data['HPASS_YEAR']; ?></td>
                            </tr>
                            <tr>
                                <td>GPA</td>
                                <td></td>
                                <td><?php echo $student_all_data['HGPA']; ?></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>

                        </table>

                        <table class="table table-responsive table-bordered" cellpadding="3">
                            <tr>
                                <td colspan="6"><b>Payment Information</b></td>
                            </tr>
                            <tr>
                                <td>
                                    <?php
                                    if ($application_data['PMTSTATUS']  == "U") {
                                        echo "Amount to Pay<br>";
                                        // echo "<span style=\"font-size:10px;\">(please ignore if already paid)</span>";
                                    } else {
                                        echo "Amount Paid";
                                    } ?>
                                </td>
                                <td></td>
                                <td><?php echo $application_data['AMOUNT_TO_PAY']; ?> TAKA</td>
                                <td>Status</td>
                                <td></td>
                                <td>
                                    <?php
                                    if ($application_data['PMTSTATUS']  == "U") {
                                        echo "NOT PAID";
                                    } else {
                                        echo "PAID";
                                    }
                                    ?>
                                </td>
                            </tr>
                            <?php
                            if ($application_data['PMTSTATUS']  == "P") {
                            ?>
                                <tr>
                                    <td>Transaction ID</td>
                                    <td></td>
                                    <td><?php
                                        echo str_pad(substr($application_data['TXNID'], -4), strlen($application_data['TXNID']), "*", STR_PAD_LEFT);; ?></td>
                                    <td>Payment Date</td>
                                    <td></td>
                                    <td><?php
                                        echo  $application_data['PMTDATE'];
                                        ?>
                                    </td>
                                </tr>
                            <?php
                            }
                            ?>
                            <!-- <tr>
                                <td colspan="6" style="font-size:10px;">
                                    <?php
                                    if ($application_data['PMTSTATUS']  == "U") {
                                    ?>
                                        Important Instructions:
                                        <br>
                                        [1] Please pay BDT <?php echo $application_data['AMOUNT_TO_PAY']; ?> via Sure Cash against your Application <?php echo $application_data['APPID']; ?>
                                        <br>
                                        [2] After payment login and download your confirmation slip again. Status will be PAID.
                                        <br>
                                        [3] Please print this document until get your Admitcard.
                                        <br>
                                        [4] Admit card will be available after 25th October, 2019.
                                        <br>
                                        [5] For more information visit www.pust.ac.bd

                                    <?php
                                    } else {
                                    ?>
                                        Important Instructions:
                                        <br>
                                        [1] Please print this document until get your Admitcard.
                                        <br>
                                        [2] Admit card will be available after 25th October, 2019.
                                        <br>
                                        [3] For more information visit www.pust.ac.bd
                                    <?php
                                    } ?>
                                </td>
                            </tr> -->
                        </table>
                    </td>
                </tr>
            </table>

        </div>
    </div>

</body>

</html>