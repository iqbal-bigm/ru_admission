<?php $this->load->view('common/header'); ?>
<?php $this->load->view('common/navbar'); ?>
<style>
    #myform_ask .error {
    color: red;
    font-style: italic;
}
</style>
<div id="main">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">Complain</div>

                    <div class="panel-body">
					<?php $ok = "10"; if($ok==200){ ?>
                                <div style="padding-left: 20px;">
                                   
								   Send your complain with details, tracking id and  mobile number to <b>bigmsoftpust@gmail.com</b> , we will contact with you within short time.
								   
                                 </div> <?php } ?>
								 
					
                        <div class="row">
                            <div>
                                <?php
                                if(!empty($validMsg)){
                                    ?>
                                <div style="color: blue;padding-left: 20px;">
                                   <h3><?php echo $validMsg; ?> </h3>
                                 </div>
                                <?php }
                                ?>
                            </div>

                            <div>
                                  <?php
                                  if( !empty( $this->session->flashdata('notification')) ){
                                    ?>
                                      <div style="color:red !important; padding-left: 20px;">
                                        <h3>
                                        <?php echo $this->session->flashdata('notification'); ?>
                                        <h3>
                                      </div>
                              <?php    }

                                  ?>
                            </div>
							<div style="text-align:center;">
								<h3>Submit your complain & we will contact with you within short time.</h3>
							</div>

                            <form action="<?php echo base_url() . 'ask/save' ?>" method="post" id="myform_ask" autocomplete="off">
                                <table class="table table-responsive table-bordered">
                                    <tr>
                                        <th>Name *</th>
                                        <td>
                                            <input type="text" name="name" id="name" required  class="form-control">
                                            <span id="app_id_msg" class="form-group <?php if(form_error('name')) echo "has-error"; ?>"> <?php  if(form_error('name')) echo form_label(form_error('name')); ?></span>

                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Login Phone Number *</th>
                                        <td>
                                            <input type="text" name="phone" id="phone"  required  class="form-control" value="<?php echo $this->session->userdata('student_login_mobile_no'); ?>">
                                            <span id="tans_id_msg" class="form-group <?php if(form_error('phone')) echo "has-error"; ?>"><?php  if(form_error('phone')) echo form_label(form_error('phone')); ?></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>GST Application ID *</th>
                                        <td>
                                            <input type="text" name="gstappid" id="gstappid"  required  class="form-control" value="<?php echo $this->session->userdata('gstappid'); ?>">
                                            <span id="tans_id_msg" class="form-group <?php if(form_error('gstappid')) echo "has-error"; ?>"><?php  if(form_error('gstappid')) echo form_label(form_error('gstappid')); ?></span>
                                        </td>
                                    </tr>								
                                    <tr>
                                        <th>
                                            Problem Details *
                                            <br>
                                            (Write your problem in details)
                                        </th>
                                        <td>
                                            <textarea class="form-control" cols="50" rows="5" name="details" id="details" required></textarea>
                                             <span id="details_msg" class="form-group <?php if(form_error('details')) echo "has-error"; ?>"><?php  if(form_error('details')) echo form_label(form_error('details')); ?></span>
                                        </td>
                                    </tr>
									<tr>
                                        <th></th>
                                        <td>
                                          <?php //echo $widget; ?>
                                          <?php //echo $script; ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <input type="submit" value="SUBMIT" name="submit_form" id="submit_form" class="btn btn-primary pull-right"/>
                                        </td>
                                    </tr>
                                </table>
                            </form>
                        </div>
						
                        <hr>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
  $("#myform_ask").validate();
</script>
<?php $this->load->view('common/footer'); ?>
