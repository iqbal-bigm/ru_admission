<?php $this->load->view('common/header'); ?>
<?php $this->load->view('common/navbar'); ?>

<div id="main">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">Payment Instruction</div>

                    <div class="panel-body">
                        <div class="row">

                                   <div style="color:red; font-size:16px;padding-left: 200px;" >After completing payment, payment information will auto update into web site</div>
                                   <table class="table table-responsive table-bordered">
                                       <tr><td>
                                           1. এপ্লিকেশন সম্পন্ন হলে পেমেন্ট এর অপশন আসবে. Pay Now বাটনে ক্লিক করলে পেমেন্ট এর অপশন আসবে <br>
                                           2. পেমেন্ট SSLCOMMERZ এর গেটওয়ে ব্যবহার করে করতে হবে <br>
                                           3. পেমেন্ট সম্পর্ণ হলে, ওয়েবসাইট এ পেমেন্ট স্টাট্যাস আপডেট হবে এবং একটি রোল নাম্বার প্রদান করা হবে <br>
                                           <span style="color: red;"> 4. পেমেন্ট চলাকালিন কোনোভাবে ব্রাউজার বন্ধ করা যাবে না  </span>
                                           </td></tr>
                                       <tr>
                                           <td><img width="90%" src="<?php echo base_url('how-to-pay.png'); ?>" /></td>
                                       </tr>
                                   </table>

                        </div>
                        <hr>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('common/footer'); ?>
