<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>PUST Subject Option Form</title>
    <style>
        .column {
            float: left;
            width: 50%;
            padding: 5px;
        }

        /* Clearfix (clear floats) */
        .row::after {
            content: "";
            clear: both;
            display: table;
        }

        table th {
            text-align: left;
        }

        .tbl_merit th {
            background-color: #EEEEEE;
        }

        table {
            margin-bottom: 10px;
        }
    </style>
</head>

<body>

    <table border="1" style="width: 100%;border:1px solid #cacaca;border-collapse: collapse;">
        <tr>
            <td style="width: 15%">
                <img style="width: 60px; height: auto" alt="<?php echo UNIVERSITY_NAME; ?>" src="<?php echo base_url(); ?>assets/images/logo.png" />
            </td>
            <td style="text-align: center;width: 70%" colspan="2" valign="top">
                <span style="top:0px;font-size: 20px;font-weight:bold"><?php echo UNIVERSITY_NAME; ?></span>
                <br>
                <span style="font-size:16px;"><b>1st Year Admission (Session: 2020-2021)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></span>
                <br>
                <span style="font-size:18px;color:#0000FF;"><b> Subject Choice Form<b></span>
            </td>
            <td style="width: 15%;text-align: center;">
                <span style="font-size: 14px;font-weight: bold;">
                    <?php
                    echo $unit_details['unit'];
                    if ($unit_details['unit'] == "B") {
                        //  echo "<br>(" . $unit_details['c_section_hsc_group'] . ")";
                    }
                    if ($unit_details['unit'] == "A") {
                        // echo "<br>(" . $unit_details['optional'] . ")";
                    }
                    ?>
                    <br>
                    <?php //echo $unit_details['merit']; 
                    ?>
                </span>
            </td>
        </tr>
    </table>
    <table class="tbl_merit" border="1" width="100%" cellpadding="3" style="border:1px solid #cacaca;border-collapse: collapse;font-size: 12px;">
        <tr>
            <th>Payment Status</th>
            <td><?= ($application_pust['PMTSTATUS'] == 'P') ? 'Paid' : 'Unpaid'; ?></td>
            <th>Transaction ID</th>
            <td><?php if (!empty($application_pust['TXNID'])) {
                    echo str_pad(substr($application_pust['TXNID'], -4), strlen($application_pust['TXNID']), "*", STR_PAD_LEFT);
                } ?></td>
            <td rowspan="4"><img style="width: 60px; height: auto" alt="Photo" src="<?php echo base_url() . 'photos/' . strtolower($unit_details['unit'])  . '/images_' . strtolower($unit_details['unit']) . '/' . $unit_details['exam_roll'] . '.jpg'; ?>" /></td>
        </tr>
        <tr>
            <th>Payment Date</th>
            <td><?php if (!empty($application_pust['PMTDATE'])) {
                    echo $application_pust['PMTDATE'];
                } ?></td>
            <th>Unit</th>
            <td><?php
                echo $unit_details['unit'];
                // if ($unit_details['unit'] == "B") {
                //     echo "<br>(" . $unit_details['c_section_hsc_group'] . ")";
                // }
                ?>
            </td>
        </tr>
        <tr>
            <th>GST Score</th>
            <td><?php echo $student_acadamic_data['gst_score']; ?></td>
            <th>Quota</th>
            <td>
                <?php
                if (!empty($unit_details['quota_name'])) {
                    $quota_name_arr = get_quota_array();
                    echo $quota_name_arr[$unit_details['quota_name']];
                }

                ?>
            </td>
        </tr>
        <tr>
            <th>GST Roll Number</th>
            <td><?php echo $unit_details['exam_roll']; ?></td>
            <th>GST App. ID</th>
            <td><?php echo $unit_details['app_id']; ?></td>
        </tr>
    </table>
    <table border="1" width="100%" cellpadding="3" style="border:1px solid #cacaca;border-collapse: collapse;font-size: 12px;">
        <?php $arr_all_subject_code_name = get_all_subject_code_name(); ?>
        <tr>
            <th colspan="6" style="text-align: left;padding-left: 10px;">
                Subject Choice Order: <?php if ($application_pust['WITH_ARCHITECTURE'] == 'YES') : ?><span>(* Architecture is also selected for you.)</span><?php endif; ?>
            </th>
        </tr>
        <?php
        $choice_subjects = [];
        foreach ($unit_wise_subject_list_existing as $key => $choice_subject) {
            $choice_subjects[$choice_subject['choise_order']] = $arr_all_subject_code_name[$choice_subject['sub_code_ref']];
        }
        // dd($choice_subjects);
        $total = count($unit_wise_subject_list_existing);
        // $total = 8;
        $no_of_row = ceil($total / 3);
        ?>
        <?php for ($i = 1; $i <= $no_of_row; $i++) :
            $index_1 = ($no_of_row * 0) + $i;
            $index_2 = ($no_of_row * 1) + $i;
            $index_3 = ($no_of_row * 2) + $i;

        ?>
            <tr>
                <td style="white-space:nowrap;"><?php if (isset($choice_subjects[$index_1])) echo  $index_1 ?></td>
                <td style="white-space:nowrap;"><?php if (isset($choice_subjects[$index_1])) echo  $choice_subjects[$index_1]; ?></td>
                <td style="white-space:nowrap;"><?php if (isset($choice_subjects[$index_2])) echo $index_2  ?></td>
                <td style="white-space:nowrap;"><?php if (isset($choice_subjects[$index_2])) echo  $choice_subjects[$index_2] ?></td>
                <td style="white-space:nowrap;"><?php if (isset($choice_subjects[$index_3])) echo $index_3  ?></td>
                <td style="white-space:nowrap;"><?php if (isset($choice_subjects[$index_3])) echo  $choice_subjects[$index_3] ?></td>
            </tr>
        <?php endfor; ?>

    </table>

    <table border="1" class="tbl_merit" width="100%" cellpadding="5" style="border:1px solid #cacaca;border-collapse: collapse;font-size: 11px;">
        <tr>
            <td colspan="4"><b>Personal Information:</b></td>
        </tr>
        <tr>
            <th colspan="1">Name</th>
            <td colspan="3"><?php echo $student_acadamic_data['SNAME']; ?></td>
        </tr>
        <tr>
            <th>Father's Name</th>
            <td><?php echo $student_acadamic_data['SFNAME']; ?></td>

            <th>Mother's Name</th>
            <td><?php echo $student_acadamic_data['SMNAME']; ?></td>
        </tr>
        <tr>
            <th>Nationality</th>
            <td>Bangladeshi</td>
            <th>Home District</th>
            <td><?php echo $std_per_data['district']; ?></td>

        </tr>

        <tr>
            <th>Marital Status</th>
            <td><?php echo $std_per_data['maritlal_status']; ?></td>
            <th>Gender</th>
            <td><?php echo $std_per_data['gender']; ?></td>
        </tr>

        <tr>
            <th>Date of Birth</th>
            <td><?php echo $std_per_data['dob']; ?></td>
            <th>Mobile No.</th>
            <td><?php echo $std_per_data['mobile_no']; ?></td>

        </tr>


        <tr>
            <th>Local Guardian's Name</th>
            <td><?php echo $std_per_data['local_gurdian_name']; ?></td>
            <th>Religion</th>
            <td><?php echo $std_per_data['religion']; ?></td>

        </tr>
        <tr>
            <th>Relation With Local Guardian</th>
            <td><?php echo $std_per_data['local_gurdian_realation']; ?></td>
            <th>Local Guardian's Mobile No.</th>
            <td><?php echo $std_per_data['local_gurdian_phone']; ?></td>

        </tr>
        <tr>
            <th>Permanent Address</th>
            <td>
                <?php
                //echo $std_per_data['permanent_address'];
                echo "Village/Ward: " . $std_per_data['padd_village'] . ", House: " . $std_per_data['padd_house'] . ", ";
                echo "<br>";
                echo "Road: " . $std_per_data['padd_road'] . ", Post Office: " . $std_per_data['padd_post_office'] . ", ";
                echo "<br>";
                echo "Upazila: " . $std_per_data['padd_police_station'] . ", District: " . $std_per_data['padd_district'];
                ?>
            </td>
            <th>Present Address</th>
            <td><?php //echo $std_per_data['mailing_address'];
                //echo $std_per_data['permanent_address'];
                echo "Village/Ward: " . $std_per_data['prad_village'] . ", House: " . $std_per_data['prad_house'] . ", ";
                echo "<br>";
                echo "Road: " . $std_per_data['prad_road'] . ", Post Office: " . $std_per_data['prad_post_office'] . ", ";
                echo "<br>";
                echo "Upazila: " . $std_per_data['prad_police_station'] . ", District: " . $std_per_data['prad_district'];
                ?></td>

        </tr>
    </table>
    <table class="tbl_merit" border="1" width="100%" cellpadding="2" style="border:1px solid #cacaca;border-collapse: collapse;font-size: 11px;">
        <tr>
            <td><b>Educational Information:</b></td>
        </tr>
        <tr>
            <th>Exam. Name</th>
            <th>Group</th>
            <th>ROLL NO.</th>
            <th>REG. NO.</th>
            <th>GPA</th>
            <th>Board</th>
            <th>Year</th>
        </tr>
        <tr>
            <th style="text-align: left;">SSC/ Equivalent</th>
            <td><?php echo $student_acadamic_data['SGROUP']; ?>
            <td><?php echo $student_acadamic_data['SROLL_NO']; ?></td>
            <td><?php echo $student_acadamic_data['SREG_NO']; ?></td>
            <td><?php echo $student_acadamic_data['SGPA']; ?></td>
            <td><?php echo $student_acadamic_data['SBOARD_NAME']; ?></td>
            <td><?php echo $student_acadamic_data['SPASS_YEAR']; ?></td>
        </tr>
        <tr>
            <th style="text-align: left;">HSC/ Equivalent</th>
            <td><?php echo $student_acadamic_data['HGROUP']; ?>
            <td><?php echo $student_acadamic_data['HROLL_NO']; ?></td>
            <td><?php echo $student_acadamic_data['HREG_NO']; ?></td>
            <td><?php echo $student_acadamic_data['HGPA']; ?></td>
            <td><?php echo $student_acadamic_data['HBOARD_NAME']; ?></td>
            <td><?php echo $student_acadamic_data['HPASS_YEAR']; ?></td>
        </tr>

    </table>
    <table border="1" width="100%" cellpadding="5" style="border:1px solid #cacaca;border-collapse: collapse;font-size: 11px;">
        <tr>
            <td><b>Declaration:</b></td>
        </tr>
        <tr>
            <td>
                <p>
                    I,<?php echo strtoupper($student_acadamic_data['SNAME']); ?>, do hereby declare that, (A) I, myself fill‐up this form and check properly and the above
                    mentioned information and photo are correct. (B) If any information provided by me is found false, Pabna University of
                    Science and Technology reserves the right to cancel my admission. (C) I shall be obliged to obey the rules and
                    regulations adopted by Sheikh Hasina University and to pay all the required fees.
                </p>
            </td>
        </tr>
    </table>
    <table border="0" width="100%" cellpadding="5" style="border:0px solid #cacaca;border-collapse: collapse;font-size: 12px;">
        <tr>
            <th colspan="100%"><br></th>
        </tr>
        <tr>

            <th colspan="50%" style="text-align: left !important;">
                ___________________________________________________<br>
                Signature of Father/Mother/Guardian & Date</th>
            <th align="left" colspan="50%" style="text-align: right !important;">
                ____________________________________<br>
                Signature of the Student & Date
            </th>
        </tr>

    </table>
    <table width="100%" style="border: 1px solid black;">
        <tr>
            <td colspan="2" style="text-align: left;font-size: 9px;text-align: center;">
                © 2020-2021; <?php echo UNIVERSITY_NAME; ?>. All rights reserved.
            </td>
        </tr>
    </table>


</body>

</html>