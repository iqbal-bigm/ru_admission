<?php $this->load->view('common/header'); ?>
<?php $this->load->view('common/navbar'); ?>
<style>
    #cancel_form .error {
        color: red;
        font-style: italic;
    }
</style>

<div id="main">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel-body">
                        <form action="<?php echo base_url('dashboard/admission_cancel_save'); ?>" method="post" name="cancel_form" id="cancel_form">
                            <div class="row">
                                <div class="col-md-12">
                                    <fieldset>
                                        <legend>Application for admission cancel</legend>
                                    </fieldset>

                                    <table class="table table-responsive table-bordered">
                                        <tr>
                                            <th>
                                                To
                                                <br>
                                                The Additional Registrar
                                                <br>
                                                Sheikh Hasina University
                                                <br>
                                                Pabna-6600.
                                            </th>
                                        </tr>

                                        <tr>
                                            <th>
                                                <?php
                                                $arr_all_subject_dep_name = get_all_subject_dep_name();
                                                $arr_all_subject_code_name = get_all_subject_code_name();
                                                ?>
                                                Through: Chairman, Department of <i><?php echo $arr_all_subject_dep_name[$sub_code]; ?></i>, PUST.
                                            </th>
                                        </tr>


                                        <tr>
                                            <td>

                                                Respected Sir,
                                                <br>
                                                I am writing this letter to request for the cancellation of admission (<b>Subject: <?php echo $arr_all_subject_code_name[$sub_code]; ?></b>) from your university. I regret to inform you that I wish to cancel my admission from your University due to the following causes:
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>

                                                <div class="form-group">
                                                    ( NOTE: Please write in english )
                                                    <br>
                                                    <textarea id="cancel_reason" name="cancel_reason" rows="5" cols="100" required></textarea>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>
                                                I request you with honor to cancel my admission and return me the documents that I have been submitted (all original documents) at the admission desk to the department. I would like to inform you, I have paid admission cancellation fee of Tk. 2000
                                                <br>
                                                I am looking forward for an early response from you.
                                            </th>
                                        </tr>
                                    </table>
                                </div>

                            </div>

                            <div class="row">

                                <div class="col-md-12" style="text-align: center;">

                                    <input type="hidden" id="sub_code" name="sub_code" value="<?php echo $sub_code; ?>">
                                    <input type="hidden" id="unique_id" name="unique_id" value="<?php echo $unique_id; ?>">
                                    <input type="hidden" id="exam_roll" name="exam_roll" value="<?php echo $exam_roll; ?>">
                                    <input type="hidden" id="unit_name" name="unit_name" value="<?php echo $unit_name; ?>">

                                    <?= form_submit('submit', 'CONFIRM AND DOWNLOAD', ['class' => 'btn btn-primary']); ?>

                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
</div>
</div>
<script>
    $("#cancel_form").validate();
</script>
<?php $this->load->view('common/footer'); ?>