<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>PUST Admission</title>
    <style>
        table th {
            text-align: left;
        }

        .tbl_merit th {
            background-color: #EEEEEE;
        }
    </style>
</head>

<body>
    <?php
    foreach ($MAIN_DATA as $MAIN_DATA_key => $MAIN_DATA_value) {
    ?>

        <table border="0" style="width: 100%;border:0px solid #cacaca;border-collapse: collapse;">
            <thead>
                <tr>
                    <td style="width: 15%">
                        <img style="width: 50px; height: auto;text-align: center;" alt="<?php echo UNIVERSITY_NAME; ?>" src="<?php echo base_url(); ?>assets/images/logo.png" />
                    </td>
                    <td style="text-align: center;width: 70%" colspan="2" valign="top">
                        <span style="top:0px;font-size: 20px;font-weight:bold"><?php echo UNIVERSITY_NAME; ?></span>
                        <br> <br>
                        <span style="font-size:16px;"><b>Student's Registration Form</b></span>
                    </td>
                    <td style="width: 15%;text-align: center;">
                        <img style="width: 50px; height: auto" alt="Photo" src="<?php echo "http://admission1920.pust.ac.bd/"; //base_url(); 
                                                                                ?>uploads/<?php echo $MAIN_DATA_value['student_acadamic_data']['IMGUPLOAD_PATH']; ?>/<?php echo $MAIN_DATA_value['my_result']['system_reg_id_ref']; ?>.jpg" />
                    </td>
                </tr>
            </thead>
        </table>
        <br>
        <?php
        $arr_all_subject_code_name = get_all_subject_code_name();
        $arr_all_subject_faculty_name = get_all_subject_faculty_name();
        $arr_all_subject_dep_name = get_all_subject_dep_name();
        $arr_all_course_name = get_all_course_name();
        ?>
        <table class="tbl_merit" border="1" width="100%" cellpadding="1" style="border:1px solid #cacaca;border-collapse: collapse;font-size: 12px;">
            <tr>
                <th style="text-align: center;">Student ID No.</th>
                <th style="text-align: center;">Session</th>
                <th style="text-align: center;">Subject</th>
                <th style="text-align: center;">Course</th>
                <th style="text-align: center;">Department Code</th>
                <th style="text-align: center;">Class Roll No.</th>
                <th style="text-align: center;">Registration No.</th>
            </tr>
            <tr>
                <td style="text-align: center;"><?php echo $MAIN_DATA_value['my_result']['uni_id_no']; ?></td>
                <td style="text-align: center;">2019-2020</td>
                <td style="text-align: center;"><?php echo $arr_all_subject_code_name[$MAIN_DATA_value['my_result']['sub_code_7']]; ?></td>
                <td style="text-align: center;"><?php echo $arr_all_course_name[$MAIN_DATA_value['my_result']['sub_code_7']]; ?></td>
                <td style="text-align: center;"><?php echo $MAIN_DATA_value['my_result']['sub_code_7']; ?></td>
                <td style="text-align: center;"><?php echo $MAIN_DATA_value['my_result']['class_roll']; ?></td>
                <td style="text-align: center;"><?php echo $MAIN_DATA_value['my_result']['class_reg']; ?></td>
            </tr>

        </table>
        <br>
        <table class="tbl_merit" border="1" width="100%" cellpadding="3" style="border:1px solid #cacaca;border-collapse: collapse;font-size: 10px;">
            <tr>
                <th style="text-align: center;">Amount Paid</th>
                <th style="text-align: center;">Payment Date</th>
                <th style="text-align: center;">Transaction ID</th>
                <th style="text-align: center;">Unit</th>
                <th style="text-align: center;">Admission Roll.</th>
                <th style="text-align: center;">Merit</th>
                <th style="text-align: center;">Score</th>
                <th style="text-align: center;">Quota</th>
            </tr>
            <tr>
                <td style="text-align: center;"><?php echo  $MAIN_DATA_value['my_result']['paid_amount']; ?></td>
                <td style="text-align: center;"><?php echo  $MAIN_DATA_value['my_result']['payment_date']; ?></td>
                <td style="text-align: center;"><?php echo  $MAIN_DATA_value['my_result']['payment_trans_id']; ?></td>
                <td style="text-align: center;"><?php echo  $MAIN_DATA_value['unit_details']['unit']; ?></td>
                <td style="text-align: center;"><?php echo  $MAIN_DATA_value['unit_details']['exam_roll']; ?></td>
                <td style="text-align: center;"><?php echo  $MAIN_DATA_value['unit_details']['merit']; ?></td>
                <td style="text-align: center;"><?php echo  $MAIN_DATA_value['unit_details']['score']; ?></td>
                <td style="text-align: center;"><?php echo  $MAIN_DATA_value['unit_details']['quota_name']; ?></td>
            </tr>

        </table>
        <br>
        <table border="1" class="tbl_merit" width="100%" cellpadding="5" style="border:1px solid #cacaca;border-collapse: collapse;font-size: 10px;">
            <tr>
                <th colspan="2">1 (a). Name of the Student (In Bangla)</th>
                <td colspan="2"><br> ________________________________________________________</td>
            </tr>
            <tr>
                <th colspan="2">1 (b). Name of the Student (In English)</th>
                <td colspan="2"><?php echo $MAIN_DATA_value['student_acadamic_data']['SNAME']; ?></td>
            </tr>

            <tr>
                <th>2. Father's Name</th>
                <td><?php echo $MAIN_DATA_value['student_acadamic_data']['SFNAME']; ?></td>

                <th>3. Mother's Name</th>
                <td><?php echo $MAIN_DATA_value['student_acadamic_data']['SMNAME']; ?></td>
            </tr>
            <tr>
                <th>4. Father's Occupation</th>
                <td><?php echo $MAIN_DATA_value['std_per_data']['father_occupation']; ?></td>

                <th>5. Mothers's Occupation</th>
                <td><?php echo $MAIN_DATA_value['std_per_data']['mother_occupation']; ?></td>
            </tr>
            <tr>
                <th>6. Yearly Family Income (in taka)</th>
                <td><?php echo $MAIN_DATA_value['std_per_data']['family_income']; ?></td>

                <th>7. BNCC/Rover Scout</th>
                <td><?php echo $MAIN_DATA_value['std_per_data']['scout']; ?></td>
            </tr>
            <tr>
                <th>8. Nationality</th>
                <td>Bangladeshi</td>
                <th>9. Home District</th>
                <td><?php echo $MAIN_DATA_value['std_per_data']['district']; ?></td>

            </tr>

            <tr>
                <th>10. Marital Status</th>
                <td><?php echo $MAIN_DATA_value['std_per_data']['maritlal_status']; ?></td>
                <th>11. Gender</th>
                <td><?php echo $MAIN_DATA_value['std_per_data']['gender']; ?></td>
            </tr>

            <tr>
                <th>12. Date of Birth</th>
                <td><?php echo $MAIN_DATA_value['std_per_data']['dob']; ?></td>
                <th>13. Mobile No.</th>
                <td><?php echo $MAIN_DATA_value['std_per_data']['mobile_no']; ?></td>

            </tr>


            <tr>
                <th>14. Local Guardian's Name</th>
                <td><?php echo $MAIN_DATA_value['std_per_data']['local_gurdian_name']; ?></td>
                <th>15. Religion</th>
                <td><?php echo $MAIN_DATA_value['std_per_data']['religion']; ?></td>

            </tr>
            <tr>
                <th>16. Relation With Local Guardian</th>
                <td><?php echo $MAIN_DATA_value['std_per_data']['local_gurdian_realation']; ?></td>
                <th>17. Local Guardian's Mobile No.</th>
                <td><?php echo $MAIN_DATA_value['std_per_data']['local_gurdian_phone']; ?></td>

            </tr>
            <tr>
                <th>18. Permanent Address</th>
                <td>
                    <span style="font-size:8px !important;">
                        <?php
                        //echo $std_per_data['permanent_address'];
                        echo "Village/Ward: " . $MAIN_DATA_value['std_per_data']['padd_village'] . ", House: " . $MAIN_DATA_value['std_per_data']['padd_house'] . ", ";
                        echo "<br>";
                        echo "Road: " . $MAIN_DATA_value['std_per_data']['padd_road'] . ", Post Office: " . $MAIN_DATA_value['std_per_data']['padd_post_office'] . ", ";
                        echo "<br>";
                        echo "Upazila: " . $MAIN_DATA_value['std_per_data']['padd_police_station'] . ", District: " . $MAIN_DATA_value['std_per_data']['padd_district'];
                        ?>
                    </span>
                </td>
                <th>19. Present Address</th>
                <td><span style="font-size:8px !important;"><?php
                                                            //echo $MAIN_DATA_value['std_per_data']['mailing_address']; 

                                                            //echo $std_per_data['permanent_address'];
                                                            echo "Village/Ward: " . $MAIN_DATA_value['std_per_data']['prad_village'] . ", House: " . $MAIN_DATA_value['std_per_data']['prad_house'] . ", ";
                                                            echo "<br>";
                                                            echo "Road: " . $MAIN_DATA_value['std_per_data']['prad_road'] . ", Post Office: " . $MAIN_DATA_value['std_per_data']['prad_post_office'] . ", ";
                                                            echo "<br>";
                                                            echo "Upazila: " . $MAIN_DATA_value['std_per_data']['	prad_police_station'] . ", District: " . $MAIN_DATA_value['std_per_data']['prad_district'];
                                                            ?> </span>
                </td>
            </tr>
            <tr>
                <th>20. Local Guardian's Address</th>
                <td><span style="font-size:8px !important;"><?php echo wordwrap($MAIN_DATA_value['std_per_data']['local_gurdian_address'], 15, "<br>\n"); ?></span></td>
                <th>21. Name of the Hall</th>
                <td>
                    <?php
                    if ($MAIN_DATA_value['std_per_data']['gender'] == "MALE") {
                        echo "Bangabandhu Sheikh Mujibur Rahman Hall";
                    } else {
                        echo "Sheikh Hasina Hall";
                    }
                    ?>
                </td>
            </tr>
        </table>
        <br>
        <table class="tbl_merit" border="1" width="100%" cellpadding="1" style="border:1px solid #cacaca;border-collapse: collapse;font-size: 10px;">
            <tr>
                <td colspan="7"><b>22. Previous Academic Record:</b></td>
            </tr>
            <tr>
                <th>Exam. Name</th>
                <th>Group</th>
                <th>ROLL NO.</th>
                <th>REG. NO.</th>
                <th>GPA</th>
                <th>Board</th>
                <th>Year</th>
            </tr>
            <tr>
                <th style="text-align: left;">SSC/ Equivalent</th>
                <td><?php echo $MAIN_DATA_value['student_acadamic_data']['SGROUP']; ?>
                <td><?php echo $MAIN_DATA_value['student_acadamic_data']['SROLL_NO']; ?></td>
                <td><?php echo $MAIN_DATA_value['student_acadamic_data']['SREG_NO']; ?></td>
                <td><?php echo $MAIN_DATA_value['student_acadamic_data']['SGPA']; ?></td>
                <td><?php echo $MAIN_DATA_value['student_acadamic_data']['SBOARD_NAME']; ?></td>
                <td><?php echo $MAIN_DATA_value['student_acadamic_data']['SPASS_YEAR']; ?></td>
            </tr>
            <tr>
                <th style="text-align: left;">HSC/ Equivalent</th>
                <td><?php echo $MAIN_DATA_value['student_acadamic_data']['HGROUP']; ?>
                <td><?php echo $MAIN_DATA_value['student_acadamic_data']['HROLL_NO']; ?></td>
                <td><?php echo $MAIN_DATA_value['student_acadamic_data']['HREG_NO']; ?></td>
                <td><?php echo $MAIN_DATA_value['student_acadamic_data']['HGPA']; ?></td>
                <td><?php echo $MAIN_DATA_value['student_acadamic_data']['HBOARD_NAME']; ?></td>
                <td><?php echo $MAIN_DATA_value['student_acadamic_data']['HPASS_YEAR']; ?></td>
            </tr>

        </table>


        <table border="1" width="100%" cellpadding="1" style="border:1px solid #cacaca;border-collapse: collapse;font-size: 10px;">
            <tr>
                <td><b>Declaration:</b></td>
            </tr>
            <tr>
                <td>
                    <p>
                        I, <?php echo strtoupper($MAIN_DATA_value['student_acadamic_data']['SNAME']); ?>, do hereby undertake that I will be held responsible for
                        violation of the rules of the University, for any damage I may do to the University property and for any sort of misconduct. I will also follow other General Rules of Discipline for the Students in accordance with the University rules and regulations.
                    </p>
                </td>
            </tr>
        </table>

        <table border="0" width="100%" cellpadding="5" style="border:0px solid #cacaca;border-collapse: collapse;font-size: 12px;">
            <tr>
                <th colspan="100%"></th>
            </tr>
            <tr>

                <th colspan="50%" style="text-align: left !important;">
                    ____________________________________________<br>
                    Signature of the Student & Date<br><br>
                    (The above statements of the Student are correct)
                </th>
            </tr>
            <tr>
                <th colspan="100%">
                    <div style="padding-top: 50px;"><br></div>
                </th>
            </tr>
            <tr>

                <th colspan="50%" style="text-align: left !important;">
                    ___________________________________<br>
                    Signature of the Chairman<br>
                    (Office Seal & Date)
                </th>
                <th align="left" colspan="50%" style="text-align: right !important;">
                    _______________________________________________________________<br>
                    Signature of the Head of the Academic Section <br>
                    (Office Seal & Date)
                </th>
            </tr>

        </table>
        <pagebreak />
    <?php
    }
    ?>
</body>

</html>