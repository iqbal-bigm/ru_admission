<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>PUST</title>
    <style>
        table th {
            text-align: left;
        }

        .tbl_merit th {
            background-color: #EEEEEE;
        }
    </style>
</head>

<body>

    <table border="0" style="width: 100%;border:0px solid #cacaca;border-collapse: collapse;">
        <thead>
            <tr>
                <td style="width: 15%">
                    <img style="width: 60px; height: auto" alt="<?php echo UNIVERSITY_NAME; ?>" src="<?php echo base_url(); ?>assets/images/logo.png" />
                </td>
            </tr>
        </thead>
    </table>
    <br>
    <table class="tbl_merit" border="1" width="100%" cellpadding="1" style="border:1px solid #cacaca;border-collapse: collapse;font-size: 12px;">
        <tr>
            <td>
                To
                <br>
                The Additional Registrar
                <br>
                Sheikh Hasina University
                <br>
                Pabna-6600.
            </td>
        </tr>
        <tr>
            <td style="padding: 5px;">
                <?php
                $arr_all_subject_dep_name = get_all_subject_dep_name();
                $arr_all_subject_code_name = get_all_subject_code_name();
                ?>

                Through: Chairman, <?php echo $arr_all_subject_dep_name[$sub_code]; ?>, PUST.
            </td>
        </tr>
        <tr>
            <td style="padding: 5px;">
                Respected Sir,
                <br>
                I am writing this letter to request for the cancellation of admission from your university. I regret to inform you that I wish to cancel my admission from your University due to the following causes:
            </td>
        </tr>
        <tr>
            <td style="padding: 20px;">
                <?php echo $result_details['cancel_reason']; ?>
            </td>
        </tr>
        <tr>
            <td style="padding: 5px;">
                I request you with honor to cancel my admission and return me the documents that I have been submitted (all original documents) at the admission desk to the department. I would like to inform you, I have paid admission cancellation fee of Tk. 2025.
                I am looking forward for an early response from you.
            </td>
        </tr>
        <tr>
            <td>
                Sincerely yourself
                <br><br><br>
                ___________________________<br>
                (Signature of the student)
            </td>
        </tr>

    </table>
    <br>

    <br>
    <table border="1" class="tbl_merit" width="100%" cellpadding="5" style="border:1px solid #cacaca;border-collapse: collapse;font-size: 11px;">
        </tr>
        <tr>
            <th colspan="1">Name</th>
            <td colspan="3"><?php echo $student_acadamic_data['SNAME']; ?></td>
            <td rowspan="8">
                <img style="width: 130px; height: auto" alt="Photo" src="<?php echo "http://admission1718.pust.ac.bd/"; //base_url(); 
                                                                            ?>uploads/<?php echo $student_login_photo_path; ?>/<?php echo $result_details['system_reg_id_ref']; ?>.jpg" />
            </td>
        </tr>

        <tr>
            <th colspan="1">Father's Name</th>
            <td colspan="3"><?php echo $student_acadamic_data['SFNAME']; ?></td>
        </tr>
        <tr>
            <th colspan="1">Subject</th>
            <td colspan="3"><?php echo $arr_all_subject_code_name[$sub_code]; ?></td>
        </tr>

        <tr>
            <th>Admission Roll</th>
            <td><?php echo $result_details['exam_roll_ref']; ?></td>
            <th>Merit Position</th>
            <td><?php echo $result_details['merit_pos_ref']; ?></td>
        </tr>

        <tr>
            <th colspan="1">Address</th>
            <td colspan="3">
                <?php
                echo "Village/Ward: " . $std_per_data['padd_village'] . ", House: " . $std_per_data['padd_house'] . ", ";
                echo "<br>";
                echo "Road: " . $std_per_data['padd_road'] . ", Post Office: " . $std_per_data['padd_post_office'] . ", ";
                echo "<br>";
                echo "Upazila: " . $std_per_data['padd_police_station'] . ", District: " . $std_per_data['padd_district'];
                ?>
            </td>
        </tr>
        <tr>
            <th colspan="1">Mobile No.</th>
            <td colspan="3"><?php echo $std_per_data['mobile_no']; ?></td>
        </tr>
        <tr>
            <th>Amount</th>
            <td><?php echo $result_details['cancel_paid_amount']; ?></td>
            <th>Payment Trxn. ID</th>
            <td><?php echo $result_details['cancel_payment_trans_id']; ?></td>
        </tr>
        <tr>
            <th colspan="1">Payment Date</th>
            <td colspan="3"><?php echo $result_details['cancel_payment_date']; ?></td>
        </tr>

    </table>
    <br>
    <table border="1" class="tbl_merit" width="100%" cellpadding="5" style="border:1px solid #cacaca;border-collapse: collapse;font-size: 11px;">
        <tr>
            <td><br><br>
                ______________________________<br>
                Recommendation of the Chairman<br>
                (For Department Clearance)
            </td>
            <td><br><br>
                ______________________________<br>
                Recommendation of Hall Provost<br>
                (For Hall Clarence)
            </td>
        </tr>
    </table>
    <br>
    <br>
    <table width="100%" style="border: 1px solid black;">
        <tr>
            <td colspan="2" style="text-align: left;font-size: 9px;text-align: center;">
                © 2017-2018; <?php echo UNIVERSITY_NAME; ?>. All rights reserved.
            </td>
        </tr>
    </table>

</body>

</html>