    <?php $this->load->view('common/header'); ?>
    <?php $this->load->view('common/navbar'); ?>
    <style>
        .unit_radio_style {
            padding-bottom: 10px;
        }

        .my_error {
            color: red;
        }
    </style>
    <?php
    $choice_on_off = choice_on_off_status();
    ?>
    <div id="main">
        <div class="container">
            <div class="row">
                <div class="col-md-12">

                    <div class="panel panel-primary">
                        <div class="panel-heading">Student Details Information</div>

                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <fieldset>
                                        <legend><span style="color:#f4a270;"><?php //echo $choice_on_off['msg'];  
                                                                                ?></span></legend>
                                    </fieldset>

                                    <div>
                                        <?php //if($sess_student_is_secnd_meirt <> "YES"){ 
                                        ?>
                                        <!-- <fieldset>
    <legend><span style="color:#f4a270;">
    <?php
    $admission_payment_messg = admission_payment_messg();
    //echo $admission_payment_messg['msg'];
    ?>
    <br>
    Date of Payment and  Admission:  09th and 10th January 2018
    </span>
    </legend>
    </fieldset> -->

                                        <?php
                                        if (can_give_waiting_choise() == "YES") { // only for A unit
                                        ?>
                                            <style>
                                                .blink_me {
                                                    animation: blinker 1s linear infinite;
                                                }

                                                @keyframes blinker {
                                                    50% {
                                                        opacity: 0;
                                                    }
                                                }
                                            </style>
                                            <fieldset>
                                                <legend>
                                                    <div style="color: blue;" class="blink_me">Want to take admission from waiting ?</div>
                                                </legend>
                                            </fieldset>
                                            <table class="table table-responsive table-bordered">
                                                <?php
                                                $wants_to_admit_frm_waiting = wants_to_admit_frm_waiting();
                                                if ($wants_to_admit_frm_waiting == "NO") {
                                                ?>
                                                    <tr>
                                                        <td colspan="2">Subject will be allocated based on merit position and seat availability. If you are failed to apply within the time, you will never selected for admission.</td>
                                                    </tr>
                                                <?php
                                                }
                                                ?>

                                                <tr>
                                                    <th>
                                                        <h4>
                                                            <?php
                                                            $wants_to_admit_frm_waiting = wants_to_admit_frm_waiting();
                                                            if ($wants_to_admit_frm_waiting == "YES") {
                                                                echo "Your Application for Admission in PUST has been accepted. Please wait for final result.";
                                                            } else {
                                                                //   echo "<div style=\"color: blue;\" class=\"blink_me\">Are you interested to take admission? Click next Button to Confirm.</div>";
                                                            }
                                                            ?>
                                                        </h4>
                                                    </th>
                                                    <th>
                                                        <?php
                                                        //hide migration
                                                        $test = 1100;
                                                        if ($test == 10) {
                                                            if ($wants_to_admit_frm_waiting == "NO") {
                                                        ?>
                                                                <a onclick="onoffConfirmBoxWaiting('<?php echo base_url() . "dashboard/admit_from_waiting/"; ?>')" href="javascript:void(0)"><button type="button" class="btn btn-success">I am interested to take admission</button></a>
                                                        <?php
                                                            }
                                                        }
                                                        ?>
                                                    </th>
                                                </tr>
                                            </table>

                                            <br><br>
                                        <?php
                                        }
                                        ?>


                                        <?php
                                        if ($my_admission_status['admission_done'] == "admission_done") { // Migration on / off option
                                        ?>

                                            <fieldset>
                                                <legend>Migration Option: </legend>
                                            </fieldset>
                                            <table class="table table-responsive table-bordered">
                                                <tr>
                                                    <th>
                                                        <h4>
                                                            <?php
                                                            if ($my_admission_status['migration_on_off'] == "ON") {
                                                                echo "Your Migration is ON.";
                                                            } else {
                                                                echo "Your Migration is OFF.";
                                                            }
                                                            ?>
                                                        </h4>
                                                    </th>
                                                    <th>
                                                        <?php
                                                        //hide migration
                                                        $test = 10;
                                                        if ($test == 10) {
                                                            if ($my_admission_status['migration_on_off'] == "ON" && $value_['unit_ref'] <> "A_ARCH") {
                                                        ?>
                                                                <a onclick="onoffConfirmBox('<?php echo base_url() . "dashboard/migration_process/"; ?>')" href="javascript:void(0)"><button type="button" class="btn btn-success">CLICK HERE TO OFF MIGRATION</button></a>
                                                        <?php
                                                            }
                                                        }
                                                        ?>
                                                    </th>
                                                </tr>
                                            </table>
                                        <?php
                                        }
                                        ?>



                                        <?php
                                        if (!empty($my_result_cancel)) {

                                        ?>
                                            <fieldset>
                                                <legend>My Admission Cancel Details: </legend>
                                            </fieldset>
                                            <table class="table table-responsive table-bordered">
                                                <tr>
                                                    <th>Tracking ID For Cancel</th>
                                                    <th>Subject</th>
                                                    <th>Roll No.</th>
                                                    <th>Quota</th>
                                                    <th>Cancel Status</th>
                                                    <th>Cancel Amount to Pay</th>
                                                    <th>Cancel Payment Status</th>
                                                    <th colspan="2">Download Cancel Form</th>
                                                </tr>
                                                <?php
                                                $arr_all_subject_code_name = get_all_subject_code_name();
                                                $arr_all_subject_faculty_name = get_all_subject_faculty_name();

                                                if (!empty($my_result_cancel)) {
                                                    foreach ($my_result_cancel as $key_1 => $value_1) {
                                                ?>
                                                        <tr>
                                                            <td><?php echo $value_1['cancel_id']; ?></td>
                                                            <td><?php echo $arr_all_subject_code_name[$value_1['sub_code_1']]; ?></td>
                                                            <td><?php echo $value_1['exam_roll_ref']; ?></td>

                                                            <td><?php
                                                                if (!empty($value_1['quota_ref'])) {
                                                                    $quota_name_arr = get_quota_array();
                                                                    echo $quota_name_arr[$value_1['quota_ref']];
                                                                }

                                                                ?></td>
                                                            <td><?php echo $value_1['cancel_status']; ?></td>
                                                            <td><?php echo $value_1['cancel_amount_to_pay']; ?></td>
                                                            <td>
                                                                <?php
                                                                if ($value_1['cancel_paid_status'] == "P") {
                                                                    echo "PAID";
                                                                }
                                                                ?>
                                                            </td>


                                                            <td>
                                                                <?php
                                                                if ($value_1['cancel_paid_status'] == "P") {
                                                                ?>
                                                                    <a href="<?php echo base_url() . "dashboard/admission_cancel_pdf_download/" . $value_1['unique_id_ref'] . "/" . $value_1['exam_roll_ref'] . "/" . $value_1['unit_ref'] . "/" . $value_1['sub_code_1']; ?>"><button type="button" class="btn btn-primary">Download Cancel Form</button></a>
                                                                <?php } ?>
                                                            </td>

                                                        </tr>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </table>
                                        <?php } ?>

                                        <fieldset>
                                            <legend>My Result/Subject: </legend>
                                            <!-- <h3>Admission Date: 6th Jan 2019 to 9th Jan 2019</h3> -->
                                            <h4>Admission guideline : <a targer="_blank" href="<?php echo base_url(); ?>migration_files/PUST_Admission_Guideline_2019-2020.pdf">Download</a></h4>
                                            <h4>Admission cancellation guideline : <a targer="_blank" href="<?php echo base_url(); ?>migration_files/PUST_Admission_Cancellation_Guideline_2019-2020.pdf">Download</a></h4>
                                        </fieldset>

                                        <?php
                                        if (!empty($my_result)) {

                                        ?>
                                            <table class="table table-responsive table-bordered">
                                                <tr>
                                                    <th>Tracking ID</th>
                                                    <th>Subject</th>
                                                    <th>Migrated Subject 1</th>
                                                    <th>Migrated Subject 2</th>
                                                    <th>Migrated Subject 3</th>
                                                    <th>Migrated Subject 4</th>
                                                    <th>Migrated Subject 5</th>
                                                    <th>Faculty Name</th>
                                                    <th>Amount to Pay</th>
                                                    <th>Payment Status</th>
                                                    <th>Roll No.</th>
                                                    <th>Unit</th>
                                                    <th>Quota</th>
                                                    <th colspan="2">Download</th>
                                                </tr>
                                                <?php
                                                $arr_all_subject_code_name = get_all_subject_code_name();
                                                $arr_all_subject_faculty_name = get_all_subject_faculty_name();

                                                if (!empty($my_result)) {
                                                    foreach ($my_result as $key_ => $value_) {
                                                        $opacity = 'opacity: 0.7;';
                                                        if ($value_['paid_status'] == "P") {
                                                            $opacity = '';
                                                        }
                                                ?>
                                                        <tr style="<?php echo $opacity; ?>">
                                                            <td><?php echo $value_['tracking_id']; ?></td>
                                                            <td><?php echo $arr_all_subject_code_name[$value_['sub_code_1']]; ?></td>

                                                            <?php
                                                            if (!empty($value_['sub_code_2'])) {
                                                                if ($value_['sub_code_1'] <> $value_['sub_code_2']) { //first migration
                                                            ?>
                                                                    <td><?php echo $arr_all_subject_code_name[$value_['sub_code_2']]; ?></td>
                                                                <?php
                                                                } else {
                                                                ?>
                                                                    <td></td>
                                                                <?php
                                                                }
                                                            } else {
                                                                ?>
                                                                <td></td>
                                                            <?php
                                                            }
                                                            ?>

                                                            <?php
                                                            if (!empty($value_['sub_code_3'])) {
                                                                if ($value_['sub_code_2'] <> $value_['sub_code_3']) { //2nd migration
                                                            ?>
                                                                    <td><?php echo $arr_all_subject_code_name[$value_['sub_code_3']]; ?></td>
                                                                <?php
                                                                } else {
                                                                ?>
                                                                    <td></td>
                                                                <?php
                                                                }
                                                            } else {
                                                                ?>
                                                                <td></td>
                                                            <?php
                                                            }
                                                            ?>

                                                            <?php
                                                            if (!empty($value_['sub_code_4'])) {
                                                                if ($value_['sub_code_3'] <> $value_['sub_code_4']) { //3rd migration
                                                            ?>
                                                                    <td><?php echo $arr_all_subject_code_name[$value_['sub_code_4']]; ?></td>
                                                                <?php
                                                                } else {
                                                                ?>
                                                                    <td></td>
                                                                <?php
                                                                }
                                                            } else {
                                                                ?>
                                                                <td></td>
                                                            <?php
                                                            }
                                                            ?>

                                                            <?php
                                                            if (!empty($value_['sub_code_5'])) {
                                                                if ($value_['sub_code_4'] <> $value_['sub_code_5']) { //4th migration
                                                            ?>
                                                                    <td><?php echo $arr_all_subject_code_name[$value_['sub_code_5']]; ?></td>
                                                                <?php
                                                                } else {
                                                                ?>
                                                                    <td></td>
                                                                <?php
                                                                }
                                                            } else {
                                                                ?>
                                                                <td></td>
                                                            <?php
                                                            }
                                                            ?>

                                                            <?php
                                                            if (!empty($value_['sub_code_6'])) {
                                                                if ($value_['sub_code_5'] <> $value_['sub_code_6']) { //5th migration
                                                            ?>
                                                                    <td><?php echo $arr_all_subject_code_name[$value_['sub_code_6']]; ?></td>
                                                                <?php
                                                                } else {
                                                                ?>
                                                                    <td></td>
                                                                <?php
                                                                }
                                                            } else {
                                                                ?>
                                                                <td></td>
                                                            <?php
                                                            }
                                                            ?>
                                                            <td><?php echo $arr_all_subject_faculty_name[$value_['sub_code_1']]; ?></td>

                                                            <td><?php echo $value_['amount_to_pay']; ?></td>
                                                            <td>
                                                                <?php
                                                                if ($value_['paid_status'] == "P") {
                                                                    echo "PAID";
                                                                }
                                                                ?>
                                                            </td>
                                                            <td><?php echo $value_['exam_roll_ref']; ?></td>
                                                            <td><?php echo $value_['unit_ref']; ?></td>
                                                            <td><?php
                                                                if (!empty($value_['quota_ref'])) {
                                                                    $quota_name_arr = get_quota_array();
                                                                    echo $quota_name_arr[$value_['quota_ref']];
                                                                }

                                                                ?></td>

                                                            <td>
                                                                <?php
                                                                if ($value_['paid_status'] == "P") {
                                                                ?>
                                                                    <a href="<?php echo base_url() . "dashboard/admission_pdf_download/" . $value_['unique_id_ref'] . "/" . $value_['exam_roll_ref'] . "/" . $value_['unit_ref']; ?>"><button type="button" class="btn btn-primary">Download Admission Form</button></a>
                                                                <?php } ?>
                                                            </td>
                                                            <td>
                                                                <?php
                                                                //hide cancel
                                                                $test = 10;
                                                                if ($test == 10) {
                                                                    if ($value_['paid_status'] == "P" && $value_['cancel_status'] <> "cancel_apply") {
                                                                ?>
                                                                        <a href="<?php echo base_url() . "dashboard/admission_cancel_form/" . $value_['unique_id_ref'] . "/" . $value_['exam_roll_ref'] . "/" . $value_['unit_ref'] . "/" . $value_['sub_code_1']; ?>"><button type="button" class="btn btn-danger">Apply To Cancel Admission</button></a>
                                                                <?php }
                                                                } ?>
                                                            </td>
                                                        </tr>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </table>
                                        <?php } else { ?>
                                            <table class="table table-responsive table-bordered">
                                                <tr>
                                                    <th colspan="9">
                                                        <h3>
                                                            <?php
                                                            if (is_in_waiting() == "in_waiting") {
                                                                //  echo "Your are in waiting list.";
                                                            }
                                                            ?>
                                                        </h3>
                                                    </th>
                                                </tr>
                                            </table>
                                        <?php } ?>
                                        <?php // } 
                                        ?>
                                    </div>

                                    <fieldset>
                                        <legend>Unit List: </legend>
                                    </fieldset>
                                    <?php
                                    //  if($sess_student_is_secnd_meirt == "YES"){ 
                                    ?>
                                    <!--   <fieldset>
    <h4>Choice Form Fill-up Date: 26 Nov 2019 To 02 Dec 2019</h4>
    <h4><a href="<?= site_url('migration_files/PUST_Choice_Form_Guideline_2019-2020.pdf'); ?>" target="_blank">Guideline for choice form fill up.</a></h4>
    </fieldset> -->
                                    <?php //} 
                                    ?>
                                    <table class="table table-responsive table-bordered">
                                        <tr>
                                            <th style="white-space:nowrap">Tracking ID</th>
                                            <th style="white-space:nowrap">Application ID</th>
                                            <th style="white-space:nowrap">Roll No.</th>
                                            <th style="white-space:nowrap">Unit</th>
                                            <th style="white-space:nowrap">Score</th>
                                            <th style="white-space:nowrap">Merit Position</th>
                                            <th style="white-space:nowrap">Quota</th>
                                            <th style="white-space:nowrap">Selected Subject</th>
                                            <th style="white-space:nowrap" colspan="2">Action</th>
                                        </tr>
                                        <?php
                                        $arr_all_subject_code_name = get_all_subject_code_name();

                                        if (!empty($eligible_unit_list)) {
                                            foreach ($eligible_unit_list as $key => $value) {

                                        ?>
                                                <tr>
                                                    <td><?php echo $value['tracking_id']; ?></td>
                                                    <td><?php echo $value['app_id']; ?></td>
                                                    <td><?php echo $value['exam_roll']; ?></td>
                                                    <td><?php
                                                        echo $value['unit'];
                                                        if ($value['unit'] == "A") {
                                                            echo "<br>(" . $value['optional'] . ")";
                                                        }
                                                        ?></td>
                                                    <td><?php echo $value['score']; ?></td>
                                                    <td><span style="font-size:19px;"><b><?php echo $value['merit']; ?></b></span></td>
                                                    <td><?php
                                                        if (!empty($value['quota_name'])) {
                                                            $quota_name_arr = get_quota_array();
                                                            echo $quota_name_arr[$value['quota_name']];
                                                        }

                                                        ?></td>
                                                    <td>
                                                        <?php
                                                        $my_unique_id = $value['unique_id'];
                                                        $my_exam_roll = $value['exam_roll'];
                                                        $my_unit_name = $value['unit'];

                                                        $my_data = get_my_saved_subject_list($my_unique_id, $my_exam_roll, $my_unit_name);
                                                        if (!empty($my_data)) {

                                                            $subject_index = 1;
                                                            foreach ($my_data as $mykey => $myvalue) {
                                                        ?>
                                                                <div style="padding-bottom: 10px;">
                                                                    <?php
                                                                    echo $subject_index . ". " . $arr_all_subject_code_name[$myvalue['sub_code_ref']]; ?>
                                                                </div>
                                                        <?php
                                                                $subject_index++;
                                                            }
                                                        }
                                                        ?>
                                                    </td>
                                                    <td>
                                                        <?php
                                                        //if($sess_student_is_secnd_meirt == "YES"){
                                                        if ($choice_on_off['on_off'] == "ON") {
                                                            // if($value['is_secnd_meirt'] == "YES"){
                                                        ?>
                                                            <a href="<?php echo base_url() . "dashboard/my_subject_option/" . $value['unique_id'] . "/" . $value['exam_roll'] . "/" . $value['unit'] . "/" . $value['is_secnd_meirt']; ?>"><button type="button" class="btn btn-success">Click here to Choice Subject <br> & Personal Information</button></a>
                                                        <?php //}
                                                        } //}  
                                                        ?>

                                                    </td>
                                                    <td>
                                                        <?php
                                                        if (!empty($my_data) && !empty($std_per_data['dob'])) {
                                                        ?>
                                                            <a href="<?php echo base_url() . "dashboard/download/" . $value['unique_id'] . "/" . $value['exam_roll'] . "/" . $value['unit']; ?>"><button type="button" class="btn btn-primary">Download</button></a>
                                                        <?php } else { ?>
                                                            <span style="color:red;">You Must Complete Subject Choice and Fill Personal Information to Get Download Button</span>
                                                        <?php } ?>
                                                    </td>
                                                </tr>
                                        <?php
                                            }
                                        }
                                        ?>
                                    </table>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <fieldset>
                                        <legend style="font-size: 16px;"><b>Mobile No:</b> <?= $this->session->userdata('student_login_mobile_no'); ?></b></legend>
                                    </fieldset>

                                    <fieldset>
                                        <legend style="font-size: 16px;"><b>Student Name:</b> <?= $student_all_data['SNAME']; ?></legend>
                                    </fieldset>
                                    <fieldset>
                                        <legend style="font-size: 16px;"><b>Father's Name:</b> <?= $student_all_data['SFNAME']; ?></legend>
                                    </fieldset>
                                    <fieldset>
                                        <legend style="font-size: 16px;"><b>Mother's Name:</b> <?= $student_all_data['SMNAME']; ?></legend>
                                    </fieldset>

                                </div>

                                <div class="col-md-6">
                                    <fieldset>
                                        <legend>Photo</legend>
                                    </fieldset>
                                    <div id="photo_holder" style="background-color :#E3E3E3; height:220px;width:180px;">
                                        <?php
                                        if (!empty($student_all_data) && $student_all_data['IMGUPLOAD'] == "Y") {

                                        ?>
                                            <img height="220px;" width="180px;" src="<?php echo base_url() . 'uploads/' . $student_all_data['IMGUPLOAD_PATH'] . '/' . $student_all_data['REGISID'] . '.jpg' . '?' . time(); ?>" alt="photo" />
                                        <?php
                                        }
                                        ?>
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-12">&nbsp;
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <fieldset>
                                        <legend>S.S.C/Equivalent</legend>
                                    </fieldset>

                                    <table class="table table-responsive table-bordered">

                                        <tr>
                                            <th>Roll No : </th>
                                            <td><?= $student_all_data['SROLL_NO']; ?></td>
                                        </tr>
                                        <tr>
                                            <th>Registration No : </th>
                                            <td><?= $student_all_data['SREG_NO']; ?></td>
                                        </tr>

                                        <tr>
                                            <th>Board : </th>
                                            <td><?= $student_all_data['SBOARD_NAME']; ?></td>
                                        </tr>

                                        <tr>
                                            <th>Passing Year : </th>
                                            <td><?= $student_all_data['SPASS_YEAR']; ?></td>
                                        </tr>
                                        <tr>
                                            <th>Group : </th>
                                            <td><?= $student_all_data['SGROUP']; ?></td>
                                        </tr>
                                        <tr>
                                            <th>GPA : </th>
                                            <td><?= $student_all_data['SGPA']; ?></td>
                                        </tr>


                                    </table>
                                </div>
                                <div class="col-md-6">
                                    <fieldset>
                                        <legend>H.S.C/Equivalent</legend>
                                    </fieldset>

                                    <table class="table table-responsive table-bordered">

                                        <tr>
                                            <th>Roll No : </th>
                                            <td><?= $student_all_data['HROLL_NO']; ?></td>
                                        </tr>
                                        <tr>
                                            <th>Registration No : </th>
                                            <td><?= $student_all_data['HREG_NO']; ?></td>
                                        </tr>

                                        <tr>
                                            <th>Board : </th>
                                            <td><?= $student_all_data['HBOARD_NAME']; ?></td>
                                        </tr>

                                        <tr>
                                            <th>Passing Year : </th>
                                            <td><?= $student_all_data['HPASS_YEAR']; ?></td>
                                        </tr>
                                        <tr>
                                            <th>Group : </th>
                                            <td><?= $student_all_data['HGROUP']; ?></td>
                                        </tr>
                                        <tr>
                                            <th>GPA : </th>
                                            <td><?= $student_all_data['HGPA']; ?></td>
                                        </tr>

                                    </table>
                                </div>
                            </div>




                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>
    <script>
        function onoffConfirmBox(id) {
            bootbox.confirm({
                message: "You will not  get option to 'ON' migration again. Are you sure to OFF migration?",
                buttons: {
                    confirm: {
                        label: 'Yes',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: 'No',
                        className: 'btn-danger'
                    }
                },
                callback: function(result) {
                    if (result) {
                        window.location = id;
                    }
                }
            });

        }

        function onoffConfirmBoxWaiting(id) {
            bootbox.confirm({
                message: "Are you interested to take admission. Are you sure?",
                buttons: {
                    confirm: {
                        label: 'YES',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: 'NO',
                        className: 'btn-danger'
                    }
                },
                callback: function(result) {
                    if (result) {
                        window.location = id;
                    }
                }
            });

        }
    </script>
    <?php $this->load->view('common/footer'); ?>