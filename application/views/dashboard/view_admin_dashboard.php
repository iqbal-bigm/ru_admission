<?php $this->load->view('common/header'); ?>
<?php $this->load->view('admin/view_admin_navbar'); ?>

<div id="main">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">Admin Dashboard</div>

                    <div class="panel-body">                        
                        <div class="row">

                           <table class="table table-responsive table-bordered">
                                    <tr>
                                        <th style="width: 60%;">Total Registration Done : </th>
                                        <td><?php echo get_total_registation_done(); ?></td>
                                    </tr>
                                <tr>
                                        <th style="width: 60%;">Total Subject Choice Done : </th>
                                        <td><?php echo get_total_sub_choice(); ?></td>
                                    </tr>              
                                <tr>
                                        <th style="width: 60%;">Total Subject Choice Done A1 : </th>
                                        <td><?php echo get_total_sub_choice_unit('A1'); ?></td>
                               </tr>                                       
                                <tr>
                                        <th style="width: 60%;">Total Subject Choice Done A2 : </th>
                                        <td><?php echo get_total_sub_choice_unit('A2'); ?></td>
                               </tr>                                       
                                <tr>
                                        <th style="width: 60%;">Total Subject Choice Done B : </th>
                                        <td><?php echo get_total_sub_choice_unit('B'); ?></td>
                               </tr> 
                                <tr>
                                        <th style="width: 60%;">Total Subject Choice Done C : </th>
                                        <td><?php echo get_total_sub_choice_unit('C'); ?></td>
                               </tr>                                
                             </table>                                                           
                        </div>
                        <hr>
                        <div class="row">
                            <?php
                                $sub_code_arr = array(
                                    "01" => "A1, Computer Science and Engineering",
                                    "17" => "A1, Urban and Regional Planning",
                                    "11" => "A1, Civil Engineering",
                                    "06" => "A1, Information and Communication Engineering",
                                    "05" => "A1, Electronic and Telecommunication Engineering",
                                    "02" => "A1, Electrical and Electronic Engineering",
                                    "12" => "A2, Architecture",
                                    "09" => "B, Geography and Environment",
                                    "16" => "B, Statistics",
                                    "14" => "B, Chemistry",
                                    "13" => "B, Pharmacy",
                                    "07" => "B, Physics",
                                    "03" => "B, Mathematics",
                                    "21" => "C, Tourism and Hospitality Management",
                                    "19" => "C, Public Administration",
                                    "18" => "C, English",
                                    "15" => "C, Social Work",
                                    "10" => "C, Bengali",
                                    "08" => "C, Economics",
                                    "04" => "C, Business Administration",
                                    "20" => "C, History and Bangladesh Studies"
                                );
                            ?>
                           <table class="table table-responsive table-bordered">
                            <tr>
                                   <th style="width: 60%;"><h3>Subject Name</h3></th>
                                        <th><h3>Total Choice Given</h3></th>
                                    </tr>                                  
                                <?php
                                foreach ($sub_code_arr as $key => $value) {
                                    ?>
                               <tr>
                                   <th style="width: 60%;"><?php echo $value ?> : </th>
                                        <td><?php echo get_total_subwise_choice($key); ?></td>
                                    </tr>                               
                               <?php
                                }
                                ?>                                                                                                      
                             </table>                                                           
                        </div>                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('common/footer'); ?>
