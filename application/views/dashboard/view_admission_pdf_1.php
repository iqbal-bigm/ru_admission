<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>PUST Admission</title>
    <style>
        table th {
            text-align: left;
        }

        .tbl_merit th {
            background-color: #EEEEEE;
        }
    </style>
</head>

<body>

    <table border="1" style="width: 100%;border:1px solid #cacaca;border-collapse: collapse;">
        <thead>
            <tr>
                <td style="width: 30%">
                    <img style="width: 60px; height: auto" alt="<?php echo UNIVERSITY_NAME; ?>" src="<?php echo base_url(); ?>assets/images/logo.png" />
                </td>
                <td style="text-align: center;width: 70%" colspan="2" valign="top">
                    <span style="top:0px;font-size: 20px;font-weight:bold"><?php echo UNIVERSITY_NAME; ?></span>
                    <br>
                    Pabna, Bangladesh
                    <br>
                    <span style="font-size:16px;"><b>Student Admission Form</b></span>
                </td>
            </tr>
        </thead>
    </table>
    <br>

    <table class="tbl_merit" border="1" width="100%" cellpadding="3" style="border:1px solid #cacaca;border-collapse: collapse;font-size: 11px;">
        <tr>
            <td width="20%">
                <img style="width: 150px; height: auto" alt="Photo" src="<?php echo base_url(); ?>uploads/<?php echo $student_login_photo_path; ?>/<?php echo $unit_details['system_reg_id']; ?>.jpg" />
            </td>
            <td width="40%" valign="top">
                <table class="tbl_merit" border="1" width="100%" cellpadding="11" style="border:1px solid #cacaca;border-collapse: collapse;font-size: 11px;">
                    <tr>
                        <th colspan="2" style="text-align: center">For official use only</th>
                    </tr>
                    <tr>
                        <th>Session</th>
                        <td>2017-2018</td>
                    </tr>
                    <tr>
                        <th>Subject</th>
                        <td></td>
                    </tr>
                    <tr>
                        <th>Course</th>
                        <td></td>
                    </tr>
                    <tr>
                        <th>Registration No.</th>
                        <td></td>
                    </tr>
                </table>

            </td>
            <td width="40%" valign="top">
                <table class="tbl_merit" border="1" width="100%" cellpadding="11" style="border:1px solid #cacaca;border-collapse: collapse;font-size: 11px;">
                    <tr>
                        <th colspan="2" style="text-align: center">For official use only</th>
                    </tr>
                    <tr>
                        <th>Class Roll</th>
                        <td></td>
                    </tr>
                    <tr>
                        <th>Amount Paid</th>
                        <td></td>
                    </tr>
                    <tr>
                        <th>Payment Date</th>
                        <td></td>
                    </tr>
                    <tr>
                        <th>Transaction ID.</th>
                        <td></td>
                    </tr>
                    <tr>

                        <th> Dormitory/Hall Code</th>
                        <td>
                            <?php
                            if ($std_per_data['gender'] == "Male") {
                                echo "Bangabandhu Sheikh Mujibur Rahman Hall";
                            } else {
                                echo "Female Student Hostel";
                            }
                            ?>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>

    </table>
    <br>
    <table border="1" class="tbl_merit" width="100%" cellpadding="5" style="border:1px solid #cacaca;border-collapse: collapse;font-size: 10px;">

        <tr>
            <th colspan="1">Name In English</th>
            <td colspan="3"><?php echo $student_acadamic_data['SNAME']; ?></td>
        </tr>

        <tr>
            <th colspan="1">Name In Bengali</th>
            <td colspan="3"><br> ________________________________________________________</td>
        </tr>

        <tr>
            <th>Father's Name</th>
            <td><?php echo $student_acadamic_data['SFNAME']; ?></td>

            <th>Mother's Name</th>
            <td><?php echo $student_acadamic_data['SMNAME']; ?></td>
        </tr>
        <tr>
            <th>Father's Occupation</th>
            <td><?php echo $std_per_data['father_occupation']; ?></td>

            <th>Mothers's Occupation</th>
            <td><?php echo $std_per_data['mother_occupation']; ?></td>
        </tr>
        <tr>
            <th>Yearly Family Income (in taka)</th>
            <td><?php echo $std_per_data['family_income']; ?></td>

            <th>BNCC/Rover Scout</th>
            <td><?php echo $std_per_data['scout']; ?></td>
        </tr>
        <tr>
            <th>Nationality</th>
            <td>Bangladeshi</td>
            <th>Home District</th>
            <td><?php echo $std_per_data['district']; ?></td>

        </tr>

        <tr>
            <th>Marital Status</th>
            <td><?php echo $std_per_data['maritlal_status']; ?></td>
            <th>Gender</th>
            <td><?php echo $std_per_data['gender']; ?></td>
        </tr>

        <tr>
            <th>Date of Birth</th>
            <td><?php echo $std_per_data['dob']; ?></td>
            <th>Mobile No.</th>
            <td><?php echo $std_per_data['mobile_no']; ?></td>

        </tr>

        <tr>
            <th>Permanent Address</th>
            <td>
                <?php
                //echo $std_per_data['permanent_address']; 
                echo "Village/Ward: " . $std_per_data['padd_village'] . ", House: " . $std_per_data['padd_house'] . ", ";
                echo "<br>";
                echo "Road: " . $std_per_data['padd_road'] . ", Post Office: " . $std_per_data['padd_post_office'] . ", ";
                echo "<br>";
                echo "Upazila: " . $std_per_data['padd_police_station'] . ", District: " . $std_per_data['padd_district'];
                ?>
            </td>
            <th>Present Address</th>
            <td><?php echo $std_per_data['mailing_address']; ?></td>
        </tr>
        <tr>
            <th>Local Guardian's Name</th>
            <td><?php echo $std_per_data['local_gurdian_name']; ?></td>
            <th>Religion</th>
            <td><?php echo $std_per_data['religion']; ?></td>

        </tr>
        <tr>
            <th>Relation With Local Guardian</th>
            <td><?php echo $std_per_data['local_gurdian_realation']; ?></td>
            <th>Local Guardian's Mobile No.</th>
            <td><?php echo $std_per_data['local_gurdian_phone']; ?></td>

        </tr>
        <tr>
            <th>Local Guardian's Address</th>
            <td colspan="3"><?php echo $std_per_data['local_gurdian_address']; ?></td>
        </tr>
    </table>
    <br>
    <table class="tbl_merit" border="1" width="100%" cellpadding="1" style="border:1px solid #cacaca;border-collapse: collapse;font-size: 10px;">
        <tr>
            <td><b>Educational Information:</b></td>
        </tr>
        <tr>
            <th>Exam. Name</th>
            <th>Group</th>
            <th>ROLL NO.</th>
            <th>REG. NO.</th>
            <th>GPA</th>
            <th>Board</th>
            <th>Year</th>
        </tr>
        <tr>
            <th style="text-align: left;">SSC/ Equivalent</th>
            <td><?php echo $student_acadamic_data['SGROUP']; ?>
            <td><?php echo $student_acadamic_data['SROLL_NO']; ?></td>
            <td><?php echo $student_acadamic_data['SREG_NO']; ?></td>
            <td><?php echo $student_acadamic_data['SGPA']; ?></td>
            <td><?php echo $student_acadamic_data['SBOARD_NAME']; ?></td>
            <td><?php echo $student_acadamic_data['SPASS_YEAR']; ?></td>
        </tr>
        <tr>
            <th style="text-align: left;">HSC/ Equivalent</th>
            <td><?php echo $student_acadamic_data['HGROUP']; ?>
            <td><?php echo $student_acadamic_data['HROLL_NO']; ?></td>
            <td><?php echo $student_acadamic_data['HREG_NO']; ?></td>
            <td><?php echo $student_acadamic_data['HGPA']; ?></td>
            <td><?php echo $student_acadamic_data['HBOARD_NAME']; ?></td>
            <td><?php echo $student_acadamic_data['HPASS_YEAR']; ?></td>
        </tr>

    </table>
    <br>
    <table border="1" width="100%" cellpadding="5" style="border:1px solid #cacaca;border-collapse: collapse;font-size: 11px;">
        <tr>
            <td><b>Documents to be attached:</b></td>
        </tr>
        <tr>
            <td>
                1. S.S.C./Dakil/O-Level/Equivalent Original Marks Sheet and Certificate
                <br>
                2. H.S.C./Alim/A-Level/Equivalent Original Marks Sheet and Certificate
            </td>
        </tr>
    </table>
    <br>
    <pagebreak />
    <table border="1" style="width: 100%;border:1px solid #cacaca;border-collapse: collapse;">
        <thead>
            <tr>
                <td style="width: 30%">
                    <img style="width: 60px; height: auto" alt="<?php echo UNIVERSITY_NAME; ?>" src="<?php echo base_url(); ?>assets/images/logo.png" />
                </td>
                <td style="text-align: center;width: 70%" colspan="2" valign="top">
                    <span style="top:0px;font-size: 20px;font-weight:bold"><?php echo UNIVERSITY_NAME; ?></span>
                    <br>
                    Pabna, Bangladesh
                    <br>
                    <span style="font-size:16px;"><b>Student Admission Form</b></span>
                </td>
            </tr>
        </thead>
    </table>
    <br>

    <table border="1" style="width: 100%;border:1px solid #cacaca;border-collapse: collapse;">
        <thead>
            <tr>
                <td style="text-align: center;width: 100%" colspan="2" valign="top">
                    <span style="top:0px;font-size: 20px;font-weight:bold"> Undertaking by the Students</span>
                </td>
            </tr>
        </thead>
    </table>
    <br>
    <table border="1" width="100%" cellpadding="1" style="border:1px solid #cacaca;border-collapse: collapse;font-size: 11px;">

        <tr>
            <td><b>I do hereby undertake as follows::</b></td>
        </tr>
        <tr>
            <td>
                <p>
                    1. I will be held responsible if I violate the rules of the university or any damages I may do to
                    the university property or any sort of misconduct. I will also follow other General Rules of
                    Discipline for the students in accordance with the university rules & regulations.
                </p>
                <br>
                <p>
                    2. (a) I will pay the fees as decided by the university authorities.
                    <br>
                    (b) I will also pay other fees to be fixed by the university authorities from time to time.
                </p>
                <br>
                <p>
                    3. I will follow the general instructions as mentioned in the admission form.
                    <br><br>
                    I, therefore, declare that, if I violate the above mentioned rules of the university and if I might
                    have concealed any information/provided any false documents at the time of my admission, the
                    university authorities will have every right to cancel my admission including all exams, for
                    which I will have no objection.
                </p>
            </td>
        </tr>
    </table>

    <br>
    <table border="0" width="100%" cellpadding="5" style="border:0px solid #cacaca;border-collapse: collapse;font-size: 12px;">
        <tr>
            <th colspan="100%"><br></th>
        </tr>
        <tr>

            <th colspan="50%" style="text-align: right !important;">
                ______________________________<br>
                Full Name of the Student<br>
            </th>
        </tr>
    </table>
    <br>
    <table border="1" class="tbl_merit" width="100%" cellpadding="5" style=" border:1px solid #cacaca;border-collapse: collapse;font-size: 11px;">
        <tr>
            <th>Course</th>
            <td></td>

            <th>Session</th>
            <td></td>
        </tr>

        <tr>
            <th colspan="1">Father's Name</th>
            <td colspan="3"><?php echo $student_acadamic_data['SFNAME']; ?></td>
        </tr>
        <tr>
            <th colspan="1">Mother's Name</th>
            <td colspan="3"><?php echo $student_acadamic_data['SMNAME']; ?></td>
        </tr>
        <tr>
            <th colspan="1">Village/Ward</th>
            <td colspan="3"><?php echo $std_per_data['padd_village']; ?></td>
        </tr>
        <tr>
            <th>House</th>
            <td><?php echo $std_per_data['padd_house']; ?></td>

            <th>Road</th>
            <td><?php echo $std_per_data['padd_road']; ?></td>
        </tr>
        <tr>
            <th colspan="1">Post Office</th>
            <td colspan="3"><?php echo $std_per_data['padd_post_office']; ?></td>
        </tr>

        <tr>
            <th>Upazila</th>
            <td><?php echo $std_per_data['padd_police_station']; ?></td>

            <th>District</th>
            <td><?php echo $std_per_data['padd_district']; ?></td>
        </tr>


    </table>

    <br>
    <table border="0" width="100%" cellpadding="5" style="border:0px solid #cacaca;border-collapse: collapse;font-size: 12px;">
        <tr>
            <th colspan="100%"><br></th>
        </tr>
        <tr>
            <th colspan="100%">The above student has signed this undertaking before me.</th>
        </tr>

        <tr>

            <th colspan="50%" style="text-align: left !important;">
                <br> _______________________________<br>
                Signature of the Chairman<br><br>
                Department of..........................................<br>
                <br><br>
                Seal.......................Date..............................
            </th>
            <th align="left" colspan="50%" style="text-align: right !important;">
            </th>
        </tr>
    </table>
    <br>
    <br>
    <table width="100%" style="border: 1px solid black;">
        <tr>
            <td colspan="2" style="text-align: left;font-size: 9px;text-align: center;">
                © 2017-2018; <?php echo UNIVERSITY_NAME; ?>. All rights reserved.
            </td>
        </tr>
    </table>

</body>

</html>