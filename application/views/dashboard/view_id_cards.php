<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>PUST ID</title>

</head>

<body>
    <?php
    $arr_all_subject_code_name = get_all_subject_code_name();
    $arr_all_subject_faculty_name = get_all_subject_faculty_name();
    $arr_all_subject_dep_name = get_all_subject_dep_name();
    $arr_all_course_name = get_all_course_name();
    ?>
    <?php
    $index = 0;
    foreach ($MAIN_DATA as $MAIN_DATA_key => $MAIN_DATA_value) {
    ?>
        <table border="0" style="width: 850px;">
            <tr>
                <td style="width: 45%;">

                    <table border="0" style="width: 100%;border:0px solid #cacaca;border-collapse: collapse;">
                        <tr>
                            <td colspan="3" style="text-align: center;width: 100%">
                                <span style="top:0px;font-size: 17px;font-weight:bold"><?php echo UNIVERSITY_NAME; ?></span>
                                <br>
                                Student Identity Card
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 15%;text-align: left;">
                                <img style="width: 50px; height: auto" alt="<?php echo UNIVERSITY_NAME; ?>" src="<?php echo base_url(); ?>assets/images/logo.png" />
                            </td>
                            <td style="width: 65%;text-align: justify;">
                                <?php echo "ID CARD NO. " . $MAIN_DATA_value['my_result']['uni_id_no']; ?>
                                <br>
                                <?php echo   $MAIN_DATA_value['student_acadamic_data']['SNAME']; ?>
                                <br>
                                <?php echo   "DOB: " . $MAIN_DATA_value['std_per_data']['dob']; ?>
                                <br>

                            </td>
                            <td style="width: 20%;text-align: right;">
                                <img style="width: 50px; height: auto" alt="Photo" src="<?php echo  "http://admission1920.pust.ac.bd"; ?>/uploads/<?php echo $MAIN_DATA_value['student_acadamic_data']['IMGUPLOAD_PATH']; ?>/<?php echo $MAIN_DATA_value['my_result']['system_reg_id_ref']; ?>.jpg" />
                            </td>
                        </tr>

                        <tr>
                            <td colspan="3" style="text-align: center;width: 100%;font-size: 14px;">
                                <?php echo "Department: " . $arr_all_subject_dep_name[$MAIN_DATA_value['my_result']['sub_code_6']]; ?>
                                <br>
                                <?php echo "Reg. No. " . $MAIN_DATA_value['my_result']['class_reg']; ?> <?php echo "Roll No. " . $MAIN_DATA_value['my_result']['class_roll']; ?>
                                <br>
                                <?php
                                if ($MAIN_DATA_value['std_per_data']['gender'] == "MALE") {
                                    echo "Bangabandhu Sheikh Mujibur Rahman Hall";
                                } else {
                                    echo "Sheikh Hasina Hall";
                                }
                                ?>
                                <br>
                            </td>
                        </tr>


                    </table>
                    <table border="0" style="width: 100%;border:0px solid #cacaca;border-collapse: collapse;">
                        <tr>
                            <td><br><br>Student's Signature</td>
                            <td style="text-align: right;"><br><br>Signature of Provost</td>
                        </tr>
                    </table>

                </td>
                <td style="width: 5%;"></td>
                <td style="width: 45%;vertical-align: top;">

                    <table border="0" style="width: 100%;border:0px solid #cacaca;border-collapse: collapse;">
                        <tr>
                            <td colspan="2" style="text-align: center;width: 100%; font-size: 13px;">
                                Permanent Address:
                                <?php
                                echo "Village/Ward: " . strtolower($MAIN_DATA_value['std_per_data']['padd_village']) . ", House: " . strtolower($MAIN_DATA_value['std_per_data']['padd_house']) . ", ";
                                echo "<br>";
                                echo "Road: " . strtolower($MAIN_DATA_value['std_per_data']['padd_road']) . ", Post Office: " . strtolower($MAIN_DATA_value['std_per_data']['padd_post_office']) . ", ";
                                echo "<br>";
                                echo "Upazila: " . strtolower($MAIN_DATA_value['std_per_data']['padd_police_station']) . ", District: " . strtolower($MAIN_DATA_value['std_per_data']['padd_district']);
                                ?></td>
                        </tr>
                        <tr>
                            <td colspan="2" style="text-align: center;">Mobile No. <?php echo $MAIN_DATA_value['std_per_data']['mobile_no']; ?>, <?php echo $MAIN_DATA_value['std_per_data']['local_gurdian_phone']; ?></td>
                        </tr>
                        <tr>
                            <td colspan="2" style="text-align: center;">Blood Group: ______ , Valid Till : Dec 2023 </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="text-align: center;width: 100%; font-size: 13px;">
                                If this card is found anywhere other than in possession of the legal owner, please return it to the address below:
                            </td>
                        </tr>
                        <tr>
                            <?php
                            $arr_replace =  array('.', ';', ',');
                            $array_mother =  array(001021351);
                            //if( $MAIN_DATA_value['my_result']['class_reg'] == "1021351" || $MAIN_DATA_value['my_result']['class_reg'] == "1195147" || $MAIN_DATA_value['my_result']['class_reg'] == "1195111"){
                            if (in_array($MAIN_DATA_value['my_result']['class_reg'], $array_mother)) {
                            ?>
                                <td style="width: 25%"> <img src="https://chart.googleapis.com/chart?chs=75x75&cht=qr&chl=<?php echo str_replace($arr_replace,  "", $MAIN_DATA_value['student_acadamic_data']['SNAME']) . ' ' . $MAIN_DATA_value['my_result']['class_roll'] . ' ' . $MAIN_DATA_value['my_result']['class_reg'] . ' ' . str_replace($arr_replace,  "", $MAIN_DATA_value['student_acadamic_data']['SFNAME']); ?> &choe=UTF-8"></td>
                            <?php } else {  ?>
                                <td style="width: 25%"> <img src="https://chart.googleapis.com/chart?chs=75x75&cht=qr&chl=<?php echo str_replace($arr_replace,  "", $MAIN_DATA_value['student_acadamic_data']['SNAME']) . ' ' . $MAIN_DATA_value['my_result']['class_roll'] . ' ' . $MAIN_DATA_value['my_result']['class_reg']; ?> &choe=UTF-8"></td>
                            <?php } ?>
                            <td style="width: 75% ; font-size: 12px;">
                                Sheikh Hasina University Rajapur,Pabna 6600
                                <br>
                                Phone: 0731-66742 , Web: www.pust.ac.bd
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">&nbsp;&nbsp;&nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>



    <?php
        $index++;
        if ($index % 4 == 0) {
            echo "<pagebreak />";
        } else {
            echo "<br><br><br>";
        }
    } ?>
</body>

</html>