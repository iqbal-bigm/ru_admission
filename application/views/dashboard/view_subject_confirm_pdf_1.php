<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>PUST Subject Option Form</title>
    <style>
        table th {
            text-align: left;
        }
    </style>
</head>

<body>

    <table style="width: 100%" border="0">
        <tr>
            <td>
                <img style="width: 80px; height: auto" alt="<?php echo UNIVERSITY_NAME; ?>" src="<?php echo base_url(); ?>assets/images/logo.png" />
            </td>
            <td style="text-align: center;" colspan="2">
                <p style="top:0px;font-size: 23px;color:#C00000;font-weight:bold"><?php echo UNIVERSITY_NAME; ?></p>
                <br>
                <p style="font-size:17px;color:#0000FF;"><b>Subject Choice Confirmation Slip (Session 2017-2018)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></p>
            </td>
            <td>
                <img style="width: 80px; height: auto" alt="Photo" src="<?php echo base_url(); ?>uploads/<?php echo $student_login_photo_path; ?>/<?php echo $unit_details['system_reg_id']; ?>.jpg" />
            </td>
        </tr>
    </table>
    <br>
    <table border="1" width="100%" cellpadding="5" style="border:1px solid #cacaca;border-collapse: collapse;font-size: 15px;">
        <tr>
            <th>Merit Position</th>
            <th>Score</th>
            <th>Roll Number</th>
            <th>Application ID</th>
            <th>Unit</th>
            <th>Quota</th>
        </tr>
        <tr>
            <td><?php echo $unit_details['merit']; ?></td>
            <td><?php echo $unit_details['score']; ?></td>
            <td><?php echo $unit_details['exam_roll']; ?></td>
            <td><?php echo $unit_details['app_id']; ?></td>
            <td><?php echo $unit_details['unit']; ?></td>
            <td>
                <?php
                if (!empty($unit_details['quota_name'])) {
                    $quota_name_arr = get_quota_array();
                    echo $quota_name_arr[$unit_details['quota_name']];
                }

                ?>
            </td>
        </tr>
        <tr>
            <th colspan="2" style="text-align: left;padding: 10px;">
                Subject Choice ::
            </th>
            <td colspan="4" style="text-align: left;padding: 10px;">
                <?php
                $arr_all_subject_code_name = get_all_subject_code_name();

                if (!empty($unit_wise_subject_list_existing)) {
                    $indexer = 1;
                    foreach ($unit_wise_subject_list_existing as $key => $value) {
                        echo $indexer . ". " . $arr_all_subject_code_name[$value['sub_code_ref']] . " ";
                        $indexer++;
                    }
                }
                ?>

            </td>
    </table>
    <br>

    <table border="1" width="100%" cellpadding="5" style="border:1px solid #cacaca;border-collapse: collapse;font-size: 12px;">

        <tr>
            <th colspan="2">Name</th>
            <td colspan="2"><?php echo $student_acadamic_data['SNAME']; ?></td>
        </tr>
        <tr>
            <th>Father's Name</th>
            <td><?php echo $student_acadamic_data['SFNAME']; ?></td>

            <th>Mother's Name</th>
            <td><?php echo $student_acadamic_data['SMNAME']; ?></td>
        </tr>
        <tr>
            <th>Nationality</th>
            <td>Bangladeshi</td>
            <th>Home District</th>
            <td><?php echo $std_per_data['district']; ?></td>

        </tr>

        <tr>
            <th>Marital Status</th>
            <td><?php echo $std_per_data['maritlal_status']; ?></td>
            <th>Gender</th>
            <td><?php echo $std_per_data['gender']; ?></td>
        </tr>

        <tr>
            <th>Birth Date</th>
            <td><?php echo $std_per_data['dob']; ?></td>
            <th>Mobile No.</th>
            <td><?php echo $std_per_data['mobile_no']; ?></td>

        </tr>


        <tr>
            <th>Local Guardian's Name</th>
            <td><?php echo $std_per_data['local_gurdian_name']; ?></td>
            <th>Religion</th>
            <td><?php echo $std_per_data['religion']; ?></td>

        </tr>
        <tr>
            <th>Relation With Local Guardian</th>
            <td><?php echo $std_per_data['local_gurdian_realation']; ?></td>
            <th>Local Guardian's Mobile No.</th>
            <td><?php echo $std_per_data['local_gurdian_phone']; ?></td>

        </tr>
        <tr>
            <th>Permanent Address</th>
            <td><?php echo $std_per_data['permanent_address']; ?></td>
            <th>Mailing Address</th>
            <td><?php echo $std_per_data['mailing_address']; ?></td>

        </tr>
    </table>
    <br>
    <table border="1" width="100%" cellpadding="5" style="border:1px solid #cacaca;border-collapse: collapse;font-size: 12px;">
        <tr>
            <th></th>
            <th>Group</th>
            <th>ROLL NO.</th>
            <th>REG. NO.</th>
            <th>GPA</th>
            <th>Board</th>
            <th>Year</th>
        </tr>
        <tr>
            <th style="text-align: left;">SSC/ Equivalent</th>
            <td><?php echo $student_acadamic_data['SGROUP']; ?>
            <td><?php echo $student_acadamic_data['SROLL_NO']; ?></td>
            <td><?php echo $student_acadamic_data['SREG_NO']; ?></td>
            <td><?php echo $student_acadamic_data['SGPA']; ?></td>
            <td><?php echo $student_acadamic_data['SBOARD_NAME']; ?></td>
            <td><?php echo $student_acadamic_data['SPASS_YEAR']; ?></td>
        </tr>
        <tr>
            <th style="text-align: left;">HSC/ Equivalent</th>
            <td><?php echo $student_acadamic_data['HGROUP']; ?>
            <td><?php echo $student_acadamic_data['HROLL_NO']; ?></td>
            <td><?php echo $student_acadamic_data['HREG_NO']; ?></td>
            <td><?php echo $student_acadamic_data['HGPA']; ?></td>
            <td><?php echo $student_acadamic_data['HBOARD_NAME']; ?></td>
            <td><?php echo $student_acadamic_data['HPASS_YEAR']; ?></td>
        </tr>

    </table>

    <br>
    <table border="1" width="100%" cellpadding="5" style="border:1px solid #cacaca;border-collapse: collapse;font-size: 12px;">
        <!--  <tr>
                        <th colspan="50%">For Official Use Only</th>
                        <th colspan="50%">For Department Use Only</th>
                    </tr>     
                    <tr>
                        <td  colspan="50%">
                            <p>Session....................</p>
                            <p>Subject....................</p>
                            <p>Course....................</p>
                            <p>Registration NO.....................</p>
                        </td>
                        <td colspan="50%">For Department Use Only</td>
                    </tr>   -->
        <tr>
            <td>
                <p> I, <?php echo $student_acadamic_data['SNAME']; ?>, do hereby declare that the above mentioned information and photo are correct. If
                    any information provided by me is found false, National University reserves the right to cancel my admission.
                    I shall be obliged to obey the rules and regulations of <?php echo UNIVERSITY_NAME; ?> as well as my college and to pay all
                    the required fees.</p>
            </td>
        </tr>
    </table>

    <br>
    <table border="0" width="100%" cellpadding="5" style="border:1px solid #cacaca;border-collapse: collapse;font-size: 12px;">
        <tr>
            <th colspan="100%"><br><br></th>
        </tr>
        <tr>
            <th colspan="50%">
                ___________________________________________<br>
                Signature of the Student & Date
            </th>
            <th colspan="50%" style="text-align: right !important;">
                __________________________________________________<br>
                Signature of Father/Mother/Guardian & Date</th>
        </tr>
        <tr>
            <th colspan="100%"><br><br><br></th>
        </tr>
        <tr>
            <th colspan="50%">
                ____________________________________________<br>
                Seal, Signature of Dept. Head & Date
            </th>
            <th colspan="50%" style="text-align: right !important;">
                _________________________________________________<br>
                Seal, Signature of Dept. Chairman & Date</th>
        </tr>

    </table>

    <br>
    <table width="100%" style="border: 1px solid black;">
        <tr>
            <td colspan="2" style="text-align: left;font-size: 12px;text-align: center;">
                © 2017-2018; <?php echo UNIVERSITY_NAME; ?>. All rights reserved.
            </td>
        </tr>
    </table>
    <table width="100%">
        <tr>
            <td colspan="2" style="font-size: 12px;text-align: center;">
                www.pust.ac.bd
            </td>

        </tr>
    </table>

</body>

</html>