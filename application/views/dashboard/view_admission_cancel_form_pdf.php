<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>PUST</title>
    <style>
        table th {
            text-align: left;
        }

        .tbl_merit th {
            background-color: #EEEEEE;
        }
    </style>
</head>

<body>

    <table class="tbl_merit" border="0" width="100%" cellpadding="1" style="border:0px solid #cacaca;border-collapse: collapse;font-size: 12px;">
        <tr>
            <td>
                To
                <br>
                The Additional Registrar
                <br>
                Sheikh Hasina University
                <br>
                Pabna-6600.
            </td>
        </tr>
        <tr>
            <td style="padding: 5px;">
                <?php
                $arr_all_subject_dep_name = get_all_subject_dep_name();
                $arr_all_subject_code_name = get_all_subject_code_name();
                ?>
                <br>
                Through: Chairman, <?php echo $arr_all_subject_dep_name[$sub_code]; ?>, PUST.
                <br>
                <br>
            </td>
        </tr>
        <tr>
            <td style="padding: 5px;">
                Respected Sir,
                <br>
                I am writing this letter to request for the cancellation of admission from your university. I regret to inform you that I wish to cancel my admission from your University due to the following causes:
            </td>
        </tr>
        <tr>
            <td style="padding: 15px;">
                <p>
                    <?php echo $result_details['cancel_reason']; ?>
                </p>
            </td>
        </tr>
        <tr>
            <td style="padding: 5px;">
                I request you with honor to cancel my admission and return me the documents that I have been submitted (all original documents) at the admission desk to the department. I would like to inform you, I have paid admission cancellation fee of Tk. 2000.
                I am looking forward for an early response from you.
            </td>
        </tr>
        <tr>
            <td>
                Sincerely yours
                <br><br><br>
                ___________________________<br>
                (Signature of the student)
            </td>
        </tr>

    </table>
    <br>

    <br>
    <table border="1" class="tbl_merit" width="100%" cellpadding="5" style="border:1px solid #cacaca;border-collapse: collapse;font-size: 11px;">
        </tr>
        <tr>
            <th colspan="1">Name</th>
            <td colspan="3"><?php echo $student_acadamic_data['SNAME']; ?></td>
            <td rowspan="8">
                <img style="width: 130px; height: auto" alt="Photo" src="<?php echo "http://admission1920.pust.ac.bd/"; //base_url(); 
                                                                            ?>uploads/<?php echo $student_login_photo_path; ?>/<?php echo $result_details['system_reg_id_ref']; ?>.jpg" />
            </td>
        </tr>

        <tr>
            <th colspan="1">Father's Name</th>
            <td colspan="3"><?php echo $student_acadamic_data['SFNAME']; ?></td>
        </tr>
        <tr>
            <th colspan="1">Subject</th>
            <td colspan="3"><?php echo $arr_all_subject_code_name[$sub_code]; ?></td>
        </tr>

        <tr>
            <th>Admission Roll</th>
            <td><?php echo $result_details['exam_roll_ref']; ?></td>
            <th>Merit Position</th>
            <td><?php echo $result_details['merit_pos_ref']; ?></td>
        </tr>

        <tr>
            <th colspan="1">Address</th>
            <td colspan="3">
                <?php
                echo "Village/Ward: " . $std_per_data['padd_village'] . ", House: " . $std_per_data['padd_house'] . ", ";
                echo "<br>";
                echo "Road: " . $std_per_data['padd_road'] . ", Post Office: " . $std_per_data['padd_post_office'] . ", ";
                echo "<br>";
                echo "Upazila: " . $std_per_data['padd_police_station'] . ", District: " . $std_per_data['padd_district'];
                ?>
            </td>
        </tr>
        <tr>
            <th colspan="1">Mobile No.</th>
            <td colspan="3"><?php echo $std_per_data['mobile_no']; ?></td>
        </tr>
        <tr>
            <th>Amount</th>
            <td><?php echo $result_details['cancel_paid_amount']; ?></td>
            <th>Payment Trxn. ID</th>
            <td><?php echo $result_details['cancel_payment_trans_id']; ?></td>
        </tr>
        <tr>
            <th colspan="1">Payment Date</th>
            <td colspan="3"><?php echo $result_details['cancel_payment_date']; ?></td>
        </tr>

    </table>
    <br>
    <br>
    <br>

    <table border="0" class="tbl_merit" width="100%" cellpadding="5" style="border:0px solid #cacaca;border-collapse: collapse;font-size: 11px;">
        <tr>
            <td><br><br>
                ___________________________________<br>
                Recommendation of the Chairman<br>
                <?php echo $arr_all_subject_dep_name[$sub_code]; ?> <br>
                (For Department Clearance)
            </td>
            <td style="text-align: right;"><br><br>
                __________________________________<br>
                Recommendation of Hall Provost<br>
                (For Hall Clearance)
            </td>
        </tr>
    </table>
    <br>

</body>

</html>