<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>PUST Admission 2019</title>
    <style>
        table th {
            text-align: left;
        }

        .tbl_merit th {
            background-color: #EEEEEE;
        }
    </style>
</head>

<body>
    <table border="1" style="width: 100%;border:1px solid #cacaca;border-collapse: collapse;">

        <tr>
            <td style="width: 100%;">
                <table border="0" style="width: 100%;border:0px solid #cacaca;border-collapse: collapse;">
                    <thead>
                        <tr>
                            <td style="width: 20%">
                                <img style="width: 40px; height: auto" alt="<?php echo UNIVERSITY_NAME; ?>" src="<?php echo base_url(); ?>assets/images/logo.png" />
                            </td>
                            <td style="text-align: center;width: 70%" colspan="2" valign="top">
                                <span style="top:0px;font-size: 16px;font-weight:bold"><?php echo UNIVERSITY_NAME; ?></span>
                                <br>
                                Pabna - 6600, Bangladesh
                                <br>
                                <span style="font-size:14px;"><b>Student Admission Form (Session 2019-2020) </b></span>
                            </td>
                        </tr>
                    </thead>
                </table>
            </td>
        </tr>
    </table>
    <br>

    <table class="tbl_merit" border="1" width="100%" cellpadding="2" style="border:1px solid #cacaca;border-collapse: collapse;font-size: 10px;">
        <tr>
            <td width="20%">
                <img style="width: 130px; height: auto" alt="Photo" src="<?php echo "http://admission1920.pust.ac.bd/"; ?>uploads/<?php echo $student_login_photo_path; ?>/<?php echo $unit_details['system_reg_id']; ?>.jpg" />
            </td>
            <td width="60%" valign="top">
                <?php
                $arr_all_subject_code_name = get_all_subject_code_name();
                $arr_all_subject_faculty_name = get_all_subject_faculty_name();
                $arr_all_subject_dep_name = get_all_subject_dep_name();
                ?>
                <table class="tbl_merit" border="1" width="100%" cellpadding="1" style="border:1px solid #cacaca;border-collapse: collapse;font-size: 11px;">
                    <tr>
                        <td colspan="4" style="text-align: center">For official use only</td>
                    </tr>
                    <tr>
                        <td>Subject</td>
                        <td colspan="3"><?php echo $arr_all_subject_code_name[$my_result['sub_code_1']]; ?></td>
                    </tr>
                    <tr>
                        <td>Faculty</td>
                        <td colspan="3"><?php echo $arr_all_subject_faculty_name[$my_result['sub_code_1']]; ?></td>
                    </tr>
                    <tr>
                        <td>Hall</td>
                        <td colspan="3">
                            <?php
                            if ($std_per_data['gender'] == "MALE") {
                                echo "Bangabandhu Sheikh Mujibur Rahman Hall";
                            } else {
                                echo "Sheikh Hasina Hall";
                            }
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Admission Roll</td>
                        <td><?php echo $unit_details['exam_roll']; ?></td>
                        <td>Unit</td>
                        <td>
                            <?php
                            echo $unit_details['unit'];
                            if ($unit_details['unit'] == "A") {
                                echo ' (' . $unit_details['optional'] . ')';
                            };
                            if (!empty($my_result['quota_ref'])) {
                                $quota_ref_arr = get_quota_array();
                                echo "( Quota: " . $quota_ref_arr[$my_result['quota_ref']] . " )";
                            }
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Merit Position</td>
                        <td><?php echo $unit_details['merit']; ?></td>
                        <td>Score</td>
                        <td><?php echo $unit_details['score']; ?></td>
                    </tr>
                    <tr>
                        <td>Session</td>
                        <td>2019-2020</td>
                        <td>Amount Paid</td>
                        <td><?php echo $my_result['paid_amount']; ?></td>
                    </tr>
                    <tr>
                        <td>Registration No.</td>
                        <td></td>
                        <td>Txn ID</td>
                        <td><?php echo $my_result['payment_trans_id']; ?></td>
                    </tr>
                    <tr>
                        <td>Class Roll</td>
                        <td></td>
                        <td>Payment Date</td>
                        <td><?php echo $my_result['payment_date']; ?></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

    <br>
    <table border="1" class="tbl_merit" width="100%" cellpadding="2" style="border:1px solid #cacaca;border-collapse: collapse;font-size: 10px;">

        <tr>
            <th colspan="1">1. a. Name In English</th>
            <td colspan="3"><?php echo $student_acadamic_data['SNAME']; ?></td>
        </tr>

        <tr>
            <th colspan="1">1. b. Name In Bangla</th>
            <td colspan="3"><br>...........................................................................</td>
        </tr>

        <tr>
            <th>2. a. Father's Name</th>
            <td><?php echo $student_acadamic_data['SFNAME']; ?></td>

            <th>2. c. Mother's Name</th>
            <td><?php echo $student_acadamic_data['SMNAME']; ?></td>
        </tr>
        <tr>
            <th>2. b. Father's Occupation</th>
            <td><?php echo $std_per_data['father_occupation']; ?></td>

            <th>2. d. Mothers's Occupation</th>
            <td><?php echo $std_per_data['mother_occupation']; ?></td>
        </tr>
        <tr>
            <th>2. e. Yearly Family Income (in taka)</th>
            <td><?php echo $std_per_data['family_income']; ?></td>

            <th>6. BNCC/Rover Scout</th>
            <td><?php echo $std_per_data['scout']; ?></td>
        </tr>
        <tr>
            <th>3. Nationality</th>
            <td>Bangladeshi</td>
            <th>7. Home District</th>
            <td><?php echo $std_per_data['district']; ?></td>

        </tr>

        <tr>
            <th>4. Marital Status</th>
            <td> <?php echo $std_per_data['maritlal_status']; ?></td>
            <th>8. Gender</th>
            <td><?php echo $std_per_data['gender']; ?></td>
        </tr>

        <tr>
            <th>5. Date of Birth</th>
            <td><?php echo $std_per_data['dob']; ?></td>
            <th>9. Mobile No.</th>
            <td><?php echo $std_per_data['mobile_no']; ?></td>

        </tr>

        <tr>
            <th>10. Permanent Address</th>
            <td>
                <?php
                //echo $std_per_data['permanent_address'];
                echo "Village/Ward: " . $std_per_data['padd_village'] . ", House: " . $std_per_data['padd_house'] . ", ";
                echo "<br>";
                echo "Road: " . $std_per_data['padd_road'] . ", Post Office: " . $std_per_data['padd_post_office'] . ", ";
                echo "<br>";
                echo "Upazila: " . $std_per_data['padd_police_station'] . ", District: " . $std_per_data['padd_district'];
                ?>
            </td>
            <th>11. Present Address</th>
            <td><?php //echo $std_per_data['mailing_address'];
                echo "Village/Ward: " . $std_per_data['prad_village'] . ", House: " . $std_per_data['prad_house'] . ", ";
                echo "<br>";
                echo "Road: " . $std_per_data['prad_road'] . ", Post Office: " . $std_per_data['prad_post_office'] . ", ";
                echo "<br>";
                echo "Upazila: " . $std_per_data['prad_police_station'] . ", District: " . $std_per_data['prad_district'];
                ?>
            </td>
        </tr>
        <tr>
            <th>12. a. Local Guardian's Name</th>
            <td><?php echo $std_per_data['local_gurdian_name']; ?></td>
            <th>12. b. Local Guardian's Mobile</th>
            <td><?php echo $std_per_data['local_gurdian_phone']; ?></td>
        </tr>
        <tr>
            <th>12. c. Relation With Local Guardian</th>
            <td><?php echo $std_per_data['local_gurdian_realation']; ?></td>
            <th>13. Religion</th>
            <td><?php echo $std_per_data['religion']; ?></td>

        </tr>
        <tr>
            <th>12. d. Local Guardian's Address</th>
            <td colspan="3"><?php echo $std_per_data['local_gurdian_address']; ?></td>
        </tr>
    </table>
    <br>
    <table class="tbl_merit" border="1" width="100%" cellpadding="1" style="border:1px solid #cacaca;border-collapse: collapse;font-size: 10px;">
        <tr>
            <td><b>14. Educational Information:</b></td>
        </tr>
        <tr>
            <th>Exam. Name</th>
            <th>Group</th>
            <th>ROLL NO.</th>
            <th>REG. NO.</th>
            <th>GPA</th>
            <th>Board</th>
            <th>Year</th>
        </tr>
        <tr>
            <th style="text-align: left;">SSC/ Equivalent</th>
            <td><?php echo $student_acadamic_data['SGROUP']; ?>
            <td><?php echo $student_acadamic_data['SROLL_NO']; ?></td>
            <td><?php echo $student_acadamic_data['SREG_NO']; ?></td>
            <td><?php echo $student_acadamic_data['SGPA']; ?></td>
            <td><?php echo $student_acadamic_data['SBOARD_NAME']; ?></td>
            <td><?php echo $student_acadamic_data['SPASS_YEAR']; ?></td>
        </tr>
        <tr>
            <th style="text-align: left;">HSC/ Equivalent</th>
            <td><?php echo $student_acadamic_data['HGROUP']; ?>
            <td><?php echo $student_acadamic_data['HROLL_NO']; ?></td>
            <td><?php echo $student_acadamic_data['HREG_NO']; ?></td>
            <td><?php echo $student_acadamic_data['HGPA']; ?></td>
            <td><?php echo $student_acadamic_data['HBOARD_NAME']; ?></td>
            <td><?php echo $student_acadamic_data['HPASS_YEAR']; ?></td>
        </tr>

    </table>
    <table border="1" width="100%" cellpadding="2" style="border:1px solid #cacaca;border-collapse: collapse;font-size: 10px;">
        <tr>
            <td colspan="4"><b>15. Documents to be attached: [ Original Set and 02 Sets Photocopy=03 Sets]:</b></td>
        </tr>
        <tr>
            <td>A.</td>
            <td>SSC/Equivalent Original Mark sheet and Certificate</td>
            <td>D.</td>
            <td>HSC/Equivalent Original Testimonial</td>
        </tr>
        <tr>
            <td>B.</td>
            <td>HSC/Equivalent Original Mark Sheet and Certificate</td>
            <td>E.</td>
            <td>Original Admit Card of Admission Test</td>
        </tr>
        <tr>
            <td>C.</td>
            <td>HSC/Equivalent Original Registration Card</td>
            <td>F.</td>
            <td>Four copy color PP size photograph</td>
        </tr>
    </table>
    <table border="1" width="100%" cellpadding="2" style="border:1px solid #cacaca;border-collapse: collapse;font-size: 10px;">
        <tr>
            <td colspan="2"><b>16. Instructions:</b></td>
        </tr>
        <tr>
            <td valign="top">A.</td>
            <td>In this Admission form any concealed/wrong information supplied or any false mark sheet enclosed by the student will be considered as an
                offence and for this offence his/her admission will be cancelled and he/she will be punished according to the rules of the university.</td>
        </tr>
        <tr>
            <td valign="top">B.</td>
            <td>If a student to himself admitted in another Institution/different institutions in the same session/ in different sessions in several courses, he/she
                must have to produce cancellation certificate of such admissions to the Registrar within 14 days of his/her admission in the University.</td>
        </tr>
    </table>

    <br>
    <table border="0" width="100%" cellpadding="5" style="border:0px solid #cacaca;border-collapse: collapse;font-size: 12px;">

        <tr>

            <th colspan="50%" style="text-align: left !important;">
                ______________________________________<br>
                Signature of the Student & Date<br>
            </th>
            <th align="left" colspan="50%" style="text-align: right !important;">
                ____________________________________<br>
                Signature of Hall Provost<br>
                (Seal)
            </th>
        </tr>
        <tr>
            <th colspan="100%"><br></th>
        </tr>
        <tr>

            <th colspan="50%" style="text-align: left !important;">
                __________________________________________________<br>
                Signature of the Dean of the Faculty<br>
                (Seal)
            </th>
            <th align="left" colspan="50%" style="text-align: right !important;">
                __________________________________________________<br>
                Signature of Chairman of the Department<br>
                (Seal)
            </th>
        </tr>

    </table>

    <pagebreak />
    <table border="1" style="width: 100%;border:1px solid #cacaca;border-collapse: collapse;">

        <tr>
            <td style="width: 100%;">
                <table border="0" style="width: 100%;border:0px solid #cacaca;border-collapse: collapse;">
                    <thead>
                        <tr>
                            <td style="width: 20%">
                                <img style="width: 40px; height: auto" alt="<?php echo UNIVERSITY_NAME; ?>" src="<?php echo base_url(); ?>assets/images/logo.png" />
                            </td>
                            <td style="text-align: center;width: 70%" colspan="2" valign="top">
                                <span style="top:0px;font-size: 16px;font-weight:bold"><?php echo UNIVERSITY_NAME; ?></span>
                                <br>
                                Pabna - 6600, Bangladesh
                                <br>
                                <span style="font-size:14px;"><b>Student Admission Form (Session 2019-2020) </b></span>
                            </td>
                        </tr>
                    </thead>
                </table>
            </td>
        </tr>
    </table>
    <br>

    <table border="1" style="width: 100%;border:1px solid #cacaca;border-collapse: collapse;">
        <thead>
            <tr>
                <td style="text-align: center;width: 100%" colspan="2" valign="top">
                    <span style="top:0px;font-size: 20px;font-weight:bold"> Undertaking by the Students</span>
                </td>
            </tr>
        </thead>
    </table>
    <br>
    <table border="1" width="100%" cellpadding="1" style="border:1px solid #cacaca;border-collapse: collapse;font-size: 11px;">

        <tr>
            <td><b>I do hereby undertake as follows::</b></td>
        </tr>
        <tr>
            <td style="padding: 10px;">
                <p style="padding-left: 15px;">
                    1. I will be held responsible if I violate the rules of the university or any damages I may do to the university
                    property or any sort of misconduct. I will also follow other General Rules of Discipline for the students in
                    accordance with the university rules and regulations.
                </p>
                <br>
                <p style="padding-left: 15px;">
                    2. (a) I will pay the fees as decided by the university authorities.
                    <br>
                    (b) I will also pay other fees to be fixed by the university authorities from time to time.
                </p>
                <br>
                <p style="padding-left: 15px;">
                    3. I will follow the general instructions as mentioned in the admission form.
                    <br><br>
                </p>
            </td>
        </tr>
    </table>
    <br>
    <table style="width: 100%;border:0px solid #cacaca;border-collapse: collapse;font-size: 11px;">
        <tr>
            <td>
                <p style="text-align: justify; padding-left: 15px;">
                    I, therefore, declare that, if I violate the above mentioned rules of the university and if I might have concealed
                    any information/provided any false documents at the time of my admission, the university authorities will have
                    every right to cancel my admission including all exams, for which I will have no objection.</p>
            </td>
        </tr>
    </table>

    <br>
    <table border="0" width="100%" cellpadding="5" style="border:0px solid #cacaca;border-collapse: collapse;font-size: 12px;">
        <tr>
            <th colspan="100%"><br></th>
        </tr>
        <tr>

            <th colspan="50%" style="text-align: right !important;">
                ______________________________<br>
                Signature of the Student<br>
            </th>
        </tr>
    </table>
    <br>
    <table border="1" class="tbl_merit" width="100%" cellpadding="5" style=" border:1px solid #cacaca;border-collapse: collapse;font-size: 11px;">
        <tr>
            <th>Student Name</th>
            <td colspan="3"><?php echo $student_acadamic_data['SNAME']; ?></td>
        </tr>

        <tr>
            <th>Department</th>
            <td colspan="3"><?php echo $arr_all_subject_code_name[$my_result['sub_code_1']]; ?></td>
        </tr>
        <tr>
            <th>Faculty</th>
            <td><?php echo $arr_all_subject_faculty_name[$my_result['sub_code_1']]; ?></td>

            <th>Session</th>
            <td>2019-2020</td>
        </tr>

        <tr>
            <th colspan="1">Father's Name</th>
            <td colspan="3"><?php echo $student_acadamic_data['SFNAME']; ?></td>
        </tr>
        <tr>
            <th colspan="1">Mother's Name</th>
            <td colspan="3"><?php echo $student_acadamic_data['SMNAME']; ?></td>
        </tr>
        <tr>
            <th>Admission Roll</th>
            <td><?php echo $unit_details['exam_roll']; ?></td>

            <th>Merit Position</th>
            <td><?php echo $unit_details['merit']; ?></td>
        </tr>
        <tr>
            <th>Mobile No.</th>
            <td><?php echo $std_per_data['mobile_no']; ?></td>

            <th>Score</th>
            <td><?php echo $unit_details['score']; ?></td>
        </tr>
        <tr>
            <th>Village/Ward</th>
            <td><?php echo $std_per_data['padd_village']; ?></td>
            <th>Post Office</th>
            <td><?php echo $std_per_data['padd_post_office']; ?></td>
        </tr>
        <tr>
            <th>House No.</th>
            <td><?php echo $std_per_data['padd_house']; ?></td>

            <th>Road No.</th>
            <td><?php echo $std_per_data['padd_road']; ?></td>
        </tr>
        <tr>
            <th>Upazila</th>
            <td><?php echo $std_per_data['padd_police_station']; ?></td>

            <th>District</th>
            <td><?php echo $std_per_data['padd_district']; ?></td>
        </tr>

    </table>

    <br>
    <table border="0" width="100%" cellpadding="5" style="border:0px solid #cacaca;border-collapse: collapse;font-size: 12px;">
        <tr>
            <th colspan="100%"><br></th>
        </tr>
        <tr>
            <th colspan="100%">The above student has signed this undertaking before me.</th>
        </tr>

        <tr>

            <th colspan="50%" style="text-align: left !important;">
                <br>_______________________________<br>
                Signature of the Chairman<br><br>
                <?php echo $arr_all_subject_dep_name[$my_result['sub_code_1']]; ?> <br>
                <br><br>
                Date........................
                <br><br>
                Seal.......................
            </th>
            <th align="left" colspan="50%" style="text-align: right !important;">
            </th>
        </tr>
    </table>
    <br>
    <br>
    <table width="100%" style="border: 1px solid black;">
        <tr>
            <td colspan="2" style="text-align: left;font-size: 9px;text-align: center;">
                © 2019-2020; <?php echo UNIVERSITY_NAME; ?>. All rights reserved.
            </td>
        </tr>
    </table>

</body>

</html>