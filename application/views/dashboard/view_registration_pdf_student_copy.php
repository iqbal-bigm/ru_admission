<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>PUST Admission</title>
    <style>
        table th {
            text-align: left;
        }

        .tbl_merit th {
            background-color: #EEEEEE;
        }
    </style>
</head>

<body>
    <?php
    $arr_all_subject_code_name = get_all_subject_code_name();
    $arr_all_subject_faculty_name = get_all_subject_faculty_name();
    $arr_all_subject_dep_name = get_all_subject_dep_name();
    $arr_all_course_name = get_all_course_name();
    ?>
    <?php
    $index = 0;
    foreach ($MAIN_DATA as $MAIN_DATA_key => $MAIN_DATA_value) {
    ?>
        <table border="0" style="width: 100%;border:0px solid #cacaca;border-collapse: collapse;">
            <thead>
                <tr>
                    <td style="width: 15%;text-align: center;">
                        <img style="width: 60px; height: auto" alt="<?php echo UNIVERSITY_NAME; ?>" src="<?php echo base_url(); ?>assets/images/logo.png" />
                    </td>
                    <td style="text-align: center;width: 70%" colspan="2" valign="top">
                        <span style="top:0px;font-size: 20px;font-weight:bold"><?php echo UNIVERSITY_NAME; ?></span>
                        <br> <br>
                        <span style="font-size:16px;"><b>Student's Registration Card</b></span>
                    </td>
                    <td style="width: 15%;text-align: center;">
                        <img style="width: 60px; height: auto" alt="Photo" src="<?php echo  "http://admission1920.pust.ac.bd/"; ?>uploads/<?php echo $MAIN_DATA_value['student_acadamic_data']['IMGUPLOAD_PATH']; ?>/<?php echo $MAIN_DATA_value['my_result']['system_reg_id_ref']; ?>.jpg" />
                    </td>
                </tr>
            </thead>
        </table>
        <br>
        <table border="1" class="tbl_merit" width="100%" cellpadding="5" style="border:1px solid #cacaca;border-collapse: collapse;font-size: 11px;">
            <tr>
                <th style="width: 45%" colspan="1">Name of the Student (In Bangla)</th>
                <td colspan="3"><br> ________________________________________________________</td>
            </tr>
            <tr>
                <th style="width: 45%" colspan="1">Name of the Student (In English)</th>
                <td colspan="3"><?php echo $MAIN_DATA_value['student_acadamic_data']['SNAME']; ?></td>
            </tr>

            <tr>
                <th style="width: 45%" colspan="1">Father's Name</th>
                <td colspan="3"><?php echo $MAIN_DATA_value['student_acadamic_data']['SFNAME']; ?></td>
            </tr>
            <tr>
                <th style="width: 45%" colspan="1">Mother's Name</th>
                <td colspan="3"><?php echo $MAIN_DATA_value['student_acadamic_data']['SMNAME']; ?></td>
            </tr>
            <tr>
                <th style="width: 45%" colspan="1">Name of the Faculty</th>
                <td colspan="3"><?php echo $arr_all_subject_faculty_name[$MAIN_DATA_value['my_result']['sub_code_7']]; ?></td>
            </tr>
            <tr>
                <th style="width: 45%" colspan="1">Department</th>
                <td colspan="3"><?php echo $arr_all_subject_dep_name[$MAIN_DATA_value['my_result']['sub_code_7']]; ?></td>
            </tr>
            <tr>
                <th style="width: 45%" colspan="1">Session</th>
                <td colspan="3">2019-2020</td>
            </tr>
            <tr>
                <th style="width: 45%" colspan="1">Class Roll No.</th>
                <td colspan="3"><?php echo $MAIN_DATA_value['my_result']['class_roll']; ?></td>
            </tr>

            <tr>
                <th style="width: 45%" colspan="1">Hall</th>
                <td colspan="3">
                    <?php
                    if ($MAIN_DATA_value['std_per_data']['gender'] == "MALE") {
                        echo "Bangabandhu Sheikh Mujibur Rahman Hall";
                    } else {
                        echo "Sheikh Hasina Hall";
                    }
                    ?>
                </td>
            </tr>
            <tr>
                <th style="width: 45%" colspan="1">The above named student has been registered <br> in this University and his/her Registration no. is</th>
                <td colspan="3" valign="top"><b> <span style="font-size: 16px;"> <?php echo $MAIN_DATA_value['my_result']['class_reg']; ?> </span> </b></td>
            </tr>
        </table>
        <br>

        <table border="0" width="100%" cellpadding="5" style="border:0px solid #cacaca;border-collapse: collapse;font-size: 12px;">
            <tr>

                <th width="50%" style="text-align: left !important;">
                    Administration Building<br>
                    Sheikh Hasina University<br>
                    Pabna, Bangladesh
                </th>
                <th width="30%" style="text-align: left !important;" valign="top">
                    <img src="https://chart.googleapis.com/chart?chs=75x75&cht=qr&chl=<?php echo $MAIN_DATA_value['student_acadamic_data']['SNAME'] . ' ' . $MAIN_DATA_value['my_result']['class_roll'] . ' ' . $MAIN_DATA_value['my_result']['class_reg'] . ' ' . str_replace($arr_replace, "", $MAIN_DATA_value['student_acadamic_data']['SFNAME']) . ' ' . str_replace($arr_replace, "", $MAIN_DATA_value['student_acadamic_data']['SMNAME']); ?> &choe=UTF-8">
                </th>
                <th align="right" width="20%" style="text-align: right !important;">
                    &nbsp;&nbsp;&nbsp; ___________<br>
                    &nbsp;&nbsp;&nbsp; Registrar
                </th>
            </tr>
        </table>

    <?php
        $index++;
        if ($index % 2 == 0) {
            echo "<pagebreak />";
        } else {
            echo "<br><br>";
        }
    } ?>
</body>

</html>