<?php $this->load->view('common/header'); ?>
<?php $this->load->view('common/navbar'); ?>
<style>
    .unit_radio_style {
        padding-bottom: 10px;
    }

    .my_error {
        color: red;
    }

    #sortable {
        list-style: none;
        text-align: left;
    }

    #sortable li {
        margin: 0 0 10px 0;
        height: 75px;
        background: #dbd9d9;
        border: 1px solid #999999;
        border-radius: 5px;
        color: #333333;
    }

    #sortable li span {
        background-color: #b4b3b3;
        background-image: url('<?php echo base_url(); ?>drag.png');
        background-repeat: no-repeat;
        background-position: center;
        width: 30px;
        height: 75px;
        display: inline-block;
        float: left;
        cursor: move;
    }

    #sortable li img {
        height: 65px;
        border: 5px solid #cccccc;
        display: inline-block;
        float: left;
    }

    #sortable li div {
        padding: 5px;
    }

    #sortable li h2 {
        font-size: 16px;
        line-height: 20px;
    }


    #sortable_SL {
        list-style: none;
        text-align: center;
    }

    #sortable_SL li {
        margin: 0 0 10px 0;
        height: 75px;
        background: #f9f9f9;
        border: 1px solid #999999;
        border-radius: 5px;
        color: #333333;
        width: 100%;
        padding-top: 15px;
        font-weight: bold;
    }

    #choice_form .error {
        color: red;
        font-style: italic;
    }
</style>
<?php
$choice_on_off = choice_on_off_status();
?>
<div id="main">
    <div class="container">
        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-primary">
                    <div class="panel-heading">Student Details Information</div>

                    <div class="panel-body">
                        <form action="<?php echo base_url('dashboard/my_subject_option_save'); ?>" method="post" name="choice_form" id="choice_form">
                            <div class="row">
                                <div class="col-md-12">
                                    <fieldset>
                                        <legend><span style="color:#f4a270;"><?php echo $choice_on_off['msg'];  ?></span></legend>
                                    </fieldset>
                                </div>
                                <div class="col-md-8">
                                    <fieldset>
                                        <legend>
                                            Subject Choice Area :: <span class="alert-info">&nbsp;&nbsp;<?= $unit_name; ?>&nbsp;&nbsp;</span> ::
                                            <?php
                                            if (!empty($unit_details['quota_name'])) {
                                                $quota_name_arr = get_quota_array();
                                            ?>
                                                (Quota: <span class="alert-info"><?php echo $quota_name_arr[$unit_details['quota_name']];; ?></span>)
                                            <?php
                                            }
                                            ?>
                                        </legend>
                                    </fieldset>
                                    <div style="padding-bottom: 20px;">
                                        <?php
                                        if ($show_from == "unit_wise_subject_list_existing") {
                                        ?>
                                            <!--                                     <a href="<?php //echo base_url()."dashboard/download/".$unique_id."/".$exam_roll."/".$unit_name; 
                                                                                                ?>"><button type="button" class="btn btn-primary">Download Subject List Confirmation PDF</button></a>-->
                                        <?php
                                        } ?>
                                    </div>


                                    <input type="hidden" id="unique_id" name="unique_id" value="<?php echo $unique_id; ?>">
                                    <input type="hidden" id="exam_roll" name="exam_roll" value="<?php echo $exam_roll; ?>">
                                    <input type="hidden" id="unit_name" name="unit_name" value="<?php echo $unit_name; ?>">
                                    <table class="table table-responsive table-bordered">
                                        <?php
                                        if ($with_architecture_status == "YES" && $unit_details['unit'] == "A") {
                                            echo "<tr><td colspan=\"2\"><i>* Architecture</i> is also selected for you.<td></tr>";
                                        }
                                        ?>
                                        <tr>
                                            <td colspan="2">
                                                <div style="padding:20px; color:#cc6363;text-align: center;"> Please use this web site from "desktop computer/laptop" to fill-up choice form</div>
                                            </td>
                                        </tr>
                                        <tr>

                                            <td colspan="2"><span class="help-block" style="text-align: center; color: #094B7B;font-size: 16px;font-weight: bold;">Drag and drop to choice your subject preference</span></td>
                                        <tr>
                                        <tr>
                                            <th>Choice Order</th>
                                            <th>Subject List</th>
                                        </tr>
                                        <tr>
                                            <td>
                                                <?php
                                                if ($show_from == "unit_wise_subject_list_existing") {
                                                ?>
                                                    <ul id="sortable_SL">

                                                        <?php
                                                        $arr_all_subject_code_name = get_all_subject_code_name();

                                                        if (!empty($unit_wise_subject_list_existing)) {
                                                            $indexer = 1;
                                                            foreach ($unit_wise_subject_list_existing as $key => $value) {

                                                        ?>
                                                                <li class="dd-item dd3-item" id="<?php echo $value['sub_code_ref']; ?>">
                                                                    <?php echo $indexer; ?>
                                                                </li>
                                                        <?php
                                                                $indexer++;
                                                            }
                                                        }
                                                        ?>
                                                    </ul>
                                                <?php
                                                } else {       ?>
                                                    <ul id="sortable_SL">

                                                        <?php
                                                        if (!empty($unit_wise_subject_list)) {
                                                            $indexerY = 1;
                                                            foreach ($unit_wise_subject_list as $key => $value) {
                                                        ?>
                                                                <li class="dd-item dd3-item" id="<?php echo $value['sub_code']; ?>">
                                                                    <?php echo $indexerY; ?>
                                                                </li>
                                                        <?php
                                                                $indexerY++;
                                                            }
                                                        }
                                                        ?>
                                                    </ul>
                                                <?php } ?>
                                            </td>
                                            <td>
                                                <?php
                                                if ($show_from == "unit_wise_subject_list_existing") {
                                                ?>
                                                    <ul id="sortable">

                                                        <?php
                                                        $arr_all_subject_code_name = get_all_subject_code_name();

                                                        if (!empty($unit_wise_subject_list_existing)) {
                                                            foreach ($unit_wise_subject_list_existing as $key => $value) {

                                                        ?>
                                                                <li class="dd-item dd3-item" id="<?php echo $value['sub_code_ref']; ?>">
                                                                    <span style="padding-right: 50px;"></span>
                                                                    <div class="dd-handle dd3-handle"></div>
                                                                    <div style="padding-left: 60px;" class="tip" data-tip="Drag and drop using arrow to choice your subject preference">
                                                                        <?php echo $arr_all_subject_code_name[$value['sub_code_ref']]; ?>
                                                                    </div>
                                                                </li>
                                                        <?php
                                                            }
                                                        }
                                                        ?>
                                                    </ul>
                                                <?php
                                                } else {       ?>
                                                    <ul id="sortable">

                                                        <?php
                                                        if (!empty($unit_wise_subject_list)) {
                                                            foreach ($unit_wise_subject_list as $key => $value) {
                                                        ?>
                                                                <li class="dd-item dd3-item" id="<?php echo $value['sub_code']; ?>" title="Drag and drop with arrow to choice your subject preference">
                                                                    <span style="padding-right: 50px;"></span>
                                                                    <div class="dd-handle dd3-handle"></div>
                                                                    <div style="padding-left: 60px;">
                                                                        <?php echo $value['sub_name']; ?>
                                                                    </div>
                                                                </li>
                                                        <?php
                                                            }
                                                        }
                                                        ?>
                                                    </ul>
                                                <?php } ?>
                                            </td>
                                        </tr>

                                    </table>
                                </div>
                                <div class="col-md-4">
                                    <fieldset>
                                        <legend> GST Score and Others Information</legend>
                                    </fieldset>
                                    <div id="photo_holder" style="padding-bottom: 20px; background-color :#E3E3E3; height:120px;width:100px;">
                                        <?php
                                        //if (!empty($student_login_photo_path)) {

                                        ?>
                                        <img height="120px;" width="100px;" src="<?php echo base_url() . 'photos/' . strtolower($unit_details['unit'])  . '/images_' . strtolower($unit_details['unit']) . '/' . $unit_details['exam_roll'] . '.jpg' . '?' . time(); ?>" alt="PHOTO" />
                                        <?php
                                        // }
                                        ?>

                                    </div>
                                    <fieldset>
                                        <legend style="font-size: 16px;"><b>Mobile No:</b> <?= $this->session->userdata('student_login_mobile_no'); ?></legend>
                                    </fieldset>

                                    <fieldset>
                                        <legend style="font-size: 16px;"><b>Student Name:</b> <?= $this->session->userdata('student_login_sname'); ?></legend>
                                    </fieldset>
                                    <!-- <fieldset>
                                        <legend style="font-size: 16px;"><b>Tracking ID:</b> <?= $unit_details['tracking_id']; ?></legend>
                                    </fieldset> -->
                                    <fieldset>
                                        <legend style="font-size: 16px;"><b>GST Application ID:</b> <?= $unit_details['app_id']; ?></legend>
                                    </fieldset>
                                    <fieldset>
                                        <legend style="font-size: 16px;"><b>GST Roll No:</b> <?= $unit_details['exam_roll']; ?></legend>
                                    </fieldset>

                                    <fieldset>
                                        <legend style="font-size: 16px;"><b>Unit:</b> <?= $unit_details['unit']; ?></legend>
                                    </fieldset>
                                    <!-- <fieldset>
                                    <legend style="font-size: 16px;"><b>Merit Position:</b> <?= $unit_details['merit']; ?></legend>
                                </fieldset> -->
                                    <fieldset>
                                        <legend style="font-size: 16px;"><b>GST Score:</b> <?= $final_data->gst_score; ?></legend>
                                    </fieldset>

                                    <?php
                                    if (!empty($unit_details['quota_name'])) {
                                        $quota_name_arr = get_quota_array();
                                    ?>
                                        <fieldset>
                                            <legend style="font-size: 16px;"><b>Quota:</b> <?php echo $quota_name_arr[$unit_details['quota_name']]; ?></legend>
                                        </fieldset>
                                    <?php
                                    }
                                    ?>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <fieldset>
                                        <legend>Personal Information Details</legend>
                                    </fieldset>

                                    <table class="table table-responsive table-bordered">
                                        <tr>
                                            <th width="30%">Nationality</th>
                                            <td>Bangladeshi</td>
                                        </tr>
                                        <tr>
                                            <th>Home District</th>
                                            <td>
                                                <?php
                                                $id_district = 'class="form-control" id="district" required';
                                                $dis_array = get_district_array();

                                                if (!empty($std_per_data['district'])) {
                                                    echo form_dropdown('district', $dis_array, $std_per_data['district'], $id_district);
                                                } else {
                                                    echo form_dropdown('district', $dis_array, '', $id_district);
                                                }
                                                // echo $dis_array[$std_per_data['district']];

                                                ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Gender</th>
                                            <td>
                                                <?php
                                                $g_arry = gender_array();
                                                // echo form_dropdown('gender',gender_array(), set_value('gender', $std_per_data['gender']), ['id' => 'gender', 'class' => 'form-control', 'required'=>'required']);
                                                echo $g_arry[$std_per_data['gender']];
                                                ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Date of Birth<br>(dd-mm-YYYY, ex 18-08-1993)</th>
                                            <td>
                                                <!-- <table>
                                                <tr>
                                                    <td>
                                                        <?php
                                                        //$dob_day = "";
                                                        //echo form_dropdown('dob_day',get_day_array(), set_value('dob_day', $dob_day), ['id' => 'dob_day', 'class' => 'form-control', 'required']);
                                                        ?>
                                                    </td>
                                                    <td>
                                                        <?php
                                                        // $dob_month = "";
                                                        // echo form_dropdown('dob_month',get_month_array(), set_value('dob_month', $dob_month), ['id' => 'dob_month', 'class' => 'form-control', 'required']);

                                                        ?>
                                                    </td>
                                                    <td>
                                                        <?php
                                                        //$dob_year = "";
                                                        // echo form_dropdown('dob_year',get_year_array(), set_value('dob_year', $dob_year), ['id' => 'dob_year', 'class' => 'form-control', 'required']);

                                                        ?>
                                                    </td>
                                                </tr>
                                            </table> -->
                                                <?php
                                                $form_input = array(
                                                    'name'  => 'dob',
                                                    'id'  => 'dob',
                                                    'value' => set_value('dob', (isset($std_per_data['dob'])) ? $std_per_data['dob'] : ''),
                                                    'autocomplete' => 'off',
                                                    'class'  => 'form-control',
                                                    'placeholder'  => 'Date of Birth',
                                                    'required' => 'required'
                                                );
                                                echo form_input($form_input);
                                                ?>

                                            </td>
                                        </tr>

                                        <tr>
                                            <th>Marital Status</th>
                                            <td>
                                                <?php
                                                echo form_dropdown('maritlal_status', marital_status_array(), set_value('maritlal_status', $std_per_data['maritlal_status']), ['id' => 'maritlal_status', 'class' => 'form-control', 'required' => 'required']);

                                                ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Father's Occupation : </th>
                                            <td>
                                                <div class="form-group">
                                                    <?php
                                                    $form_input = array(
                                                        'name'  => 'father_occupation',
                                                        'id'  => 'father_occupation',
                                                        'value' => set_value('father_occupation', (isset($std_per_data['father_occupation'])) ? $std_per_data['father_occupation'] : ''),
                                                        'autocomplete' => 'off',
                                                        'class'  => 'form-control',
                                                        'placeholder'  => 'Father\'s Occupation',
                                                        'required' => 'required'
                                                    );
                                                    echo form_input($form_input);
                                                    ?>
                                                </div>
                                            </td>
                                        </tr>

                                        <tr>
                                            <th>Mothers's Occupation : </th>
                                            <td>
                                                <div class="form-group">
                                                    <?php
                                                    $form_input = array(
                                                        'name'  => 'mother_occupation',
                                                        'id'  => 'mother_occupation',
                                                        'value' => set_value('mother_occupation', (isset($std_per_data['mother_occupation'])) ? $std_per_data['mother_occupation'] : ''),
                                                        'autocomplete' => 'off',
                                                        'class'  => 'form-control',
                                                        'placeholder'  => 'Mothers\'s Occupation',
                                                        'required' => 'required'
                                                    );
                                                    echo form_input($form_input);
                                                    ?>
                                                </div>
                                            </td>
                                        </tr>

                                        <tr>
                                            <th>Yearly Family Income (in taka) : </th>
                                            <td>
                                                <div class="form-group">
                                                    <?php
                                                    $form_input = array(
                                                        'name'  => 'family_income',
                                                        'id'  => 'family_income',
                                                        'value' => set_value('family_income', (isset($std_per_data['family_income'])) ? $std_per_data['family_income'] : ''),
                                                        'autocomplete' => 'off',
                                                        'class'  => 'form-control',
                                                        'placeholder'  => 'Yearly Family Income',
                                                        'required' => 'required'
                                                    );
                                                    echo form_input($form_input);
                                                    ?>
                                                </div>
                                            </td>
                                        </tr>

                                        <tr>
                                            <th>Local Guardian's Name : </th>
                                            <td>
                                                <div class="form-group">
                                                    <?php
                                                    $form_input = array(
                                                        'name'  => 'local_gurdian_name',
                                                        'id'  => 'local_gurdian_name',
                                                        'value' => set_value('local_gurdian_name', (isset($std_per_data['local_gurdian_name'])) ? $std_per_data['local_gurdian_name'] : ''),
                                                        'autocomplete' => 'off',
                                                        'class'  => 'form-control',
                                                        'placeholder'  => 'Local Guardian\'s Name',
                                                        'required' => 'required'
                                                    );
                                                    echo form_input($form_input);
                                                    ?>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Local Guardian's Address : </th>
                                            <td>
                                                <div class="form-group">
                                                    <?php
                                                    $form_input = array(
                                                        'name'  => 'local_gurdian_address',
                                                        'id'  => 'local_gurdian_address',
                                                        'value' => set_value('local_gurdian_address', (isset($std_per_data['local_gurdian_address'])) ? $std_per_data['local_gurdian_address'] : ''),
                                                        'autocomplete' => 'off',
                                                        'class'  => 'form-control',
                                                        'placeholder'  => 'Local Guardian\'s Address',
                                                        'required' => 'required'
                                                    );
                                                    echo form_input($form_input);
                                                    ?>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Local Guardian's Phone No. : </th>
                                            <td>
                                                <div class="form-group">
                                                    <?php
                                                    $form_input = array(
                                                        'name'  => 'local_gurdian_phone',
                                                        'id'  => 'local_gurdian_phone',
                                                        'value' => set_value('local_gurdian_phone', (isset($std_per_data['local_gurdian_phone'])) ? $std_per_data['local_gurdian_phone'] : ''),
                                                        'autocomplete' => 'off',
                                                        'class'  => 'form-control',
                                                        'placeholder'  => 'Local Guardian\'s Phone',
                                                        'required' => 'required'
                                                    );
                                                    echo form_input($form_input);
                                                    ?>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Relation With Local Guardian's : </th>
                                            <td>
                                                <div class="form-group">
                                                    <?php
                                                    $form_input = array(
                                                        'name'  => 'local_gurdian_realation',
                                                        'id'  => 'local_gurdian_realation',
                                                        'value' => set_value('local_gurdian_realation', (isset($std_per_data['local_gurdian_realation'])) ? $std_per_data['local_gurdian_realation'] : ''),
                                                        'autocomplete' => 'off',
                                                        'class'  => 'form-control',
                                                        'placeholder'  => 'Relation With Local Guardian\'s',
                                                        'required' => 'required'
                                                    );
                                                    echo form_input($form_input);
                                                    ?>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Religion : </th>
                                            <td>
                                                <div class="form-group">
                                                    <?php
                                                    /* $form_input = array(
                                                    'name'  => 'religion',
                                                    'id'  => 'religion',
                                                    'value' => set_value('religion', (isset($std_per_data['religion'])) ? $std_per_data['religion'] : ''),
                                                    'autocomplete' => 'off',
                                                    'class'  => 'form-control',
                                                    'placeholder'  => 'Religion',
                                                    'required'=>'required'
                                                ); */
                                                    //echo form_input($form_input);

                                                    echo form_dropdown('religion', get_religion(), set_value('religion', $std_per_data['religion']), ['id' => 'religion', 'class' => 'form-control', 'required' => 'required']);

                                                    ?>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Permanent Address : </th>
                                            <td>
                                                <table class="table table-responsive table-bordered">
                                                    <tr>
                                                        <td>Village/Ward</td>
                                                        <td>
                                                            <?php
                                                            $form_input = array(
                                                                'name'  => 'padd_village',
                                                                'id'  => 'padd_village',
                                                                'value' => set_value('padd_village', (isset($std_per_data['padd_village'])) ? $std_per_data['padd_village'] : ''),
                                                                'autocomplete' => 'off',
                                                                'class'  => 'form-control'
                                                                //'required'=>'required'
                                                            );
                                                            echo form_input($form_input);
                                                            ?>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>House</td>
                                                        <td>
                                                            <?php
                                                            $form_input = array(
                                                                'name'  => 'padd_house',
                                                                'id'  => 'padd_house',
                                                                'value' => set_value('padd_house', (isset($std_per_data['padd_house'])) ? $std_per_data['padd_house'] : ''),
                                                                'autocomplete' => 'off',
                                                                'class'  => 'form-control'
                                                                // 'required'=>'required'
                                                            );
                                                            echo form_input($form_input);
                                                            ?>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Road</td>
                                                        <td>
                                                            <?php
                                                            $form_input = array(
                                                                'name'  => 'padd_road',
                                                                'id'  => 'padd_road',
                                                                'value' => set_value('padd_road', (isset($std_per_data['padd_village'])) ? $std_per_data['padd_road'] : ''),
                                                                'autocomplete' => 'off',
                                                                'class'  => 'form-control'
                                                                // 'required'=>'required'
                                                            );
                                                            echo form_input($form_input);
                                                            ?>

                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Post Office</td>
                                                        <td>
                                                            <?php
                                                            $form_input = array(
                                                                'name'  => 'padd_post_office',
                                                                'id'  => 'padd_post_office',
                                                                'value' => set_value('padd_post_office', (isset($std_per_data['padd_post_office'])) ? $std_per_data['padd_post_office'] : ''),
                                                                'autocomplete' => 'off',
                                                                'class'  => 'form-control',
                                                                'required' => 'required'
                                                            );
                                                            echo form_input($form_input);
                                                            ?>

                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Upazila</td>
                                                        <td>
                                                            <?php
                                                            $form_input = array(
                                                                'name'  => 'padd_police_station',
                                                                'id'  => 'padd_police_station',
                                                                'value' => set_value('padd_police_station', (isset($std_per_data['padd_police_station'])) ? $std_per_data['padd_police_station'] : ''),
                                                                'autocomplete' => 'off',
                                                                'class'  => 'form-control',
                                                                'required' => 'required'
                                                            );
                                                            echo form_input($form_input);
                                                            ?>

                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>District</td>
                                                        <td>
                                                            <?php
                                                            $id_district = 'class="form-control" id="padd_district" required';
                                                            $padddis_array = get_district_array();
                                                            if (!empty($std_per_data['padd_district'])) {
                                                                echo form_dropdown('padd_district', $padddis_array, $std_per_data['padd_district'], $id_district);
                                                            } else {
                                                                echo form_dropdown('padd_district', $padddis_array, '', $id_district);
                                                            }

                                                            ?>

                                                        </td>
                                                    </tr>
                                                </table>
                                                <!--                                            <div class="form-group">
                                                <textarea id="permanent_address" rows="5" cols="100"name="permanent_address" required ><?php //echo $std_per_data['permanent_address'];
                                                                                                                                        ?></textarea>
                                            </div>-->
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Present Address : </th>
                                            <td>



                                                <table class="table table-responsive table-bordered">
                                                    <tr>
                                                        <td>Village/Ward</td>
                                                        <td>
                                                            <?php
                                                            $form_input = array(
                                                                'name'  => 'prad_village',
                                                                'id'  => 'prad_village',
                                                                'value' => set_value('prad_village', (isset($std_per_data['prad_village'])) ? $std_per_data['prad_village'] : ''),
                                                                'autocomplete' => 'off',
                                                                'class'  => 'form-control'
                                                                //'required'=>'required'
                                                            );
                                                            echo form_input($form_input);
                                                            ?>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>House</td>
                                                        <td>
                                                            <?php
                                                            $form_input = array(
                                                                'name'  => 'prad_house',
                                                                'id'  => 'prad_house',
                                                                'value' => set_value('prad_house', (isset($std_per_data['prad_house'])) ? $std_per_data['prad_house'] : ''),
                                                                'autocomplete' => 'off',
                                                                'class'  => 'form-control'
                                                                // 'required'=>'required'
                                                            );
                                                            echo form_input($form_input);
                                                            ?>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Road</td>
                                                        <td>
                                                            <?php
                                                            $form_input = array(
                                                                'name'  => 'prad_road',
                                                                'id'  => 'prad_road',
                                                                'value' => set_value('prad_road', (isset($std_per_data['prad_road'])) ? $std_per_data['prad_road'] : ''),
                                                                'autocomplete' => 'off',
                                                                'class'  => 'form-control'
                                                                // 'required'=>'required'
                                                            );
                                                            echo form_input($form_input);
                                                            ?>

                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Post Office</td>
                                                        <td>
                                                            <?php
                                                            $form_input = array(
                                                                'name'  => 'prad_post_office',
                                                                'id'  => 'prad_post_office',
                                                                'value' => set_value('prad_post_office', (isset($std_per_data['prad_post_office'])) ? $std_per_data['prad_post_office'] : ''),
                                                                'autocomplete' => 'off',
                                                                'class'  => 'form-control',
                                                                'required' => 'required'
                                                            );
                                                            echo form_input($form_input);
                                                            ?>

                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Upazila</td>
                                                        <td>
                                                            <?php
                                                            $form_input = array(
                                                                'name'  => 'prad_police_station',
                                                                'id'  => 'prad_police_station',
                                                                'value' => set_value('prad_police_station', (isset($std_per_data['prad_police_station'])) ? $std_per_data['prad_police_station'] : ''),
                                                                'autocomplete' => 'off',
                                                                'class'  => 'form-control',
                                                                'required' => 'required'
                                                            );
                                                            echo form_input($form_input);
                                                            ?>

                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>District</td>
                                                        <td>
                                                            <?php
                                                            $id_district_prad = 'class="form-control" id="prad_district" required';
                                                            $padddis_array = get_district_array();
                                                            if (!empty($std_per_data['prad_district'])) {
                                                                echo form_dropdown('prad_district', $padddis_array, $std_per_data['prad_district'], $id_district_prad);
                                                            } else {
                                                                echo form_dropdown('prad_district', $padddis_array, '', $id_district_prad);
                                                            }

                                                            ?>

                                                        </td>
                                                    </tr>
                                                </table>
                                                <!--                                                <textarea id="mailing_address" name="mailing_address" rows="5" cols="100" required ><?php //echo $std_per_data['mailing_address'];
                                                                                                                                                                                        ?></textarea>-->

                                            </td>
                                        </tr>
                                        <tr>
                                            <th>BNCC/Rover Scout: </th>
                                            <td>
                                                <div class="form-group">
                                                    <?php
                                                    /* $form_input = array(
                                                    'name'  => 'scout',
                                                    'id'  => 'scout',
                                                    'value' => set_value('scout', (isset($std_per_data['scout'])) ? $std_per_data['scout'] : ''),
                                                    'autocomplete' => 'off',
                                                    'class'  => 'form-control',
                                                    'placeholder'  => 'BNCC/Rover Scout'
                                                );
                                                echo form_input($form_input); */
                                                    echo form_dropdown('scout', get_scout(), set_value('scout', $std_per_data['scout']), ['id' => 'scout', 'class' => 'form-control']);
                                                    ?>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <span class="help-block"><b>Declaration: I do ​hereby declare that the above mentioned information and photo are true and correct to the best of my knowledge. If any information provided by me is found false , Sheikh Hasina University reserves the right to cancel my admission.</b></span>
                                </div>
                            </div>
                            <div class="row">

                                <div class="col-md-12" style="text-align: center;">

                                    <input type="hidden" name="login_table_auto_id" value="<?php echo $std_per_data['id']; ?>">
                                    <?= form_submit('submit', 'SAVE INFORMATION', ['class' => 'btn btn-primary']); ?>

                                </div>
                            </div>
                        </form>
                    </div>

                </div>

            </div>
        </div>
    </div>
</div>
<script>
    $(function() {
        $('#sortable').sortable({
            axis: 'y',
            opacity: 0.7,
            handle: 'span',
            update: function(event, ui) {
                var list_sortable = $(this).sortable('toArray').toString();
                var unique_id = $("#unique_id").val();
                var exam_roll = $("#exam_roll").val();
                var unit_name = $("#unit_name").val();
                // change order in the database using Ajax
                $.ajax({
                    url: siteHost() + "dashboard/save_sub_choice",
                    type: 'POST',
                    data: {
                        list_order: list_sortable,
                        unique_id: unique_id,
                        exam_roll: exam_roll,
                        unit_name: unit_name
                    },
                    success: function(data) {
                        //  $("#alert-success-listsaved").show();
                    }
                });
            }
        }); // fin sortable


    });

    $("#choice_form").validate();

    $(document).ready(function() {
        $('.tip').tipr();
    });
</script>
<?php $this->load->view('common/footer'); ?>