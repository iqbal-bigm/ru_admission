<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>PUST Admission</title>
    <style>
        table th {
            text-align: left;
        }

        .tbl_merit th {
            background-color: #EEEEEE;
        }
    </style>
</head>

<body>

    <table border="1" style="width: 100%;border:1px solid #cacaca;border-collapse: collapse;">
        <thead>
            <tr>
                <td style="width: 15%">
                    <img style="width: 60px; height: auto" alt="<?php echo UNIVERSITY_NAME; ?>" src="<?php echo base_url(); ?>assets/images/logo.png" />
                </td>
                <td style="text-align: center;width: 70%" colspan="2" valign="top">
                    <span style="top:0px;font-size: 20px;font-weight:bold"><?php echo UNIVERSITY_NAME; ?></span>
                    <br>
                    <span style="font-size:16px;"><b>Student’s Registration Form (Honours)</b></span>
                </td>
                <td style="width: 15%;text-align: center;">
                    <img style="width: 60px; height: auto" alt="Photo" src="<?php echo "http://pust.cloudonebd.com/"; //base_url(); 
                                                                            ?>uploads/<?php echo $student_login_photo_path; ?>/<?php echo $unit_details['system_reg_id']; ?>.jpg" />
                </td>
            </tr>
        </thead>
    </table>
    <br>
    <table class="tbl_merit" border="1" width="100%" cellpadding="1" style="border:1px solid #cacaca;border-collapse: collapse;font-size: 12px;">
        <tr>
            <th>Student ID No.</th>
            <th>Session</th>
            <th>Subject</th>
            <th>Course</th>
            <th>Department Code</th>
            <th>Roll No.</th>
            <th>Registration No.</th>
        </tr>
        <tr>
            <td></td>
            <td>2017‐2018</td>
            <td></td>
            <td></td>
            <td> </td>
            <td> </td>
            <td></td>
        </tr>

    </table>
    <br>
    <table class="tbl_merit" border="1" width="100%" cellpadding="3" style="border:1px solid #cacaca;border-collapse: collapse;font-size: 12px;">
        <tr>
            <th>Amount Paid</th>
            <th>Payment Date</th>
            <th>Transaction ID</th>
            <th>Dormitory/Hall Code</th>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td>
                <?php
                if ($std_per_data['gender'] == "Male") {
                    echo "Bangabandhu Sheikh Mujibur Rahman Hall";
                } else {
                    echo "Female Student Hostel";
                }
                ?>
            </td>
        </tr>

    </table>
    <br>
    <table border="1" class="tbl_merit" width="100%" cellpadding="5" style="border:1px solid #cacaca;border-collapse: collapse;font-size: 10px;">
        <tr>
            <th colspan="1">Name In Bengali</th>
            <td colspan="3"><br> ________________________________________________________</td>
        </tr>
        <tr>
            <th colspan="1">Name In English</th>
            <td colspan="3"><?php echo $student_acadamic_data['SNAME']; ?></td>
        </tr>

        <tr>
            <th>Father's Name</th>
            <td><?php echo $student_acadamic_data['SFNAME']; ?></td>

            <th>Mother's Name</th>
            <td><?php echo $student_acadamic_data['SMNAME']; ?></td>
        </tr>
        <tr>
            <th>Father's Occupation</th>
            <td><?php echo $std_per_data['father_occupation']; ?></td>

            <th>Mothers's Occupation</th>
            <td><?php echo $std_per_data['mother_occupation']; ?></td>
        </tr>
        <tr>
            <th>Yearly Family Income (in taka)</th>
            <td><?php echo $std_per_data['family_income']; ?></td>

            <th>BNCC/Rover Scout</th>
            <td><?php echo $std_per_data['scout']; ?></td>
        </tr>
        <tr>
            <th>Nationality</th>
            <td>Bangladeshi</td>
            <th>Home District</th>
            <td><?php echo $std_per_data['district']; ?></td>

        </tr>

        <tr>
            <th>Marital Status</th>
            <td><?php echo $std_per_data['maritlal_status']; ?></td>
            <th>Gender</th>
            <td><?php echo $std_per_data['gender']; ?></td>
        </tr>

        <tr>
            <th>Date of Birth</th>
            <td><?php echo $std_per_data['dob']; ?></td>
            <th>Mobile No.</th>
            <td><?php echo $std_per_data['mobile_no']; ?></td>

        </tr>


        <tr>
            <th>Local Guardian's Name</th>
            <td><?php echo $std_per_data['local_gurdian_name']; ?></td>
            <th>Religion</th>
            <td><?php echo $std_per_data['religion']; ?></td>

        </tr>
        <tr>
            <th>Relation With Local Guardian</th>
            <td><?php echo $std_per_data['local_gurdian_realation']; ?></td>
            <th>Local Guardian's Mobile No.</th>
            <td><?php echo $std_per_data['local_gurdian_phone']; ?></td>

        </tr>
        <tr>
            <th>Permanent Address</th>
            <td>
                <?php
                //echo $std_per_data['permanent_address'];
                echo "Village/Ward: " . $std_per_data['padd_village'] . ", House: " . $std_per_data['padd_house'] . ", ";
                echo "<br>";
                echo "Road: " . $std_per_data['padd_road'] . ", Post Office: " . $std_per_data['padd_post_office'] . ", ";
                echo "<br>";
                echo "Upazila: " . $std_per_data['padd_police_station'] . ", District: " . $std_per_data['padd_district'];
                ?>
            </td>
            <th>Present Address</th>
            <td><?php echo $std_per_data['mailing_address']; ?></td>
        </tr>
        <tr>
            <th>Local Guardian's Address</th>
            <td><?php echo $std_per_data['local_gurdian_address']; ?></td>
            <th></th>
            <td></td>
        </tr>
    </table>
    <br>
    <table class="tbl_merit" border="1" width="100%" cellpadding="1" style="border:1px solid #cacaca;border-collapse: collapse;font-size: 10px;">
        <tr>
            <td><b>Educational Information:</b></td>
        </tr>
        <tr>
            <th>Exam. Name</th>
            <th>Group</th>
            <th>ROLL NO.</th>
            <th>REG. NO.</th>
            <th>GPA</th>
            <th>Board</th>
            <th>Year</th>
        </tr>
        <tr>
            <th style="text-align: left;">SSC/ Equivalent</th>
            <td><?php echo $student_acadamic_data['SGROUP']; ?>
            <td><?php echo $student_acadamic_data['SROLL_NO']; ?></td>
            <td><?php echo $student_acadamic_data['SREG_NO']; ?></td>
            <td><?php echo $student_acadamic_data['SGPA']; ?></td>
            <td><?php echo $student_acadamic_data['SBOARD_NAME']; ?></td>
            <td><?php echo $student_acadamic_data['SPASS_YEAR']; ?></td>
        </tr>
        <tr>
            <th style="text-align: left;">HSC/ Equivalent</th>
            <td><?php echo $student_acadamic_data['HGROUP']; ?>
            <td><?php echo $student_acadamic_data['HROLL_NO']; ?></td>
            <td><?php echo $student_acadamic_data['HREG_NO']; ?></td>
            <td><?php echo $student_acadamic_data['HGPA']; ?></td>
            <td><?php echo $student_acadamic_data['HBOARD_NAME']; ?></td>
            <td><?php echo $student_acadamic_data['HPASS_YEAR']; ?></td>
        </tr>

    </table>
    <br>
    <table border="1" width="100%" cellpadding="5" style="border:1px solid #cacaca;border-collapse: collapse;font-size: 11px;">
        <tr>
            <td><b>Documents to be attached:</b></td>
        </tr>
        <tr>
            <td>
                1. S.S.C./Dakil/O-Level/Equivalent Marks Sheet and Certificate
                <br>
                2. H.S.C./Alim/A-Level/Equivalent Marks Sheet and Certificate
            </td>
        </tr>
    </table>
    <br>
    <pagebreak />
    <table border="1" style="width: 100%;border:1px solid #cacaca;border-collapse: collapse;">
        <thead>
            <tr>
                <td style="width: 15%">
                    <img style="width: 60px; height: auto" alt="<?php echo UNIVERSITY_NAME; ?>" src="<?php echo base_url(); ?>assets/images/logo.png" />
                </td>
                <td style="text-align: center;width: 70%" colspan="2" valign="top">
                    <span style="top:0px;font-size: 20px;font-weight:bold"><?php echo UNIVERSITY_NAME; ?></span>
                    <br>
                    <span style="font-size:16px;"><b>Student’s Registration Form (Honours)</b></span>
                </td>
                <td style="width: 15%;text-align: center;">
                    <img style="width: 60px; height: auto" alt="Photo" src="<?php echo  "http://pust.cloudonebd.com/"; //base_url(); 
                                                                            ?>uploads/<?php echo $student_login_photo_path; ?>/<?php echo $unit_details['system_reg_id']; ?>.jpg" />
                </td>
            </tr>
        </thead>
    </table>
    <br>
    <table border="1" width="100%" cellpadding="1" style="border:1px solid #cacaca;border-collapse: collapse;font-size: 10px;">
        <tr>
            <td><b>Declaration:</b></td>
        </tr>
        <tr>
            <td>
                <p>
                    I, <?php echo strtoupper($student_acadamic_data['SNAME']); ?>, do hereby declare that, (A) I, myself fill‐up this form and check properly and the above
                    mentioned information and photo are correct. (B) If any information provided by me is found false, Pabna University of
                    Science and Technology reserves the right to cancel my admission. (C) I shall be obliged to obey the rules and
                    regulations adopted by Sheikh Hasina University and to pay all the required fees.
                </p>
            </td>
        </tr>
    </table>

    <br>
    <table border="0" width="100%" cellpadding="5" style="border:0px solid #cacaca;border-collapse: collapse;font-size: 12px;">
        <tr>
            <th colspan="100%"><br></th>
        </tr>
        <tr>

            <th colspan="50%" style="text-align: left !important;">
                __________________________________________________________<br>
                Signature of the Student & Date<br>
                (The above statements of the Student are correct)
            </th>
        </tr>
        <tr>
            <th colspan="100%"><br></th>
        </tr>
        <tr>

            <th colspan="50%" style="text-align: left !important;">
                ___________________________________<br>
                Signature of the Chairman<br>
                (Office Seal & Date)
            </th>
            <th align="left" colspan="50%" style="text-align: right !important;">
                _______________________________________________________________<br>
                Signature of the Head of the Academic Section
                (Office Seal & Date)
            </th>
        </tr>

    </table>

    <br>
    <table width="100%" style="border: 1px solid black;">
        <tr>
            <td colspan="2" style="text-align: left;font-size: 9px;text-align: center;">
                © 2017-2018; <?php echo UNIVERSITY_NAME; ?>. All rights reserved.
            </td>
        </tr>
    </table>
    <pagebreak />
    <table border="1" style="width: 100%;border:1px solid #cacaca;border-collapse: collapse;">
        <thead>
            <tr>
                <td style="width: 15%">
                    <img style="width: 60px; height: auto" alt="<?php echo UNIVERSITY_NAME; ?>" src="<?php echo base_url(); ?>assets/images/logo.png" />
                </td>
                <td style="text-align: center;width: 70%" colspan="2" valign="top">
                    <span style="top:0px;font-size: 20px;font-weight:bold"><?php echo UNIVERSITY_NAME; ?></span>
                    <br>
                    <span style="font-size:16px;"><b>Student’s Registration Form (Honours)</b></span>
                </td>
                <td style="width: 15%;text-align: center;">
                    <img style="width: 60px; height: auto" alt="Photo" src="<?php echo  "http://pust.cloudonebd.com/"; //base_url(); 
                                                                            ?>uploads/<?php echo $student_login_photo_path; ?>/<?php echo $unit_details['system_reg_id']; ?>.jpg" />
                </td>
            </tr>
        </thead>
    </table>
    <br>
    <table border="1" class="tbl_merit" width="100%" cellpadding="5" style="border:1px solid #cacaca;border-collapse: collapse;font-size: 11px;">
        <tr>
            <th colspan="1">Name In Bengali</th>
            <td colspan="3"><br> ________________________________________________________</td>
        </tr>
        <tr>
            <th colspan="1">Name In English</th>
            <td colspan="3"><?php echo $student_acadamic_data['SNAME']; ?></td>
        </tr>

        <tr>
            <th colspan="1">Father's Name</th>
            <td colspan="3"><?php echo $student_acadamic_data['SFNAME']; ?></td>
        </tr>
        <tr>
            <th colspan="1">Mother's Name</th>
            <td colspan="3"><?php echo $student_acadamic_data['SMNAME']; ?></td>
        </tr>
        <tr>
            <th colspan="1">Name of the Faculty</th>
            <td colspan="3"></td>
        </tr>

        <tr>
            <th colspan="1">Session</th>
            <td colspan="3"></td>
        </tr>
        <tr>
            <th colspan="1">Roll No .</th>
            <td colspan="3"></td>
        </tr>

        <tr>
            <th colspan="1">Dormitory/Hall</th>
            <td colspan="3">
                <?php
                if ($std_per_data['gender'] == "Male") {
                    echo "Bangabandhu Sheikh Mujibur Rahman Hall";
                } else {
                    echo "Female Student Hostel";
                }
                ?>
            </td>
        </tr>
        <tr>
            <th colspan="1">The above named student has been registered in this University and his/her Registration no. is</th>
            <td colspan="3"><br>..................................................</td>
        </tr>
    </table>
    <br>
    <table border="0" width="100%" cellpadding="5" style="border:0px solid #cacaca;border-collapse: collapse;font-size: 12px;">
        <tr>
            <th colspan="100%"><br></th>
        </tr>
        <tr>

            <th colspan="50%" style="text-align: left !important;">
                Administration Building<br>
                Sheikh Hasina University<br>
                Pabna, Bangladesh

            </th>
            <th align="left" colspan="50%" style="text-align: right !important;">
                ___________<br>
                Registrar
            </th>
        </tr>
    </table>
    <br>
    <br>
    <table width="100%" style="border: 1px solid black;">
        <tr>
            <td colspan="2" style="text-align: left;font-size: 9px;text-align: center;">
                © 2017-2018; <?php echo UNIVERSITY_NAME; ?>. All rights reserved.
            </td>
        </tr>
    </table>

</body>

</html>