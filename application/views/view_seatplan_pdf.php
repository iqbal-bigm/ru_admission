<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<title>Attendance Sheet - PUST</title>

</head>

<body>

	<div id="container">
		<table border="1" width="100%" style="border:1px solid #cacaca;border-collapse: collapse;font-size: 12px;">
			<tr>
				<td style="border:0px;padding-top:10px;">
					<img width="70px" height="72px" style="" src="<?= base_url('assets/images/logo.png'); ?>" />
				</td>
				<td style="border:0px;text-align:left;" colspan="5">
					<span style="font-size:14px;">
						<b>
							Sheikh Hasina University
							<br>
							Admission Test (Session 2019-2020)
						</b>
					</span>

				</td>
			</tr>
			<tr>
				<td colspan="6" style="border:0px;text-align: center;font-size:14px;">
					<b>Attendance Sheet, UNIT : <?php echo $exam_unit; ?> ( <?php echo $type; ?> ) </b>
				</td>
			</tr>
			<tr>
				<td colspan="4" style="border:0px;">
					<br>
					<b>Examination Venue :</b> <?php echo $exam_centre; ?>
				</td>
			</tr>
			<tr>
				<td colspan="4" style="border:0px;">

					<b>Building Name :</b> <?php echo $building_name; ?>
				</td>
				<td colspan="2" style="border:0px;">


				</td>
			</tr>
			<tr>
				<td colspan="2" style="border:0px;">
					<b>Room No. :</b> <?php echo $room_no; ?>
				</td>
			</tr>
			<tr>
				<td colspan="6" style="border:0px;">
					<b>Start Roll :</b> <?php echo $roll_from; ?> <b>End Roll :</b> <?php echo $roll_to; ?> <b>No. of Students : </b> <?php echo  $total_seat; ?>
				</td>

			</tr>

			<tr>

				<td colspan="6" style="text-align:left;border:0px;padding-top:6px;padding-bottom:20px;">
					<br>
					<br>
					<b>Invigilator's Signature :</b>__________________________________________________________________________________
					<br>
					<br>
				</td>
			</tr>
		</table>

		<table repeat_header="1" border="1" width="100%" style="border:1px solid #cacaca;border-collapse: collapse;font-size: 12px;">
			<thead>
				<tr>
					<td width="5%" style="text-align: center;"><b>SL</b></td>
					<td width="10%" style="text-align: center;"><b>ROLL No.</b></td>
					<td width="20%" style="text-align: center;">
						<b>Name of the candidate</b>
					</td>
					<td width="15%" style="text-align: center;"><b>Photo</b></td>

					<td width="28%" style="text-align: center;"><b>Signature of the candidate</b></td>
					<td width="9%" style="text-align: center;"><b>Comment</b></td>
				</tr>
			</thead>

			<?php
			$i = 1;
			foreach ($res as $key => $value) {
			?>
				<tr>
					<td style="text-align: center;">
						<?php

						echo $i;

						?>
					</td>

					<td style="text-align: center;">
						<?php
						if (!empty($value->ROLLNO_FINAL)) {
							echo $value->ROLLNO_FINAL;
						} else {
							echo "";
						}
						?>
					</td>

					<td style="text-align: center;">
						<?php
						if (!empty($value->SNAME)) {
							echo $value->SNAME;
						} else {
							echo "";
						}
						echo "<br>Father's Name: <br>" . $value->SFNAME;
						echo "<br>Mother's Name: <br>" . $value->SMNAME;
						$IMGUPLOAD_PATH = get_photo_path($value->REGISID);
						?>
					</td>
					<td style="text-align:center;">
						<img src="<?php echo 'http://admission1920.pust.ac.bd/uploads/' . $IMGUPLOAD_PATH . '/' . $value->REGISID . '.jpg'; ?>" alt="photo" style="width:90px;height: 100px" />
					</td>

					<td style="text-align: center;">
					</td>
					<td style="text-align: center;">
					</td>
				</tr>
			<?php
				$i++;
			}
			?>
		</table>
	</div>

</body>

</html>