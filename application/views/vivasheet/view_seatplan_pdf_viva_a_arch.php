<div>
    <table repeat_header="1" border="1" width="100%" style="border:0px solid #cacaca;border-collapse: collapse;font-size: 11px;">
        <thead>
            <tr>
                <td width="20%">
                    <img width="40px" height="60px" style="" src="<?php echo base_url('assets/images/logo.png'); ?>" />
                </td>
                <td width="80%" valign="top" colspan="5">
                    <span style="font-size:16px;text-align: center;"><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sheikh Hasina University</b></span>
                    <br>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1st Year Admission Viva-voce (Session: 2019-2020)
                </td>
            </tr>
            <tr>
                <td colspan="3" style="text-align: left;font-size:12px;">Unit: A_ARCH</td>
                <td colspan="3" style="text-align: left;font-size:12px;">Date: </td>
            </tr>
            <tr>
                <td colspan="3" style="text-align: center;font-size:12px;"><b>Attendance Sheet</b></td>
                <td style="text-align: center;font-size:12px;">Total : <?php echo count($res); ?></td>
            </tr>
            <tr>
                <td width="5%"><b>Sl.</b></td>
                <td width="10%"><b>Roll No.</b></td>
                <td width="16%"><b>App. ID, Score & Merit</b></td>
                <td width="40%"><b>Applicant's Info</b></td>
                <td width="17%"><b>Photo</b></td>
                <td width="12%"><b>Applicant's Signature</b></td>
            </tr>
        </thead>
        <tbody>

            <?php
            $arr_all_subject_code_name = get_all_subject_code_name();
            $i = 1;
            foreach ($res as $key => $value) {
            ?>
                <tr>
                    <td><?php echo $i; ?></td>

                    <td>
                        <?php
                        echo $value['exam_roll'];
                        ?>
                    </td>
                    <td>
                        <?php
                        echo "Tracking ID: " . $value['tracking_id'] . "<br>";
                        echo "Application ID: " . $value['app_id'];
                        echo "<br><br>Score: " . $value['score'];
                        echo "<br>Merit Position: " . $value['merit'];
                        if (!empty($value['optional'])) {
                            echo "<br> ( " . $value['optional'] . " )";
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        // echo "<br>";
                        echo "Name: " . $value['sname'];
                        echo "<br>";
                        echo "<br>Father's Name: " . $value['sfname'];
                        echo "<br>Mother's  Name: " . $value['smname'];
                        echo "<br>SSC Roll: " . $value['ssc_roll'] . ' Board:' . $value['ssc_board'];
                        echo "<br>GPA: " . $value['ssc_gpa'] . " Reg. No." . $value['ssc_reg'];
                        echo "<br>HSC Roll: " . $value['hsc_roll'] . ' Board:' . $value['hsc_board'];
                        echo "<br>GPA: " . $value['hsc_gpa'] . " Reg. No." . $value['hsc_reg'];
                        echo "<br>";

                        //print_r($value['res_my_sub_choice']);
                        if (!empty($value['res_my_sub_choice'])) {
                            $indexer = 1;
                            foreach ($value['res_my_sub_choice'] as $key_ => $value_) {
                                echo $indexer . ". " . $arr_all_subject_code_name[$value_['sub_code_ref']] . " ";
                                $indexer++;
                            }
                        }

                        $IMGUPLOAD_PATH = get_photo_path($value['system_reg_id']);
                        ?>
                    </td>
                    <td>
                        <img src="<?php echo 'http://admission1920.pust.ac.bd/uploads/' . $IMGUPLOAD_PATH . '/' . $value['system_reg_id']  . '.jpg'; ?>" alt="photo" style="width:120px;height: 100px" />
                    </td>
                    <td>

                    </td>

                </tr>
            <?php
                $i++;
            }
            ?>
        </tbody>
    </table>
</div>