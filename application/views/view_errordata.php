<?php $this->load->view('common/header'); ?>
<?php $this->load->view('common/navbar'); ?>

<div id="main">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">Error</div>
                    <div class="panel-body">
                        <div class="row">
                            <p>
                            <h4 style="padding: 20px;color:red;">

                                You have error in HSC subject's GPA. Please submit a complain with HSc 'Subject Code-Subject Name', we will check and get back to you.<br>
                                <a href="<?php echo base_url('ask'); ?>">Click here to submit complain</a>
                            </h4>
                            </p>
                        </div>
                    </div>

                </div>
            </div>
        </div>


    </div>
</div>

<?php $this->load->view('common/footer'); ?>