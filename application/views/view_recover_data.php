<?php $this->load->view('common/header'); ?>
<?php $this->load->view('common/navbar'); ?>
<style>
    #nextstep .error {
    color: red;
        font-style: italic;
}
</style>  
<div id="main">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">Application  Details</div>

                    <?php $msg = $this->session->flashdata('notification');

                    if( !empty($msg)): ?>
                    <div class="panel-body">
                        <div class=" has-error">
                            <span class="pd15 bg-danger help-block"><?= $msg; ?></span>
                        </div>
                    </div>
                    <?php endif; ?>
                    <div class="panel-body">                       
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table table-responsive table-bordered">
                                    <tr>
                                        <th>Application ID</th><th>Date</th><th>Payment Status</th>
                                    </tr>
                                    <?php
                                    $LAST_MOB_NUM = '';
                                    $applied_units =  array();
                                    if(!empty($applied_unit)){
                                        foreach ($applied_unit as $key => $value) {
                                            $applied_units[] =  $value['UNIT'];//UNIT
                                            $LAST_MOB_NUM = $value['MOBNUMBER'];
                                       ?>
                                    <tr>
                                        <td><?php 
										echo $value['APPID']."<br>"; 
										?></td>
										<td><?php echo $value['APPLYDATE']; ?></td>
                                        <td><?php 
                                            if($value['PMTSTATUS'] == "P"){
                                                echo "<span class=\"btn btn-success\">Paid</span>";
                                            }else{
                                                 echo "<span class=\"btn btn-danger\">Not Paid</span>";
                                            }                                                
                                            ?></td>
                                    </tr>                                    
                                    <?php
                                        }
                                    }
																		
                                    ?>
                                </table>
                            </div>
                        </div>
                        

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?= base_url('assets/js/jquery.validate.js'); ?>"></script>
<script> 
  $("#nextstep").validate();
</script>   
<?php $this->load->view('common/footer'); ?>
