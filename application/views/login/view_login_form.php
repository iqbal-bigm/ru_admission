<?php $this->load->view('common/header'); ?>
<?php $this->load->view('common/navbar'); ?>

<div id="main">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">Login Form</div>
                    <div style="padding:20px; color:#cc6363;"> Please login with your GST information</div>
                    <?php $msg = $this->session->flashdata('notification');

                    if (!empty($msg)) : ?>
                        <div class="panel-body">
                            <div class=" has-error">
                                <span class="pd15 bg-danger help-block"><?php echo $msg; ?></span>
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php $notification_signup = $this->session->flashdata('notification_signup');

                    if (!empty($notification_signup)) : ?>
                        <div class="panel-body">
                            <div class="">
                                <span class="pd15 bg-success help-block"><?php echo $notification_signup; ?></span>
                            </div>
                        </div>
                    <?php endif; ?>

                    <div class="panel-body">
                        <?= form_open('login/process'); ?>
                        <?php $this->form_validation->set_error_delimiters('<span class="help-block">', '</span>'); ?>

                        <div class="row form-group">
                            <div class="col-md-2">
                                <p><strong>GST Mobile Number</strong></p>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group <?php if (form_error('mobile_no')) echo "has-error"; ?>">
                                    <?php
                                    $form_input = array(
                                        'type'  => 'text',
                                        'name'  => 'mobile_no',
                                        'id'  => 'mobile_no',
                                        'value' => set_value('mobile_no', $mobile_no),
                                        'autocomplete' => 'off',
                                        'class'  => 'form-control',
                                        'placeholder'  => 'GST Mobile Number'
                                    );
                                    echo form_input($form_input);
                                    if (form_error('mobile_no')) echo form_label(form_error('mobile_no'));
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-md-2">
                                <p><strong>GST Applicant ID</strong></p>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group <?php if (form_error('applicant_id')) echo "has-error"; ?>">
                                    <?php
                                    $form_input = array(
                                        'type'  => 'text',
                                        'name'  => 'applicant_id',
                                        'id'  => 'applicant_id',
                                        'value' => set_value('applicant_id', $applicant_id),
                                        'autocomplete' => 'off',
                                        'class'  => 'form-control',
                                        'placeholder'  => 'GST Applicant ID'
                                    );
                                    echo form_input($form_input);
                                    if (form_error('applicant_id')) echo form_label(form_error('applicant_id'));
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-md-2">
                                <p><strong>GST Admission Roll</strong></p>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group <?php if (form_error('admission_roll')) echo "has-error"; ?>">
                                    <?php
                                    $form_input = array(
                                        'type'  => 'text',
                                        'name'  => 'admission_roll',
                                        'id'  => 'admission_roll',
                                        'value' => set_value('admission_roll', $admission_roll),
                                        'autocomplete' => 'off',
                                        'class'  => 'form-control',
                                        'placeholder'  => 'GST Admission Roll'
                                    );
                                    echo form_input($form_input);
                                    if (form_error('admission_roll')) echo form_label(form_error('admission_roll'));
                                    ?>
                                </div>
                            </div>
                        </div>
                        <!-- <div class="row form-group">
                            <div class="col-md-2">
                                <p><strong>Password</strong></p>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group <?php if (form_error('password')) echo "has-error"; ?>">
                                    <?php
                                    $form_input = array(
                                        'type'  => 'password',
                                        'name'  => 'password',
                                        'id'  => 'password',
                                        'value' => set_value('password', $password),
                                        'autocomplete' => 'off',
                                        'class'  => 'form-control',
                                        'placeholder'  => 'Password'
                                    );
                                    echo form_input($form_input);
                                    if (form_error('password')) echo form_label(form_error('password'));
                                    ?>
                                </div>
                            </div>
                        </div> -->

                        <hr>
                        <div class="col-md-6">
                            <?= form_submit('submit', 'LOGIN', ['class' => 'btn btn-primary pull-right']); ?>
                            <?= form_close(); ?>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('common/footer'); ?>