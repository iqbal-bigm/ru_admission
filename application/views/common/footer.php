<div class="copyright">
    <p style="padding-left: 110px;">
        <!-- <span>&copy; Copyright 2019-2020 PUST. All Rights Reserved </span> -->
        <span>&copy; Copyright <?= date("Y", strtotime("-1 year")) ?>-<?= date("Y"); ?> <?= UNIVERSITY_NAME ?>. All rights reserved.</span>
        <!-- <span> Developed and Maintained by:</span> -->
        <!-- <a style="color:#940408" href="http://www.bigm-bd.com"> BIG M Resources Limited</a> -->
    </p>
</div>
<script src="<?= base_url('assets/bootstrap/js/bootstrap.min.js') ?>" type="text/javascript"></script>
<script src="<?= base_url('assets/js/custom.js') ?>" type="text/javascript"></script>
</body>

</html>