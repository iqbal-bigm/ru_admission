<!doctype html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php echo UNIVERSITY_NAME; ?></title>
    <link href="<?php echo base_url('assets/images/logo.png'); ?>" rel="shortcut icon" type="text/css" />
    <link rel="stylesheet" href="<?= base_url('assets/bootstrap/css/bootstrap.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/font-awesome/css/font-awesome.min.css'); ?>">

    <link rel="stylesheet" href="<?= base_url('assets/css/style.css?v=2'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/css/tipr.css'); ?>">

    <script type="text/javascript" src="<?= base_url('assets/js/jquery-3.2.1.min.js'); ?>"></script>
    <script type="text/javascript" src="<?= base_url('assets/js/jquery-ui-1.10.4.custom.min.js'); ?>"></script>
    <script type="text/javascript" src="<?= base_url('assets/js/jquery.validate.js'); ?>"></script>
    <script type="text/javascript" src="<?= base_url('assets/js/tipr.min.js'); ?>"></script>
    <script type="text/javascript">
        // function siteHost() { // Read-only-ish
        //     return "<?php echo base_url(); ?>";
        // }
    </script>
</head>

<body>