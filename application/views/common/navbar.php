<style>
    li.active {
        color: #f9832c !important;
    }

    li.active>a {
        color: #f9832c !important;
    }

    a.active {
        color: #f9832c !important;
    }
</style>
<header>
    <nav class="container-fluid topbar">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <!--                    <p class="pull-left">For Inquiry or Problem Email to: cloudonebd2016@gmail.com </p>-->
                </div>
                <div class="col-md-6">
                    <!--                    <p class="pull-right">HELP LINE :: 01765525069 </p>-->
                </div>
            </div>
        </div>
    </nav>

    <div class="container text-center">
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="header">
                    <h2><img src="<?= base_url('assets/images/logo.png'); ?>" class="logo" alt="<?php echo UNIVERSITY_NAME; ?>"><?php echo UNIVERSITY_NAME; ?> </h2>
                    <h3>Admission 2020-2021</h3>
                    <!--                    <h3 style="color:#e02222 !important;font-size:15px;">If you face any problem,send problem details with mobile number , name and hsc roll number at <u>bigmsoftpust@gmail.com </u></h3>-->
                    <!--                    <h3>Admission for 7th Result is on 26 Feb 2020</h3>-->
                    <!--<h3><a  style="color:#e02222 !important;" href="<?= site_url('migration_files/Noticefor4th_waitinglist.pdf'); ?>" target="_blank">Important Notice for 4th waiting list</a> |   <a  style="color:#e02222 !important;" href="<?= site_url('migration_files/4th_waiting_help.pdf'); ?>" target="_blank">Click here for guideline</a></h3>-->

                </div>
            </div>
        </div>
    </div>
</header>

<nav class="navbar ">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="navbar-nav">
                <li class="<?= ($this->uri->segment(1) == '') ? 'active' : '' ?>"><a href="<?= site_url('dashboard'); ?>">Home | <span class="sr-only">(current)</span></a></li>
                <!-- <li class="active"><a href="<?= site_url('admitcard'); ?>">Admission Guideline and Result | <span class="sr-only"></span></a></li> -->
                <?php
                if ($this->session->userdata('student_logged_in') == false) {
                ?>
                    <!--                  <li class="active"><a href="<?= site_url('registration'); ?>">Registration  &nbsp;|</a></li>                  -->
                    <li class=""><a class="<?= ($this->uri->segment(1) == 'login') ? 'active' : '' ?>" href="<?= site_url('login'); ?>">Login</a> | </li>
                    <!-- <li class=""><a class="<?= ($this->uri->segment(1) == 'admRegistration') ? 'active' : '' ?>" href="<?= site_url('admRegistration'); ?>">Registration</a> | </li> -->
                <?php }
                ?>


                <!--      <li><a href="<?= site_url('admitcard'); ?>">Result |</a></li>
                 
               <li><a href="<?= site_url(''); ?>">Pdf From(O and A Level) |</a></li> -->

                <?php
                if ($this->session->userdata('student_logged_in') == true) {
                ?>
                    <li class="<?= ($this->uri->segment(1) == 'dashboard') ? 'active' : '' ?>"><a href="<?= site_url('dashboard'); ?>">My Dashboard &nbsp;| <span class="sr-only">(current)</span></a></li>
                    <li class=""><a href="<?= site_url('logout'); ?>">Logout</a> | </li>
                <?php }
                ?>

                <li><a href="<?= site_url('RabindraUniversity.pdf'); ?>" target="_blank">Application Guideline</a>|</li>
                <!--                <li class=""><a href="<?= site_url('content'); ?>">Notice |</a></li>-->
                <li class="<?= ($this->uri->segment(1) == 'circular') ? 'active' : '' ?>"><a href="#">Circular |</a></li>
                <!-- <li class="<?= ($this->uri->segment(1) == 'circular') ? 'active' : '' ?>"><a href="<?= site_url('circular'); ?>">Circular |</a></li> -->
                <!--                <li class="<?= ($this->uri->segment(2) == 'result') ? 'active' : '' ?>"><a href="<?= site_url('content'); ?>">Result |</a></li>-->
                <!-- <li class="<?= ($this->uri->segment(1) == 'recover' && $this->uri->segment(2) == 'password') ? 'active' : '' ?>"><a href="<?= site_url('recover/password/'); ?>">Forgot Password | </a></li> -->
                 
                <li><a class="<?= ($this->uri->segment(1) == 'payment') ? 'active' : '' ?>" href="<?= site_url('payment'); ?>">How to Pay |</a></li>
                <li><a href="<?= site_url('ask'); ?>">Complain</a></li>
            </ul>

        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>