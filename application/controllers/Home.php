<?php

defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Class Home
 * @property home_model $home_model
 */
class Home extends CI_Controller
{

    private $data;

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('common_helper');
        $this->load->model('home_model');
        //  redirect('recover');exit;
        //redirect('content');

        // if(! $this->session->userdata('isTESTLogin') ){
        //  redirect('login');
        //}
    }

    public function index()
    {
        $this->load->library('form_validation');

        // For Default Form Validation Default Value
        $form['ssc_board'] = '';
        $form['ssc_passing_year'] = '';
        $form['ssc_roll'] = '';
        $form['ssc_registration'] = '';
        $form['hsc_board'] = '';
        $form['hsc_passing_year'] = '';
        $form['hsc_roll'] = '';
        $form['hsc_registration'] = '';

        $form['widget'] = $this->recaptcha->getWidget();
        $form['script'] =   $this->recaptcha->getScriptTag();

        $this->load->view('home/user_reg_form', $form);
    }

    public function nextstep()
    {
        $this->load->library('form_validation');

        // Form Validation Rules
        $this->form_validation->set_rules('ssc_board', 'SSC Board Or Institute', 'required');
        $this->form_validation->set_rules('ssc_passing_year', 'SSC Passing Year', 'required');
        $this->form_validation->set_rules('ssc_roll', 'SSC Roll No', 'required');
        $this->form_validation->set_rules('ssc_registration', 'SSC Registration', 'required');
        $this->form_validation->set_rules('hsc_board', 'HSC Board Or Institute', 'required');
        $this->form_validation->set_rules('hsc_passing_year', 'HSC Passing Year', 'required');
        $this->form_validation->set_rules('hsc_roll', 'HSC Roll No', 'required');
        $this->form_validation->set_rules('hsc_registration', 'HSC Registration', 'required');

        /*$recaptcha = $this->input->post('g-recaptcha-response');
          if (!empty($recaptcha)) {
            $response = $this->recaptcha->verifyResponse($recaptcha);
            if (isset($response['success']) and $response['success'] === true) {
		*/
        if (1) {
            if (1) {
                if ($this->form_validation->run() === FALSE) {
                    $form['ssc_board'] = '';
                    $form['ssc_passing_year'] = '';
                    $form['ssc_roll'] = '';
                    $form['ssc_registration'] = '';
                    $form['hsc_board'] = '';
                    $form['hsc_passing_year'] = '';
                    $form['hsc_roll'] = '';
                    $form['hsc_registration'] = '';

                    $form['widget'] = $this->recaptcha->getWidget();
                    $form['script'] =   $this->recaptcha->getScriptTag();

                    $this->load->view('home/user_reg_form', $form);
                } else {

                    $ssc_board = $this->input->post('ssc_board', TRUE);
                    $ssc_passing_year = $this->input->post('ssc_passing_year', TRUE);
                    $ssc_roll = $this->input->post('ssc_roll', TRUE);
                    $ssc_registration = $this->input->post('ssc_registration', TRUE);

                    $hsc_board = $this->input->post('hsc_board', TRUE);
                    $hsc_passing_year = $this->input->post('hsc_passing_year', TRUE);
                    $hsc_roll = $this->input->post('hsc_roll', TRUE);
                    $hsc_registration = $this->input->post('hsc_registration', TRUE);


                    //$validation_ssc_data = $this->home_model->ssc_info($ssc_board, $ssc_passing_year, $ssc_roll, $ssc_registration);
                    //$validation_hsc_data = $this->home_model->hsc_info($hsc_board, $hsc_passing_year, $hsc_roll, $hsc_registration);

                    $validation_ssc_data = $this->call_teletalk_api($ssc_board, $ssc_passing_year, $ssc_roll, $ssc_registration, 'ssc');
                    $validation_hsc_data = $this->call_teletalk_api($hsc_board, $hsc_passing_year, $hsc_roll, $hsc_registration, 'hsc');


                    //  print_r($validation_ssc_data);
                    //  print_r($validation_hsc_data);
                    //  exit;

                    if (!empty($validation_ssc_data) && !empty($validation_hsc_data) && trim($validation_ssc_data['NAME']) == trim($validation_hsc_data['NAME'])) {    // *** check exact Name
                        // Registration Number Generate by Inputed Data
                        //$REGISID = substr($hsc_board, 0, 2) . substr($hsc_passing_year, 2, 2) . $hsc_registration . $hsc_roll;
                        $REGISID = substr($hsc_board, 0, 2) . substr($hsc_passing_year, 2, 2) . trim($hsc_registration) . trim($hsc_roll);
                        // Check Student is Already Registered or Not by Generated Registration Number
                        $reg_status = $this->home_model->check_registration_status($REGISID);
                        if ($reg_status['reg_status'] == FALSE) { // create new registration

                            $this->home_model->create_new_registration($REGISID, $validation_ssc_data, $validation_hsc_data);
                        }

                        // *** Set Session Data ***--
                        $newapp = array(
                            'ses_REGISID' => $REGISID,
                            'ses_sRoll' => $ssc_roll,
                            'ses_sReg' => $ssc_registration,
                            'ses_hRoll' => $hsc_roll,
                            'ses_hYear' => $hsc_passing_year,
                            'ses_hReg' => $hsc_registration,
                            'ses_board_name' =>  $hsc_board
                        );
                        $this->session->set_userdata($newapp);

                        // $this->session->set_flashdata('notification', $validation_input_data);
                        redirect('home/apply/' . $REGISID);
                    } else {
                        if (trim($validation_ssc_data['NAME']) <> trim($validation_hsc_data['NAME'])) {
                            $this->session->set_flashdata('notification', 'Invalid Information, SSC and HSC Name Mismatch');
                        } else {
                            $this->session->set_flashdata('notification', 'Invalid Information');
                        }

                        redirect('/home');
                    }
                }
            } else {

                $this->session->set_flashdata('notification', 'Validation Failed');
                redirect('/home');
            }
        } else {
            $this->session->set_flashdata('notification', 'Validation Failed');
            redirect('/home');
        }
    }

    public function apply($reg_id = '')
    {
        if (empty($reg_id)) {
            redirect('/home');
        }

        $my_ses_REGISID = $this->session->userdata('ses_REGISID');

        $reg_id_clean = htmlspecialchars($reg_id, ENT_QUOTES, 'UTF-8');

        if ($my_ses_REGISID <> $reg_id_clean) {  // seesion reg id and url reg id  does not match
            redirect('/home');
        }

        $reg_status = $this->home_model->check_registration_status($reg_id_clean);

        if ($reg_status['reg_status'] === TRUE) { //already registerd, to check  and get applied unit and get photo if any
            $student_all_data = $this->home_model->get_registration_details($reg_id_clean);
            $this->data['student_all_data'] = $student_all_data;
            $this->data['REGISID'] = $reg_id_clean;
            $this->data['applied_unit'] = $this->home_model->get_already_applied_unit($reg_id_clean);

            $this->load->view('home/user_reg_details_form', $this->data);
        } else {
            redirect('/home');
        }
    }

    public function process()
    { //_STOP
        $REGISID = $this->input->post('REGISID', TRUE);
        $application_unit = $this->input->post('application_unit', TRUE);
        $quota = $this->input->post('quota', TRUE);
        $mobile_no = $this->input->post('mobile_no', TRUE);

        $district = $this->input->post('district', TRUE);
        $exam_question_tpe = $this->input->post('exam_question_tpe', TRUE);
        $with_arch = '';
        if ($application_unit == "A") {
            $with_arch = $this->input->post('with_arch', TRUE);
        }



        if (!empty($REGISID) && !empty($application_unit)) {

            //check if this student already applied in same unit, as one apply per student
            $query_Check = $this->db->where('REGISID', $REGISID);
            $query_Check = $this->db->where('UNIT', $application_unit);
            $query_Check = $this->db->from('tbl_application_pust');
            $query_Check = $this->db->get();
            $rowcount_Check = $query_Check->num_rows();

            if ($rowcount_Check <= 0) {

                $arr =  array(
                    'reg_id' => $REGISID,
                    'unitcode' => $application_unit,
                    'qname' => $quota,
                    'mobile' => $mobile_no,
                    'district' => $district,
                    'exam_question_tpe' => $exam_question_tpe,
                    'with_arch' => $with_arch
                );

                $app_id = $this->home_model->apply_unit($arr);
                if (!empty($app_id)) {
                    redirect('home/confirm/' . $app_id . '/' . $REGISID);
                } else {
                    redirect('notfound');
                }
            } else { // already applied
                redirect('notfound');
            }
        } else {
            redirect('home');
        }
    }

    function confirm($app_id, $reg_id)
    {
        $app_id_final = $this->security->xss_clean($app_id);
        $reg_id_final =   $this->security->xss_clean($reg_id);
        $this->data['application_data'] = $this->home_model->get_application_data($app_id_final, $reg_id_final);
        $student_all_data = $this->home_model->get_registration_details($reg_id_final);
        $this->data['student_all_data'] = $student_all_data;
        $this->load->view('home/view_confirm_page', $this->data);
    }

    function confirm_edit($app_id, $reg_id)
    {
        $app_id_final = $this->security->xss_clean($app_id);
        $reg_id_final =   $this->security->xss_clean($reg_id);
        $this->data['application_data'] = $this->home_model->get_application_data($app_id_final, $reg_id_final);
        $student_all_data = $this->home_model->get_registration_details($reg_id_final);
        $this->data['student_all_data'] = $student_all_data;
        $this->load->view('home/view_confirm_page_edit', $this->data);
    }

    function app_download($app_id, $reg_id)
    {
        $app_id_final = $this->security->xss_clean($app_id);
        $reg_id_final =   $this->security->xss_clean($reg_id);
        $this->data['application_data'] = $this->home_model->get_application_data($app_id_final, $reg_id_final);
        $student_all_data = $this->home_model->get_registration_details($reg_id_final);
        $this->data['student_all_data'] = $student_all_data;
        $html = $this->load->view('home/view_application_pdf', $this->data, true);
        // dd($this->data, $html);
        $pdfFilePath = 'rub_' . $app_id . ".pdf";

        //load mPDF library
        $this->load->library('m_pdf');

        //generate the PDF from the given html
        $this->m_pdf->pdf->SETHeader('Date: {DATE j-m-Y g:i a}');
        $this->m_pdf->pdf->WriteHTML($html);

        //download it.
        $this->m_pdf->pdf->Output($pdfFilePath, "D");
    }


    function ajax_uploadphoto()
    {
        $arr = '';
        $valid_file_exnt_array = array('jpg', 'jpeg');
        $reg_id = $this->input->post('REGISID_PHOTO', TRUE);
        $reg_id_clean = htmlspecialchars($reg_id, ENT_QUOTES, 'UTF-8');
        $orginal_file_name = $_FILES['userImage']['name'];
        if (!empty($reg_id_clean) && !empty($orginal_file_name)) {

            $temp = explode('.', $orginal_file_name); /* split the file name to get extension */
            $total_token = count($temp);
            $final_image_exten = $temp[$total_token - 1];
            if (in_array($final_image_exten, $valid_file_exnt_array)) {

                $img_url = $this->home_model->upload_photo($reg_id_clean);
                $arr['status'] = 'ok';
                $arr['msg'] = '';
                $arr['image_url'] = $img_url;
            } else {
                $arr['status'] = 'invalid';
                $arr['msg'] = 'Invalid file extension';
            }
        } else {
            $arr['status'] = 'error';
            $arr['msg'] = 'Please select photo and upload';
        }

        echo json_encode($arr);
    }

    function ajax_apply_validation()
    { //_STOP
        $arr = '';


        $reg_id = $this->input->post('reg_id', TRUE);
        $reg_id_clean = htmlspecialchars($reg_id, ENT_QUOTES, 'UTF-8');
        $unit = $this->input->post('unit', TRUE);

        $can_apply = 'no';

        if (!empty($reg_id_clean) && !empty($unit)) {

            $unit = $this->input->post('unit', TRUE);

            $s_group = $this->input->post('s_group', TRUE);
            $s_gpa = $this->input->post('s_gpa', TRUE);

            $h_group = $this->input->post('h_group', TRUE);
            $h_gpa = $this->input->post('h_gpa', TRUE);

            $t_gpa = $this->input->post('t_gpa', TRUE);

            $student_all_grade = $this->home_model->get_student_all_grade($reg_id_clean);

            $t = explode(',', $student_all_grade);
            $grade_arr = '';
            foreach ($t as $key => $value) {
                if (!empty($value)) {
                    $temp = explode(':', $value);
                    //$grade_arr[$temp[0]] = trim($temp[1]);
                    if ($this->session->userdata('ses_board_name') == "TECHNICAL") {
                        $grade_arr[$temp[0]] = trim(substr($temp[1], 3));
                    } else {
                        $grade_arr[$temp[0]] = trim($temp[1]);
                    }
                }
            }

            //print_r($grade_arr);

            $total_gpa = ($s_gpa + $h_gpa);

            $get_grading_gpa = get_grading_gpa();
            $hsc_board_name = $this->session->userdata('ses_board_name');
            if (!empty($hsc_board_name)) {


                if (strtoupper($hsc_board_name) == "MADRASAH") {

                    if ($unit == "A") {
                        //Math 228 +229
                        //BIOLOGY 230 +231

                        //chemistry  226 +227
                        //physics 224 +225
                        //English 238 +239


                        if ($s_gpa >= 3.5 && $h_gpa >= 3.5 && $total_gpa >= 8.0 && ($get_grading_gpa[$grade_arr['228']] >= 3.5 || $get_grading_gpa[$grade_arr['230']] >= 3.5) && $get_grading_gpa[$grade_arr['226']] >= 3.5 && $get_grading_gpa[$grade_arr['224']] >= 3.5 && $get_grading_gpa[$grade_arr['238']]  >= 3) {
                            $can_apply = 'yes';
                            $msg = "You are eligible to apply for this unit.";
                        } else {
                            $msg = "You are not eligible to apply this unit.";
                            $msg .= " Your GPA is less than the Required GPA.";
                            $msg .= "<br> Your SSC and HSC GPA should be atleast 3.5 and you must get minimum 8.00 total in both Exam. Also you should have minimum 3.5 in Math/Biology, Physics and Chemistry and 3.0 in English";
                        }
                    } else if ($unit == "B") {
                        if (strtoupper($h_group) == "SCIENCE") {
                            if ($s_gpa >= 3.5 && $h_gpa >= 3.5 && $total_gpa >= 8) {
                                $can_apply = 'yes';
                                $msg = "You are eligible to apply for this unit.";
                            } else {
                                $msg = "You are not eligible to apply this unit.";
                                $msg .= " Your GPA is less than the Required GPA.";
                                $msg .= "<br> Your SSC and HSC GPA should be atleast 3.50 and you must get minimum 8.0 total in both exam.";
                            }
                        } else {
                            if ($s_gpa >= 3.0 && $h_gpa >= 3.0 && $total_gpa >= 7.5) {
                                $can_apply = 'yes';
                                $msg = "You are eligible to apply for this unit.";
                            } else {
                                $msg = "You are not eligible to apply this unit.";
                                $msg .= " Your GPA is less than the Required GPA.";
                                $msg .= "<br> Your SSC and HSC GPA should be atleast 3.0 and you must get minimum 7.5 total in both exam.";
                            }
                        }
                    }
                } else  if (strtoupper($hsc_board_name) == "TEC") {

                    if ($unit == "A") {
                        //Math 1421
                        //physics  1422
                        //chemistry 1423
                        //boi 1324
                        //english 1542,1122 , 1622, 1812, 1832

                        //$eng_sub_code =  array(1542,1122,1622,1812,1832);					
                        $eng_gpa = '';
                        $mathematics_gpa_tech = '';
                        $boi_gpa_tech = '';

                        if (array_key_exists(1421, $grade_arr)) {
                            $mathematics_gpa_tech = trim($grade_arr['1421']); //Math
                        }

                        if (array_key_exists(1324, $grade_arr)) {
                            $boi_gpa_tech = trim($grade_arr['1324']); //boi
                        }

                        //find eng
                        if (array_key_exists(1542, $grade_arr)) {
                            $eng_gpa = trim($grade_arr['1542']);
                        } else if (array_key_exists(1122, $grade_arr)) {
                            $eng_gpa = trim($grade_arr['1122']);
                        } else if (array_key_exists(1622, $grade_arr)) {
                            $eng_gpa = trim($grade_arr['1622']);
                        } else if (array_key_exists(1812, $grade_arr)) {
                            $eng_gpa = trim($grade_arr['1812']);
                        } else if (array_key_exists(1832, $grade_arr)) {
                            $eng_gpa = trim($grade_arr['1832']);
                        }

                        if ($s_gpa >= 3.5 && $h_gpa >= 3.5 && $total_gpa >= 8.0 && ($get_grading_gpa[$mathematics_gpa_tech] >= 3.5 ||  $get_grading_gpa[$boi_gpa_tech] >= 3.5)  && $get_grading_gpa[$grade_arr[1422]] >= 3.5 && $get_grading_gpa[$grade_arr[1423]] >= 3.5 && $get_grading_gpa[$eng_gpa] >= 3) {
                            $can_apply = 'yes';
                            $msg = "You are eligible to apply for this unit.";
                        } else {
                            $msg = "You are not eligible to apply this unit.";
                            $msg .= " Your GPA is less than the Required GPA.";
                            $msg .= "<br> Your SSC and HSC GPA should be atleast 3.5 and you must get minimum 8.00 total in both Exam. Also you should have minimum 3.5 in Math/Biology, Physics, Chemistry and 3.0 in English";
                        }
                    } else if ($unit == "B") {
                        if (strtoupper($h_group) == "SCIENCE") {
                            if ($s_gpa >= 3.5 && $h_gpa >= 3.5 && $total_gpa >= 8) { //Technical with SCIENCE all are considers as Science
                                $can_apply = 'yes';
                                $msg = "You are eligible to apply for this unit.";
                            } else {
                                $msg = "You are not eligible to apply this unit.";
                                $msg .= " Your GPA is less than the Required GPA.";
                                $msg .= "<br> Your SSC and HSC GPA should be atleast 3.5 and you must get minimum 8.0 total in both exam.";
                            }
                        } else {
                            if ($s_gpa >= 3.0 && $h_gpa >= 3.0 && $total_gpa >= 7.5) {
                                $can_apply = 'yes';
                                $msg = "You are eligible to apply for this unit.";
                            } else {
                                $msg = "You are not eligible to apply this unit.";
                                $msg .= " Your GPA is less than the Required GPA.";
                                $msg .= "<br> Your SSC and HSC GPA should be atleast 3.0 and you must get minimum 7.50 total in both exam.";
                            }
                        }
                    }
                } else  if (strtoupper($hsc_board_name) == "BISD") { //|| strtoupper( $hsc_board_name == "TEC"
                    if ($unit == "B") {

                        if ($s_gpa >= 3.0 && $h_gpa >= 3.0 && $total_gpa >= 7.5) {  //DIBS/TEC BM all are commerce/accounting /bm
                            $can_apply = 'yes';
                            $msg = "You are eligible to apply for this unit.";
                        } else {
                            $msg = "You are not eligible to apply this unit.";
                            $msg .= " Your GPA is less than the Required GPA.";
                            $msg .= "<br> Your SSC and HSC GPA should be atleast 3.0 and you must get minimum 7.50 total in both exam.";
                        }
                    }
                } else {       // general line
                    $mathematics_gpa = '';

                    if (array_key_exists(127, $grade_arr)) {
                        $mathematics_gpa = trim($grade_arr['127']); //Math
                    } else if (array_key_exists(265, $grade_arr)) {
                        $mathematics_gpa = trim($grade_arr['265']); //higher math
                    }

                    if ($unit == "A") {
                        //Math 127, 265
                        //boilogoy 178				

                        //physics  174
                        //chemistry 176

                        //english 107

                        //echo $get_grading_gpa[$grade_arr[107]]." # ".$get_grading_gpa[$mathematics_gpa].' # '. $get_grading_gpa[$grade_arr[174]].' # '.$get_grading_gpa[$grade_arr[176]] .  ' # '.$sGpa.  ' # '.$hGpa.  ' # '.$ave;
                        if ($s_gpa >= 3.5 && $h_gpa >= 3.5 && $total_gpa >= 8.0 && ($get_grading_gpa[$mathematics_gpa] >= 3.5 || $get_grading_gpa[$grade_arr[178]] >= 3.5) && $get_grading_gpa[$grade_arr[174]] >= 3.5 && $get_grading_gpa[$grade_arr[176]] >= 3.5 && $get_grading_gpa[$grade_arr[107]] >= 3) {
                            $can_apply = 'yes';
                            $msg = "You are eligible to apply for this unit.";
                        } else {
                            $msg = "You are not eligible to apply this unit.";
                            $msg .= " Your GPA is less than the Required GPA.";
                            $msg .= "<br> Your SSC and HSC GPA should be atleast 3.5 and you must get minimum 8.00 total in both Exam. Also you should have minimum 3.5 in Math/Biology, Physics, Chemistry and 3.0 in English";
                        }
                    } else if ($unit == "B") {
                        if (strtoupper($h_group) == "SCIENCE") {
                            if ($s_gpa >= 3.5 && $h_gpa >= 3.5 && $total_gpa >= 8) {
                                $can_apply = 'yes';
                                $msg = "You are eligible to apply for this unit.";
                            } else {
                                $msg = "You are not eligible to apply this unit.";
                                $msg .= " Your GPA is less than the Required GPA.";
                                $msg .= "<br> Your SSC and HSC GPA should be atleast 3.5 and you must get minimum 8 total in both exam.";
                            }
                        } else {
                            if ($s_gpa >= 3.0 && $h_gpa >= 3.0 && $total_gpa >= 7.5) {
                                $can_apply = 'yes';
                                $msg = "You are eligible to apply for this unit.";
                            } else {
                                $msg = "You are not eligible to apply this unit.";
                                $msg .= " Your GPA is less than the Required GPA.";
                                $msg .= "<br> Your SSC and HSC GPA should be atleast 3.0 and you must get minimum 7.50 total in both exam.";
                            }
                        }
                    }
                }
            } else {
                $can_apply = 'error'; //'error';
                $msg = 'Board Missing From Session';
            }
        } else {

            $can_apply = 'error'; //'error';
            $msg = 'REG. ID Missing';
        }

        $arr['can_apply'] = $can_apply;
        $arr['msg'] = $msg;

        echo json_encode($arr);
    }


    function edit($edit_id, $reg_id)
    {

        if (!empty($edit_id)  && !empty($reg_id)) {

            //edit option

            $this->db->where('REGISID', $reg_id);
            $this->db->where('EDIT_ID', $edit_id);
            $this->db->where('edited', 'no');
            $this->db->from('tbl_application_pust');

            $query = $this->db->get();

            $rowcount = $query->num_rows();

            if ($rowcount > 0) {
                $rec =  $query->row_array();
                $data['edit_data'] = $rec;

                $this->load->view('home/user_app_form_edit', $data);
            } else {
                // return null;
                redirect('home');
            }
        } else {

            redirect('home');
        }
    }

    function processupdate()
    {
        $this->load->library('form_validation');

        // Form Validation Rules
        $this->form_validation->set_rules('edit_id', 'Edit ID', 'required');
        $this->form_validation->set_rules('edit_appid', 'APPID', 'required');
        $this->form_validation->set_rules('edit_unit', 'Unit', 'required');
        $this->form_validation->set_rules('mobile_no', 'Mobile', 'required');
        $this->form_validation->set_rules('quota', 'Quota', 'required');
        $this->form_validation->set_rules('district', 'District', 'required');
        $this->form_validation->set_rules('exam_question_tpe', 'Version of question paper', 'required');

        if ($this->form_validation->run() === FALSE) {
            redirect('home');
        } else { //update

            $arr =  array(
                'QNAME' => $this->input->post('quota', TRUE),
                'MOBNUMBER' => $this->input->post('mobile_no', TRUE),
                'HDISTRICT' => $this->input->post('district', TRUE),
                'EXAM_VERSION' => $this->input->post('exam_question_tpe', TRUE),
                'edited'    => 'yes'
            );

            $edit_id =         $this->input->post('edit_id', TRUE);
            $edit_appid =     $this->input->post('edit_appid', TRUE);
            $edit_unit =     $this->input->post('edit_unit', TRUE);

            $this->db->where('EDIT_ID', $edit_id);
            $this->db->where('APPID', $edit_appid);
            $this->db->where('UNIT', $edit_unit);
            $this->db->where('edited', 'no');

            $this->db->update(' tbl_application_pust', $arr);

            $reg_id = $this->input->post('reg_id', TRUE);

            redirect('home/confirm_edit/' . $edit_appid . '/' . $reg_id);
        }
    }

    function call_teletalk_api($board, $passing_year, $roll, $registration, $ssc_hsc_type)
    {
        $API_KEY = 'd49c6512c7134fe5c0ce5595873bb7a4';
        try {
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, "http://ebapi.teletalk.com.bd//v3.0/ebapi.php"); //http://ebapi.teletalk.com.bd/v3.0/ebapi.php

            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");

            $jsonData = array(
                'commandID' => 'getDetailsResult',
                'exam' =>   $ssc_hsc_type,
                'board' =>  $board,
                'rollNo' => $roll,
                'year' =>   $passing_year,
                'regNo' =>  $registration
            );

            $data_string = json_encode($jsonData);

            $headers = array();
            $headers[] = "Content-Type: application/json";
            $headers[] = "APIKEY: " . $API_KEY . "";
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);

            $result = curl_exec($ch);
            $arr_data_temp = json_decode($result, true);
            //print_r($arr_data_temp);
            $arr_data = '';

            //process response data
            if (!empty($arr_data_temp)) {

                if ($arr_data_temp['responseDesc'] == "Success") {

                    $arr_data['board'] = $arr_data_temp['board'];
                    $arr_data['rollno'] = $arr_data_temp['rollNo'];
                    $arr_data['passyear'] = $arr_data_temp['passYear'];
                    $arr_data['name'] = $arr_data_temp['name'];
                    $arr_data['father'] = $arr_data_temp['father'];
                    $arr_data['mother'] = $arr_data_temp['mother'];
                    $arr_data['regno'] = $arr_data_temp['regNo'];
                    $arr_data['gender'] = $arr_data_temp['gender'];
                    $arr_data['result'] = $arr_data_temp['result'];
                    $arr_data['gpa'] = $arr_data_temp['gpa'];
                    $arr_data['gpaExc4th'] = $arr_data_temp['gpaExc4th'];
                    $arr_data['stud_group'] = $arr_data_temp['studGroup'];
                    $arr_data['total_obt_mark'] = $arr_data_temp['totalObtMark'];
                    $arr_data['total_exc_4th'] = $arr_data_temp['totalExc4TH'];

                    $arr_data['total_obt_mark_cal'] = '';


                    $ltrgd = '';
                    if (!empty($arr_data_temp['subject']) && is_array($arr_data_temp['subject'])) {
                        foreach ($arr_data_temp['subject'] as $key => $value) {

                            if ($ssc_hsc_type == "hsc") {
                                $ltrgd .= $value["subCode"] . ":" . $value["grade"] . ":" . $value["gpoint"] . ",";
                            } else {
                                $ltrgd .= $value["subCode"] . ":" . $value["grade"] . ",";
                            }
                        }
                    }

                    if ($ssc_hsc_type == "ssc") { //get dob
                        $arr_data['dob'] = $arr_data_temp['dob'];
                    }

                    if ($ssc_hsc_type == "hsc") {
                        //4th sub
                        $arr_data['4th_sub_code'] = $arr_data_temp['sub4thCode'];
                    }

                    $arr_data['ltrgd'] = $ltrgd;
                }
            }
            //print_r($arr_data);
            return $arr_data;

            if (curl_errno($ch)) {
                //echo 'Error:' . curl_error($ch);
                $this->write_api_call_error_log(curl_error($ch));
                return false;
            }

            curl_close($ch);
        } catch (Exception $exc) {
            $this->write_api_call_error_log($exc);
            curl_close($ch);
            return false;
        }
    }

    function write_api_call_error_log($jsonData, $exp, $type = "ERROR")
    {
        file_put_contents("teletalk_log.txt", $type . "   Date: " . date('Y-m-d H:i:s') . " Request Data : " . json_encode($jsonData) . " Error: " . $exp . "\n", FILE_APPEND);
    }

    function write_others_log($infodata,  $type = "INFO")
    {
        file_put_contents("teletalk_info_log.txt", $type . "   Date: " . date('Y-m-d H:i:s') . " Data : " . $infodata . "\n", FILE_APPEND);
    }
}
