<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Dashboard
 *
 * @author Mhabub
 */
class Dashboard extends MY_Controller
{
    private  $data;
    public function __construct()
    {
        parent::__construct();
        $this->load->model('dashboard_model');
        $this->load->model('home_model');
        $this->load->helper('common_helper');
        $this->load->library('form_validation');

        if (!$this->session->userdata('student_logged_in')) {
            redirect('login');
        }
    }

    function index()
    {

        $student_login_mobile_no = $this->session->userdata('student_login_mobile_no');
        $student_login_hsc_board = $this->session->userdata('student_login_hsc_board');
        $student_login_hsc_year = $this->session->userdata('student_login_hsc_year');
        $student_login_hsc_reg = $this->session->userdata('student_login_hsc_reg');
        $student_login_hsc_roll = $this->session->userdata('student_login_hsc_roll');
        $student_login_regisid = $this->session->userdata('student_login_regisid');
        $sess_student_is_secnd_meirt =  $this->session->userdata('student_is_secnd_meirt');
        $apply_status = $this->dashboard_model->checkIsApply($this->session->userdata('student_login_regisid'));
        // dd($student_login_regisid, $apply_status ? 'Y' : 'N');
        if (!$apply_status) {
            redirect('stdApplication/applicationForm');
        }

        $this->data['std_per_data'] = $this->dashboard_model->get_std_dtl_from_login_tbl($student_login_hsc_board, $student_login_hsc_year, $student_login_hsc_roll, $student_login_hsc_reg);

        $student_all_data = $this->dashboard_model->get_student_details($student_login_hsc_board, $student_login_hsc_year, $student_login_hsc_roll, $student_login_hsc_reg);
        $this->data['student_all_data'] = $student_all_data;

        $this->data['eligible_unit_list'] = $this->dashboard_model->get_eligible_unit_list($student_login_regisid);

        $this->data['std_my_app_details'] =  $this->home_model->get_application_data_by_regid($student_login_regisid);


        $this->data['my_result'] =  ''; //$this->dashboard_model->get_result_subject_list($this->data['eligible_unit_list']);
        $this->data['my_admission_status'] = ''; //$this->dashboard_model->get_admission_status($student_login_regisid);

        $this->data['my_result_cancel'] = ''; // $this->dashboard_model->get_result_cancel_request($student_login_regisid);

        $this->data['sess_student_is_secnd_meirt'] =  $sess_student_is_secnd_meirt;

        $this->load->view('dashboard/view_std_details_form', $this->data);
    }

    function my_subject_option()
    {

        // redirect('dashboard');
        // exit;
        ////////////
        $choice_on_off = choice_on_off_status();
        $payment_status = $this->VivaPaymentStatus();

        //$sess_student_is_secnd_meirt =  $this->session->userdata('student_is_secnd_meirt');

        //      if($sess_student_is_secnd_meirt  <> "YES" ){ //subject choise for 2nd merit list
        //        redirect('dashboard');
        //      }

        $SHOWME = $this->uri->segment(6);

        // if($SHOWME <> 'YES'){
        //   redirect('dashboard');
        if ($choice_on_off['on_off'] == "off") {
            redirect('dashboard');
        }
        //  }
        if (!$payment_status) {
            redirect('dashboard');
        }

        $unique_id = $this->uri->segment(3);
        $exam_roll = $this->uri->segment(4);
        $unit_name = $this->uri->segment(5);
        // dd($unique_id, $exam_roll, $unit_name);

        $own_application =  is_own_application($unique_id, $exam_roll, $unit_name);
        if ($own_application == "no") {
            redirect('dashboard');
        }

        $this->data['unique_id'] = $unique_id;
        $this->data['exam_roll'] = $exam_roll;
        $this->data['unit_name'] = $unit_name;

        $student_login_mobile_no = $this->session->userdata('student_login_mobile_no');
        $student_login_hsc_board = $this->session->userdata('student_login_hsc_board');
        $student_login_hsc_year = $this->session->userdata('student_login_hsc_year');
        $student_login_hsc_reg = $this->session->userdata('student_login_hsc_reg');
        $student_login_hsc_roll = $this->session->userdata('student_login_hsc_roll');
        $student_login_regisid = $this->session->userdata('student_login_regisid');
        $student_login_photo_path = $this->session->userdata('student_login_photo_path');

        $this->data['student_login_regisid'] = $this->session->userdata('student_login_regisid');

        $this->data['student_login_photo_path'] = $this->session->userdata('student_login_photo_path');

        $res_sub_choice = $this->dashboard_model->get_saved_subject_list($unique_id, $exam_roll, $unit_name);
        // $res_sub_choice = [];

        $unit_details = $this->dashboard_model->get_eligible_unit_details($unique_id, $exam_roll, $unit_name, $student_login_regisid);
        $std_per_data = $this->dashboard_model->get_std_dtl_from_login_tbl($student_login_hsc_board, $student_login_hsc_year, $student_login_hsc_roll, $student_login_hsc_reg);
        $with_architecture_status = $this->WithArchitectureStatus();
        // dd('unit_name:', $unit_name, 'with_architecture_status:', $with_architecture_status, 'res_sub_choice:', $res_sub_choice);
        if ($unit_name == "A" && $with_architecture_status == true && empty($res_sub_choice)) {
            //force insert for ARCH unit subject,as ARCH has only one one subject
            $data_a2 = array(
                'unique_id' => $unique_id,
                'roll_number' => $exam_roll,
                'unit_name' => 'A',
                'sub_code_ref' => 12,
                'choise_order' => 1,
                'with_architecture' => "YES",
                'creation_date' => date('Y-m-d H:i:s'),
            );

            $this->db->insert('tbl_subject_choise', $data_a2);
        }

        if ($unit_name == "A") {
            $subj_gpa_data = get_subject_gpa_for_b();
            // print_r($subj_gpa_data);
            $this->data['subj_gpa_data'] = $subj_gpa_data;
        }
        $final_data = $this->db
            ->select('regisid,mobnumber,rollno_final,english_gpa_score,mathematics_gpa_score,physics_gpa_score,chemistry_gpa_score,biology_gpa_score,statistics_gpa_score,geography_gpa_score,economics_gpa_score,gst_score')
            ->where('regisid', $this->session->userdata('student_login_regisid'))
            ->get('tbl_pabna_data_final')->row();
        $this->data['final_data'] = $final_data;
        if (!empty($res_sub_choice)) {
            // dd($res_sub_choice);
            $this->data['unit_wise_subject_list_existing'] = $res_sub_choice;
            $this->data['show_from'] = 'unit_wise_subject_list_existing';
        } else {
            $unit_wise_subject_list = $this->dashboard_model->get_unit_wise_available_subject_list($unit_name);
            // dd($unit_wise_subject_list);
            // print_r($unit_wise_subject_list);exit;
            //print_r($subj_gpa_data);
            // if ($unit_name == "A") {
            //     $is_math_boi_answered = $unit_details['optional'];

            //     /*  For the Engineering Faculty subjects:
            //             HSC Math gp 3.5 and Must answered Math
            //             or else remove Engineering subjects
            //          */
            //     //$subj_gpa_data['mathematics_gpa_score'] = 3.5;
            //     if ($is_math_boi_answered <> 'Math' || $subj_gpa_data['mathematics_gpa_score'] < 3.5) {
            //         foreach ($unit_wise_subject_list as $key_ => $value_) {
            //             if ($value_['departments_code'] == "engineering") { //***  || $value_['departments_code'] == "science"
            //                 unset($unit_wise_subject_list[$key_]);
            //             }
            //         }
            //     }
            //     //print_r($unit_wise_subject_list);
            //     //$subj_gpa_data['chemistry_gpa_score'] = 3;
            //     //$subj_gpa_data['physics_gpa_score'] = 3;
            //     //$subj_gpa_data['mathematics_gpa_score'] = 3;
            //     //   print_r($subj_gpa_data);exit;

            //     if ($is_math_boi_answered == "Biology") {
            //         $Biology_allow = array('09', '13');
            //         foreach ($unit_wise_subject_list as $key => $value) {
            //             if (!in_array($value['sub_code'], $Biology_allow)) {
            //                 unset($unit_wise_subject_list[$key]);
            //             }
            //         }
            //     }

            //     //print_r($unit_wise_subject_list);exit;		

            //     foreach ($unit_wise_subject_list as $key => $value) {
            //         if ($value['sub_code'] == "03" && $subj_gpa_data['mathematics_gpa_score'] < 3.5) { // Mathematics
            //             unset($unit_wise_subject_list[$key]);
            //         } else if ($value['sub_code'] == "14" && ($subj_gpa_data['mathematics_gpa_score'] < 3.5 || $subj_gpa_data['chemistry_gpa_score'] <  3.5)) { // Chemistry
            //             unset($unit_wise_subject_list[$key]);
            //         } else if ($value['sub_code'] == "13" && ($subj_gpa_data['biology_gpa_score'] < 3.5 || $subj_gpa_data['chemistry_gpa_score'] <  3.5 || $is_math_boi_answered <> 'Biology')) { // Pharmacy
            //             unset($unit_wise_subject_list[$key]);
            //         } else if ($value['sub_code'] == "07" && ($subj_gpa_data['mathematics_gpa_score'] < 3.5 || $subj_gpa_data['physics_gpa_score'] <  3.5)) { // Physics
            //             unset($unit_wise_subject_list[$key]);
            //         } else if ($value['sub_code'] == "16" && ($subj_gpa_data['mathematics_gpa_score'] < 3.5 && $subj_gpa_data['statistics_gpa_score'] <  3.5)) { // Statistics
            //             unset($unit_wise_subject_list[$key]);
            //         }
            //     }
            // }


            if (empty($res_sub_choice)) { // very first time save sub choice for user

                // dd($final_data);
                foreach ($unit_wise_subject_list as $key => $subject) {
                    // ==============Engineering and Technology Faculty validation==================
                    if (!($final_data->physics_gpa_score >= 3.5 && $final_data->chemistry_gpa_score >= 3.5 && $final_data->mathematics_gpa_score >= 3.5) && $subject['departments_code'] == 'engineering') {
                        unset($unit_wise_subject_list[$key]);
                    }
                    // ==============/Engineering and Technology Faculty validation==================
                    // ==============Science Faculty validation==================
                    if (!($final_data->physics_gpa_score >= 3.5 && $final_data->chemistry_gpa_score >= 3.5 && $final_data->mathematics_gpa_score >= 3.5) && $subject['departments_code'] == 'science' && $subject['sub_code'] != 13) {
                        // Science Faculty validation without Pharmacy
                        unset($unit_wise_subject_list[$key]);
                    } elseif (!($final_data->physics_gpa_score >= 3.5 && $final_data->chemistry_gpa_score >= 3.5 && $final_data->biology_gpa_score >= 3.5) && $subject['departments_code'] == 'science' && $subject['sub_code'] == 13) {
                        // Science Faculty validation Only Pharmacy
                        unset($unit_wise_subject_list[$key]);
                    }
                    // ==============/Science Faculty validation==================

                    // ==============Life and Earth Science Faculty validation==================
                    if (!($final_data->mathematics_gpa_score >= 3.5 || $final_data->biology_gpa_score >= 3.5 || $final_data->geography_gpa_score >= 3.5) && $subject['departments_code'] == 'life_science') {
                        unset($unit_wise_subject_list[$key]);
                    }
                    // ==============/Life and Earth Science Faculty validation==================
                    // ==============Humanities and Social Science Faculty validation==================
                    if (!($final_data->mathematics_gpa_score >= 3.5 || $final_data->economics_gpa_score >= 3.5 || $final_data->statistics_gpa_score >= 3.5) && $subject['departments_code'] == 'humanities' && $subject['sub_code'] == '08') {
                        unset($unit_wise_subject_list[$key]);
                    }
                    // ==============/Humanities and Social Science Faculty validation==================
                }
                $this->save_sub_choice_first_time($unit_wise_subject_list, $unique_id, $exam_roll, $unit_name);
            }

            $this->data['unit_wise_subject_list'] = $unit_wise_subject_list; //$this->dashboard_model->get_unit_wise_subject_list($unit_name);
            $this->data['show_from'] = 'unit_wise_subject_list';
        }

        //print_r($std_per_data);
        // print_r($unit_details);exit;

        $this->data['unit_details'] = $unit_details;

        $this->data['std_per_data'] = $std_per_data;
        $this->data['with_architecture_status'] = $with_architecture_status;

        $this->load->view('dashboard/view_subject_choice_form', $this->data);
    }

    /*
    * save student personal information
    */
    function my_subject_option_save()
    { //_STOP
        $res = $this->dashboard_model->my_subject_option_save_data();
        $unique_id = $this->input->post('unique_id', TRUE);
        $exam_roll = $this->input->post('exam_roll', TRUE);
        $unit_name = $this->input->post('unit_name', TRUE);
        //redirect('dashboard/my_subject_option/'.$unique_id."/".$exam_roll."/".$unit_name);
        redirect('dashboard');
    }


    function  save_sub_choice_first_time($list_order_arr, $unique_id, $exam_roll, $unit_name)
    {
        $current_date_time = date('Y-m-d H:i:s');
        $i = 1;

        foreach ($list_order_arr as $key => $value) {

            $data = array(
                'unique_id' => $unique_id,
                'roll_number' => $exam_roll,
                'unit_name' => $unit_name,
                'sub_code_ref' => $value['sub_code'],
                'choise_order' => $i,
                'creation_date' => $current_date_time
                //'is_secnd_meirt' => 'YES' //2nd merit list
            );

            $result = $this->db->insert('tbl_subject_choise', $data);

            $i++;
        }
    }

    /*
    * call from ajax to save subject data
    */
    function save_sub_choice()
    { // STOP_

        $current_date_time = date('Y-m-d H:i:s');

        $list_order = $this->input->post('list_order', TRUE);
        $unique_id = $this->input->post('unique_id', TRUE);
        $exam_roll = $this->input->post('exam_roll', TRUE);
        $unit_name = $this->input->post('unit_name', TRUE);
        $list_order_arr = explode(',', $list_order);

        if (!empty($list_order_arr) && !empty($unique_id) && !empty($exam_roll) && !empty($unit_name)) {
            $i = 1;
            $query_delete = $this->db->where('unique_id', $unique_id);
            $query_delete = $this->db->where('roll_number', $exam_roll);
            $query_delete = $this->db->where('unit_name', $unit_name);
            $query_delete = $this->db->where('with_architecture', 'NO');
            $query_delete = $this->db->delete('tbl_subject_choise');

            foreach ($list_order_arr as $key => $value) {

                $data = array(
                    'unique_id' => $unique_id,
                    'roll_number' => $exam_roll,
                    'unit_name' => $unit_name,
                    'sub_code_ref' => $value,
                    'choise_order' => $i,
                    'creation_date' => $current_date_time
                    //'is_secnd_meirt' => 'YES' //2nd merit list
                );

                $result = $this->db->insert('tbl_subject_choise', $data);

                $i++;
            }
        }
        echo "OK";
    }

    function download()
    {
        $unique_id = $this->uri->segment(3);
        $exam_roll = $this->uri->segment(4);
        $unit_name = $this->uri->segment(5);

        $own_application =  is_own_application($unique_id, $exam_roll, $unit_name);
        if ($own_application == "no") {
            redirect('dashboard');
        }

        $this->data['unique_id'] = $unique_id;
        $this->data['exam_roll'] = $exam_roll;
        $this->data['unit_name'] = $unit_name;


        $student_login_mobile_no = $this->session->userdata('student_login_mobile_no');
        $student_login_hsc_board = $this->session->userdata('student_login_hsc_board');
        $student_login_hsc_year = $this->session->userdata('student_login_hsc_year');
        $student_login_hsc_reg = $this->session->userdata('student_login_hsc_reg');
        $student_login_hsc_roll = $this->session->userdata('student_login_hsc_roll');
        $student_login_regisid = $this->session->userdata('student_login_regisid');
        $student_login_photo_path = $this->session->userdata('student_login_photo_path');


        $student_acadamic_data = $this->dashboard_model->get_student_all__details($student_login_regisid);

        $res_sub_choice = $this->dashboard_model->get_saved_subject_list($unique_id, $exam_roll, $unit_name);

        $this->data['unit_details'] = $this->dashboard_model->get_eligible_unit_details($unique_id, $exam_roll, $unit_name, $student_login_regisid);

        $this->data['student_login_photo_path'] = $this->session->userdata('student_login_photo_path');
        $this->data['student_acadamic_data'] = $student_acadamic_data;
        $this->data['unit_wise_subject_list_existing'] = $res_sub_choice;
        $this->data['application_pust'] = $this->db->where('regisid', $this->session->userdata('student_login_regisid'))->get('tbl_application_pust')->row_array();

        $std_per_data = $this->dashboard_model->get_std_dtl_from_login_tbl($student_login_hsc_board, $student_login_hsc_year, $student_login_hsc_roll, $student_login_hsc_reg);
        $this->data['std_per_data'] = $std_per_data;
        $html = $this->load->view('dashboard/view_subject_confirm_pdf', $this->data, true);
        // dd($this->data, $html);

        //this the the PDF filename that user will get to download
        $pdfFilePath = "PUST_202021_" . $unique_id . ".pdf";

        //load mPDF library
        $this->load->library('m_pdf');

        //generate the PDF from the given html
        $this->m_pdf->pdf->WriteHTML($html);

        //download it.
        $this->m_pdf->pdf->Output($pdfFilePath, "D");
        ob_clean();
    }

    function admission()
    {
    }

    function admission_pdf_download()
    {
        $unique_id = $this->uri->segment(3);
        $exam_roll = $this->uri->segment(4);
        $unit_name = $this->uri->segment(5);

        //	   $own_application =  is_own_application( $unique_id, $exam_roll, $unit_name);
        //	   if($own_application == "no"){
        //		   redirect('dashboard');
        //	   }

        $this->data['unique_id'] = $unique_id;
        $this->data['exam_roll'] = $exam_roll;
        $this->data['unit_name'] = $unit_name;

        $my_result = $this->dashboard_model->get_result_subject_details($unique_id);
        if (empty($my_result)) {
            redirect('dashboard');
        }
        $this->data['my_result'] = $my_result;

        $student_login_mobile_no = $this->session->userdata('student_login_mobile_no');
        $student_login_hsc_board = $this->session->userdata('student_login_hsc_board');
        $student_login_hsc_year = $this->session->userdata('student_login_hsc_year');
        $student_login_hsc_reg = $this->session->userdata('student_login_hsc_reg');
        $student_login_hsc_roll = $this->session->userdata('student_login_hsc_roll');
        $student_login_regisid = $this->session->userdata('student_login_regisid');
        $student_login_photo_path = $this->session->userdata('student_login_photo_path');


        $student_acadamic_data = $this->dashboard_model->get_student_all__details($student_login_regisid);

        $res_sub_choice = $this->dashboard_model->get_saved_subject_list($unique_id, $exam_roll, $unit_name);

        $this->data['unit_details'] = $this->dashboard_model->get_eligible_unit_details($unique_id, $exam_roll, $unit_name, $student_login_regisid);

        $this->data['student_login_photo_path'] = $this->session->userdata('student_login_photo_path');
        $this->data['student_acadamic_data'] = $student_acadamic_data;
        $this->data['unit_wise_subject_list_existing'] = $res_sub_choice;

        $std_per_data = $this->dashboard_model->get_std_dtl_from_login_tbl($student_login_hsc_board, $student_login_hsc_year, $student_login_hsc_roll, $student_login_hsc_reg);
        $this->data['std_per_data'] = $std_per_data;
        //     echo   $html = $this->load->view('dashboard/view_admission_pdf', $this->data,true);exit;

        //update download track
        $arr_update =  array(
            'form_downloaded' => 'YES'
        );
        $this->db->where('unique_id_ref', $unique_id);
        $this->db->update('tbl_result', $arr_update);
        //
        $html = $this->load->view('dashboard/view_admission_pdf', $this->data, true);

        $this->load->library('m_pdf');

        $this->m_pdf->pdf->SetHTMLHeader('
                                        <div style="text-align: right; font-weight: bold;">
                                        </div>');
        //this the the PDF filename that user will get to download
        $pdfFilePath = "PUST_admission_" . $unique_id . ".pdf";

        //load mPDF library


        //generate the PDF from the given html
        $this->m_pdf->pdf->WriteHTML($html);

        //download it.
        $this->m_pdf->pdf->Output($pdfFilePath, "D");
    }

    function registation_pdf_download()
    {
        $unique_id = $this->uri->segment(3);
        $exam_roll = $this->uri->segment(4);
        $unit_name = $this->uri->segment(5);

        //	   $own_application =  is_own_application( $unique_id, $exam_roll, $unit_name);
        //	   if($own_application == "no"){
        //		   redirect('dashboard');
        //	   }

        $this->data['unique_id'] = $unique_id;
        $this->data['exam_roll'] = $exam_roll;
        $this->data['unit_name'] = $unit_name;


        $student_login_mobile_no = $this->session->userdata('student_login_mobile_no');
        $student_login_hsc_board = $this->session->userdata('student_login_hsc_board');
        $student_login_hsc_year = $this->session->userdata('student_login_hsc_year');
        $student_login_hsc_reg = $this->session->userdata('student_login_hsc_reg');
        $student_login_hsc_roll = $this->session->userdata('student_login_hsc_roll');
        $student_login_regisid = $this->session->userdata('student_login_regisid');
        $student_login_photo_path = $this->session->userdata('student_login_photo_path');


        $student_acadamic_data = $this->dashboard_model->get_student_all__details($student_login_regisid);

        $res_sub_choice = $this->dashboard_model->get_saved_subject_list($unique_id, $exam_roll, $unit_name);

        $this->data['unit_details'] = $this->dashboard_model->get_eligible_unit_details($unique_id, $exam_roll, $unit_name, $student_login_regisid);

        $this->data['student_login_photo_path'] = $this->session->userdata('student_login_photo_path');
        $this->data['student_acadamic_data'] = $student_acadamic_data;
        $this->data['unit_wise_subject_list_existing'] = $res_sub_choice;

        $std_per_data = $this->dashboard_model->get_std_dtl_from_login_tbl($student_login_hsc_board, $student_login_hsc_year, $student_login_hsc_roll, $student_login_hsc_reg);
        $this->data['std_per_data'] = $std_per_data;
        //      echo   $html = $this->load->view('dashboard/view_admission_pdf', $this->data,true);exit;
        $html = $this->load->view('dashboard/view_admission_pdf', $this->data, true);

        $this->load->library('m_pdf');

        $this->m_pdf->pdf->SetHTMLHeader('
                                        <div style="text-align: right; font-weight: bold;">
                                        </div>');
        //this the the PDF filename that user will get to download
        $pdfFilePath = "PUST_admission_" . $unique_id . ".pdf";

        //load mPDF library


        //generate the PDF from the given html
        $this->m_pdf->pdf->WriteHTML($html);

        //download it.
        $this->m_pdf->pdf->Output($pdfFilePath, "D");
    }

    function admission_cancel_form()
    { //stop with _
        $unique_id = $this->uri->segment(3);
        $exam_roll = $this->uri->segment(4);
        $unit_name = $this->uri->segment(5);
        $sub_code = $this->uri->segment(6);

        $this->data['sub_code'] = $sub_code;
        $this->data['unique_id'] = $unique_id;
        $this->data['exam_roll'] = $exam_roll;
        $this->data['unit_name'] = $unit_name;
        $this->load->view('dashboard/view_admission_cancel_form', $this->data);
    }

    function admission_cancel_save()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('cancel_reason', 'Cancel Reason', 'required');

        $sub_code = $this->input->post('sub_code', TRUE);
        $unique_id = $this->input->post('unique_id', TRUE);
        $exam_roll = $this->input->post('exam_roll', TRUE);
        $unit_name = $this->input->post('unit_name', TRUE);

        $student_login_mobile_no = $this->session->userdata('student_login_mobile_no');
        $student_login_hsc_board = $this->session->userdata('student_login_hsc_board');
        $student_login_hsc_year = $this->session->userdata('student_login_hsc_year');
        $student_login_hsc_reg = $this->session->userdata('student_login_hsc_reg');
        $student_login_hsc_roll = $this->session->userdata('student_login_hsc_roll');
        $student_login_regisid = $this->session->userdata('student_login_regisid');
        $student_login_photo_path = $this->session->userdata('student_login_photo_path');

        if ($this->form_validation->run() === FALSE) {


            redirect('dashboard/admission_cancel_form/' . $unique_id . '/' . $exam_roll . '/' . $unit_name . '/' . $sub_code);
        } else {
            $cancel_reason = $this->input->post('cancel_reason', TRUE);

            //$unit_details =  $this->dashboard_model->get_eligible_unit_details($unique_id,$exam_roll,$unit_name,$student_login_regisid);

            $arr_update =  array(
                'cancel_id' => 'c' . $unique_id,
                'cancel_status' => 'cancel_apply',
                'cancel_reason' => $cancel_reason,
                'cancel_amount_to_pay' => 2000,
                'cancel_paid_status' => 'U',
                'cancel_apply_date' => date('Y-m-d H:i:s')
            );
            $this->db->where('unique_id_ref', $unique_id);
            $this->db->update('tbl_result', $arr_update);

            redirect('dashboard');
            /*    $arr_save =  array(
                'system_reg_id_ref' => $student_login_regisid,
                'unique_id_ref' => $unique_id,
                'tracking_id' => $unit_details['tracking_id'],
                'sub_code' => $sub_code,
                'cancel_reason' => $cancel_reason,
                'amount_to_pay' => 2020,
                'cancel_status' => 'cancel_apply',
                'updated_dt' => date('Y-m-d H:i:s')

            );
            $this->db->insert('tbl_cancel', $arr_save);
        */
            //redirect('dashboard/admission_cancel_pdf_download/'.$unique_id.'/'.$exam_roll.'/'.$unit_name.'/'.$sub_code);
        }
    }


    function admission_cancel_pdf_download()
    {
        //check if cancel fee paid by the student
        $unique_id = $this->uri->segment(3);
        $exam_roll = $this->uri->segment(4);
        $unit_name = $this->uri->segment(5);
        $sub_code = $this->uri->segment(6);

        $this->data['unique_id'] = $unique_id;
        $this->data['exam_roll'] = $exam_roll;
        $this->data['unit_name'] = $unit_name;
        $this->data['sub_code'] = $sub_code;

        $student_login_mobile_no = $this->session->userdata('student_login_mobile_no');
        $student_login_hsc_board = $this->session->userdata('student_login_hsc_board');
        $student_login_hsc_year = $this->session->userdata('student_login_hsc_year');
        $student_login_hsc_reg = $this->session->userdata('student_login_hsc_reg');
        $student_login_hsc_roll = $this->session->userdata('student_login_hsc_roll');
        $student_login_regisid = $this->session->userdata('student_login_regisid');
        $student_login_photo_path = $this->session->userdata('student_login_photo_path');

        $student_acadamic_data = $this->dashboard_model->get_student_all__details($student_login_regisid);
        $result_details =  $this->dashboard_model->get_result_subject_details($unique_id);

        $std_per_data = $this->dashboard_model->get_std_dtl_from_login_tbl($student_login_hsc_board, $student_login_hsc_year, $student_login_hsc_roll, $student_login_hsc_reg);

        $this->data['student_login_photo_path'] = $student_login_photo_path;
        $this->data['result_details'] = $result_details;
        $this->data['student_acadamic_data'] = $student_acadamic_data;
        $this->data['std_per_data'] = $std_per_data;

        $html =    $this->load->view('dashboard/view_admission_cancel_form_pdf', $this->data, true);
        $this->load->library('m_pdf');

        $this->m_pdf->pdf->SetHTMLHeader('
                                        <div style="text-align: right; font-weight: bold;">
                                        </div>');
        //this the the PDF filename that user will get to download
        $pdfFilePath = "PUST_admission_cancel_std_" . $unique_id . ".pdf";

        //load mPDF library


        //generate the PDF from the given html
        $this->m_pdf->pdf->WriteHTML($html);

        //download it.
        $this->m_pdf->pdf->Output($pdfFilePath, "D");
    }


    function migration_process()
    { //stop migration

        $student_login_regisid = $this->session->userdata('student_login_regisid');

        if (!empty($student_login_regisid)) {
            $arr_update =  array(
                'migration_on_off' => 'OFF',
                'migration_on_off_dt' => date('Y-m-d H:i:s')
            );
            $this->db->where('system_reg_id_ref', $student_login_regisid);
            $this->db->where('paid_status', 'P');
            $this->db->update('tbl_result', $arr_update);
        }
        redirect('dashboard');
    }


    function admit_from_waiting__()
    {
        $student_login_regisid = $this->session->userdata('student_login_regisid');


        if (!empty($student_login_regisid)) {
            $arr_update =  array(
                'wants_to_admit_frm_waiting' => 'YES',
                'wants_to_admit_frm_waiting_dt' => date('Y-m-d H:i:s')
            );

            $this->db->where('system_reg_id', $student_login_regisid);
            //  $this->db->where('unit', 'A'); // only A unit
            $this->db->where('allocated_subject IS NULL'); // only A unit and waiting
            $this->db->update('tbl_merit_list', $arr_update);
            //echo $this->db->last_query();exit;
        }
        redirect('dashboard');
    }
    public function success()
    {
        // $this->data['payment_message'] = '';

        $this->load->view('OnlinePayment/success_payment');
    }
    public function failed()
    {
        // $this->data['payment_message'] = '';

        $this->load->view('OnlinePayment/failed_payment');
    }
}
