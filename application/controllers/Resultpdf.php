<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Resultpdf extends CI_Controller {
	var $orginal_file_path = '';
	var $orginal_file_path_q = '';
        var $orginal_file_path_waiting = '';
		var $orginal_file_path_waiting_quota = '';
    public function __construct() {

        parent::__construct();
		$this->orginal_file_path = realpath(APPPATH . '../');// resultpdf/7th
		$this->orginal_file_path_q = realpath(APPPATH . '../resultpdf');
                $this->orginal_file_path_waiting = realpath(APPPATH . '../waiting');
				$this->orginal_file_path_waiting_quota = realpath(APPPATH . '../waiting');
                 $this->load->model('dashboard_model');


    }

function create($sub_code,$unit){ //$group_c
	$this->load->library('m_pdf');
	set_time_limit(0);

        // $unit = 'A1';
        $quota_name = ''; // 'PH';

        $data = '';
        //$sql = "select * from `tbl_result`  where sub_code_1='$sub_code'";
		
		$sql = "select * from `tbl_result`  where sub_code_1='$sub_code' order by `c_section_hsc_group`,`merit_pos_ref`";
        //   $sql = "select * from `tbl_result`  where sub_code_1='$sub_code' and c_section_hsc_group='$group_c' order by `c_section_hsc_group`,`merit_pos_ref`";


        $query = $this->db->query($sql);
        $rowcount = $query->num_rows();

        if($rowcount > 0){
                                $arr = '';
                                $rec = $query->result();
                                foreach ($rec as $key => $value) {
                                    $system_reg_id = $value->system_reg_id_ref;
                                    $id = $value->id;
                                    $sql2 = "select * from `tbl_pabna_data_final` where  REGISID='".$system_reg_id."'";
                                    $query2 = $this->db->query($sql2);
                                    $rec2 = $query2->row();

                                    $sql3 = "select * from `tbl_merit_list` where  unique_id='".$value->unique_id_ref."'";
                                    $query3 = $this->db->query($sql3);
                                    $rec3 = $query3->row();


                                    $arr[$id]['id'] = $id;
                                    $arr[$id]['unique_id'] = $value->unique_id_ref;
                                    $arr[$id]['tracking_id'] = $value->tracking_id;
                                    $arr[$id]['system_reg_id'] = $system_reg_id;
                                    $arr[$id]['exam_roll'] = $value->exam_roll_ref;
                                    $arr[$id]['app_id'] = $value->app_id_ref;
                                    $arr[$id]['unit'] = $value->unit_ref;
                                    $arr[$id]['c_section_hsc_group'] = $value->c_section_hsc_group;

                                    $arr[$id]['score'] = $rec3->score;
                                    $arr[$id]['merit'] = $rec3->merit;
                                    $arr[$id]['quota_name'] = $rec3->quota_ref;
                                    $arr[$id]['optional'] = $rec3->optional;

                                    $arr[$id]['sname'] = $rec2->SNAME;
                                    $arr[$id]['sfname'] = $rec2->SFNAME;
                                    $arr[$id]['smname'] = $rec2->SMNAME;

                                    $arr[$id]['ssc_board'] = $rec2->SBOARD_NAME;
                                    $arr[$id]['ssc_gpa'] = $rec2->SGPA;
                                    $arr[$id]['ssc_roll'] = $rec2->SROLL_NO;
                                    $arr[$id]['ssc_reg'] = $rec2->SREG_NO;


                                    $arr[$id]['hsc_board'] = $rec2->HBOARD_NAME;
                                    $arr[$id]['hsc_gpa'] = $rec2->HGPA;
                                    $arr[$id]['hsc_roll'] = $rec2->HROLL_NO;
                                    $arr[$id]['hsc_reg'] = $rec2->HREG_NO;

                                    $arr[$id]['img_path'] = $rec2->IMGUPLOAD_PATH;

                                }


                                    $sql4 = "select * from `tbl_subject` where  sub_code='".$sub_code."'";
                                    $query4 = $this->db->query($sql4);
                                    $rec4 = $query4->row();


				$data['res'] = $arr;
                                $data['UNIT'] = $unit;
                                $data['sub_code'] = $sub_code;
                                $data['sub_name'] = $rec4->sub_name;
                                $data['faculty_name'] = $rec4->faculty_name;
                                $data['total_seat'] = $rec4->total_seat;

                               // $data['group_c'] = $group_c;

								$html = '';
                                 $html = $this->load->view('result_pdf/view_result_pdf', $data, true);

                                    $this->m_pdf->pdf->SetHTMLHeader('
                                        <div style="text-align: right; font-weight: bold;">
                                        </div>');

                                $this->m_pdf->pdf->setFooter("Page {PAGENO} of {nb}");
                                $this->m_pdf->pdf->WriteHTML($html);
								$pdfFilePath = 	'';
                                $sub_name_pdf = str_replace( " ", "", $data['sub_name'] );
                                $pdfFilePath = $this->orginal_file_path .'/'.$sub_code."_".$sub_name_pdf."_result.pdf";


                                //$sub_name_temp = str_replace(" ", "_", $data['sub_name']);
                                //$pdfFilePath = $this->orginal_file_path .'/C/'.$sub_code."_".$group_c."_".$sub_name_temp."_result.pdf";
                                // $pdfFilePath = $this->orginal_file_path .'/'.$unit.'_'.$quota_name.'_'.$position."_viva.pdf";


				$this->m_pdf->pdf->Output($pdfFilePath, "F");
				echo $pdfFilePath."   OK \r\n";
			}else{
				echo "  No Record \r\n";
			}

	}


        function create_migration($sub_code,$unit){ //$group_c
	$this->load->library('m_pdf');

	set_time_limit(0);
       // $unit = 'A1';
        $quota_name =''; // 'PH';

			$data = '';
                        $sql = "select * from `tbl_result`  where sub_code_2='$sub_code' and where first_migration_done='YES'";


			$query = $this->db->query($sql);
			$rowcount = $query->num_rows();

			if($rowcount > 0){
                                $arr = '';
                                $rec = $query->result();
                                foreach ($rec as $key => $value) {
                                    $system_reg_id = $value->system_reg_id_ref;
                                    $id = $value->id;
                                    $sql2 = "select * from `tbl_pabna_data_final` where  REGISID='".$system_reg_id."'";
                                    $query2 = $this->db->query($sql2);
                                    $rec2 = $query2->row();

                                    $sql3 = "select * from `tbl_merit_list` where  unique_id='".$value->unique_id_ref."'";
                                    $query3 = $this->db->query($sql3);
                                    $rec3 = $query3->row();


                                    $arr[$id]['id'] = $id;
                                    $arr[$id]['unique_id'] = $value->unique_id_ref;
                                    $arr[$id]['tracking_id'] = $value->tracking_id;
                                    $arr[$id]['system_reg_id'] = $system_reg_id;
                                    $arr[$id]['exam_roll'] = $value->exam_roll_ref;
                                    $arr[$id]['app_id'] = $value->app_id_ref;
                                    $arr[$id]['unit'] = $value->unit_ref;
                                    $arr[$id]['c_section_hsc_group'] = $value->c_section_hsc_group;

                                    $arr[$id]['score'] = $rec3->score;
                                    $arr[$id]['merit'] = $rec3->merit;
                                    $arr[$id]['quota_name'] = $rec3->quota_ref;

                                    $arr[$id]['sname'] = $rec2->SNAME;
                                    $arr[$id]['sfname'] = $rec2->SFNAME;
                                    $arr[$id]['smname'] = $rec2->SMNAME;

                                    $arr[$id]['ssc_board'] = $rec2->SBOARD_NAME;
                                    $arr[$id]['ssc_gpa'] = $rec2->SGPA;
                                    $arr[$id]['ssc_roll'] = $rec2->SROLL_NO;
                                    $arr[$id]['ssc_reg'] = $rec2->SREG_NO;


                                    $arr[$id]['hsc_board'] = $rec2->HBOARD_NAME;
                                    $arr[$id]['hsc_gpa'] = $rec2->HGPA;
                                    $arr[$id]['hsc_roll'] = $rec2->HROLL_NO;
                                    $arr[$id]['hsc_reg'] = $rec2->HREG_NO;

                                    $arr[$id]['img_path'] = $rec2->IMGUPLOAD_PATH;

                                }


                                    $sql4 = "select * from `tbl_subject` where  sub_code='".$sub_code."'";
                                    $query4 = $this->db->query($sql4);
                                    $rec4 = $query4->row();


				$data['res'] = $arr;
                                $data['UNIT'] = $unit;
                                $data['sub_code'] = $sub_code;
                                $data['sub_name'] = $rec4->sub_name;
                                $data['faculty_name'] = $rec4->faculty_name;
                                $data['total_seat'] = $rec4->total_seat;

                               // $data['group_c'] = $group_c;

				 $html = '';
                                 $html = $this->load->view('result_pdf/view_result_pdf', $data, true);  // view_seatplan_pdf_OK_OK

                                    $this->m_pdf->pdf->SetHTMLHeader('
                                        <div style="text-align: right; font-weight: bold;">
                                        </div>');
//                                    $this->m_pdf->pdf->SetHTMLFooter('
//                                    <table width="100%">
//                                    <tr> <td width="100%" align="center"><br><br><br></td> </tr>
//                                        <tr>
//                                            <td width="100%" align="center"><hr>Signature of Board Members(Sign above the line with date)</td>
//                                        </tr>
//                                    </table>');

                                $this->m_pdf->pdf->WriteHTML($html);
				$pdfFilePath = 	'';

                                //$sub_name_temp = str_replace(" ", "_", $data['sub_name']);
                                //$pdfFilePath = $this->orginal_file_path .'/C/'.$sub_code."_".$group_c."_".$sub_name_temp."_result.pdf";
                                $sub_name_pdf = str_replace(" ","",$data['sub_name']);
                                $pdfFilePath = $this->orginal_file_path .'/'.$sub_code."_".$sub_name_pdf ."_migration_result.pdf";
                               // $pdfFilePath = $this->orginal_file_path .'/'.$unit.'_'.$quota_name.'_'.$position."_viva.pdf";

				$this->m_pdf->pdf->Output($pdfFilePath, "F");
				echo "   OK \r\n";
			}else{
				echo "  No Record \r\n";
			}

	}

function create_2nd_merit($sub_code,$unit){ //$group_c  2nd, 3rd , 4th merit, all are created using this function
	$this->load->library('m_pdf');
	set_time_limit(0);

        // $unit = 'A1';
        $quota_name = ''; // 'PH';

        $data = '';
		//2nd  merit
           $sql = "select * from `tbl_result_6h`  where sub_code_6='$sub_code' and  is_seven_merit='YES' and quota_ref = '' order by `merit_pos_ref`"; // quota_ref = '' and
         //$sql = "select * from `tbl_result_5th_res`  where sub_code_5='$sub_code' and is_fifth_meirt='YES' and quota_ref = '' order by `c_section_hsc_group`,`merit_pos_ref`";
	 
	 

       //     $sql = "select * from `tbl_result`  where sub_code_2='$sub_code' and quota_ref = '' and  is_third_meirt='YES' order by `merit_pos_ref`"; // quota_ref = '' and
       //     $sql = "select * from `tbl_result`  where sub_code_2='$sub_code' and quota_ref = '' and is_third_meirt='YES' order by `c_section_hsc_group`,`merit_pos_ref`";

         //  $sql = "select * from `tbl_result`  where sub_code_2='$sub_code' and quota_ref = '' and  	is_fourth_meirt='YES' order by `merit_pos_ref`"; // quota_ref = '' and
          //  $sql = "select * from `tbl_result`  where sub_code_2='$sub_code' and quota_ref = '' and is_fourth_meirt='YES' order by `c_section_hsc_group`,`merit_pos_ref`";

            //$sql = "select * from `tbl_result`  where sub_code_2='$sub_code' and quota_ref = '' and  	is_fifth_meirt='YES' order by `merit_pos_ref`"; // quota_ref = '' and
          //  $sql = "select * from `tbl_result`  where sub_code_2='$sub_code' and quota_ref = '' and is_fifth_meirt='YES' order by `c_section_hsc_group`,`merit_pos_ref`";

            //$sql = "select * from `tbl_result`  where sub_code_6='$sub_code' and quota_ref = '' and  	is_sixth_merit='YES' order by `merit_pos_ref`"; // quota_ref = '' and
          //  $sql = "select * from `tbl_result`  where sub_code_6='$sub_code' and quota_ref = '' and is_sixth_merit='YES' order by `c_section_hsc_group`,`merit_pos_ref`";

               //$sql = "select * from `tbl_result`  where sub_code_7='$sub_code' and quota_ref = '' and  	is_seven_merit='YES' order by `merit_pos_ref`"; // quota_ref = '' and
               //

                //$sql = "select * from `tbl_result`  where sub_code_10='$sub_code' and quota_ref = '' and is_ten_merit='YES' order by `merit_pos_ref`"; // quota_ref = '' and

             //  $sql = "select * from `tbl_result`  where sub_code_7='$sub_code' and quota_ref = '' and is_seven_merit='YES' order by `c_section_hsc_group`,`merit_pos_ref`";

                     // $sql = "select * from `tbl_result`  where sub_code_1='$sub_code' and  is_secnd_meirt='YES' and  quota_ref = '' order by `c_section_hsc_group`,`merit_pos_ref`";

                        //$sql = "select * from `tbl_result`  where sub_code_3='$sub_code' and  quota_ref = '' and  is_third_meirt='YES'";
                       // $sql = "select * from `tbl_result`  where sub_code_3='$sub_code' and  is_third_meirt='YES' and  quota_ref = '' order by `c_section_hsc_group`,`merit_pos_ref`";

                      //   $sql = "select * from `tbl_result`  where sub_code_4='$sub_code' and  quota_ref = '' and  is_fourth_meirt='YES'";

                       //    $sql = "select * from `tbl_result`  where sub_code_4='$sub_code' and  quota_ref = '' and  is_sixth_merit='YES'";


        $query = $this->db->query($sql);
        $rowcount = $query->num_rows();

        if($rowcount > 0){
                                $arr = '';
                                $rec = $query->result();
                                foreach ($rec as $key => $value) {
                                    $system_reg_id = $value->system_reg_id_ref;
                                    $id = $value->id;
                                    $sql2 = "select * from `tbl_pabna_data_final` where  REGISID='".$system_reg_id."'";
                                    $query2 = $this->db->query($sql2);
                                    $rec2 = $query2->row();

                                    $sql3 = "select * from `tbl_merit_list` where  unique_id='".$value->unique_id_ref."'";
                                    $query3 = $this->db->query($sql3);
                                    $rec3 = $query3->row();


                                    $arr[$id]['id'] = $id;
                                    $arr[$id]['unique_id'] = $value->unique_id_ref;
                                    $arr[$id]['tracking_id'] = $value->tracking_id;
                                    $arr[$id]['system_reg_id'] = $system_reg_id;
                                    $arr[$id]['exam_roll'] = $value->exam_roll_ref;
                                    $arr[$id]['app_id'] = $value->app_id_ref;
                                    $arr[$id]['unit'] = $value->unit_ref;
                                    $arr[$id]['c_section_hsc_group'] = $value->c_section_hsc_group;

                                    $arr[$id]['score'] = $rec3->score;
                                    $arr[$id]['merit'] = $rec3->merit;
                                    $arr[$id]['quota_name'] = $rec3->quota_ref;
                                    $arr[$id]['optional'] = $rec3->optional;

                                    $arr[$id]['sname'] = $rec2->SNAME;
                                    $arr[$id]['sfname'] = $rec2->SFNAME;
                                    $arr[$id]['smname'] = $rec2->SMNAME;

                                    $arr[$id]['ssc_board'] = $rec2->SBOARD_NAME;
                                    $arr[$id]['ssc_gpa'] = $rec2->SGPA;
                                    $arr[$id]['ssc_roll'] = $rec2->SROLL_NO;
                                    $arr[$id]['ssc_reg'] = $rec2->SREG_NO;


                                    $arr[$id]['hsc_board'] = $rec2->HBOARD_NAME;
                                    $arr[$id]['hsc_gpa'] = $rec2->HGPA;
                                    $arr[$id]['hsc_roll'] = $rec2->HROLL_NO;
                                    $arr[$id]['hsc_reg'] = $rec2->HREG_NO;

                                    $arr[$id]['img_path'] = $rec2->IMGUPLOAD_PATH;

                                }


                                    $sql4 = "select * from `tbl_subject` where  sub_code='".$sub_code."'";
                                    $query4 = $this->db->query($sql4);
                                    $rec4 = $query4->row();


								$data['res'] = $arr;
                                $data['UNIT'] = $unit;
                                $data['sub_code'] = $sub_code;
                                $data['sub_name'] = $rec4->sub_name;
                                $data['faculty_name'] = $rec4->faculty_name;
                                $data['total_seat'] = $rec4->total_seat;

                               // $data['group_c'] = $group_c;

								 $html = '';
                                 $html = $this->load->view('result_pdf/view_result_pdf', $data, true);

                                    $this->m_pdf->pdf->SetHTMLHeader('
                                        <div style="text-align: right; font-weight: bold;">
                                        </div>');

                                $this->m_pdf->pdf->setFooter("Page {PAGENO} of {nb}");
                                $this->m_pdf->pdf->WriteHTML($html);

								$pdfFilePath = 	'';
								$sub_name_pdf = str_replace( " ", "", $data['sub_name'] );
                                $pdfFilePath = $this->orginal_file_path .'/'.$sub_code."_".$sub_name_pdf."_result_7th.pdf";



				$this->m_pdf->pdf->Output($pdfFilePath, "F");
				echo $pdfFilePath."   OK \r\n";
			}else{
				echo "  No Record \r\n";
			}

	}

function create_2nd_merit__($sub_code,$unit){ //$group_c
	$this->load->library('m_pdf');

	set_time_limit(0);
       // $unit = 'A1';
        $quota_name =''; // 'PH';

			$data = '';
		                   $sql = "select * from `tbl_result`  where sub_code_2='$sub_code' and  quota_ref = '' and  is_secnd_meirt='YES'";
                     // $sql = "select * from `tbl_result`  where sub_code_1='$sub_code' and  is_secnd_meirt='YES' and  quota_ref = '' order by `c_section_hsc_group`,`merit_pos_ref`";

                        //$sql = "select * from `tbl_result`  where sub_code_3='$sub_code' and  quota_ref = '' and  is_third_meirt='YES'";
                       // $sql = "select * from `tbl_result`  where sub_code_3='$sub_code' and  is_third_meirt='YES' and  quota_ref = '' order by `c_section_hsc_group`,`merit_pos_ref`";

                      //   $sql = "select * from `tbl_result`  where sub_code_4='$sub_code' and  quota_ref = '' and  is_fourth_meirt='YES'";

                       //    $sql = "select * from `tbl_result`  where sub_code_4='$sub_code' and  quota_ref = '' and  is_sixth_merit='YES'";
			$query = $this->db->query($sql);
			$rowcount = $query->num_rows();

			if($rowcount > 0){
                                $arr = '';
                                $rec = $query->result();
                                foreach ($rec as $key => $value) {
                                    $system_reg_id = $value->system_reg_id_ref;
                                    $id = $value->id;
                                    $sql2 = "select * from `tbl_pabna_data_final` where  REGISID='".$system_reg_id."'";
                                    $query2 = $this->db->query($sql2);
                                    $rec2 = $query2->row();

                                    $sql3 = "select * from `tbl_merit_list` where  unique_id='".$value->unique_id_ref."'";
                                    $query3 = $this->db->query($sql3);
                                    $rec3 = $query3->row();


                                    $arr[$id]['id'] = $id;
                                    $arr[$id]['unique_id'] = $value->unique_id_ref;
                                    $arr[$id]['tracking_id'] = $value->tracking_id;
                                    $arr[$id]['system_reg_id'] = $system_reg_id;
                                    $arr[$id]['exam_roll'] = $value->exam_roll_ref;
                                    $arr[$id]['app_id'] = $value->app_id_ref;
                                    $arr[$id]['unit'] = $value->unit_ref;
                                    $arr[$id]['c_section_hsc_group'] = $value->c_section_hsc_group;

                                    $arr[$id]['score'] = $rec3->score;
                                    $arr[$id]['merit'] = $rec3->merit;
                                    $arr[$id]['quota_name'] = $rec3->quota_ref;

                                    $arr[$id]['sname'] = $rec2->SNAME;
                                    $arr[$id]['sfname'] = $rec2->SFNAME;
                                    $arr[$id]['smname'] = $rec2->SMNAME;

                                    $arr[$id]['ssc_board'] = $rec2->SBOARD_NAME;
                                    $arr[$id]['ssc_gpa'] = $rec2->SGPA;
                                    $arr[$id]['ssc_roll'] = $rec2->SROLL_NO;
                                    $arr[$id]['ssc_reg'] = $rec2->SREG_NO;


                                    $arr[$id]['hsc_board'] = $rec2->HBOARD_NAME;
                                    $arr[$id]['hsc_gpa'] = $rec2->HGPA;
                                    $arr[$id]['hsc_roll'] = $rec2->HROLL_NO;
                                    $arr[$id]['hsc_reg'] = $rec2->HREG_NO;

                                    $arr[$id]['img_path'] = $rec2->IMGUPLOAD_PATH;

                                }


                                    $sql4 = "select * from `tbl_subject` where  sub_code='".$sub_code."'";
                                    $query4 = $this->db->query($sql4);
                                    $rec4 = $query4->row();


				$data['res'] = $arr;
                                $data['UNIT'] = $unit;
                                $data['sub_code'] = $sub_code;
                                $data['sub_name'] = $rec4->sub_name;
                                $data['faculty_name'] = $rec4->faculty_name;
                                $data['total_seat'] = $rec4->total_seat;

                               // $data['group_c'] = $group_c;

				 $html = '';
                                 $html = $this->load->view('result_pdf/view_result_pdf', $data, true);  // view_seatplan_pdf_OK_OK

                                    $this->m_pdf->pdf->SetHTMLHeader('
                                        <div style="text-align: right; font-weight: bold;">
                                        </div>');
//                                    $this->m_pdf->pdf->SetHTMLFooter('
//                                    <table width="100%">
//                                    <tr> <td width="100%" align="center"><br><br><br></td> </tr>
//                                        <tr>
//                                            <td width="100%" align="center"><hr>Signature of Board Members(Sign above the line with date)</td>
//                                        </tr>
//                                    </table>');

                                $this->m_pdf->pdf->WriteHTML($html);
				$pdfFilePath = 	'';

                                $sub_name_pdf = str_replace(" ","",$data['sub_name']);
                                //$pdfFilePath = $this->orginal_file_path .'/2nd/'.$sub_code."_".$sub_name_pdf."_2nd_merit_result.pdf";
                                $pdfFilePath = $this->orginal_file_path .'/2nd/'.$sub_code."_".$sub_name_pdf."_6th_merit_result.pdf";


				$this->m_pdf->pdf->Output($pdfFilePath, "F");
				echo "   OK \r\n";
			}else{
				echo "  No Record \r\n";
			}

	}

function create_quota_merit($unit,$quota){ //$group_c $sub_code,
	$this->load->library('m_pdf');

	set_time_limit(0);
       // $unit = 'A1';
        $quota_name =''; // 'PH';

			$data = '';
            //$sql = "select * from `tbl_result`  where quota_ref='$quota' and unit_ref='$unit'  order by merit_pos_ref, sub_code_1";//A
			$sql = "select * from `tbl_result_4th_res`  where quota_ref='$quota' and unit_ref='$unit' and `is_fourth_meirt`='YES'  order by merit_pos_ref, sub_code_1,c_section_hsc_group"; //B



			$query = $this->db->query($sql);
			$rowcount = $query->num_rows();

			if($rowcount > 0){
                                $arr = '';
                                $rec = $query->result();
                                foreach ($rec as $key => $value) {
                                    $system_reg_id = $value->system_reg_id_ref;
                                    $id = $value->id;
                                    $sql2 = "select * from `tbl_pabna_data_final` where  REGISID='".$system_reg_id."'";
                                    $query2 = $this->db->query($sql2);
                                    $rec2 = $query2->row();

                                    $sql3 = "select * from `tbl_merit_list` where  unique_id='".$value->unique_id_ref."'";
                                    $query3 = $this->db->query($sql3);
                                    $rec3 = $query3->row();


                                    $arr[$id]['id'] = $id;
                                    $arr[$id]['unique_id'] = $value->unique_id_ref;
                                    $arr[$id]['tracking_id'] = $value->tracking_id;
                                    $arr[$id]['system_reg_id'] = $system_reg_id;
                                    $arr[$id]['exam_roll'] = $value->exam_roll_ref;
                                    $arr[$id]['app_id'] = $value->app_id_ref;
                                    $arr[$id]['unit'] = $value->unit_ref;
                                    $arr[$id]['c_section_hsc_group'] = $value->c_section_hsc_group;

                                    $arr[$id]['score'] = $rec3->score;
                                    $arr[$id]['merit'] = $rec3->merit;
                                    $arr[$id]['quota_name'] = $rec3->quota_ref;
									$arr[$id]['math_boi'] = $rec3->math_boi_ans;

                                    $arr[$id]['sname'] = $rec2->SNAME;
                                    $arr[$id]['sfname'] = $rec2->SFNAME;
                                    $arr[$id]['smname'] = $rec2->SMNAME;

                                    $arr[$id]['ssc_board'] = $rec2->SBOARD_NAME;
                                    $arr[$id]['ssc_gpa'] = $rec2->SGPA;
                                    $arr[$id]['ssc_roll'] = $rec2->SROLL_NO;
                                    $arr[$id]['ssc_reg'] = $rec2->SREG_NO;


                                    $arr[$id]['hsc_board'] = $rec2->HBOARD_NAME;
                                    $arr[$id]['hsc_gpa'] = $rec2->HGPA;
                                    $arr[$id]['hsc_roll'] = $rec2->HROLL_NO;
                                    $arr[$id]['hsc_reg'] = $rec2->HREG_NO;

                                    $arr[$id]['img_path'] = $rec2->IMGUPLOAD_PATH;

                                    $sql4 = "select * from `tbl_subject` where  sub_code='".$value->sub_code_1."'";
                                    $query4 = $this->db->query($sql4);
                                    $rec4 = $query4->row();
                                    $arr[$id]['sub_code'] = $value->sub_code_1;
                                    $arr[$id]['sub_name'] = $rec4->sub_name;
                                    $arr[$id]['faculty_name'] = $rec4->faculty_name;
                                    $arr[$id]['total_seat'] = $rec4->total_seat;
                                }




				$data['res'] = $arr;
                                $data['UNIT'] = $unit;
				$data['QUOTA'] = $quota;



                               // $data['group_c'] = $group_c;

				 $html = '';
                                $html = $this->load->view('result_pdf/view_result_pdf_quota', $data, true);  // view_seatplan_pdf_OK_OK

                                     $this->m_pdf->pdf->setFooter("Page {PAGENO} of {nb}");
                                    $this->m_pdf->pdf->SetHTMLHeader('
                                        <div style="text-align: right; font-weight: bold;">
                                        </div>');


                                $this->m_pdf->pdf->WriteHTML($html);
				$pdfFilePath = 	'';

                                //$sub_name_temp = str_replace(" ", "_", $data['sub_name']);
                                //$pdfFilePath = $this->orginal_file_path .'/C/'.$sub_code."_".$group_c."_".$sub_name_temp."_result.pdf";
                                //$sub_name_pdf = str_replace(" ","",$data['sub_name']);
                                $pdfFilePath = $this->orginal_file_path_q .'/quota/'.$quota."_".$unit."_result_quota_4th.pdf";

				$this->m_pdf->pdf->Output($pdfFilePath, "F");
				echo "   OK \r\n";
			}else{
				echo "  No Record \r\n";
			}

	}
	
function waiting_quota($unit,$quota='') { //$hsc_group $hsc_group // ,$hsc_group

        $this->load->library('m_pdf');

	set_time_limit(0);
       // $unit = 'A1';
           $quota_name =''; // 'PH';

			$data = '';


                         $sql = "select * from `tbl_merit_list`  where unit='$unit' and `quota_name`='$quota' and allocated_subject is null and  `is_prent_in_viva`='YES' and `sub_choice_done`='YES' order by merit ASC"; //A						 
						 
                         //$sql = "select * from `tbl_merit_list`  where unit='$unit' and `quota_name`='' and  allocated_subject is null and  `is_prent_in_viva`='YES' and `sub_choice_done`='YES' order by merit ASC"; // arch
                        // $sql = "select * from `tbl_merit_list`  where unit='$unit' and allocated_subject is null and `quota_name`='' and c_section_hsc_group='$hsc_group' and `is_prent_in_viva`='YES' and `sub_choice_done`='YES' order by merit ASC"; //b

			$query = $this->db->query($sql);
			$rowcount = $query->num_rows();

			if($rowcount > 0){
                                $arr = '';
                                $rec = $query->result();
                                foreach ($rec as $key => $value) {
                                    $system_reg_id = $value->system_reg_id;
                                    $id = $value->id;
                                    $sql2 = "select * from `tbl_pabna_data_final` where  REGISID='".$system_reg_id."'";
                                    $query2 = $this->db->query($sql2);
                                    $rec2 = $query2->row();

                                    $arr[$id]['id'] = $id;
                                    $arr[$id]['unique_id'] = $value->unique_id;
                                    $arr[$id]['tracking_id'] = $value->tracking_id;
                                    $arr[$id]['system_reg_id'] = $system_reg_id;
                                    $arr[$id]['exam_roll'] = $value->exam_roll;
                                    $arr[$id]['app_id'] = $value->app_id;
                                    $arr[$id]['unit'] = $value->unit;
                                    $arr[$id]['c_section_hsc_group'] = $value->c_section_hsc_group;
                                    $arr[$id]['optional'] = $value->optional;

                                    $arr[$id]['score'] = $value->score;
                                    $arr[$id]['merit'] = $value->merit;
                                    $arr[$id]['quota_name'] = $value->quota_ref;
                                    $arr[$id]['optional'] = $value->optional;

                                    $arr[$id]['sname'] = $rec2->SNAME;
                                    $arr[$id]['sfname'] = $rec2->SFNAME;
                                    $arr[$id]['smname'] = $rec2->SMNAME;

                                    $arr[$id]['ssc_board'] = $rec2->SBOARD_NAME;
                                    $arr[$id]['ssc_gpa'] = $rec2->SGPA;
                                    $arr[$id]['ssc_roll'] = $rec2->SROLL_NO;
                                    $arr[$id]['ssc_reg'] = $rec2->SREG_NO;


                                    $arr[$id]['hsc_board'] = $rec2->HBOARD_NAME;
                                    $arr[$id]['hsc_gpa'] = $rec2->HGPA;
                                    $arr[$id]['hsc_roll'] = $rec2->HROLL_NO;
                                    $arr[$id]['hsc_reg'] = $rec2->HREG_NO;

                                    $arr[$id]['img_path'] = $rec2->IMGUPLOAD_PATH;

                                    $sql3 = "select mobile_no from `tbl_login` where  system_regid='".$system_reg_id."'";
                                    $query3 = $this->db->query($sql3);
                                    $rec3 = $query3->row();

                                    $arr[$id]['mobile_no'] = $rec3->mobile_no;
									$arr[$id]['res_my_sub_choice'] = $this->dashboard_model->get_saved_subject_list($value->unique_id,$value->exam_roll,$value->unit);

                                }


				$data['res'] = $arr;
                                $data['UNIT'] = $unit;
								$data['quota'] = $quota;

				 $html = '';
                                $html = $this->load->view('result_pdf/view_waiting_pdf_quota', $data, true);  // view_seatplan_pdf_OK_OK

                                    $this->m_pdf->pdf->SetHTMLHeader('
                                        <div style="text-align: right; font-weight: bold;">
                                        </div>');

                                $this->m_pdf->pdf->setFooter("Page {PAGENO} of {nb}");
                                $this->m_pdf->pdf->WriteHTML($html);


                               $pdfFilePath = $this->orginal_file_path_waiting_quota .'/quota/'.$unit."_waiting_first_quota.pdf";
                              //  $pdfFilePath = $this->orginal_file_path_waiting .'/'.$unit."_".$hsc_group."_waiting_first.pdf";

                               // $sub_name_temp = str_replace(" ", "_", $data['sub_name']);
                              //  $pdfFilePath = $this->orginal_file_path .'/C/'.$sub_code."_".$group_c."_".$sub_name_temp."_result.pdf";

				//$pdfFilePath = $this->orginal_file_path_waiting .'/4th/'.$unit."_".$hsc_group."_waiting.pdf";

                              //  $pdfFilePath = $this->orginal_file_path_waiting .'/'.$unit.'_'.$quota_name.'_'.$position."_viva.pdf";

				$this->m_pdf->pdf->Output($pdfFilePath, "F");
				echo "   OK \r\n";
			}else{
				echo "  No Record \r\n";
			}
        }	

        function waiting($unit,$hsc_group='') { //$hsc_group $hsc_group // ,$hsc_group

        $this->load->library('m_pdf');

	set_time_limit(0);
       // $unit = 'A1';
           $quota_name =''; // 'PH';

			$data = '';


                        // $sql = "select * from `tbl_merit_list`  where unit='$unit' and `quota_name`='' and allocated_subject is null and  `is_prent_in_viva`='YES' and `sub_choice_done`='YES'  and wants_to_admit_frm_waiting='YES' order by merit ASC"; //A						 
                        // $sql = "select * from `tbl_merit_list`  where unit='$unit' and `quota_name`='' and  allocated_subject is null and  `is_prent_in_viva`='YES' and `sub_choice_done`='YES'  and wants_to_admit_frm_waiting='YES' order by merit ASC"; // arch
                         $sql = "select * from `tbl_merit_list`  where unit='$unit' and allocated_subject is null and `quota_name`='' and c_section_hsc_group='$hsc_group' and `is_prent_in_viva`='YES' and `sub_choice_done`='YES' and wants_to_admit_frm_waiting='YES' order by merit ASC"; //b

			$query = $this->db->query($sql);
			$rowcount = $query->num_rows();

			if($rowcount > 0){
                                $arr = '';
                                $rec = $query->result();
                                foreach ($rec as $key => $value) {
                                    $system_reg_id = $value->system_reg_id;
                                    $id = $value->id;
                                    $sql2 = "select * from `tbl_pabna_data_final` where  REGISID='".$system_reg_id."'";
                                    $query2 = $this->db->query($sql2);
                                    $rec2 = $query2->row();

                                    $arr[$id]['id'] = $id;
                                    $arr[$id]['unique_id'] = $value->unique_id;
                                    $arr[$id]['tracking_id'] = $value->tracking_id;
                                    $arr[$id]['system_reg_id'] = $system_reg_id;
                                    $arr[$id]['exam_roll'] = $value->exam_roll;
                                    $arr[$id]['app_id'] = $value->app_id;
                                    $arr[$id]['unit'] = $value->unit;
                                    $arr[$id]['c_section_hsc_group'] = $value->c_section_hsc_group;
                                    $arr[$id]['optional'] = $value->optional;

                                    $arr[$id]['score'] = $value->score;
                                    $arr[$id]['merit'] = $value->merit;
                                    $arr[$id]['quota_name'] = $value->quota_ref;
                                    $arr[$id]['optional'] = $value->optional;

                                    $arr[$id]['sname'] = $rec2->SNAME;
                                    $arr[$id]['sfname'] = $rec2->SFNAME;
                                    $arr[$id]['smname'] = $rec2->SMNAME;

                                    $arr[$id]['ssc_board'] = $rec2->SBOARD_NAME;
                                    $arr[$id]['ssc_gpa'] = $rec2->SGPA;
                                    $arr[$id]['ssc_roll'] = $rec2->SROLL_NO;
                                    $arr[$id]['ssc_reg'] = $rec2->SREG_NO;


                                    $arr[$id]['hsc_board'] = $rec2->HBOARD_NAME;
                                    $arr[$id]['hsc_gpa'] = $rec2->HGPA;
                                    $arr[$id]['hsc_roll'] = $rec2->HROLL_NO;
                                    $arr[$id]['hsc_reg'] = $rec2->HREG_NO;

                                    $arr[$id]['img_path'] = $rec2->IMGUPLOAD_PATH;

                                    $sql3 = "select mobile_no from `tbl_login` where  system_regid='".$system_reg_id."'";
                                    $query3 = $this->db->query($sql3);
                                    $rec3 = $query3->row();

                                    $arr[$id]['mobile_no'] = $rec3->mobile_no;
									$arr[$id]['res_my_sub_choice'] = $this->dashboard_model->get_saved_subject_list($value->unique_id,$value->exam_roll,$value->unit);

                                }


				$data['res'] = $arr;
                                $data['UNIT'] = $unit;

				 $html = '';
                                $html = $this->load->view('result_pdf/view_waiting_pdf', $data, true);  // view_seatplan_pdf_OK_OK

                                    $this->m_pdf->pdf->SetHTMLHeader('
                                        <div style="text-align: right; font-weight: bold;">
                                        </div>');

                                $this->m_pdf->pdf->setFooter("Page {PAGENO} of {nb}");
                                $this->m_pdf->pdf->WriteHTML($html);


                                //$pdfFilePath = $this->orginal_file_path_waiting .'/'.$unit."_waiting_5th.pdf";
                                $pdfFilePath = $this->orginal_file_path_waiting .'/'.$unit."_".$hsc_group."_waiting_5th.pdf";

                               // $sub_name_temp = str_replace(" ", "_", $data['sub_name']);
                              //  $pdfFilePath = $this->orginal_file_path .'/C/'.$sub_code."_".$group_c."_".$sub_name_temp."_result.pdf";

				//$pdfFilePath = $this->orginal_file_path_waiting .'/4th/'.$unit."_".$hsc_group."_waiting.pdf";

                              //  $pdfFilePath = $this->orginal_file_path_waiting .'/'.$unit.'_'.$quota_name.'_'.$position."_viva.pdf";

				$this->m_pdf->pdf->Output($pdfFilePath, "F");
				echo "   OK \r\n";
			}else{
				echo "  No Record \r\n";
			}
        }


        function final_pdf($sub_code,$unit) {
            $this->load->library('m_pdf');

          	set_time_limit(0);

			$data = '';

                         $sql = "select * from `tbl_result`  where sub_code_7='$sub_code' and  paid_status='P' and class_roll is not NULL  order by `class_roll`";
                       // $sql = "select * from `tbl_result`  where sub_code_7='$sub_code' and   	paid_status='P' and cancel_register_approv_status is NULL  order by merit_pos_ref,quota_ref";// merit_pos_ref,quota_ref
 
			$query = $this->db->query($sql);
			$rowcount = $query->num_rows();

			if($rowcount > 0){
                                $arr = '';
                                $rec = $query->result();
                                foreach ($rec as $key => $value) {
                                    $system_reg_id = $value->system_reg_id_ref;
                                    $id = $value->id;
                                    $sql2 = "select * from `tbl_pabna_data_final` where  REGISID='".$system_reg_id."'";
                                    $query2 = $this->db->query($sql2);
                                    $rec2 = $query2->row();

                                    $sql3 = "select * from `tbl_merit_list` where  unique_id='".$value->unique_id_ref."'";
                                    $query3 = $this->db->query($sql3);
                                    $rec3 = $query3->row();


                                    $arr[$id]['id'] = $id;
                                    $arr[$id]['unique_id'] = $value->unique_id_ref;
                                    $arr[$id]['class_roll'] = $value->class_roll;
                                    $arr[$id]['class_reg'] = $value->class_reg;
                                    $arr[$id]['tracking_id'] = $value->tracking_id;
                                    $arr[$id]['system_reg_id'] = $system_reg_id;
                                    $arr[$id]['exam_roll'] = $value->exam_roll_ref;
                                    $arr[$id]['app_id'] = $value->app_id_ref;
                                    $arr[$id]['unit'] = $value->unit_ref;
                                    $arr[$id]['c_section_hsc_group'] = $value->c_section_hsc_group;

                                    $arr[$id]['score'] = $rec3->score;
                                    $arr[$id]['merit'] = $rec3->merit;
                                    $arr[$id]['quota_name'] = $value->quota_ref;
                                    $arr[$id]['optional'] = $rec3->optional;

                                    $arr[$id]['sname'] = $rec2->SNAME;
                                    $arr[$id]['sfname'] = $rec2->SFNAME;
                                    $arr[$id]['smname'] = $rec2->SMNAME;

                                    $arr[$id]['ssc_board'] = $rec2->SBOARD_NAME;
                                    $arr[$id]['ssc_gpa'] = $rec2->SGPA;
                                    $arr[$id]['ssc_roll'] = $rec2->SROLL_NO;
                                    $arr[$id]['ssc_reg'] = $rec2->SREG_NO;


                                    $arr[$id]['hsc_board'] = $rec2->HBOARD_NAME;
                                    $arr[$id]['hsc_gpa'] = $rec2->HGPA;
                                    $arr[$id]['hsc_roll'] = $rec2->HROLL_NO;
                                    $arr[$id]['hsc_reg'] = $rec2->HREG_NO;

                                    $arr[$id]['img_path'] = $rec2->IMGUPLOAD_PATH;

                                }


                                    $sql4 = "select * from `tbl_subject` where  sub_code='".$sub_code."'";
                                    $query4 = $this->db->query($sql4);
                                    $rec4 = $query4->row();


				$data['res'] = $arr;
                                $data['UNIT'] = $unit;
                                $data['sub_code'] = $sub_code;
                                $data['sub_name'] = $rec4->sub_name;
                                $data['faculty_name'] = $rec4->faculty_name;
                                $data['total_seat'] = $rec4->total_seat;

                               // $data['group_c'] = $group_c;

				 $html = '';
                                 $html = $this->load->view('result_pdf/view_final_pdf', $data, true);  // view_seatplan_pdf_OK_OK

                                    $this->m_pdf->pdf->SetHTMLHeader('
                                        <div style="text-align: right; font-weight: bold;">
                                        </div>');
//                                    $this->m_pdf->pdf->SetHTMLFooter('
//                                    <table width="100%">
//                                    <tr> <td width="100%" align="center"><br><br><br></td> </tr>
//                                        <tr>
//                                            <td width="100%" align="center"><hr>Signature of Board Members(Sign above the line with date)</td>
//                                        </tr>
//                                    </table>');

                                $this->m_pdf->pdf->WriteHTML($html);
				$pdfFilePath = 	'';

                                $sub_name_pdf = str_replace(" ","",$data['sub_name']);
                               // $pdfFilePath = $this->orginal_file_path .'/final_pdf_for_uni/A/'.$sub_code."_".$sub_name_pdf."final.pdf";
                                $pdfFilePath = $this->orginal_file_path .'/final_pdf_for_uni/B/'.$sub_code."_".$sub_name_pdf."final.pdf";


				$this->m_pdf->pdf->Output($pdfFilePath, "F");
				echo "   OK \r\n";
			}else{
				echo "  No Record \r\n";
			}

        }
        function registation_pdf($sub_code) {

          	set_time_limit(0);



       // $sql = "select * from `tbl_result`  where sub_code_10='$sub_code' and   paid_status='P' and cancel_register_approv_status is NULL and class_roll is not null   order by `class_roll`"; // merit_pos_ref,quota_ref
		$sql = "select * from `tbl_result`  where sub_code_7='$sub_code' and   paid_status='P' and  class_roll is not null   order by `class_roll`"; // merit_pos_ref,quota_ref
        $query = $this->db->query($sql);
        $rowcount = $query->num_rows();

        if ($rowcount > 0) {
            $arr = '';
            $rec = $query->result_array();
            $INDEX = 0;
            foreach ($rec as $key => $value) {
                $unique_id =  $value['unique_id_ref'];
                $exam_roll = $value['exam_roll_ref'];
                $unit_name =  $value['unit_ref'];
                $student_login_regisid = $value['system_reg_id_ref'];

                $student_acadamic_data = $this->dashboard_model->get_student_all__details($student_login_regisid);

            //    $res_sub_choice = $this->dashboard_model->get_saved_subject_list($unique_id, $exam_roll, $unit_name);

                $this->data['MAIN_DATA'][$INDEX]['unit_details'] = $this->dashboard_model->get_eligible_unit_details($unique_id, $exam_roll, $unit_name, $student_login_regisid);

                $this->data['MAIN_DATA'][$INDEX]['student_login_photo_path'] = $this->session->userdata('student_login_photo_path');
                $this->data['MAIN_DATA'][$INDEX]['student_acadamic_data'] = $student_acadamic_data;
             //   $this->data['MAIN_DATA'][$INDEX]['unit_wise_subject_list_existing'] = $res_sub_choice;

                $my_result = $this->dashboard_model->get_result_subject_details($unique_id);
                $this->data['MAIN_DATA'][$INDEX]['my_result'] = $my_result;

                $this->db->where('system_regid', $student_login_regisid);
                $query = $this->db->get('tbl_login');


                $std_per_data = $query->row_array();


                $this->data['MAIN_DATA'][$INDEX]['std_per_data'] = $std_per_data;
                $INDEX++;
            }
                   }
                 //  print_r( $this->data['MAIN_DATA']);
                  // exit;
           $html = $this->load->view('dashboard/view_registration_pdf_uni_copy', $this->data,true);

        $this->load->library('m_pdf');

        $this->m_pdf->pdf->SetHTMLHeader('
                                        <div style="text-align: right; font-weight: bold;">
                                        </div>');
         //this the the PDF filename that user will get to download
                $pdfFileNAME = "PUST_reg_".$sub_code . ".pdf";

                //load mPDF library
                $pdfFilePath = $this->orginal_file_path .'/final_reg_pdf_for_uni/'.$pdfFileNAME;

                //generate the PDF from the given html
                $this->m_pdf->pdf->WriteHTML($html);

                //download it.
                $this->m_pdf->pdf->Output($pdfFilePath, "F");

                echo "DONE";
        }

   function registation_pdf_student($sub_code) {

          	set_time_limit(0);



        //$sql = "select * from `tbl_result`  where sub_code_10='$sub_code' and   paid_status='P' and cancel_register_approv_status is NULL and class_roll is not null  order by `class_roll`"; // merit_pos_ref,quota_ref
		$sql = "select * from `tbl_result`  where sub_code_7='$sub_code' and   paid_status='P' and class_roll is not null  order by `class_roll`"; // merit_pos_ref,quota_ref
        $query = $this->db->query($sql);
        $rowcount = $query->num_rows();

        if ($rowcount > 0) {
            $arr = '';
            $rec = $query->result_array();
            $INDEX = 0;
            foreach ($rec as $key => $value) {
                $unique_id =  $value['unique_id_ref'];
                $exam_roll = $value['exam_roll_ref'];
                $unit_name =  $value['unit_ref'];
                $student_login_regisid = $value['system_reg_id_ref'];

                $student_acadamic_data = $this->dashboard_model->get_student_all__details($student_login_regisid);

            //    $res_sub_choice = $this->dashboard_model->get_saved_subject_list($unique_id, $exam_roll, $unit_name);

                $this->data['MAIN_DATA'][$INDEX]['unit_details'] = $this->dashboard_model->get_eligible_unit_details($unique_id, $exam_roll, $unit_name, $student_login_regisid);

                $this->data['MAIN_DATA'][$INDEX]['student_login_photo_path'] = $this->session->userdata('student_login_photo_path');
                $this->data['MAIN_DATA'][$INDEX]['student_acadamic_data'] = $student_acadamic_data;
             //   $this->data['MAIN_DATA'][$INDEX]['unit_wise_subject_list_existing'] = $res_sub_choice;

                $my_result = $this->dashboard_model->get_result_subject_details($unique_id);
                $this->data['MAIN_DATA'][$INDEX]['my_result'] = $my_result;

                $this->db->where('system_regid', $student_login_regisid);
                $query = $this->db->get('tbl_login');


                $std_per_data = $query->row_array();


                $this->data['MAIN_DATA'][$INDEX]['std_per_data'] = $std_per_data;
                $INDEX++;
            }
                   }
                 //  print_r( $this->data['MAIN_DATA']);
                //   exit;
        $html = $this->load->view('dashboard/view_registration_pdf_student_copy', $this->data,true);

        $this->load->library('m_pdf');

        $this->m_pdf->pdf->SetHTMLHeader('
                                        <div style="text-align: right; font-weight: bold;">
                                        </div>');
         //this the the PDF filename that user will get to download
                $pdfFileNAME = "PUST_reg_std_".$sub_code . ".pdf";

                //load mPDF library

                 $pdfFilePath = $this->orginal_file_path .'/final_reg_pdf_for_student/'.$pdfFileNAME;

                //generate the PDF from the given html
                $this->m_pdf->pdf->WriteHTML($html);

                //download it.
                $this->m_pdf->pdf->Output($pdfFilePath, "F");
                 echo "DONE";
        }


 function id_card_pdf($sub_code,$gender) {

          	set_time_limit(0);



       // $sql = "select * `tbl_result` from `tbl_result`,tbl_login  where sub_code_6='$sub_code' and   	paid_status='P' and cancel_register_approv_status is NULL  order by `class_roll` limit 6"; // merit_pos_ref,quota_ref
        $sql = "select `tbl_result`.*,tbl_login.gender from tbl_result, tbl_login where tbl_login.system_regid =tbl_result.system_reg_id_ref and sub_code_7='$sub_code' and tbl_login.gender='$gender' and paid_status='P' and  class_roll is not null and cancel_register_approv_status is NULL order by `class_roll`";
//echo $sql;exit;
        //$sql = "select `tbl_result`.*,tbl_login.gender from tbl_result, tbl_login where tbl_login.system_regid =tbl_result.system_reg_id_ref and tbl_login.gender is NULL and paid_status='P' and cancel_register_approv_status is NULL order by `class_roll`";
        $query = $this->db->query($sql);
        $rowcount = $query->num_rows();

        if ($rowcount > 0) {
            $arr = '';
            $rec = $query->result_array();
            $INDEX = 0;
            foreach ($rec as $key => $value) {
                $unique_id =  $value['unique_id_ref'];
                $exam_roll = $value['exam_roll_ref'];
                $unit_name =  $value['unit_ref'];
                $student_login_regisid = $value['system_reg_id_ref'];

                $student_acadamic_data = $this->dashboard_model->get_student_all__details($student_login_regisid);

            //    $res_sub_choice = $this->dashboard_model->get_saved_subject_list($unique_id, $exam_roll, $unit_name);

                $this->data['MAIN_DATA'][$INDEX]['unit_details'] = $this->dashboard_model->get_eligible_unit_details($unique_id, $exam_roll, $unit_name, $student_login_regisid);

                $this->data['MAIN_DATA'][$INDEX]['student_login_photo_path'] = $this->session->userdata('student_login_photo_path');
                $this->data['MAIN_DATA'][$INDEX]['student_acadamic_data'] = $student_acadamic_data;
             //   $this->data['MAIN_DATA'][$INDEX]['unit_wise_subject_list_existing'] = $res_sub_choice;

                $my_result = $this->dashboard_model->get_result_subject_details($unique_id);
                $this->data['MAIN_DATA'][$INDEX]['my_result'] = $my_result;

                $this->db->where('system_regid', $student_login_regisid);
                $query = $this->db->get('tbl_login');


                $std_per_data = $query->row_array();


                $this->data['MAIN_DATA'][$INDEX]['std_per_data'] = $std_per_data;
                $INDEX++;
            }
                   }
                 //  print_r( $this->data['MAIN_DATA']);
                //   exit;
        $html = $this->load->view('dashboard/view_id_cards', $this->data,true);

        $this->load->library('m_pdf');

        $this->m_pdf->pdf->SetHTMLHeader('
                                        <div style="text-align: right; font-weight: bold;">
                                        </div>');
         //this the the PDF filename that user will get to download
                $pdfFileNAME = "PUST_id_card_".$sub_code .'_'.$gender.".pdf";

               // $pdfFileNAME = "PUST_id_card_others.pdf";

                 $pdfFilePath = $this->orginal_file_path .'/id_card/'.$pdfFileNAME;

                //load mPDF library


                //generate the PDF from the given html
                $this->m_pdf->pdf->WriteHTML($html);

                //download it.
                $this->m_pdf->pdf->Output($pdfFilePath, "F");
                 echo "DONE";
        }
}
