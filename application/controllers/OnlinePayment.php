<?php
defined('BASEPATH') or exit('No direct script access allowed');

class OnlinePayment extends CI_Controller
{
    private  $data;
    protected $student_login_regisid;
    public function __construct()
    {
        parent::__construct();
        // log_message('info', 'OnlinePaymentController Class Initialized');
        // $this->load->model('dashboard_model');
        // $this->load->model('home_model');
        // $this->load->helper('common_helper');
        // $this->load->library('form_validation');
        $this->student_login_regisid = $this->session->userdata('student_login_regisid');

        log_message('info', 'Payment construct  Response Data :: ' . json_encode($_POST));

        //  if (!$this->session->userdata('student_logged_in')) {
        //    redirect('login');
        // }
    }

    public function index()
    {
        $this->load->view('OnlinePayment/home');
    }

    public function hosted_view()
    {
        $this->load->view('OnlinePayment/hostedcheckout');
    }
    public function easycheckout_view()
    {
        $this->load->view('OnlinePayment/easycheckout');
    }

    public function request_api_hosted()
    {
        if (!$this->session->userdata('student_logged_in')) {
            redirect('login');
        }
        $this->session->unset_userdata('tarndata');
        $query = $this->db
            ->select('
            tbl_pabna_data_final.appid,
            tbl_pabna_data_final.sname,
            tbl_pabna_data_final.mobnumber,
            tbl_application_pust.amount_to_pay,
            tbl_application_pust.id as tap_id,
            ')
            ->where('tbl_pabna_data_final.regisid', $this->student_login_regisid)
            ->join('tbl_application_pust', 'tbl_application_pust.regisid=tbl_pabna_data_final.regisid')
            ->get('tbl_pabna_data_final');
        if ($this->input->get_post('placeorder') && $query->num_rows() == 1) {
            $candidate = $query->row();
            // dd($candidate);
            $post_data = array();
            $post_data['total_amount'] = number_format((float)$candidate->amount_to_pay, 2, '.', '');
            $post_data['currency'] = "BDT";
            $post_data['tran_id'] = $candidate->appid;
            $post_data['success_url'] = base_url() . "success";
            $post_data['fail_url'] = base_url() . "fail";
            $post_data['cancel_url'] = base_url() . "cancel";
            $post_data['ipn_url'] = base_url() . "ipn";
            # $post_data['multi_card_name'] = "mastercard,visacard,amexcard";  # DISABLE TO DISPLAY ALL AVAILABLE

            # EMI INFO
            // $post_data['emi_option'] = "1";
            // $post_data['emi_max_inst_option'] = "9";
            // $post_data['emi_selected_inst'] = "9";

            # CUSTOMER INFORMATION
            $post_data['cus_name'] = $candidate->sname;
            $post_data['cus_email'] = 'admin@rub.ac.bd';
            $post_data['cus_add1'] = 'Shahjadpur Upozila, Rabindra University, Bangladesh';
            $post_data['cus_city'] = 'Shahjadpur';
            $post_data['cus_state'] = 'Sirajganj';
            $post_data['cus_postcode'] = '6700-6760';
            $post_data['cus_country'] = 'Bangladesh';
            $post_data['cus_phone'] = $candidate->mobnumber;

            # SHIPMENT INFORMATION
            // $post_data['ship_name'] = $this->input->post('fname') . " " . $this->input->post('fname');
            // $post_data['ship_add1'] = $this->input->post('add1');
            // $post_data['ship_city'] = $this->input->post('state');
            // $post_data['ship_state'] = $this->input->post('state');
            // $post_data['ship_postcode'] = $this->input->post('postcode');
            // $post_data['ship_country'] = $this->input->post('country');

            # OPTIONAL PARAMETERS
            // $post_data['value_a'] = "ref001";
            // $post_data['value_b'] = "ref002";
            // $post_data['value_c'] = "ref003";
            // $post_data['value_d'] = "ref004";

            $post_data['product_profile'] = "non-physical-goods";
            $post_data['shipping_method'] = "No";
            $post_data['num_of_item'] = "1";
            $post_data['product_name'] = "Admission";
            $post_data['product_category'] = "Education";
            // dd($post_data);

            // log_message('info', 'Payment Request Data ::' . json_encode($post_data));
            $payment_request_info = json_encode($post_data);
            $applicant_id = $post_data['tran_id'];
            file_put_contents("payment_log.txt", "Date: " . date('Y-m-d H:i:s') . " Transaction ID/Applicant ID: " . $applicant_id . "  Payment Request info : " . $payment_request_info  . "\n", FILE_APPEND);
            // $this->load->library('session');


            $session = array(
                'tran_id' => $post_data['tran_id'],
                'amount' => $post_data['total_amount'],
                'currency' => $post_data['currency']
            );
            $this->session->set_userdata('tarndata', $session);

            // echo "<pre>";
            // print_r($post_data);
            if ($this->sslcommerz->RequestToSSLC($post_data, SSLCZ_STORE_ID, SSLCZ_STORE_PASSWD)) {
                // echo "Pending";
                /***************************************
				# Change your database status to Pending.
                 ****************************************/
                //  $this->db->where('appid', $post_data['tran_id'])->update('tbl_application_pust', ['pmtstatus' => 'U']);
                // dd($aa);
                // die();
            }
        }
    }

    public function easycheckout_endpoint()
    {


        $tran_id = $_REQUEST['order'];
        $jsondata = json_decode($_REQUEST['cart_json'], true);

        $post_data = array();
        $post_data['total_amount'] = $jsondata['amount'];
        $post_data['currency'] = "BDT";
        $post_data['tran_id'] = $tran_id;
        $post_data['success_url'] = base_url() . "success";
        $post_data['fail_url'] = base_url() . "fail";
        $post_data['cancel_url'] = base_url() . "cancel";
        $post_data['ipn_url'] = base_url() . "ipn";
        # $post_data['multi_card_name'] = "mastercard,visacard,amexcard";  # DISABLE TO DISPLAY ALL AVAILABLE

        # EMI INFO
        // $post_data['emi_option'] = "1";
        // $post_data['emi_max_inst_option'] = "9";
        // $post_data['emi_selected_inst'] = "9";

        # CUSTOMER INFORMATION
        $post_data['cus_name'] = $jsondata['cus_name'];
        $post_data['cus_email'] = $jsondata['email'];
        $post_data['cus_add1'] = $jsondata['address'];
        $post_data['cus_city'] = $jsondata['state'];
        $post_data['cus_state'] = $jsondata['state'];
        $post_data['cus_postcode'] = $jsondata['amount'];
        $post_data['cus_country'] = $jsondata['zip'];
        $post_data['cus_phone'] = $jsondata['phone'];

        # SHIPMENT INFORMATION
        $post_data['ship_name'] = $jsondata['cus_name'];
        $post_data['ship_add1'] = $jsondata['address'];
        $post_data['ship_city'] = $jsondata['state'];
        $post_data['ship_state'] = $jsondata['state'];
        $post_data['ship_postcode'] = $jsondata['zip'];
        $post_data['ship_country'] = $jsondata['country'];

        # OPTIONAL PARAMETERS
        $post_data['value_a'] = "ref001";
        $post_data['value_b'] = "ref002";
        $post_data['value_c'] = "ref003";
        $post_data['value_d'] = "ref004";

        $post_data['product_profile'] = "physical-goods";
        $post_data['shipping_method'] = "YES";
        $post_data['num_of_item'] = "3";
        $post_data['product_name'] = "Computer,Speaker";
        $post_data['product_category'] = "Ecommerce";
        // dd($post_data);
        // die('ok');
        $this->load->library('session');

        $session = array(
            'tran_id' => $post_data['tran_id'],
            'amount' => $post_data['total_amount'],
            'currency' => $post_data['currency']
        );
        $this->session->set_userdata('tarndata', $session);

        // echo "<pre>";
        // print_r($post_data);
        if ($this->sslcommerz->EasyCheckout($post_data, SSLCZ_STORE_ID, SSLCZ_STORE_PASSWD)) {
            echo "Pending";
            /***************************************
			# Change your database status to Pending.
             ****************************************/
        }
    }

    public function success_payment()
    {
        // $this->ipn_listener();
        // log_message('info', 'Payment Response Data :: ' . json_encode($_POST));
        $payment_response_info = json_encode($_POST);
        $applicant_id = $_POST['tran_id'];
        file_put_contents("payment_log.txt", "Date: " . date('Y-m-d H:i:s') . " Transaction ID/Applicant ID: " . $applicant_id . "  Payment Success Response info : " . $payment_response_info  . "\n", FILE_APPEND);

        if (!isset($_POST['tran_id'])) {
            redirect('dashboard');
        }

        $payment_log_data = [
            'appid' => $applicant_id,
            'paymentid' => $_POST['val_id'],
            'amount' => $_POST['amount'],
            'paymentdate' => date('Y-m-d H:i:s'),
            'payment_status' => $_POST['status'],
            'txndate' =>  $_POST['tran_date'],
            'billerid' =>  'success_payment',
        ];
        $this->db->insert('tbl_payment_pust', $payment_log_data);
        // $this->data['payment_message'] = '';
        $query = $this->db->select('pmtstatus,unit,rollno')->where('appid', $_POST['tran_id'])->get('tbl_application_pust');
        $database_order_status = '';
        $rollno = '';
        if ($query->num_rows() == 1) {
            $application = $query->row();
            $database_order_status = $application->pmtstatus;
            $rollno = $application->rollno;
        }
        // dd($database_order_status);
        $sesdata = $this->session->userdata('tarndata');
        // dd($sesdata, $_POST);

        // if (($sesdata['tran_id'] == $_POST['tran_id']) && ($sesdata['amount'] == $_POST['currency_amount']) && ($sesdata['currency'] == $_POST['currency'])) {
        // echo 2;
        // if ($this->sslcommerz->ValidateResponse($_POST, $_POST['currency_amount'], $sesdata['currency'])) {
        if ($this->sslcommerz->ValidateResponse($_POST, $_POST['currency_amount'], $_POST['currency'])) {
            // echo 3;
            if ($database_order_status != 'P') {
                // echo 4;
                // echo 'update process';
                /*****************************************************************************/
                # Change your database status to Processing & You can redirect to success page from here
                $update_payment = [
                    'pmtdate' => $_POST['tran_date'],
                    'pmtstatus' => 'P',
                    'pmtamount' => $_POST['amount'],
                    'txnid' => $_POST['val_id'],
                    'bank_tran_id' => $_POST['bank_tran_id'],
                    'rollno' => $rollno ? $rollno : $this->generate_roll_no($application->unit),
                ];
                $this->db->where('appid', $_POST['tran_id'])->update('tbl_application_pust', $update_payment);

                /******************************************************************************/

                // echo "Transaction Successful<br>";
                // echo "Processing";
                // echo "<pre>";
                // print_r($this->generate_roll_no($application->unit));
                // exit;
            } else {
                // echo 5;
                /******************************************************************
					# Just redirect to your success page status already changed by IPN.
                 ******************************************************************/
                // echo "Just redirect to your success page";
            }
            redirect('dashboard/success');
        } else {
            // echo 6;
            show_404();
        }
        // echo 7;
        //}
        // echo 8;
    }
    public function fail_payment()
    {
        $payment_response_info = json_encode($_POST);
        $applicant_id = $_POST['tran_id'];
        file_put_contents("payment_log.txt", "Date: " . date('Y-m-d H:i:s') . " Transaction ID/Applicant ID: " . $applicant_id . "  Payment Failed Response info : " . $payment_response_info  . "\n", FILE_APPEND);
        $payment_log_data = [
            'appid' => $applicant_id,
            'paymentid' => $_POST['val_id'],
            'amount' => $_POST['amount'],
            'paymentdate' => date('Y-m-d H:i:s'),
            'payment_status' => $_POST['status'],
            'txndate' =>  $_POST['tran_date'],
            'billerid' =>  'fail_payment',
        ];
        $this->db->insert('tbl_payment_pust', $payment_log_data);
        redirect('dashboard/failed');
        // $database_order_status = 'Pending'; // Check this from your database here Pending is dummy data,
        // if ($database_order_status == 'Pending') {
        //     /*****************************************************************************
        // 	# Change your database status to FAILED & You can redirect to failed page from here
        //      ******************************************************************************/
        //     echo "<pre>";
        //     print_r($_POST);
        //     echo "Transaction Faild";
        // } else {
        //     /******************************************************************
        // 	# Just redirect to your success page status already changed by IPN.
        //      ******************************************************************/
        //     echo "Just redirect to your failed page";
        // }
    }
    public function cancel_payment()
    {
        $payment_response_info = json_encode($_POST);
        $applicant_id = $_POST['tran_id'];
        file_put_contents("payment_log.txt", "Date: " . date('Y-m-d H:i:s') . " Transaction ID/Applicant ID: " . $applicant_id  . "  Payment Cancel Response info : " . $payment_response_info . "\n", FILE_APPEND);
        $payment_log_data = [
            'appid' => $applicant_id,
            'paymentid' => $_POST['val_id'],
            'amount' => $_POST['amount'],
            'paymentdate' => date('Y-m-d H:i:s'),
            'payment_status' => $_POST['status'],
            'txndate' =>  $_POST['tran_date'],
            'billerid' =>  'cancel_payment',
        ];
        $this->db->insert('tbl_payment_pust', $payment_log_data);
        redirect('dashboard');
        // $database_order_status = 'Pending'; // Check this from your database here Pending is dummy data,
        // if ($database_order_status == 'Pending') {
        //     /*****************************************************************************
        // 	# Change your database status to CANCELLED & You can redirect to cancelled page from here
        //      ******************************************************************************/
        //     echo "<pre>";
        //     print_r($_POST);
        //     echo "Transaction Canceled";
        // } else {
        //     /******************************************************************
        // 	# Just redirect to your cancelled page status already changed by IPN.
        //      ******************************************************************/
        //     echo "Just redirect to your failed page";
        // }
    }
    public function ipn_listener()
    {
        $payment_response_info = json_encode($_POST);
        $applicant_id = $_POST['tran_id'];

        file_put_contents("payment_log.txt", "Date: " . date('Y-m-d H:i:s') . " Transaction ID/Applicant ID: " . $applicant_id . " IPN STATUS " . $_POST['status'] . "  Payment IPN Listener Response info : " . $payment_response_info  . "\n", FILE_APPEND);

        if (!isset($_POST['tran_id'])) {
            redirect('dashboard');
        }


        $payment_log_data = [
            'appid' => $applicant_id,
            'paymentid' => $_POST['val_id'],
            'amount' => $_POST['amount'],
            'paymentdate' => date('Y-m-d H:i:s'),
            'payment_status' => $_POST['status'],
            'txndate' =>  $_POST['tran_date'],
            'billerid' =>  'ipn_listener',
        ];
        $this->db->insert('tbl_payment_pust', $payment_log_data);
        // die('OnlinePayment@ipn_listener called');
        // $database_order_status = 'Pending'; // Check this from your database here Pending is dummy data,
        $query = $this->db->select('pmtstatus,unit,rollno')->where('appid', $_POST['tran_id'])->get('tbl_application_pust');
        $database_order_status = '';
        $rollno = '';
        if ($query->num_rows() == 1) {
            $application = $query->row();
            $database_order_status = $application->pmtstatus;
            $rollno = $application->rollno;
        }
        $store_passwd = SSLCZ_STORE_PASSWD;
        if ($ipn = $this->sslcommerz->ipn_request($store_passwd, $_POST)) {
            if (($ipn['gateway_return']['status'] == 'VALIDATED' || $ipn['gateway_return']['status'] == 'VALID') && $ipn['ipn_result']['hash_validation_status'] == 'SUCCESS') {
                if ($database_order_status != 'P') {
                    // echo $ipn['gateway_return']['status'] . "<br>";
                    // echo $ipn['ipn_result']['hash_validation_status'] . "<br>";
                    /*****************************************************************************
                	# Check your database order status, if status = 'Pending' then chang status to 'Processing'.
                     ******************************************************************************/
                    $update_payment = [
                        'pmtdate' => $_POST['tran_date'],
                        'pmtstatus' => 'P',
                        'pmtamount' => $_POST['amount'],
                        'txnid' => $_POST['val_id'],
                        'bank_tran_id' => $_POST['bank_tran_id'],
                        'rollno' => $rollno ? $rollno : $this->generate_roll_no($application->unit),
                    ];
                    $this->db->where('appid', $_POST['tran_id'])->update('tbl_application_pust', $update_payment);
                }
            } elseif ($ipn['gateway_return']['status'] == 'FAILED' && $ipn['ipn_result']['hash_validation_status'] == 'SUCCESS') {
                if ($database_order_status != 'P') {
                    // echo $ipn['gateway_return']['status'] . "<br>";
                    // echo $ipn['ipn_result']['hash_validation_status'] . "<br>";
                    /*****************************************************************************
                	# Check your database order status, if status = 'Pending' then chang status to 'FAILED'.
                     ******************************************************************************/
                    $update_payment = ['pmtstatus' => 'U'];
                    $this->db->where('appid', $_POST['tran_id'])->update('tbl_application_pust', $update_payment);
                }
            } elseif ($ipn['gateway_return']['status'] == 'CANCELLED' && $ipn['ipn_result']['hash_validation_status'] == 'SUCCESS') {
                if ($database_order_status != 'P') {
                    // echo $ipn['gateway_return']['status'] . "<br>";
                    // echo $ipn['ipn_result']['hash_validation_status'] . "<br>";
                    /*****************************************************************************
                	# Check your database order status, if status = 'Pending' then chang status to 'CANCELLED'.
                     ******************************************************************************/
                    $update_payment = ['pmtstatus' => 'U'];
                    $this->db->where('appid', $_POST['tran_id'])->update('tbl_application_pust', $update_payment);
                }
            } else {
                if ($database_order_status != 'P') {
                    echo "Order status not " . $ipn['gateway_return']['status'];
                    /*****************************************************************************
                	# Check your database order status, if status = 'Pending' then chang status to 'FAILED'.
                     ******************************************************************************/
                    $update_payment = ['pmtstatus' => 'U'];
                    $this->db->where('appid', $_POST['tran_id'])->update('tbl_application_pust', $update_payment);
                }
            }
            // echo "<pre>";
            // print_r($ipn);
        }
    }
    public function generate_roll_no($unit = null)
    {
        $ru_roll = 0;
        if (!empty($unit)) {
            $row = $this->db->query("SELECT MAX(rollno) as rollno FROM tbl_application_pust where unit ='{$unit}'")->row();
            // $row = $this->db->select('id,rollno,unit')->order_by('id', 'desc')->where('rollno !=', '')->where('unit', $unit)->limit(1)->get('tbl_application_pust')->row();
            if ($unit == 'A') {
                if (!empty($row) && isset($row->rollno) && !empty($row->rollno)) {
                    $ru_roll = $row->rollno + 1;
                } else {
                    $ru_roll = 10001;
                }
                // dd('A', $ru_roll, $row);
            } elseif ($unit == 'B') {
                if (!empty($row) && isset($row->rollno) && !empty($row->rollno)) {
                    $ru_roll = $row->rollno + 1;
                } else {
                    $ru_roll = 30001;
                }
                // dd('B', $ru_roll, $row);
            } elseif ($unit == 'C') {
                if (!empty($row) && isset($row->rollno) && !empty($row->rollno)) {
                    $ru_roll = $row->rollno + 1;
                } else {
                    $ru_roll = 50001;
                }
                // dd('C', $ru_roll, $row);
            }
        }
        return $ru_roll;
    }
}
