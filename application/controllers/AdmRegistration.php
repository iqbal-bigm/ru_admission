<?php
defined('BASEPATH') or exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Dashboard
 *
 * @author Mhabub
 */
class AdmRegistration extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        redirect('dashboard');
        // $this->load->model('dashboard_model');
        $this->load->helper('common_helper');
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');

        if ($this->session->userdata('student_logged_in')) {
            redirect('dashboard');
        }

        //            if(! $this->session->userdata('isTESTLogin') ){
        //                   redirect('testauth');
        //                }
    }
    public function index()
    {


        $this->data['page_title'] = 'Registration Form';
        $this->data['reg_status'] = false;
        $this->data['is_pass'] = false;

        $this->data['admission_id'] = $this->input->post_get('admission_id');
        $this->data['mobile_no'] = $this->input->post_get('mobile_no');

        if ($this->input->server('REQUEST_METHOD') === 'POST') {
            if ($this->checkRegistrationStatus($this->data['admission_id'], $this->data['mobile_no'])) {
                redirect('login');
            }

            $this->form_validation->set_rules('admission_id', 'GST Application ID', 'required');
            $this->form_validation->set_rules('mobile_no', 'GST Mobile No.', 'required|exact_length[11]|callback_check_mobile_no_exists');
            // dd($this->data, $this->form_validation->run() ? 'Y' : 'N');

            if ($this->form_validation->run() == true) {
                $this->data['student_info'] = $this->getStudentInfo($this->data['admission_id'], $this->data['mobile_no']);
                // dd($this->data['student_info']);
                $this->data['is_pass'] = $this->checkIsPass($this->data['student_info']->total);
                if ($this->data['is_pass'] && $this->input->post_get('submit') == 'registration') {

                    $login_table_data = [
                        'system_regid' => substr($this->data['student_info']->hsc_board, 0, 2) . substr($this->data['student_info']->hsc_pass_year, 2, 2) . trim($this->data['student_info']->hsc_regi) . trim($this->data['student_info']->hsc_roll),
                        'hsc_year' => $this->data['student_info']->hsc_pass_year,
                        'hsc_board' => $this->data['student_info']->hsc_board,
                        'hsc_reg' => $this->data['student_info']->hsc_regi,
                        'hsc_roll' => $this->data['student_info']->hsc_roll,
                        'mobile_no' => $this->data['mobile_no'],
                        'password' => rand(100000, 999999),
                        'dob' => date('d-m-Y', strtotime(str_replace('/', '-', $this->data['student_info']->dob))),
                        'gender' => $this->data['student_info']->gender,
                        // 'maritlal_status' => null,
                        // 'father_occupation' => null,
                        // 'mother_occupation' => null,
                        // 'family_income' => null,
                        // 'padd_village' => null,
                        // 'padd_house' => null,
                        // 'padd_road' => null,
                        // 'padd_post_office' => null,
                        // 'padd_police_station' => null,
                        // 'padd_district' => null,
                        // 'local_gurdian_name' => null,
                        // 'local_gurdian_realation' => null,
                        // 'local_gurdian_phone' => null,
                        // 'local_gurdian_address' => null,
                        // 'religion' => null,
                        // 'scout' => null,
                        // 'permanent_address' => null,
                        // 'prad_village' => null,
                        // 'prad_house' => null,
                        // 'prad_road' => null,
                        // 'prad_post_office' => null,
                        // 'prad_police_station' => null,
                        // 'prad_district' => null,
                        // 'mailing_address' => null,
                        // 'district' => null,
                        // 'creation_dt' => null,
                        // 'last_login_dt' => null,
                        // 'is_secnd_meirt' => null,
                        // 'status' => null,
                        // 'mob_no_error' => null
                    ];

                    $registration_table_data = [
                        'REGISID' => $login_table_data['system_regid'],
                        'SNAME' => $this->data['student_info']->name,
                        'SFNAME' =>  $this->data['student_info']->fname,
                        'SMNAME' => $this->data['student_info']->mname,
                        'SBOARD_NAME' => $this->data['student_info']->ssc_board,
                        'SPASS_YEAR' => $this->data['student_info']->ssc_pass_year,
                        'SROLL_NO' => $this->data['student_info']->ssc_roll,
                        'SREG_NO' => $this->data['student_info']->ssc_regi,
                        'SGROUP' => $this->data['student_info']->ssc_study_group,
                        'SGPA' => $this->data['student_info']->ssc_gpa,
                        // 'SLTRGRD' => null,
                        // 'SRESULT' => null,
                        'HNAME' => $this->data['student_info']->name,
                        'HFNAME' => $this->data['student_info']->fname,
                        'HMNAME' => $this->data['student_info']->mname,
                        'HBOARD_NAME' => $this->data['student_info']->hsc_board,
                        'HPASS_YEAR' => $this->data['student_info']->hsc_pass_year,
                        'HROLL_NO' => $this->data['student_info']->hsc_roll,
                        'HREG_NO' => $this->data['student_info']->hsc_regi,
                        'HGROUP' => $this->data['student_info']->hsc_study_group,
                        'HGPA' => $this->data['student_info']->hsc_gpa,
                        // 'HLTRGRD' => null,
                        // 'HRESULT' => null,
                        // 'FORTH_SUB_CODE' => null,
                        // 'IMGUPLOAD' => null,
                        // 'IMGUPLOAD_PATH' => null,
                        // 'IMGUPLOA_NO_TIME' => null,
                        'STD_GENDER' => $this->data['student_info']->gender,
                        // 'CLIENTIP' => $this->data['student_info']->ip_address,
                        'CLIENTIP' => $this->input->ip_address(),
                        // 'ENTRY_DT' => null

                    ];
                    // dd($this->data['student_info']);
                    $final_data = [
                        'REGISID' => $login_table_data['system_regid'],
                        'SNAME' => $this->data['student_info']->name,
                        'SFNAME' =>  $this->data['student_info']->fname,
                        'SMNAME' => $this->data['student_info']->mname,
                        'APPID' => $this->data['student_info']->applicant_id,
                        'UNIT' => $this->data['student_info']->apply_unit,
                        // 'QNAME' => null,
                        'MOBNUMBER' => $login_table_data['mobile_no'],
                        // 'APPSTATUS' => null,
                        // 'APPLYDATE' => date('Y-m-d h:i:s', strtotime(str_replace('/', '-', $this->data['student_info']->entry_date))),
                        // 'PMTDATE' => null,
                        // 'PMTSTATUS' => null,
                        // 'PMTAMOUNT' => null,
                        // 'TXNID' => null,
                        'ROLLNO_FINAL' => $this->data['student_info']->admission_roll,
                        // 'EXAM_VERSION' => null,
                        // 'WITH_ARCHITECTURE' => null,
                        //'HDISTRICT' => $this->data['student_info']->hsc_college_district,
                        'SBOARD_NAME' => $this->data['student_info']->ssc_board,
                        'SPASS_YEAR' => $this->data['student_info']->ssc_pass_year,
                        'SROLL_NO' => $this->data['student_info']->ssc_roll,
                        'SREG_NO' => $this->data['student_info']->ssc_regi,
                        'SGROUP' => $this->data['student_info']->ssc_study_group,
                        'SGPA' => $this->data['student_info']->ssc_gpa,
                        // 'SLTRGRD' => null,
                        'HBOARD_NAME' => $this->data['student_info']->hsc_board,
                        'HPASS_YEAR' => $this->data['student_info']->hsc_pass_year,
                        'HROLL_NO' => $this->data['student_info']->hsc_roll,
                        'HREG_NO' => $this->data['student_info']->hsc_regi,
                        'HGROUP' => $this->data['student_info']->hsc_study_group,
                        'HGPA' => $this->data['student_info']->hsc_gpa,
                        // 'HLTRGRD' => null,
                        // 'IMGUPLOAD' => null,
                        // 'IMGUPLOAD_PATH' => null,
                        'STD_GENDER' => $this->data['student_info']->gender,
                        // 'CLIENTIP' => $this->data['student_info']->ip_address,
                        'CLIENTIP' => $this->input->ip_address(),
                        'english_gpa_score' => $this->getSubjectGPA(json_decode($this->data['student_info']->hsc_subject_data), $this->data['student_info']->hsc_board, ['general' => ['107', '108', '107-108', '107+108'], 'madrasah' => [238], 'technical' => [1542, 1122, 1622, 1812, 1832], 'gce' => ['English']]), //English
                        'mathematics_gpa_score' => $this->getSubjectGPA(json_decode($this->data['student_info']->hsc_subject_data), $this->data['student_info']->hsc_board, ['general' => ['265', '266', '265-266', '265+266'], 'madrasah' => [228], 'technical' => [1421], 'gce' => ['Mathematics']]),
                        'physics_gpa_score' => $this->getSubjectGPA(json_decode($this->data['student_info']->hsc_subject_data), $this->data['student_info']->hsc_board, ['general' => ['174', '175', '174-175', '174+175'], 'madrasah' => [224], 'technical' => [1422], 'gce' => ['Physics']]),
                        'chemistry_gpa_score' => $this->getSubjectGPA(json_decode($this->data['student_info']->hsc_subject_data), $this->data['student_info']->hsc_board, ['general' => ['176', '177', '176-177', '176+177'], 'madrasah' => [226], 'technical' => [1423], 'gce' => ['Chemistry']]),
                        'biology_gpa_score' => $this->getSubjectGPA(json_decode($this->data['student_info']->hsc_subject_data), $this->data['student_info']->hsc_board, ['general' => ['178', '179', '178-179', '178+179'], 'madrasah' => [230], 'technical' => [1324], 'gce' => ['Biology']]),
                        'geography_gpa_score' => $this->getSubjectGPA(json_decode($this->data['student_info']->hsc_subject_data), $this->data['student_info']->hsc_board, ['general' => ['125', '126', '125-126', '125+126'], 'madrasah' => [], 'technical' => [], 'gce' => ['Geography']]), //Geography And Environment
                        'statistics_gpa_score' => $this->getSubjectGPA(json_decode($this->data['student_info']->hsc_subject_data), $this->data['student_info']->hsc_board, ['general' => ['129', '130', '129-130', '129+130'], 'madrasah' => [], 'technical' => [], 'gce' => []]), //statistics
                        'economics_gpa_score' => $this->getSubjectGPA(json_decode($this->data['student_info']->hsc_subject_data), $this->data['student_info']->hsc_board, ['general' => ['109', '110', '109-110', '109+110'], 'madrasah' => [213], 'technical' => [], 'gce' => ['Economics']]),
                        // 'english_gpa_score' => null,
                        // 'mathematics_gpa_score' => null,
                        // 'physics_gpa_score' => null,
                        // 'chemistry_gpa_score' => null,
                        // 'biology_gpa_score' => null,
                        // 'statistics_gpa_score' => null,
                        // 'is_eligible' => null,
                        // 'building_name' => null,
                        // 'exam_centre' => null,
                        // 'room_no' => null,
                        // 'is_in_merit_list' => null,
                        // 'score' => null,
                        'gst_score' => $this->data['student_info']->total,
                        // 'merit_position' => null,
                        // 'is_quota' => null,
                        // 'quota_name' => null,
                        // 'sms_sent' => null,
                        // 'sub_code' => null,
                        // 'class_roll' => null,
                        // 'class_reg' => null,
                        // 'uni_id_no' => null,
                        // 'tracking_id' => null
                    ];
                    if (empty($final_data['english_gpa_score']) && $this->data['student_info']->hsc_board != 'gce') {
                        redirect('errordata');
                    }
                    // dd($this->data['student_info']->hsc_subject_data);
                    // dd($this->data['student_info'], $final_data);
                    // dd($login_table_data);
                    $this->db->trans_begin();
                    $this->db->insert('tbl_login', $login_table_data);
                    $this->db->insert('tbl_registration_pust', $registration_table_data);
                    $this->db->insert('tbl_pabna_data_final', $final_data);
                    if ($this->db->trans_status() === FALSE) {
                        $this->db->trans_rollback();
                    } else {
                        $this->db->trans_commit();
                    }

                    // dd($login_table_data, $this->data['student_info']);
                    $this->data['page_title'] = 'Login information';
                    $this->data['reg_status'] = true;
                    $this->data['login_info'] = [
                        'mobile_no' => $login_table_data['mobile_no'],
                        'password' => $login_table_data['password'],
                    ];

                    $this->send_pass_sms($this->data['login_info']);
                }
            }
        }
        $this->load->view('AdmRegistration/index', $this->data);
    }

    private function send_pass_sms($arr)
    {
        $mob = "88" . $arr['mobile_no'];
        $pass = $arr['password'];
        $sms_text = urlencode("PUST Admission 2020-21.Mobile No.(Login id): $mob Password: $pass Fill-up Choice Form Online http://admission2021.pust.edu.bd/");

        //  file_get_contents("http://users.sendsmsbd.com/smsapi?api_key=R60011435ea0e21487a515.22925838&type=text&contacts=$MOBNUMBER&senderid=8809612446100&msg=".$sms_text);       
    }

    private function checkIsPass($total_mark)
    {
        // return false;
        if ($total_mark >= 30) {
            return true;
        } else {
            return false;
        }
    }
    private function getStudentInfo($applicant_id, $cell_phone)
    {
        $result = $this->db
            ->where('applicant_id', $applicant_id)
            ->where('cell_phone', $cell_phone)
            ->get('gst_final_result_data');
        if ($result->num_rows() >= 1) {
            return $result->row();
        } else {
            return [];
        }
    }

    public  function check_mobile_no_exists($mobile_no)
    {
        $applicant_id = $_POST['admission_id'];
        if (empty($mobile_no)) {
            $this->form_validation->set_message('check_mobile_no_exists', 'The GST Mobile No is required');
            return false;
        } elseif (strlen($mobile_no) != 11) {
            $this->form_validation->set_message('check_mobile_no_exists', 'The GST Mobile No field must be exactly 11 characters in length.');
            return false;
        }
        if (empty($applicant_id)) {
            $this->form_validation->set_message('check_mobile_no_exists', 'The GST Application ID is required');
            return false;
        }
        $cell_phone = $mobile_no;
        $result = $this->db
            ->where('cell_phone', $cell_phone)
            ->where('applicant_id', $applicant_id)
            ->get('gst_final_result_data');
        if ($result->num_rows() >= 1) {
            return true;
        } else {
            $this->form_validation->set_message('check_mobile_no_exists', 'The GST Application ID or GST Mobile No  dose not match');
            return false;
        }
    }
    public function checkRegistrationStatus($applicant_id = null, $cell_phone = null)
    {
        // return true;

        if (!empty($applicant_id) && !empty($cell_phone)) {
            $result = $this->db
                ->where('mobile_no', $cell_phone)
                ->get('tbl_login');
            if ($result->num_rows() >= 1) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
    private function getSubjectGPA($hsc_result, $hsc_board_name, $hsc_subject_code)
    {
        $gpa_scale = get_grading_gpa();
        $gpa = 0.00;
        $no_of_subject_code = 0;
        $total_gpa = 0.00;
        $board_name = substr(strtolower(trim($hsc_board_name)), 0, 3);
        $subject_code = $board_name == 'tec' ? $hsc_subject_code['technical'] : ($board_name == 'mad' ? $hsc_subject_code['madrasah'] : ($board_name == 'gce' ? $hsc_subject_code['gce'] : $hsc_subject_code['general']));
        if (!empty($subject_code)) {
            foreach ($hsc_result as $code => $result) {
                if (in_array($code, $subject_code) && isset($result->g)) {
                    // echo 'start>>' . $code;
                    // echo '<br>';
                    if (is_numeric(trim($result->g))) {
                        // echo 'is_numeric>>' . $result->g;
                        // echo '<br>';
                        $no_of_subject_code += 1;
                        $total_gpa += $result->g;
                    } elseif (isset($gpa_scale[trim($result->g)])) {
                        // echo 'not_numeric>>' . $result->g;
                        // echo '<br>';
                        $no_of_subject_code += 1;
                        $total_gpa += $gpa_scale[trim($result->g)];
                    }
                }
            }
        }
        // dd($no_of_subject_code, $total_gpa);
        if ($no_of_subject_code) {
            $gpa = ($total_gpa / $no_of_subject_code);
        }
        return $gpa;
    }
}
