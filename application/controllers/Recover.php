<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Admitcard
 * @property home_model $home_model
 */

class Recover extends CI_Controller {
      private $data = array();
    public function __construct(){
        parent:: __construct();
         $this->load->library('form_validation');
         //$this->load->model('home_model');
         //$this->load->model('admitcard_model');		 
    }

  function index(){
        $this->load->view('view_recover_option', $this->data);
  }

  function password(){
        $this->load->view('view_forget_pass', $this->data);
  }
  
  
  function find_password(){
		$this->form_validation->set_rules('mob_no', 'Mobile Number', 'required');
        $this->form_validation->set_rules('appid', 'GST Application ID', 'required');
        $this->form_validation->set_rules('hsc_roll', 'HSC Roll No', 'required');
        $this->form_validation->set_rules('hsc_registration', 'HSC Registration', 'required');


         if ($this->form_validation->run() == FALSE) {
             
             redirect(base_url(). 'recover/index/failed');
				 
			 }else{
		
					 	
						$appid = $this->input->post('appid', TRUE);
                                                
                                                $mob_no = $this->input->post('mob_no', TRUE);
						$hsc_roll = $this->input->post('hsc_roll', TRUE);
						$hsc_registration = $this->input->post('hsc_registration', TRUE);
					
						$this->db->where('APPID', $appid);
						$query = $this->db->get('tbl_pabna_data_final');

					if ($query->num_rows() > 0) {
										
							$this->db->where('mobile_no', $mob_no);
							$this->db->where('hsc_roll', $hsc_roll);
							$this->db->where('hsc_reg', $hsc_registration);
							
							$this->db->from('tbl_login');
							
							$query = $this->db->get();
							$rowcount = $query->num_rows();    
							if($rowcount > 0){
								
							   $this->data['login_data'] =  $query->row_array();
							   $this->load->view('view_forget_pass_data', $this->data);
							   
							}else{
								//return null;
								 redirect(base_url(). 'recover/password/noapp');
							}
		
						
					}else{
						
						 redirect(base_url(). 'recover/password/notfound');
						
					}
             
         } 	  
  }
  
  function find() {
        $this->form_validation->set_rules('hsc_board', 'HSC Board Or Institute', 'required');
        $this->form_validation->set_rules('hsc_passing_year', 'HSC Passing Year', 'required');
        $this->form_validation->set_rules('hsc_roll', 'HSC Roll No', 'required');
        $this->form_validation->set_rules('hsc_registration', 'HSC Registration', 'required');


         if ($this->form_validation->run() == FALSE) {
             
             redirect(base_url(). 'recover/index/failed');
				 
			 }else{
		
					 	$hsc_board = $this->input->post('hsc_board', TRUE);
						$hsc_passing_year = $this->input->post('hsc_passing_year', TRUE);
						$hsc_roll = $this->input->post('hsc_roll', TRUE);
						$hsc_registration = $this->input->post('hsc_registration', TRUE);
					
						$REGISID = substr($hsc_board, 0, 2) . substr($hsc_passing_year, 2, 2) . trim($hsc_registration) . trim($hsc_roll);
						$this->db->where('REGISID', $REGISID);
						$query = $this->db->get('tbl_registration_pust');

					if ($query->num_rows() > 0) {
										
							$this->db->where('REGISID', $REGISID);
							$this->db->from('tbl_application_pust');
							
							$query = $this->db->get();
							$rowcount = $query->num_rows();    
							if($rowcount > 0){
								
							   $this->data['applied_unit'] =  $query->result_array();
							   $this->load->view('view_recover_data', $this->data);
							   
							}else{
								//return null;
								 redirect(base_url(). 'recover/index/noapp');
							}
		
						
					}else{
						
						 redirect(base_url(). 'recover/index/notfound');
						
					}
             
         }      
  }

  }
