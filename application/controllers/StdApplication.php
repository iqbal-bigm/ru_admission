<?php
defined('BASEPATH') or exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Dashboard
 *
 * @author Mhabub
 */
class StdApplication extends MY_Controller
{
    private  $data;
    private $payment_amount = ['A' => 650, 'with_archt' => 650, 'B' => 650, 'C' => 650];
    protected $regisid;
    public function __construct()
    {
        parent::__construct();
        // redirect('dashboard');
        // $this->load->model('dashboard_model');
        $this->load->helper('common_helper');
        $this->load->library('form_validation');
        $this->load->model('dashboard_model');

        if (!$this->session->userdata('student_logged_in')) {
            redirect('login');
        }
        $this->regisid = $this->session->userdata('student_login_regisid');
    }
    public function index()
    {
        dd('Application/index called');
    }
    public function ApplicationForm()
    {
        $this->data['page_title'] = 'Application Form';
        $this->data['apply_status'] = $this->dashboard_model->checkIsApply($this->session->userdata('student_login_regisid'));
        $this->data['payment_amount'] = $this->payment_amount;
        $this->data['with_architecture_status'] = $this->checkWithArchitectureStatus();
        $this->data['student_info'] = $this->db->where('tbl_pabna_data_final.regisid', $this->session->userdata('student_login_regisid'))
            // ->join('tbl_registration_pust', 'tbl_pabna_data_final.regisid=tbl_registration_pust.regisid')
            ->join('tbl_login', 'tbl_pabna_data_final.regisid=tbl_login.system_regid')
            ->select('
            tbl_pabna_data_final.regisid,
            tbl_pabna_data_final.appid,
            tbl_pabna_data_final.unit as apply_unit,
            tbl_pabna_data_final.sname as name,
            tbl_pabna_data_final.sfname as fname,
            tbl_pabna_data_final.smname as mname,
            tbl_pabna_data_final.hboard_name as hsc_board,
            tbl_pabna_data_final.hgpa as hsc_gpa,
            tbl_pabna_data_final.hgroup as hsc_study_group,
            tbl_pabna_data_final.sboard_name as ssc_board,
            tbl_pabna_data_final.sgpa as ssc_gpa,
            tbl_pabna_data_final.sgroup as ssc_study_group,
            tbl_pabna_data_final.score ,
            tbl_pabna_data_final.gst_score,
            tbl_pabna_data_final.mobnumber,
            tbl_login.dob,
            ')
            ->get('tbl_pabna_data_final')->row();
        // dd($this->data['student_info']);
        $this->load->view('StdApplication/view_apply_form', $this->data);
    }
    public function ApplicationSubmit()
    {
        $searchData = $this->input->post();
        $applicant_info = $this->db
            ->where('tbl_pabna_data_final.regisid', $this->session->userdata('student_login_regisid'))
            // ->join('tbl_registration_pust', 'tbl_pabna_data_final.regisid=tbl_registration_pust.regisid')
            ->join('tbl_login', 'tbl_pabna_data_final.regisid=tbl_login.system_regid')
            ->select('
            tbl_pabna_data_final.regisid,
            tbl_pabna_data_final.appid,
            tbl_pabna_data_final.unit as apply_unit,
            tbl_pabna_data_final.sname as name,
            tbl_pabna_data_final.sfname as fname,
            tbl_pabna_data_final.smname as mname,
            tbl_pabna_data_final.hboard_name as hsc_board,
            tbl_pabna_data_final.hgpa as hsc_gpa,
            tbl_pabna_data_final.hgroup as hsc_study_group,
            tbl_pabna_data_final.sboard_name as ssc_board,
            tbl_pabna_data_final.sgpa as ssc_gpa,
            tbl_pabna_data_final.sgroup as ssc_study_group,
            tbl_pabna_data_final.hdistrict,
            tbl_pabna_data_final.rollno_final,
            tbl_login.dob,
            tbl_login.mobile_no,
            ')
            ->get('tbl_pabna_data_final')->row();


        $sql_app_check = "select * from `tbl_application_pust` where  APPID='" . $applicant_info->appid . "'";
        $query = $this->db->query($sql_app_check);
        $rowcount = $query->num_rows();

        if ($rowcount == 0) {

            $application_data = [
                'appid' => $applicant_info->appid,
                'regisid' => $applicant_info->regisid,
                'unit' => $applicant_info->apply_unit,
                'qname' => $this->input->post('quota'),
                'mobnumber' => $applicant_info->mobile_no,
                // 'EMAIL' => null,
                //'hdistrict' => $applicant_info->hdistrict,
                // 'NAMEMISMATCH' => null,
                // 'APPSTATUS' => null,
                'applydate' => date('Y-m-d h:i:s'),
                // 'PMTDATE' => null,
                // 'PMTSTATUS' => null,
                // 'PMTAMOUNT' => null,
                // 'TXNID' => null,
                // 'ROLLNO' => $ru_roll,
                'ROLLNO_FINAL' => $applicant_info->rollno_final,
                // 'ADMITAVAIL' => null,
                'amount_to_pay' => ($this->input->post('with_architecture') == 'YES') ? $this->payment_amount['with_archt'] : $this->payment_amount[$applicant_info->apply_unit],
                // 'TXNDATE_DBBL' => null,
                // 'EXAM_VERSION' => null,
                'with_architecture' => $this->input->post('with_architecture') ? $this->input->post('with_architecture') : 'NO',
                // 'PAID_FRM_DBBL' => null,
                // 'PAID_FRM_SCASH' => null,
                // 'PAID_SMS_SENT' => null,
                // 'edited' => null,
                // 'EDIT_ID' => null,

            ];
            $last_unique_id = 0;
            $result = $this->db->select('unique_id')->order_by('unique_id', 'desc')->limit(1)->get('tbl_merit_list');
            if ($result->num_rows()) {
                $last_unique_id = $result->row()->unique_id;
            }
            $new_unique_id = $this->generateUniqueId(10001, $last_unique_id);
            $merit_list_data = [
                'unique_id' => $new_unique_id,
                // 'tracking_id' => 'pust' . $new_unique_id,
                'exam_roll' => $applicant_info->rollno_final,
                'app_id' => $applicant_info->appid,
                'system_reg_id' => $applicant_info->regisid,
                'unit' => $application_data['unit'],
                // 'unit' => ($this->input->post('with_architecture') == 'Yes') ? 'A_ARCH' : $applicant_info->apply_unit,
                // 'score' => null,
                // 'merit' => null,
                'quota_name' => $this->input->post('quota'),
                // 'c_section_hsc_group' => null,
                // 'sub_choice_done' => null, //*
                // 'is_prent_in_viva' => null,
                // 'is_absent' => null,
                // 'in_waiting' => null,
                // 'is_secnd_meirt' => null,
                // 'optional' => null,
                // 'wants_to_admit_frm_waiting' => null,
                // 'wants_to_admit_frm_waiting_dt' => null,
                // 'math_boi_ans' => null,
                // 'sms_sent' => null,
                // 'allocated_subject' => null,

            ];
            // dd($application_data, $merit_list_data);
            $this->db->trans_begin();
            $this->db->insert('tbl_application_pust', $application_data);
            $this->db->insert('tbl_merit_list', $merit_list_data);
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                redirect('dashboard');
            } else {
                $this->db->trans_commit();

                redirect('dashboard');
                // redirect('payment');
            }
        } else {
            redirect('dashboard');
        }
    }
    public function EditApplicationForm()
    {
        if ($this->VivaPaymentStatus()) {
            // redirect('dashboard');
        }

        $this->data['page_title'] = 'Edit Application';
        $this->data['payment_amount'] = $this->payment_amount;
        $student_info = $this->db
            ->select(' tap.id,
             tap.unit,
             tap.appid,
             tap.qname,
             tap.with_architecture,
             tap.amount_to_pay,
             tpd.gst_score,
             tpd.mobnumber,
             tpd.regisid,
             tpd.appid,
             tpd.unit as apply_unit,
             tpd.sname as name,
             tpd.sfname as fname,
             tpd.smname as mname,
             tpd.hboard_name as hsc_board,
             tpd.hgpa as hsc_gpa,
             tpd.hgroup as hsc_study_group,
             tpd.sboard_name as ssc_board,
             tpd.sgpa as ssc_gpa,
             tpd.sgroup as ssc_study_group,
             tpd.hdistrict,
             tpd.rollno_final,
             tl.dob,
             ')
            ->join('tbl_pabna_data_final tpd', 'tap.regisid=tpd.regisid')
            ->join('tbl_login tl', 'tap.regisid=tl.system_regid')
            ->where('tap.regisid', $this->regisid)
            ->get('tbl_application_pust tap')->row();
        $student_info->apply_unit = $student_info->unit;
        $this->data['student_info'] = $student_info;
        $this->data['with_architecture_status'] = $this->checkWithArchitectureStatus();
        if ($this->input->server('REQUEST_METHOD') === 'POST') {
            $application_data = [];
            $application_data['qname'] = $this->input->post('quota');
            if (!empty($this->input->post('with_architecture'))) {
                // $application_data['amount_to_pay'] = ($this->input->post('with_architecture') == 'YES') ? $this->payment_amount['with_archt'] : $this->payment_amount[$student_info->unit];
                $application_data['with_architecture'] = $this->input->post('with_architecture') ? $this->input->post('with_architecture') : 'NO';
            }
            $merit_list_data = [
                'quota_name' => $this->input->post('quota')
            ];
            $this->db->trans_begin();
            $this->db->where('id', $student_info->id)->where('regisid', $student_info->regisid)->update('tbl_application_pust', $application_data);
            $this->db->where('app_id', $student_info->appid)->where('system_reg_id', $student_info->regisid)->update('tbl_merit_list', $merit_list_data);
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                redirect('dashboard');
            } else {
                $this->db->trans_commit();

                redirect('dashboard');
                // redirect('payment');
            }
            if ($this->db->where('id', $student_info->id)->where('regisid', $student_info->regisid)->update('tbl_application_pust', $application_data)) {
                redirect('dashboard');
            };
            // dd($this->input->post(), $application_data);
        };
        $this->load->view('StdApplication/view_apply_edit_form', $this->data);
    }
    private function generateUniqueId($initial_value, $current_unique_id)
    {
        if ($current_unique_id >= $initial_value) {
            return $current_unique_id + 1;
        } else {
            return $initial_value;
        }
    }
    private function checkWithArchitectureStatus()
    {
        return true;
        // $final_data = $this->db->select('unit,mathematics_gpa_score,physics_gpa_score,chemistry_gpa_score')->where('regisid', $this->regisid)->get('tbl_pabna_data_final')->row();
        // // dd($final_data);
        // if (!empty($final_data) && $final_data->unit == 'A' && $final_data->mathematics_gpa_score >= 3.5 && $final_data->physics_gpa_score >= 3.5 && $final_data->chemistry_gpa_score >= 3.5) {
        //     return true;
        // }
        // return false;
    }
}
