<?php
/**
 * Description of admin_logout
 *
 * @author Big M Resources Ltd
 * Date : 12-December-2015
 */
class Logout extends CI_Controller{
    function __construct(){
        parent::__construct();
    }
    function index(){
         $this->session->sess_destroy();
         redirect('login');
    }
}