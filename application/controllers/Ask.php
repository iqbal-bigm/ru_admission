<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Ask
 *
 * @author Mhabub
 */
class Ask extends CI_Controller {
      private $data = array();
      private $_ASK_TABLE;
      public function __construct() {
          parent::__construct();
          $this->load->library('form_validation');
           $this->_ASK_TABLE = 'tbl_ask_pust';

            //redirect('payment/check');exit;
      }

      function index() {
       //   $this->data['widget'] = $this->recaptcha->getWidget();
        //  $this->data['script'] =   $this->recaptcha->getScriptTag();
          $this->load->view('home/view_ask', $this->data);
      }


	function save() {
	/* $recaptcha = $this->input->post('g-recaptcha-response');
		if (!empty($recaptcha)) {
			$response = $this->recaptcha->verifyResponse($recaptcha);
			if (isset($response['success']) and $response['success'] === true) {
	*/
			if(1){
				if(1){
				$this->form_validation->set_rules('name', 'Name', 'required');
				$this->form_validation->set_rules('phone', 'Phone Number', 'required');
				$this->form_validation->set_rules('gstappid', 'GST Application ID', 'required');
				$this->form_validation->set_rules('details', 'Problem Details', 'required');
				if ($this->form_validation->run() === FALSE) {
					$this->load->view('home/view_ask', $this->data);
				}else{

				$dateTime = date('Y-m-d H:i:s');
				$askdata = array(
					'name' => $this->input->post('name', TRUE),
					'phone' => $this->input->post('phone', TRUE),
                                        'gst_app_id' => $this->input->post('gstappid', TRUE),
					'details' => $this->input->post('details', TRUE),
					'date_time' => $dateTime,
					'ip' =>  $_SERVER['REMOTE_ADDR'],
				);

				$this->db->insert($this->_ASK_TABLE, $askdata);
				redirect('ask/confirm');
			}

		}else{
			$this->session->set_flashdata('notification', 'Validation Failed');
			redirect('ask');
		}
		}else{
			$this->session->set_flashdata('notification', 'Validation Failed');
			redirect('ask');
		}

	}

      function confirm() {
            $this->data['validMsg'] = 'Your complain has been submitted successfully.'.'<br>'.'We will contact with you within short time.';
            $this->load->view('home/view_ask', $this->data);
      }
}
