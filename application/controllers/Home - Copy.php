<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Home
 * @property home_model $home_model
 */
class Home extends CI_Controller {

    private $data;

    public function __construct() {
        parent:: __construct();
        $this->load->helper('common_helper');
        $this->load->model('home_model');
        
        // redirect('admitcard');
          
//        if(! $this->session->userdata('isTESTLogin') ){
//           redirect('login');
//        }
    }

    public function index() {
        $this->load->library('form_validation');

        // For Default Form Validation Default Value
        $form['ssc_board'] = '';
        $form['ssc_passing_year'] = '';
        $form['ssc_roll'] = '';
        $form['ssc_registration'] = '';
        $form['hsc_board'] = '';
        $form['hsc_passing_year'] = '';
        $form['hsc_roll'] = '';
        $form['hsc_registration'] = '';
		
        $form['widget'] = $this->recaptcha->getWidget();
        $form['script'] =   $this->recaptcha->getScriptTag();  		
        
        $this->load->view('home/user_reg_form', $form);
    }

    public function nextstep() {
        $this->load->library('form_validation');
                
        // Form Validation Rules
        $this->form_validation->set_rules('ssc_board', 'SSC Board Or Institute', 'required');
        $this->form_validation->set_rules('ssc_passing_year', 'SSC Passing Year', 'required');
        $this->form_validation->set_rules('ssc_roll', 'SSC Roll No', 'required');
        $this->form_validation->set_rules('ssc_registration', 'SSC Registration', 'required');
        $this->form_validation->set_rules('hsc_board', 'HSC Board Or Institute', 'required');
        $this->form_validation->set_rules('hsc_passing_year', 'HSC Passing Year', 'required');
        $this->form_validation->set_rules('hsc_roll', 'HSC Roll No', 'required');
        $this->form_validation->set_rules('hsc_registration', 'HSC Registration', 'required');


        if ($this->form_validation->run() === FALSE) {
            $form['ssc_board'] = '';
            $form['ssc_passing_year'] = '';
            $form['ssc_roll'] = '';
            $form['ssc_registration'] = '';
            $form['hsc_board'] = '';
            $form['hsc_passing_year'] = '';
            $form['hsc_roll'] = '';
            $form['hsc_registration'] = '';
        
			$form['widget'] = $this->recaptcha->getWidget();
			$form['script'] =   $this->recaptcha->getScriptTag();  	
			
            $this->load->view('home/user_reg_form', $form);
        } else {

            $ssc_board = $this->input->post('ssc_board', TRUE);
            $ssc_passing_year = $this->input->post('ssc_passing_year', TRUE);
            $ssc_roll = $this->input->post('ssc_roll', TRUE);
            $ssc_registration = $this->input->post('ssc_registration', TRUE);

            $hsc_board = $this->input->post('hsc_board', TRUE);
            $hsc_passing_year = $this->input->post('hsc_passing_year', TRUE);
            $hsc_roll = $this->input->post('hsc_roll', TRUE);
            $hsc_registration = $this->input->post('hsc_registration', TRUE);


            $validation_ssc_data = $this->home_model->ssc_info($ssc_board, $ssc_passing_year, $ssc_roll, $ssc_registration);
            $validation_hsc_data = $this->home_model->hsc_info($hsc_board, $hsc_passing_year, $hsc_roll, $hsc_registration);

            //  print_r($validation_ssc_data);
            //  print_r($validation_hsc_data);
            //  exit;

            if (!empty($validation_ssc_data) && !empty($validation_hsc_data) && trim($validation_ssc_data['NAME']) == trim($validation_hsc_data['NAME'])) {    // *** check exact Name                 
                // Registration Number Generate by Inputed Data
                //$REGISID = substr($hsc_board, 0, 2) . substr($hsc_passing_year, 2, 2) . $hsc_registration . $hsc_roll;
                 $REGISID = substr($hsc_board, 0, 2) . substr($hsc_passing_year, 2, 2) . trim($hsc_registration) . trim($hsc_roll);
                // Check Student is Already Registered or Not by Generated Registration Number
                $reg_status = $this->home_model->check_registration_status($REGISID);
                if ($reg_status['reg_status'] == FALSE) { // create new registration
                    $this->home_model->create_new_registration($REGISID, $validation_ssc_data, $validation_hsc_data);
                }
                
                        // *** Set Session Data ***--
                            $newapp = array(
                                 'ses_REGISID' => $REGISID,
                                 'ses_sRoll' => $ssc_roll,
                                 'ses_sReg' => $ssc_registration,
                                 'ses_hRoll' => $hsc_roll,
                                 'ses_hYear' => $hsc_passing_year,
                                 'ses_hReg' => $hsc_registration,
                                 'ses_board_name' =>  $hsc_board
                            );
                            $this->session->set_userdata($newapp);                
                          
                // $this->session->set_flashdata('notification', $validation_input_data);
                redirect('home/apply/' . $REGISID);
            } else {
                if(trim($validation_ssc_data['NAME']) <> trim($validation_hsc_data['NAME'])){
                      $this->session->set_flashdata('notification', 'Invalid Information, SSC and HSC Name Mismatch');
                }else{
                      $this->session->set_flashdata('notification', 'Invalid Information');
                }
              
                redirect('/home');
            }
        }
    }

    public function apply($reg_id = '') {
        if (empty($reg_id)) {
            redirect('/home');
        }

        $reg_id_clean = htmlspecialchars($reg_id, ENT_QUOTES, 'UTF-8');

        $reg_status = $this->home_model->check_registration_status($reg_id_clean);

        if ($reg_status['reg_status'] === TRUE) { //already registerd, to check  and get applied unit and get photo if any
            $student_all_data = $this->home_model->get_registration_details($reg_id_clean);
            $this->data['student_all_data'] = $student_all_data;
            $this->data['REGISID'] = $reg_id_clean;
            $this->data['applied_unit'] = $this->home_model->get_already_applied_unit($reg_id_clean);
            
            $this->load->view('home/user_reg_details_form', $this->data);
        } else {
            redirect('/home');
        }
    }

    public function process() {               
        $REGISID = $this->input->post('REGISID', TRUE);
        $application_unit = $this->input->post('application_unit', TRUE);
        $quota = $this->input->post('quota', TRUE);
        $mobile_no = $this->input->post('mobile_no', TRUE);
		
		$district = $this->input->post('district', TRUE);
		$exam_question_tpe = $this->input->post('exam_question_tpe', TRUE);

        
        if( !empty($REGISID) && !empty($application_unit)  ){
            
            $arr =  array(
                'reg_id' => $REGISID,
                'unitcode' => $application_unit,
                'qname' => $quota,
                'mobile' => $mobile_no,
				'district' => $district,
				'exam_question_tpe' => $exam_question_tpe
            );
            
            $app_id = $this->home_model->apply_unit($arr);
             if(!empty($app_id)){
                 redirect('home/confirm/'.$app_id.'/'.$REGISID);
             }else{
                 redirect('notfound');
            }
            
            
        }else{
            redirect('home');
        }
                
    }
    
    function confirm($app_id, $reg_id) {
         $app_id_final = $this->security->xss_clean($app_id);
         $reg_id_final =   $this->security->xss_clean($reg_id);
         $this->data['application_data'] = $this->home_model->get_application_data($app_id_final , $reg_id_final);
         $student_all_data = $this->home_model->get_registration_details($reg_id_final);
         $this->data['student_all_data'] = $student_all_data;         
         $this->load->view('home/view_confirm_page', $this->data);
    }
    
    function app_download($app_id, $reg_id) {
         $app_id_final = $this->security->xss_clean($app_id);
         $reg_id_final =   $this->security->xss_clean($reg_id);
         $this->data['application_data'] = $this->home_model->get_application_data($app_id_final , $reg_id_final);
         $student_all_data = $this->home_model->get_registration_details($reg_id_final);
         $this->data['student_all_data'] = $student_all_data;         
         $html = $this->load->view('home/view_application_pdf', $this->data,true); 
         $pdfFilePath = 'PUST_'.$app_id . ".pdf";

        //load mPDF library
        $this->load->library('m_pdf');

        //generate the PDF from the given html
        $this->m_pdf->pdf->WriteHTML($html);

        //download it.
        $this->m_pdf->pdf->Output($pdfFilePath, "D");         
    }
    

    function ajax_uploadphoto() {
        $arr = '';
        $valid_file_exnt_array = array('jpg', 'jpeg');
        $reg_id = $this->input->post('REGISID_PHOTO', TRUE);
        $reg_id_clean = htmlspecialchars($reg_id, ENT_QUOTES, 'UTF-8');
        $orginal_file_name = $_FILES['userImage']['name'];
        if (!empty($reg_id_clean) && !empty($orginal_file_name)) {

            $temp = explode('.', $orginal_file_name); /* split the file name to get extension */
            $total_token = count($temp);
            $final_image_exten = $temp[$total_token - 1];
            if (in_array($final_image_exten, $valid_file_exnt_array)) {

                $img_url = $this->home_model->upload_photo($reg_id_clean);
                $arr['status'] = 'ok';
                $arr['msg'] = '';
                $arr['image_url'] = $img_url;
            } else {
                $arr['status'] = 'invalid';
                $arr['msg'] = 'Invalid file extension';
            }
        } else {
            $arr['status'] = 'error';
            $arr['msg'] = 'Please select photo and upload';
        }

        echo json_encode($arr);
    }

    function ajax_apply_validation() {
        $arr = '';
        
         
        $reg_id = $this->input->post('reg_id', TRUE);
        $reg_id_clean = htmlspecialchars($reg_id, ENT_QUOTES, 'UTF-8');
        $unit = $this->input->post('unit', TRUE);
        
        $can_apply = 'no';

        if (!empty($reg_id_clean) && !empty($unit)) {

            $unit = $this->input->post('unit', TRUE);

            $s_group = $this->input->post('s_group', TRUE);
            $s_gpa = $this->input->post('s_gpa', TRUE);

            $h_group = $this->input->post('h_group', TRUE);
            $h_gpa = $this->input->post('h_gpa', TRUE);

            $t_gpa = $this->input->post('t_gpa', TRUE);

            $student_all_grade = $this->home_model->get_student_all_grade($reg_id_clean);
            
            $t = explode(',', $student_all_grade);
            $grade_arr = '';
            foreach ($t as $key => $value) {
                if (!empty($value)) {
                    $temp = explode(':', $value);
                    //$grade_arr[$temp[0]] = trim($temp[1]);
                    if($this->session->userdata('ses_board_name') == "TECHNICAL"){
                         $grade_arr[$temp[0]] = trim(substr($temp[1], 3));  
                    }else{
                        $grade_arr[$temp[0]] = trim($temp[1]);
                    }                    
                }
            }
                                  
           // print_r($grade_arr);
            $total_gpa = ($s_gpa + $h_gpa);

            $get_grading_gpa = get_grading_gpa();
           $hsc_board_name = $this->session->userdata('ses_board_name');
            if(!empty($hsc_board_name)){
                           
            
            if($hsc_board_name == "MADRASAH"){

            if ($unit == "A1" || $unit == "A2") {
                //Math 228+229
                //chemistry  226+227
                //physics 224+225
                if ($s_gpa >= 3.5 && $h_gpa >= 3.5 && $total_gpa >= 8.0 && $get_grading_gpa[$grade_arr['228+229']] >= 3.5 && $get_grading_gpa[$grade_arr['226+227']] >= 3.5 && $get_grading_gpa[$grade_arr['224+225']] >= 3.5) {
                   $can_apply = 'yes';
                    $msg = "You are eligible to apply for this unit.";
                } else {
                    $msg = "You are not eligible to apply this unit.";
                    $msg .= " Your GPA is less than the Required GPA.";
                    $msg .="<br> Your SSC and HSC GPA should be atleast 3.5 and you must get minimum 8.00 total in both Exam. Also you should have minimum 3.5 in Math, Physics and Chemistry";
                }
            } else if ($unit == "B") {
                if ($s_gpa >= 3.5 && $h_gpa >= 3.5 && $total_gpa >= 7.5) {
                    $can_apply = 'yes';
                    $msg = "You are eligible to apply for this unit.";
                } else {
                    $msg = "You are not eligible to apply this unit.";
                    $msg .= " Your GPA is less than the Required GPA.";
                    $msg .= "<br> Your SSC and HSC GPA should be atleast 3.5 and you must get minimum 7.50 total in both exam.";
                }
            } else if ($unit == "C") {
                if ($h_group == "SCIENCE") {
                    if ($s_gpa >= 3.5 && $h_gpa >= 3.5 && $total_gpa >= 7.5) {
                        $can_apply = 'yes';
                        $msg = "You are eligible to apply for this unit.";
                    } else {
                        $msg = "You are not eligible to apply this unit.";
                        $msg .= " Your GPA is less than the Required GPA.";
                        $msg .= "<br> Your SSC and HSC GPA should be atleast 3.50 and you must get minimum 7.50 total in both exam.";
                    }
                } else {
                    if ($s_gpa >= 3.0 && $h_gpa >= 3.0 && $total_gpa >= 7.0) {
                         $can_apply = 'yes';
                        $msg = "You are eligible to apply for this unit.";
                    } else {
                        $msg = "You are not eligible to apply this unit.";
                        $msg .= " Your GPA is less than the Required GPA.";
                        $msg .= "<br> Your SSC and HSC GPA should be atleast 3.0 and you must get minimum 7.00 total in both exam.";
                    }
                }
            }             
        }else  if($hsc_board_name == "TECHNICAL"){

            if ($unit == "A1" || $unit == "A2") {
                //Math 1421
                //physics  1422
                //chemistry 1423
                if ($s_gpa >= 3.5 && $h_gpa >= 3.5 && $total_gpa >= 8.0 && $get_grading_gpa[$grade_arr[1421]] >= 3.5 && $get_grading_gpa[$grade_arr[1422]] >= 3.5 && $get_grading_gpa[$grade_arr[1423]] >= 3.5) {
                     $can_apply = 'yes';
                    $msg = "You are eligible to apply for this unit.";
                } else {
                    $msg = "You are not eligible to apply this unit.";
                    $msg .= " Your GPA is less than the Required GPA.";
                    $msg .="<br> Your SSC and HSC GPA should be atleast 3.5 and you must get minimum 8.00 total in both Exam. Also you should have minimum 3.5 in Math, Physics and Chemistry";
                }
            } else if ($unit == "B") {
                if ($s_gpa >= 3.5 && $h_gpa >= 3.5 && $total_gpa >= 7.5) {
                     $can_apply = 'yes';
                    $msg = "You are eligible to apply for this unit.";
                } else {
                    $msg = "You are not eligible to apply this unit.";
                    $msg .= " Your GPA is less than the Required GPA.";
                    $msg .= "<br> Your SSC and HSC GPA should be atleast 3.5 and you must get minimum 7.50 total in both exam.";
                }
            } else if ($unit == "C") {
                if ($h_group == "SCIENCE") {
                    if ($s_gpa >= 3.5 && $h_gpa >= 3.5 && $total_gpa >= 7.5) {
                        $can_apply = 'yes';
                        $msg = "You are eligible to apply for this unit.";
                    } else {
                        $msg = "You are not eligible to apply this unit.";
                        $msg .= " Your GPA is less than the Required GPA.";
                        $msg .= "<br> Your SSC and HSC GPA should be atleast 3.5 and you must get minimum 7.50 total in both exam.";
                    }
                } else {
                    //if ($sGpa >= 3.0 && $h_gpa >= 3.0 && $total_gpa >= 7.0) {
                     if ($s_gpa >= 3.5 && $h_gpa >= 3.5 && $total_gpa >= 7.5) { //Technical all are considers as Science
                        $can_apply = 'yes';
                        $msg = "You are eligible to apply for this unit.";
                    } else {
                        $msg = "You are not eligible to apply this unit.";
                        $msg .= " Your GPA is less than the Required GPA.";
                        $msg .= "<br> Your SSC and HSC GPA should be atleast 3.5 and you must get minimum 7.50 total in both exam.";
                       // $msg .= "<br> Your SSC and HSC GPA should be atleast 3.0 and you must get minimum 7.00 total in both exam.";
                    }
                }
            }            
        }else  if($hsc_board_name == "BISD" || $hsc_board_name == "TEC"){
             if ($unit == "C") {

                    //if ($sGpa >= 3.0 && $h_gpa >= 3.0 && $total_gpa >= 7.0) {
                  // if ($s_gpa >= 3.5 && $h_gpa >= 3.5 && $total_gpa >= 7.5) {
                    if ($s_gpa >= 3.0 && $h_gpa >= 3.0 && $total_gpa >= 7.0) {  //DIBS/TEC BM all are commerce/accounting /bm                  
                        $can_apply = 'yes';
                        $msg = "You are eligible to apply for this unit.";
                    } else {
                        $msg = "You are not eligible to apply this unit.";
                        $msg .= " Your GPA is less than the Required GPA.";
                       // $msg .= "<br> Your SSC and HSC GPA should be atleast 3.5 and you must get minimum 7.50 total in both exam.";
                        $msg .= "<br> Your SSC and HSC GPA should be atleast 3.0 and you must get minimum 7.00 total in both exam.";
                    }
               
            }            
        }else {       // general line 
            $mathematics_gpa = '';

            if (array_key_exists(127, $grade_arr)) {
                $mathematics_gpa = trim($grade_arr['127']); //Math
            } else if (array_key_exists(265, $grade_arr)) {
                $mathematics_gpa = trim($grade_arr['265']); //higher math
            }
            
            if ($unit == "A1" || $unit == "A2") {
                //Math 127, 265
                //physics  174
                //chemistry 176
                // echo $grade_arr[174].' # '.$grade_arr[176] .'<br>';
               // echo $get_grading_gpa[$mathematics_gpa].' # '. $get_grading_gpa[$grade_arr[174]].' # '.$get_grading_gpa[$grade_arr[176]] .  ' # '.$sGpa.  ' # '.$hGpa.  ' # '.$ave;
                if ($s_gpa >= 3.5 && $h_gpa >= 3.5 && $total_gpa >= 8.0 && $get_grading_gpa[$mathematics_gpa] >= 3.5 && $get_grading_gpa[$grade_arr[174]] >= 3.5 && $get_grading_gpa[$grade_arr[176]] >= 3.5) {
                   $can_apply = 'yes';
                    $msg = "You are eligible to apply for this unit.";
                } else {
                    $msg = "You are not eligible to apply this unit.";
                    $msg .= " Your GPA is less than the Required GPA.";
                    $msg .="<br> Your SSC and HSC GPA should be atleast 3.5 and you must get minimum 8.00 total in both Exam. Also you should have minimum 3.5 in Math, Physics and Chemistry";
                }
            } else if ($unit == "B") {
                if ($s_gpa >= 3.5 && $h_gpa >= 3.5 && $total_gpa >= 7.5) {
                     $can_apply = 'yes';
                    $msg = "You are eligible to apply for this unit.";
                } else {
                    $msg = "You are not eligible to apply this unit.";
                    $msg .= " Your GPA is less than the Required GPA.";
                    $msg .= "<br> Your SSC and HSC GPA should be atleast 3.5 and you must get minimum 7.50 total in both exam.";
                }
            } else if ($unit == "C") {
                if ($h_group == "SCIENCE") {
                    if ($s_gpa >= 3.5 && $h_gpa >= 3.5 && $total_gpa >= 7.5) {
                         $can_apply = 'yes';
                        $msg = "You are eligible to apply for this unit.";
                    } else {
                        $msg = "You are not eligible to apply this unit.";
                        $msg .= " Your GPA is less than the Required GPA.";
                        $msg .= "<br> Your SSC and HSC GPA should be atleast 3.5 and you must get minimum 7.5 total in both exam.";
                    }
                } else {
                    if ($s_gpa >= 3.0 && $h_gpa >= 3.0 && $total_gpa >= 7.0) {
                        $can_apply = 'yes';
                        $msg = "You are eligible to apply for this unit.";
                    } else {
                        $msg = "You are not eligible to apply this unit.";
                        $msg .= " Your GPA is less than the Required GPA.";
                        $msg .= "<br> Your SSC and HSC GPA should be atleast 3.0 and you must get minimum 7.00 total in both exam.";
                    }
                }
            }
        }    
        
       }else{
            $can_apply = 'error'; //'error';
            $msg = 'Board Missing From Session';           
       }   
                 
     } else {

            $can_apply = 'error'; //'error';
            $msg = 'REG. ID Missing';
        }

         $arr['can_apply'] =$can_apply;
         $arr['msg'] = $msg;
                  
        echo json_encode($arr);
    }
    

}
