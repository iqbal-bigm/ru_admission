<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Crack
 * @property home_model $home_model
 */
class Crack extends CI_Controller {

    private $data;

    public function __construct() {
        parent:: __construct();
        $this->load->helper('common_helper');
        $this->load->model('home_model');

        $this->load->library('recaptcha');

          //redirect('circular');

       // if(! $this->session->userdata('isTESTLogin') ){
         // redirect('login');
       // }
    }

    public function index() {
        $this->load->library('form_validation');

        // For Default Form Validation Default Value
        $form['ssc_board'] = '';
        $form['ssc_passing_year'] = '';
        $form['ssc_roll'] = '';
        $form['ssc_registration'] = '';
        $form['hsc_board'] = '';
        $form['hsc_passing_year'] = '';
        $form['hsc_roll'] = '';
        $form['hsc_registration'] = '';

        $form['widget'] = $this->recaptcha->getWidget();
        $form['script'] =   $this->recaptcha->getScriptTag();

        $this->load->view('home/user_reg_crack', $form);
    }

    public function nextstep() {
        $this->load->library('form_validation');

        // Form Validation Rules
        $this->form_validation->set_rules('ssc_board', 'SSC Board Or Institute', 'required');
        $this->form_validation->set_rules('ssc_passing_year', 'SSC Passing Year', 'required');
        $this->form_validation->set_rules('ssc_roll', 'SSC Roll No', 'required');
        $this->form_validation->set_rules('ssc_registration', 'SSC Registration', 'required');
        $this->form_validation->set_rules('hsc_board', 'HSC Board Or Institute', 'required');
        $this->form_validation->set_rules('hsc_passing_year', 'HSC Passing Year', 'required');
        $this->form_validation->set_rules('hsc_roll', 'HSC Roll No', 'required');
        $this->form_validation->set_rules('hsc_registration', 'HSC Registration', 'required');

      /*  $recaptcha = $this->input->post('g-recaptcha-response');
        if (!empty($recaptcha)) {

        $response = $this->recaptcha->verifyResponse($recaptcha);
        if (isset($response['success']) and $response['success'] === true) {
	*/
		if(1){			
		if(1){
        if ($this->form_validation->run() === FALSE) {
            $form['ssc_board'] = '';
            $form['ssc_passing_year'] = '';
            $form['ssc_roll'] = '';
            $form['ssc_registration'] = '';
            $form['hsc_board'] = '';
            $form['hsc_passing_year'] = '';
            $form['hsc_roll'] = '';
            $form['hsc_registration'] = '';

            $form['widget'] = $this->recaptcha->getWidget();
            $form['script'] =   $this->recaptcha->getScriptTag();

            $this->load->view('home/user_reg_crack', $form);
        } else {

                $ssc_board = $this->input->post('ssc_board', TRUE);
                $ssc_passing_year = $this->input->post('ssc_passing_year', TRUE);
                $ssc_roll = $this->input->post('ssc_roll', TRUE);
                $ssc_registration = $this->input->post('ssc_registration', TRUE);

                $hsc_board = $this->input->post('hsc_board', TRUE);
                $hsc_passing_year = $this->input->post('hsc_passing_year', TRUE);
                $hsc_roll = $this->input->post('hsc_roll', TRUE);
                $hsc_registration = $this->input->post('hsc_registration', TRUE);

                if($hsc_board == 'tec'){
                  $hsc_registration =  ltrim($hsc_registration , '0'); //removing leading zero, if student enter with 000
                }
                // Registration Number Generate by Inputed Data
              //  echo $hsc_roll."<br>".$ssc_roll."<br>";
                     $REGISID = substr($hsc_board, 0, 2) . substr($hsc_passing_year, 2, 2) . trim($hsc_registration) . trim($hsc_roll);


                // Check Student is Already Registered or Not by Generated Registration Number
                $reg_status = $this->home_model->check_registration_status($REGISID);
                //print_r($reg_status);
                if ($reg_status['reg_status'] == FALSE) {  // Not registrted yet, call teletalk api

                    $validation_ssc_data = $this->call_teletalk_api ($ssc_board, $ssc_passing_year, $ssc_roll, $ssc_registration,'ssc');
                    $validation_hsc_data = $this->call_teletalk_api ($hsc_board, $hsc_passing_year, $hsc_roll, $hsc_registration, 'hsc');
					//echo"<pre>";
					//print_r( $validation_ssc_data);
					//print_r( $validation_hsc_data);
					
                    //exit;
                    //if (!empty($validation_ssc_data) && !empty($validation_hsc_data) && trim($validation_ssc_data['name']) == trim($validation_hsc_data['name'])) {    // *** check exact Name
					if (!empty($validation_ssc_data) && !empty($validation_hsc_data) ) {  

                                $this->home_model->create_new_registration($REGISID, $validation_ssc_data, $validation_hsc_data);

                                // *** Set Session Data ***--
                                    $newapp = array(
                                         'ses_REGISID' => $REGISID,
                                         'ses_sRoll' => $ssc_roll,
                                         'ses_sReg' => $ssc_registration,
                                         'ses_hRoll' => $hsc_roll,
                                         'ses_hYear' => $hsc_passing_year,
                                         'ses_hReg' => $hsc_registration,
                                         'ses_board_name' =>  $hsc_board
                                    );
                                    $this->session->set_userdata($newapp);

                                    redirect('home/apply/' . $REGISID);

                    } else {

                        if( !empty($validation_ssc_data) && !empty($validation_hsc_data) ){

                          if(trim($validation_ssc_data['name']) <> trim($validation_hsc_data['name'])){
                              $log_data = $hsc_board." ".$hsc_passing_year." ".$hsc_roll." ".$hsc_registration." Name Mismatch ssc ".$validation_ssc_data['name']." hsc ".$validation_hsc_data['name'];
                              $this->write_others_log(  $log_data );
                                $this->session->set_flashdata('notification', 'Invalid Information, SSC and HSC Data Mismatch');
                          }else{
                                $this->session->set_flashdata('notification', 'Invalid Information');
                          }

                      }else{
                            $this->session->set_flashdata('notification', 'Invalid Information.');
                      }



                        redirect('/crack');

                    }
              }else if($reg_status['reg_status'] == TRUE){ // all valid and already registation completed

                // *** Set Session Data ***--
                    $newapp = array(
                         'ses_REGISID' => $REGISID,
                         'ses_sRoll' => $ssc_roll,
                         'ses_sReg' => $ssc_registration,
                         'ses_hRoll' => $hsc_roll,
                         'ses_hYear' => $hsc_passing_year,
                         'ses_hReg' => $hsc_registration,
                         'ses_board_name' =>  $hsc_board
                    );
                    $this->session->set_userdata($newapp);

                    redirect('home/apply/' . $REGISID);

              }else{

                 redirect('/crack');

              }

        }

    }else{
        //redirect('notfound');
        $this->session->set_flashdata('notification', 'Validation Failed');
         redirect('/home');
    }

        }else{
            //redirect('notfound');
            $this->session->set_flashdata('notification', 'Validation Failed');
             redirect('/home');
        }

  }
    
function call_teletalk_api($board, $passing_year, $roll, $registration, $ssc_hsc_type) {
        $API_KEY = 'd49c6512c7134fe5c0ce5595873bb7a4';
        try {
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, "http://ebapi.teletalk.com.bd//v3.0/ebapi.php");//http://ebapi.teletalk.com.bd/v3.0/ebapi.php
							               
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");

            $jsonData = array(
                'commandID' => 'getDetailsResult',
                'exam' =>   $ssc_hsc_type,
                'board' =>  $board,
                'rollNo' => $roll,
                'year' =>   $passing_year,
                'regNo' =>  $registration
            );

            $data_string = json_encode($jsonData);

            $headers = array();
            $headers[] = "Content-Type: application/json";
            $headers[] = "APIKEY: " . $API_KEY . "";
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);

            $result = curl_exec($ch);
            $arr_data_temp = json_decode($result, true);
			//print_r($arr_data_temp);
            $arr_data = '';

            //process response data
            if (!empty($arr_data_temp)) {
				
                if ($arr_data_temp['responseDesc'] == "Success") {
					
                    $arr_data['board'] = $arr_data_temp['board'];
                    $arr_data['rollno'] = $arr_data_temp['rollNo'];
                    $arr_data['passyear'] = $arr_data_temp['passYear'];
                    $arr_data['name'] = $arr_data_temp['name'];
                    $arr_data['father'] = $arr_data_temp['father'];
                    $arr_data['mother'] = $arr_data_temp['mother'];
                    $arr_data['regno'] = $arr_data_temp['regNo'];
                    $arr_data['gender'] = $arr_data_temp['gender'];
                    $arr_data['result'] = $arr_data_temp['result'];
                    $arr_data['gpa'] = $arr_data_temp['gpa'];
					$arr_data['gpaExc4th'] = $arr_data_temp['gpaExc4th'];
                    $arr_data['stud_group'] = $arr_data_temp['studGroup'];
					$arr_data['total_obt_mark'] = $arr_data_temp['totalObtMark'];
					$arr_data['total_exc_4th'] = $arr_data_temp['totalExc4TH'];

					$arr_data['total_obt_mark_cal'] = '';


					$ltrgd = '';
					if(!empty($arr_data_temp['subject']) && is_array($arr_data_temp['subject'])){
						foreach ($arr_data_temp['subject'] as $key => $value) {

							if($ssc_hsc_type == "hsc"){
								$ltrgd .= $value["subCode"].":".$value["grade"].":".$value["gpoint"].",";
							}else{
								$ltrgd .= $value["subCode"].":".$value["grade"].",";
							}
						}
					}

					if($ssc_hsc_type == "ssc"){//get dob
						$arr_data['dob'] = $arr_data_temp['dob'];
					}

					if($ssc_hsc_type == "hsc"){									
					//4th sub
					$arr_data['4th_sub_code'] = $arr_data_temp['sub4thCode'];

					}

                    $arr_data['ltrgd'] = $ltrgd;
                }
            }
			//print_r($arr_data);
            return $arr_data;

            if (curl_errno($ch)) {
                //echo 'Error:' . curl_error($ch);
                $this->write_api_call_error_log(curl_error($ch));
                return false;
            }

            curl_close($ch);
        } catch (Exception $exc) {
            $this->write_api_call_error_log($exc);
            curl_close($ch);
            return false;
        }
    }


	function write_api_call_error_log($jsonData, $exp, $type="ERROR"){
		file_put_contents("teletalk_log.txt", $type."   Date: ".date('Y-m-d H:i:s')." Request Data : ".json_encode($jsonData)." Error: ". $exp. "\n", FILE_APPEND);
	}

  function write_others_log($infodata,  $type="INFO"){
    file_put_contents("teletalk_info_log.txt", $type."   Date: ".date('Y-m-d H:i:s')." Data : ".$infodata. "\n", FILE_APPEND);
  }

}
