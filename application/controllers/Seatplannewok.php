<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Seatplannewok extends CI_Controller {
	var $orginal_file_path = '';
    public function __construct() {
        parent::__construct();		
		$this->orginal_file_path = realpath(APPPATH . '../seatplan_pdf');				
    }
    
    //php index.php seatplan index 1
    public function index($id, $ROLLNO_FINAL_start, $ROLLNO_FINAL_end) {
        set_time_limit(0);
        echo "Start $id \r\n";
		
		$sql1 = "select * from `pust_exam_center` where id=".$id;
		$query1 = $this->db->query($sql1);
		
		$rec1 = $query1->result();
		
		foreach ($rec1 as $key1 => $value1) {
			//echo $value1->id."<br>";
			$data = '';
			$this->load->library('m_pdf');
			//$sql2 = "select * from `tbl_pabna_data_final` where unit='".$value1->unit."' and exam_centre = '".addslashes($value1->exam_center)."' and room_no='".addslashes($value1->room_no)."' and building_name='".addslashes($value1->building_name)."'  order by ROLLNO_FINAL";
			
			$sql2 = "select * from `tbl_pabna_data_final` where unit='".$value1->unit."' and exam_centre = '".addslashes($value1->exam_center)."' and room_no='".addslashes($value1->room_no)."' and building_name='".addslashes($value1->building_name)."' and (ROLLNO_FINAL >='".$ROLLNO_FINAL_start."' and ROLLNO_FINAL <='".$ROLLNO_FINAL_END."')   order by ROLLNO_FINAL";


			$query2 = $this->db->query($sql2);
			$rowcount2 = $query2->num_rows();
			
			if($rowcount2 > 0){
								
				$data['exam_centre'] = $value1->exam_center;
                $data['room_no'] = $value1->room_no;
				
				$data['building_name'] = $value1->building_name;
				
				$data['roll_from'] = $value1->roll_from;
				$data['roll_to'] = $value1->roll_to;
				$data['total_seat'] = $value1->total_seat;
				$data['exam_unit'] = $value1->unit;
				$data['type'] = $value1->type;
				
				$rec2 = '';
				$rec2 = $query2->result();
				
				$data['res'] = $rec2;
				
				$html = '';
				$html = $this->load->view('view_seatplan_pdf', $data, true);
				
				//generate the PDF from the given html
                                
                               
				$this->m_pdf->pdf->WriteHTML($html);
				
				$pdfFilePath = $this->orginal_file_path .'/'.$value1->unit .'/'.$value1->id . "_attendance_sheet.pdf";
				//download it.
				$this->m_pdf->pdf->Output($pdfFilePath, "F");
				echo "   OK \r\n";
			}else{
				echo "  No Record \r\n";
			}
							
		
		}
		
							
    echo "  DONE \r\n";
    }
    
 
    
}
