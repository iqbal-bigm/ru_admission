<?php
defined('BASEPATH') or exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Dashboard
 *
 * @author Mhabub
 */
class GstApplicationDataTest extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        // $this->load->model('dashboard_model');
        $this->load->helper('common_helper');
        // $this->load->helper(array('form', 'url'));
        // $this->load->library('form_validation');

        // if ($this->session->userdata('student_logged_in')) {
        //     redirect('dashboard');
        // }

        //    if(! $this->session->userdata('isTESTLogin') ){
        //           redirect('testauth');
        //        }
    }


    public function updateApplicantInfo()
    {
        $sl = 0;
        $applicants = $this->db
            ->select('id,mobile_no,system_regid')
            ->where('id<=', 540)
            ->get('tbl_login')
            ->result();
        foreach ($applicants as $key => $applicant) {
            $gst_data = $this->db
                // ->where('applicant_id', $applicant_id)
                ->where('cell_phone', $applicant->mobile_no)
                ->get('gst_final_result_data');
            // dd($gst_data->num_rows());
            if ($gst_data->num_rows()) {
                $gst_applicant_data = $gst_data->row();
                // $login_table_data = [ ];
                $registration_table_data = [
                    // 'SNAME' => $gst_applicant_data->name,
                    // 'SFNAME' =>  $gst_applicant_data->fname,
                    // 'SMNAME' => $gst_applicant_data->mname,
                    'SBOARD_NAME' => $gst_applicant_data->ssc_board,
                    // 'SPASS_YEAR' => $gst_applicant_data->ssc_pass_year,
                    // 'SROLL_NO' => $gst_applicant_data->ssc_roll,
                    // 'SREG_NO' => $gst_applicant_data->ssc_regi,
                    // 'SGROUP' => $gst_applicant_data->ssc_study_group,
                    // 'SGPA' => $gst_applicant_data->ssc_gpa,
                    // 'HNAME' => $gst_applicant_data->name,
                    // 'HFNAME' => $gst_applicant_data->fname,
                    // 'HMNAME' => $gst_applicant_data->mname,
                    'HBOARD_NAME' => $gst_applicant_data->hsc_board,
                    // 'HPASS_YEAR' => $gst_applicant_data->hsc_pass_year,
                    // 'HROLL_NO' => $gst_applicant_data->hsc_roll,
                    // 'HREG_NO' => $gst_applicant_data->hsc_regi,
                    // 'HGROUP' => $gst_applicant_data->hsc_study_group,
                    // 'HGPA' => $gst_applicant_data->hsc_gpa,
                    // 'STD_GENDER' => $gst_applicant_data->gender,

                ];
                $final_data = [
                    // 'SNAME' => $gst_applicant_data->name,
                    // 'SFNAME' =>  $gst_applicant_data->fname,
                    // 'SMNAME' => $gst_applicant_data->mname,
                    // 'APPID' => $gst_applicant_data->applicant_id,
                    // 'UNIT' => $gst_applicant_data->apply_unit,
                    // 'QNAME' => null,
                    // 'MOBNUMBER' => $login_table_data['mobile_no'],
                    // 'APPSTATUS' => null,
                    // 'APPLYDATE' => date('Y-m-d h:i:s', strtotime(str_replace('/', '-', $gst_applicant_data->entry_date))),
                    // 'PMTDATE' => null,
                    // 'PMTSTATUS' => null,
                    // 'PMTAMOUNT' => null,
                    // 'TXNID' => null,
                    // 'ROLLNO_FINAL' => $gst_applicant_data->admission_roll,
                    // 'EXAM_VERSION' => null,
                    // 'WITH_ARCHITECTURE' => null,
                    //'HDISTRICT' => $gst_applicant_data->hsc_college_district,
                    'SBOARD_NAME' => $gst_applicant_data->ssc_board,
                    // 'SPASS_YEAR' => $gst_applicant_data->ssc_pass_year,
                    // 'SROLL_NO' => $gst_applicant_data->ssc_roll,
                    // 'SREG_NO' => $gst_applicant_data->ssc_regi,
                    // 'SGROUP' => $gst_applicant_data->ssc_study_group,
                    // 'SGPA' => $gst_applicant_data->ssc_gpa,
                    // 'SLTRGRD' => null,
                    'HBOARD_NAME' => $gst_applicant_data->hsc_board,
                    // 'HPASS_YEAR' => $gst_applicant_data->hsc_pass_year,
                    // 'HROLL_NO' => $gst_applicant_data->hsc_roll,
                    // 'HREG_NO' => $gst_applicant_data->hsc_regi,
                    // 'HGROUP' => $gst_applicant_data->hsc_study_group,
                    // 'HGPA' => $gst_applicant_data->hsc_gpa,
                    // 'HLTRGRD' => null,
                    // 'IMGUPLOAD' => null,

                    // 'IMGUPLOAD_PATH' => null,
                    // 'STD_GENDER' => $gst_applicant_data->gender,
                    // 'CLIENTIP' => $gst_applicant_data->ip_address,
                    // 'CLIENTIP' => $this->input->ip_address(),
                    // 'english_gpa_score' => $this->getSubjectGPA(json_decode($gst_applicant_data->hsc_subject_data), $gst_applicant_data->hsc_board, ['general' => ['107', '108', '107-108', '107+108'], 'madrasah' => [238], 'technical' => [1542, 1122, 1622, 1812, 1832], 'gce' => ['English']]), //English
                    // 'mathematics_gpa_score' => $this->getSubjectGPA(json_decode($gst_applicant_data->hsc_subject_data), $gst_applicant_data->hsc_board, ['general' => ['265', '266', '265-266', '265+266'], 'madrasah' => [228], 'technical' => [1421], 'gce' => ['Mathematics']]),
                    // 'physics_gpa_score' => $this->getSubjectGPA(json_decode($gst_applicant_data->hsc_subject_data), $gst_applicant_data->hsc_board, ['general' => ['174', '175', '174-175', '174+175'], 'madrasah' => [224], 'technical' => [1422], 'gce' => ['Physics']]),
                    // 'chemistry_gpa_score' => $this->getSubjectGPA(json_decode($gst_applicant_data->hsc_subject_data), $gst_applicant_data->hsc_board, ['general' => ['176', '177', '176-177', '176+177'], 'madrasah' => [226], 'technical' => [1423], 'gce' => ['Chemistry']]),
                    // 'biology_gpa_score' => $this->getSubjectGPA(json_decode($gst_applicant_data->hsc_subject_data), $gst_applicant_data->hsc_board, ['general' => ['178', '179', '178-179', '178+179'], 'madrasah' => [230], 'technical' => [1324], 'gce' => ['Biology']]),
                    // 'geography_gpa_score' => $this->getSubjectGPA(json_decode($gst_applicant_data->hsc_subject_data), $gst_applicant_data->hsc_board, ['general' => ['125', '126', '125-126', '125+126'], 'madrasah' => [], 'technical' => [], 'gce' => ['Geography']]), //Geography And Environment
                    // 'statistics_gpa_score' => $this->getSubjectGPA(json_decode($gst_applicant_data->hsc_subject_data), $gst_applicant_data->hsc_board, ['general' => ['129', '130', '129-130', '129+130'], 'madrasah' => [], 'technical' => [], 'gce' => []]), //statistics
                    // 'economics_gpa_score' => $this->getSubjectGPA(json_decode($gst_applicant_data->hsc_subject_data), $gst_applicant_data->hsc_board, ['general' => ['109', '110', '109-110', '109+110'], 'madrasah' => [213], 'technical' => [], 'gce' => ['Economics']]),
                    // 'english_gpa_score' => null,
                    // 'mathematics_gpa_score' => null,
                    // 'physics_gpa_score' => null,
                    // 'chemistry_gpa_score' => null,
                    // 'biology_gpa_score' => null,
                    // 'statistics_gpa_score' => null,
                    // 'is_eligible' => null,
                    // 'building_name' => null,
                    // 'exam_centre' => null,
                    // 'room_no' => null,
                    // 'is_in_merit_list' => null,
                    // 'score' => null,
                    // 'gst_score' => $gst_applicant_data->total,
                    // 'merit_position' => null,
                    // 'is_quota' => null,
                    // 'quota_name' => null,
                    // 'sms_sent' => null,
                    // 'sub_code' => null,
                    // 'class_roll' => null,
                    // 'class_reg' => null,
                    // 'uni_id_no' => null,
                    // 'tracking_id' => null
                ];
                $sl += 1;
                // dd($registration_table_data, $final_data);
                $this->db->trans_begin();
                $this->db->where('regisid', $applicant->system_regid)->update('tbl_registration_pust', $registration_table_data);
                $this->db->where('regisid', $applicant->system_regid)->update('tbl_pabna_data_final', $final_data);
                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();
                } else {

                    $this->db->trans_commit();
                    echo "\r\n Update SL:" . $sl . " mobile_no:" . $applicant->mobile_no;
                }
            }
        }
    }


    public  function send_all_seme()
    {

        $sql = "select * from `tbl_application_pust`  where id <=420 and PMTSTATUS='P'";



        $query = $this->db->query($sql);
        $rowcount = $query->num_rows();

        if ($rowcount > 0) {
            $arr = '';
            $rec = $query->result_array();
            $i = 1;
            foreach ($rec as $key => $value) {

                $mob = "88" . $value['MOBNUMBER'];
                $sms_text = urlencode("PUST Admission 2020-21. Please login and complete subject choice again as soon as possible. http://admission2021.pust.edu.bd/");
                $i++;

                //  file_get_contents("http://users.sendsmsbd.com/smsapi?api_key=R60011435ea0e21487a515.22925838&type=text&contacts=$MOBNUMBER&senderid=8809612446100&msg=".$sms_text);    
            }
        }

        echo "DONE";

        // file_get_contents("http://users.sendsmsbd.com/smsapi?api_key=R60011435ea0e21487a515.22925838&type=text&contacts=$MOBNUMBER&senderid=8809612446100&msg=".$sms_text);       
    }


    public function __index()
    {

        $gst_final_result_data = $this->db
            ->select('applicant_id,hsc_subject_data,hsc_board')
            ->where('temp_update !=', 1)
            ->where('total >=', 30)
            ->limit(50000)
            ->get('gst_final_result_data')->result();
        $total = count($gst_final_result_data);
        $counter = 0;
        foreach ($gst_final_result_data as $key => $value) {
            // dd($value->hsc_subject_data);
            $final_data = [
                'english_gpa_score' => $this->getSubjectGPA(json_decode($value->hsc_subject_data), $value->hsc_board, ['general' => ['107', '108', '107-108', '107+108'], 'madrasah' => [238], 'technical' => [1542, 1122, 1622, 1812, 1832], 'gce' => ['English']]), //English
                'mathematics_gpa_score' => $this->getSubjectGPA(json_decode($value->hsc_subject_data), $value->hsc_board, ['general' => ['265', '266', '265-266', '265+266'], 'madrasah' => [228], 'technical' => [1421], 'gce' => ['Mathematics']]),
                'physics_gpa_score' => $this->getSubjectGPA(json_decode($value->hsc_subject_data), $value->hsc_board, ['general' => ['174', '175', '174-175', '174+175'], 'madrasah' => [224], 'technical' => [1422], 'gce' => ['Physics']]),
                'chemistry_gpa_score' => $this->getSubjectGPA(json_decode($value->hsc_subject_data), $value->hsc_board, ['general' => ['176', '177', '176-177', '176+177'], 'madrasah' => [226], 'technical' => [1423], 'gce' => ['Chemistry']]),
                'biology_gpa_score' => $this->getSubjectGPA(json_decode($value->hsc_subject_data), $value->hsc_board, ['general' => ['178', '179', '178-179', '178+179'], 'madrasah' => [230], 'technical' => [1324], 'gce' => ['Biology']]),
                'geography_gpa_score' => $this->getSubjectGPA(json_decode($value->hsc_subject_data), $value->hsc_board, ['general' => ['125', '126', '125-126', '125+126'], 'madrasah' => [], 'technical' => [], 'gce' => ['Geography']]), //Geography And Environment
                'statistics_gpa_score' => $this->getSubjectGPA(json_decode($value->hsc_subject_data), $value->hsc_board, ['general' => ['129', '130', '129-130', '129+130'], 'madrasah' => [], 'technical' => [], 'gce' => []]), //statistics
                'economics_gpa_score' => $this->getSubjectGPA(json_decode($value->hsc_subject_data), $value->hsc_board, ['general' => ['109', '110', '109-110', '109+110'], 'madrasah' => [213], 'technical' => [], 'gce' => ['Economics']]),
                'temp_update' => 1
            ];
            // dd($final_data);
            $counter += 1;
            if ($this->db->where('applicant_id', $value->applicant_id)->update('gst_final_result_data', $final_data)) {
                echo "\r\n Process->" . $counter . "->of->" . $total . "->Updated applicant_id:" . $value->applicant_id;
            } else {
                echo "\r\n Process->" . $counter . "->of->" . $total . "-> Error with applicant_id:" . $value->applicant_id;
            };
        }
        die();
    }
    private function getSubjectGPA($hsc_result, $hsc_board_name, $hsc_subject_code)
    {
        $gpa_scale = get_grading_gpa();
        $gpa = 0.00;
        $no_of_subject_code = 0;
        $total_gpa = 0.00;
        $board_name = substr(strtolower(trim($hsc_board_name)), 0, 3);
        $subject_code = $board_name == 'tec' ? $hsc_subject_code['technical'] : ($board_name == 'mad' ? $hsc_subject_code['madrasah'] : ($board_name == 'gce' ? $hsc_subject_code['gce'] : $hsc_subject_code['general']));
        if (!empty($subject_code)) {
            foreach ($hsc_result as $code => $result) {
                if (in_array($code, $subject_code) && isset($result->g)) {
                    // echo 'start>>' . $code;
                    // echo '<br>';
                    if (is_numeric(trim($result->g))) {
                        // echo 'is_numeric>>' . $result->g;
                        // echo '<br>';
                        $no_of_subject_code += 1;
                        $total_gpa += $result->g;
                    } elseif (isset($gpa_scale[trim($result->g)])) {
                        // echo 'not_numeric>>' . $result->g;
                        // echo '<br>';
                        $no_of_subject_code += 1;
                        $total_gpa += $gpa_scale[trim($result->g)];
                    }
                }
            }
        }
        // dd($no_of_subject_code, $total_gpa);
        if ($no_of_subject_code) {
            $gpa = ($total_gpa / $no_of_subject_code);
        }
        return $gpa;
    }
}
