<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Admin_complain
 *
 * @author Mhabub
 */
class Admin_complain extends CI_Controller
{
    private $data = array();
    private $_ASK_TABLE;
    public function __construct()
    {
        parent::__construct();
        $this->_ASK_TABLE = 'tbl_ask_pust';
        if (!$this->session->userdata('isADMINLogin')) {
            redirect('login');
        }
    }

    function index()
    {

        // $this->db->where('id >' 45);
        $this->db->from('tbl_ask_pust')
            ->select('
            tbl_ask_pust.id,
            tbl_ask_pust.name,
            tbl_ask_pust.phone,
            tbl_ask_pust.gst_app_id,
            tbl_ask_pust.details,
            tbl_ask_pust.date_time,
            tbl_ask_pust.ip,
            tbl_ask_pust.status,
            tbl_pabna_data_final.appid,
            tbl_pabna_data_final.sboard_name ssc_board,
            tbl_pabna_data_final.spass_year ssc_year,
            tbl_pabna_data_final.sroll_no ssc_roll,
            tbl_pabna_data_final.sreg_no ssc_reg,
            tbl_pabna_data_final.hboard_name hsc_board,
            tbl_pabna_data_final.hpass_year hsc_year,
            tbl_pabna_data_final.hroll_no hsc_roll,
            tbl_pabna_data_final.hreg_no hsc_reg,
            ')
            ->join('tbl_pabna_data_final', 'tbl_ask_pust.gst_app_id=tbl_pabna_data_final.appid', 'left');
        //$query = $this->db->where('id >' 45);
        $query = $this->db->where('tbl_ask_pust.id >', 0);

        $query = $this->db->get();
        // dd($query->result_array());
        $rowcount = $query->num_rows();
        if ($rowcount > 0) {
            $this->data['alldata'] =  $query->result_array();
        }
        $this->load->view('admin/view_admin_complain', $this->data);
    }


    function solvelist()
    {

        $query = $this->db->from($this->_ASK_TABLE);
        $query = $this->db->select('
            tbl_ask_pust.id,
            tbl_ask_pust.name,
            tbl_ask_pust.phone,
            tbl_ask_pust.gst_app_id,
            tbl_ask_pust.details,
            tbl_ask_pust.date_time,
            tbl_ask_pust.ip,
            tbl_ask_pust.status,
            tbl_pabna_data_final.appid,
            tbl_pabna_data_final.sboard_name ssc_board,
            tbl_pabna_data_final.spass_year ssc_year,
            tbl_pabna_data_final.sroll_no ssc_roll,
            tbl_pabna_data_final.sreg_no ssc_reg,
            tbl_pabna_data_final.hboard_name hsc_board,
            tbl_pabna_data_final.hpass_year hsc_year,
            tbl_pabna_data_final.hroll_no hsc_roll,
            tbl_pabna_data_final.hreg_no hsc_reg,
            ')
            ->join('tbl_pabna_data_final', 'tbl_ask_pust.gst_app_id=tbl_pabna_data_final.appid', 'left');
        $query = $this->db->where('tbl_ask_pust.status', 'solved');
        $query = $this->db->where('tbl_ask_pust.id >', 0);

        $query = $this->db->get();
        $rowcount = $query->num_rows();
        if ($rowcount > 0) {
            $this->data['alldata'] =  $query->result_array();
        }
        $this->load->view('admin/view_admin_complain', $this->data);
    }

    function pendinglist()
    {
        $this->db->from($this->_ASK_TABLE);
        $query = $this->db->select('
            tbl_ask_pust.id,
            tbl_ask_pust.name,
            tbl_ask_pust.phone,
            tbl_ask_pust.gst_app_id,
            tbl_ask_pust.details,
            tbl_ask_pust.date_time,
            tbl_ask_pust.ip,
            tbl_ask_pust.status,
            tbl_pabna_data_final.appid,
            tbl_pabna_data_final.sboard_name ssc_board,
            tbl_pabna_data_final.spass_year ssc_year,
            tbl_pabna_data_final.sroll_no ssc_roll,
            tbl_pabna_data_final.sreg_no ssc_reg,
            tbl_pabna_data_final.hboard_name hsc_board,
            tbl_pabna_data_final.hpass_year hsc_year,
            tbl_pabna_data_final.hroll_no hsc_roll,
            tbl_pabna_data_final.hreg_no hsc_reg,
            ')
            ->join('tbl_pabna_data_final', 'tbl_ask_pust.gst_app_id=tbl_pabna_data_final.appid', 'left');
        $query = $this->db->where('tbl_ask_pust.status', 'pending');

        $query = $this->db->get();
        $rowcount = $query->num_rows();
        if ($rowcount > 0) {
            $this->data['alldata'] =  $query->result_array();
        }
        $this->load->view('admin/view_admin_complain', $this->data);
    }

    function solve($id)
    {
        if (!empty($id)) {
            $appdata = array(
                'status' => 'solved',
            );
            $this->db->where('id', $id);
            $this->db->update($this->_ASK_TABLE, $appdata);
            redirect('admin/admin_complain/pendinglist');
        } else {
            redirect('admin/admin_complain/pendinglist');
        }
    }
}
