<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Admin_home
 *
 * @author Mhabub
 */
class Admin_home extends CI_Controller{
    private $data = '';
    public function __construct() {
        parent::__construct();
         $this->load->helper('admin_helper');
    if(! $this->session->userdata('isADMINLogin') ){
           redirect(base_url().'admin/admin_login');
        }        
        
         $this->load->model('admin_report_model');
         $this->load->model('dashboard_model');
    }
    
    function index() { 
         $this->load->view('admin/view_admin_dashboard', $this->data);
    }
    
    function cancel_request_list(){
        $cancel_list = $this->admin_report_model->get_admission_cancel_pending_list();
        $this->data['cancel_list'] = $cancel_list;
        $this->load->view('admin/view_admin_cancel_request_list', $this->data);
        
    }
    
    function cancel_request_appove_list(){
        
    }    
    
    function view_cancel_request() {
        
    }
    
    function approve_cancel_request($unique_id_ref) {
        
        if(!empty( $unique_id_ref )){
            $appdata = array(
                'cancel_register_approv_status' => 'approved',
                'cancel_register_approv_date' => date('Y-m-d H:i:s')
            );
            $this->db->where('unique_id_ref', $unique_id_ref);
            $this->db->update('tbl_result', $appdata);
        }
        redirect(base_url().'admin/admin_home/cancel_request_list');
    }
    
    function download_cancel_confirm_pdf($unique_id_ref,$sub_code) {
        
         $result_details =  $this->dashboard_model->get_result_subject_details($unique_id_ref);
         $student_acadamic_data = $this->dashboard_model->get_student_all__details($result_details['system_reg_id_ref']); 
         $std_per_data  = $this->admin_report_model->get_std_dtl_from_login($result_details['system_reg_id_ref']);
         $this->data['std_per_data'] = $std_per_data;
         $this->data['student_acadamic_data'] = $student_acadamic_data;
         $this->data['result_details'] = $result_details;
          
         $this->data['student_login_photo_path'] = $student_acadamic_data['IMGUPLOAD_PATH'];
         $this->data['sub_code'] = $sub_code;
         
        $html =    $this->load->view('admin/view_admin_cancel_request_pdf.php', $this->data,true);
        
        $this->load->library('m_pdf');
          
        $this->m_pdf->pdf->SetHTMLHeader('
                                        <div style="text-align: right; font-weight: bold;">
                                        </div>');
         //this the the PDF filename that user will get to download
                $pdfFilePath = "PUST_admission_cancel_letter_".$unique_id_ref . ".pdf";

                //load mPDF library
              

                //generate the PDF from the given html
                $this->m_pdf->pdf->WriteHTML($html);

                //download it.
                $this->m_pdf->pdf->Output($pdfFilePath, "D");         
         
    }
            
    function test() {
            $sql = "SELECT * FROM tbl_login WHERE `dob` IS NOT NULL";//and is_secnd_meirt='YES'
            $query_all = $this->db->query($sql);        
            $rec  = $query_all->result_array();
            $i = 1;
            foreach ($rec as $key => $value) {
                $sql_merit = "SELECT unique_id,merit FROM tbl_merit_list WHERE `system_reg_id`='".$value['system_regid']."'";
                $query_merit = $this->db->query($sql_merit);
                $rec_merit  = $query_merit->result_array();  
                
                foreach ($rec_merit as $key_ => $value_) {
                        $unique_id = $value_['unique_id'];
                        $sql_sub = "SELECT id FROM tbl_subject_choise WHERE `unique_id`='".$unique_id."'";
                        $query_sub = $this->db->query($sql_sub);  
                        if($query_sub->num_rows() ==  0){
                            echo "<br>". $i." =  ".$value['id']." =  ".$value['mobile_no']." =   ".$value['password']." =   ".$value_['merit'];
                            $i++;
                        }
                }
            }
    }
    
    function pass() {
            $sql = "SELECT * FROM tbl_login";
            $query_all = $this->db->query($sql);        
            $rec  = $query_all->result_array();
            $i = 1;
            foreach ($rec as $key => $value) {
                echo "Mobile: ".$value['mobile_no']." Pass: ".$value['password'];
                echo "<br>";
            }   
            
    }
}
