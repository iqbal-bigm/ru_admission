<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Admin_report
 *
 * @author Mhabub
 */
class Admin_report extends CI_Controller {
    public function __construct() {
        parent::__construct();
        if(! $this->session->userdata('isADMINLogin') ){
           redirect('login');
        }		
        $this->load->helper('admin_helper');
    }
    function index() {
         $this->load->helper(array('form', 'url'));
         $data = array();
         $this->load->view('admin/vew_admin_report', $data);
    }
}
