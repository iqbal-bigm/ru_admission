<?php
class Login extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('login_model');
        $this->load->helper('common_helper');
        $this->load->library('form_validation');

        //redirect('content');

        //              if(! $this->session->userdata('isTESTLogin') ){
        //           redirect('testauth');
        //        }
    }

    function index()
    {
        //		if($this->uri->segment( 3 ) <> "Mhabub"){
        //			redirect('content');
        //		}

        $this->session->sess_destroy();  //=================================================
        // For Default Form Validation Default Value

        $form['mobile_no'] = '';
        $form['applicant_id'] = '';
        $form['admission_roll'] = '';

        $this->load->view('login/view_login_form', $form);
    }

    public function process()
    {
        $notification = 'Please Enter Valid GST Mobile Number , Valid GST Applicant ID and Valid GST Admission Roll.';
        $this->data['mobile_no'] = $this->input->post('mobile_no', TRUE);
        $this->data['applicant_id']  = $this->input->post('applicant_id', TRUE);
        $this->data['admission_roll'] = $this->input->post('admission_roll', TRUE);
        $data_session = "";
        $this->session->unset_userdata($data_session);
        $valid_mobile_no = "";
        $valid_applicant_id = "";
        $valid_admission_roll = "";
        $valid_hsc_board = "";
        $valid_hsc_year = "";
        $valid_hsc_reg = "";
        $valid_hsc_roll = "";

        $this->form_validation->set_rules('mobile_no', 'GST Mobile No', 'required');
        $this->form_validation->set_rules('applicant_id', 'GST Applicant ID', 'required');
        $this->form_validation->set_rules('admission_roll', 'GST Admission Roll', 'required');

        if ($this->form_validation->run() === FALSE) {

            $form['mobile_no'] = '';
            $form['applicant_id'] = '';
            $form['admission_roll'] = '';

            $this->load->view('login/view_login_form', $form);
        } else {

            $mobile_no = $this->input->post('mobile_no', TRUE);
            $applicant_id = $this->input->post('applicant_id', TRUE);
            $admission_roll = $this->input->post('admission_roll', TRUE);

            $res_valid_data = $this->login_model->check_login_info($mobile_no, $applicant_id, $admission_roll);
            if (!$res_valid_data) {
                $this->db->where('cell_phone', $mobile_no);
                $this->db->where('applicant_id', $applicant_id);
                $this->db->where('admission_roll', $admission_roll);
                $query = $this->db->get('gst_final_result_data');
                if ($query->num_rows()) {
                    $gst_data = $query->row();
                    if ($gst_data->attendance == 'Present') {
                        $this->login_model->store_gst_data($gst_data);
                    } else {
                        $notification = 'We are sorry, you are not eligible to apply.';
                    }
                }
            }
            $res_valid_data = $this->login_model->check_login_info($mobile_no, $applicant_id, $admission_roll);

            if ($res_valid_data) { //!empty($valid_hsc_reg) &&
                $valid_mobile_no = $res_valid_data->mobile_no;
                $valid_hsc_board = $res_valid_data->hsc_board;
                $valid_hsc_year = $res_valid_data->hsc_year;
                $valid_hsc_reg = $res_valid_data->hsc_reg;
                $valid_hsc_roll = $res_valid_data->hsc_roll;
                $is_secnd_meirt = $res_valid_data->is_secnd_meirt;


                $res_hsc_data = $this->login_model->get_info_by_regisid($res_valid_data->system_regid);


                if (!empty($res_hsc_data['SNAME'])) {
                    $SNAME = $res_hsc_data['SNAME'];
                } else {
                    $SNAME = "";
                }

                if (!empty($res_hsc_data['IMGUPLOAD_PATH'])) {
                    $IMGUPLOAD_PATH = $res_hsc_data['IMGUPLOAD_PATH'];
                } else {
                    $IMGUPLOAD_PATH = "";
                }


                $data_session = array(
                    'student_logged_in' => true,
                    'student_login_user_type' => 'student',
                    'student_login_mobile_no' => $valid_mobile_no,
                    'student_login_hsc_board' => $valid_hsc_board,
                    'student_login_hsc_year' => $valid_hsc_year,
                    'student_login_hsc_reg' => $valid_hsc_reg,
                    'student_login_hsc_roll' => $valid_hsc_roll,
                    'student_login_sname' => $SNAME,
                    'student_login_regisid' => $res_valid_data->system_regid,
                    'student_login_photo_path' => $IMGUPLOAD_PATH,
                    'student_is_secnd_meirt' => $is_secnd_meirt
                );

                $this->session->set_userdata($data_session); /* put data into seesion */

                redirect('/dashboard');
            } else {
                $this->session->set_flashdata('notification', $notification);
                $this->load->view('login/view_login_form', $this->data);
            }
        }
    }
}
