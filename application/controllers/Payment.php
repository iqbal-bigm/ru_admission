<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Payment
 *
 * @author Mhabub
 */
class Payment extends CI_Controller {
    private $data = '';
    public function __construct(){
        parent:: __construct();
          $this->load->library('form_validation');
          $this->load->model('payment_model');
    }

  function index(){
      
        $this->load->view('home/view_payment', $this->data);
  }

    function check(){
      
	    $this->data['show_report'] = 'no';
        $this->load->view('home/view_application_payment_check', $this->data);
  }

  function validate() {
        $this->form_validation->set_rules('appid', 'Application ID', 'required');

        $APPID = $this->input->post('appid', TRUE);
       
        
        if ($this->form_validation->run() === FALSE) {
			$this->data['show_report'] = 'no';
            $this->load->view('home/view_application_payment_check', $this->data);
        }else{
                                   
        $this->db->select('APPID,PMTSTATUS,TXNID');       
        $this->db->where('APPID', $APPID);       
        $this->db->where('PMTSTATUS', 'P');
        $this->db->from('tbl_application_pust');
        $query = $this->db->get();
        $rowcount = $query->num_rows();

        if ($rowcount > 0) { //paid
		   $this->data['PMTSTATUS'] = 'PAID';
		   $this->data['APPID'] = $APPID;
        }else{
			$this->data['PMTSTATUS'] = 'NOT PAID';
			$this->data['APPID'] = $APPID;
        }
		
		$this->data['show_report'] = 'yes';

			$this->load->view('home/view_application_payment_check', $this->data);
                
		}
  }
  }