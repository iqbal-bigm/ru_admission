<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Admitcard
 * @property home_model $home_model
 */

class Admitcard extends CI_Controller {
      private $data = '';
    public function __construct(){
        parent:: __construct();
         $this->load->library('form_validation');
         $this->load->model('payment_model');
         $this->load->model('home_model');
         $this->load->model('admitcard_model');
         
       // if(! $this->session->userdata('isTESTLogin') ){
		//	redirect('login');
       // }	//exit;	 
         //redirect('circular');
    }

  function index(){
        //$this->load->view('home/view_admit_card', $this->data);
		$this->load->view('home/view_admit_card', $this->data);
  }
  
  function download() {
          $this->form_validation->set_rules('app_id', 'Application ID', 'required');
         if ($this->form_validation->run() == FALSE) {
             
             redirect(base_url(). 'admitcard');
             
         }else{
           $app_id = $this->input->post('app_id', TRUE);
           $paid  =  $this->admitcard_model->get_payment_status_for_admit($app_id);
           if($paid){//paid
               $ApplicantInfo = $this->admitcard_model->get_admitcard_application_data($app_id);
               if(!empty($ApplicantInfo)){
				   file_put_contents("admit_log.txt", "Date: ".date('Y-m-d H:i:s')." APPID : ".$app_id."\n", FILE_APPEND);  
                   $RegistrationInfo = $this->home_model->get_registration_details($ApplicantInfo['REGISID']);
               
               
               $data['ApplicantInfo'] = $ApplicantInfo;
               $data['RegistrationInfo'] = $RegistrationInfo;
                  
                $html = $this->load->view('home/view_admit_card_pdf', $data, true);

                //this the the PDF filename that user will get to download
                $pdfFilePath = "PUST_".$app_id . "_admitcard.pdf";

                //load mPDF library
                $this->load->library('m_pdf');

                //generate the PDF from the given html
                $this->m_pdf->pdf->WriteHTML($html);

                //download it.
                $this->m_pdf->pdf->Output($pdfFilePath, "D");               
               }
           }else{ //not paid yet
               $data['validMsg'] = 'Sorry, You did not complete payment or invalid Applicaiton ID.';
               $this->load->view('home/view_admit_card', $data);
           }
             
         }      
  }

  }
