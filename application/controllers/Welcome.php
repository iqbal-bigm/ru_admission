<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Class Others
 * @property home_model $home_model
 */

class Welcome extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    function index()
    {
        redirect('/dashboard');
    }
}
